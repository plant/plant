#!/usr/bin/env python
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Gustavo H. X. Shiroma, Marco Lavalle
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import os
from setuptools import setup, find_packages

__version__ = version = VERSION = '0.4.8.dev'

print('PLAnT version:', version)

directory = os.path.abspath(os.path.dirname(__file__))

with open("README.md") as f:
    long_description = f.read()

with open("LICENSE.txt") as f:
    license = f.read()

setup(
    name='plant',
    author="Gustavo H. X. Shiroma, Marco Lavalle",
    description="PLAnT: Polarimetric Interferometric Lab and Analysis Tool",
    version=version,
    license=license,
    long_description=long_description,
    url="https://gitlab.com/plant/plant",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Science/Research",
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    package_dir={'': 'src'},
    packages=find_packages(where='src'),

    entry_points={
        'console_scripts': [
            'plant_csv_reader = plant.app.plant_csv_reader:main',
            'plant_display = plant.app.plant_display:main',
            'plant_execute = plant.app.plant_execute:main',
            'plant_filter = plant.app.plant_filter:main',
            'plant_geocode = plant.app.plant_geocode:main',
            'plant_info = plant.app.plant_info:main',
            'plant_jaxafnf = plant.app.plant_jaxafnf:main',
            'plant_ls = plant.app.plant_ls:main',
            'plant_mosaic = plant.app.plant_mosaic:main',
            'plant_radiometric_correction = ' +
            'plant.app.plant_radiometric_correction:main',
            'plant_slantrange = plant.app.plant_slantrange:main',
            'plant_util = plant.app.plant_util:main',
        ],
    },
    install_requires=[

        'numpy', 'matplotlib', 'scipy', 'pillow', 'h5py',
        'python-dateutil', 'numexpr',
    ],
    python_requires=">=3.7",
)
