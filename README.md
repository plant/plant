[![pipeline status](https://gitlab.com/plant/plant/badges/stable/pipeline.svg)](https://gitlab.com/plant/plant/commits/stable)
[![coverage report](https://gitlab.com/plant/plant/badges/stable/coverage.svg)](https://plant.gitlab.io/plant/coverage)

![anaconda cloud](https://anaconda.org/plant/plant/badges/version.svg)
![last update](https://anaconda.org/plant/plant/badges/latest_release_date.svg)
![platforms](https://anaconda.org/plant/plant/badges/platforms.svg)
![installation](https://anaconda.org/plant/plant/badges/installer/conda.svg)

Try PLAnT in a working environment hosted on [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/plant%2Fplant/stable?filepath=notebook)

# PLAnT (beta version)
Polarimetric Interferometric Lab and Analysis Tool

PLANT is a collection of software tools developed at the Jet Propulsion Laboratory to support processing and analysis of Synthetic Aperture Radar (SAR) data for ecosystem and land-cover/land-use change science and applications. PLANT inherits code components from the Interferometric Scientific Computing Environment (ISCE) to generate high-resolution, polarimetric-interferometric SLC stacks from Level-0 or Level-1 data for a variety of airborne and spaceborne sensors. The goal is to provide the ecosystem and land-cover/land-use change communities with rigorous and efficient tools to perform multi-temporal, polarimetric and tomographic analyses in order to generate calibrated, geocoded and mosaicked Level-2 and Level-3 products (e.g., maps of above-ground biomass or forest disturbance).

> M. Lavalle, G. H. X. Shiroma, P. Agram, E. Gurrola, G. F. Sacco, and P. Rosen, “PLANT: Polarimetric-interferometric lab and analysis tools for ecosystem and land-cover science and applications,” in 2016 IEEE International Geoscience and Remote Sensing Symposium (IGARSS), July 2016, pp. 5354–5357.

# Usage Documentation
A preliminary Usage Documentation for PLAnT is available [here](https://plant.gitlab.io/plant).


# Installation
PLAnT provides installation via `anaconda` and has a ready-to-use `docker` image that can be pulled.

## 1. Anaconda from the plant channel (Recommended)
1. Download and install [Anaconda](www.anaconda.com) or [Miniconda](https://docs.conda.io/en/latest/miniconda.html)
2. Install PLAnT and all its dependencies from our channel on the Anaconda Cloud

On the command line:
```bash
conda install -c plant plant
```

## 2. Anaconda/pip
1. Download and install anaconda or miniconda
2. Clone PLAnT repository
3. Install PLAnT using pip:
```bash
python -m pip install .
```

## 3. Anaconda manually setting PATH and PYTHONPATH environment variables
1. Download and install Miniconda from https://conda.io/miniconda.html
2. Clone PLAnT repository
3. Install PLAnT dependencies listed in `requirements.txt`
4. Add PLAnT parent folder to PYTHONPATH variable
5. Add PLAnT applications folder to PATH variable

On a linux/Mac machine:
```bash
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh
git clone https://gitlab.com/plant/plant.git
conda install --yes --file plant/requirements.txt
export PYTHONPATH=$PWD/src/:$PYTHONPATH
export PATH=$PWD/src/plant/app/:$PATH
```
On a Windows machine:
```
Work In Progress
```

## Docker
PLAnT has a ready-to-use docker image that is based on ubuntu and comes with the library and all the dependencies installed.
1. Pull the image from the gitlab docker repository at registry.gitlab.com
2. Run a container from the image

On the command line:
```bash
docker login registry.gitlab.com
Username:gitlab+deploy-token-43283
Password:2-9mHtqAwdkwm4pQ4iWK
docker run -it --name plant registry.gitlab.com/plant/plant bash
```

You might want to mount a volume in the container, linked with a folder in the host machine. Suppose you want to link the folder /home/my_folder/
on the host machine with the folder /mounted in the container:
```bash
docker run -it --name plant_mount -v /home/my_folder:/mounted/ registry.gitlab.com/plant/plant bash
```

You might want to use PLAnT in a container environment also for graphical functionalities, like the ones exposed by the `plant_display.py` app.
In this case, further operations have to be executed to link the host machine XServer to the container.

On a Mac machine:
1. Install a XServer on your host machine. For example [XQuartz](https://www.xquartz.org/releases/index.html).
2. Launch the XServer on the host machine
3. Spawn a container linked to the address of the XServer

This is a possible list of steps. In this example, the OS assigned the name `en6` to the XServer. Change it with the name it has been assigned on your machine.
We suggest you to run `ifconfig` before and after starting the XServer, and note the name of the new interface. Besides, if you are using XQuartz, go to the “Security” tab and make sure you’ve got “Allow connections from network clients” ticked.
```bash
open -a XQuartz
ip=$(ifconfig en6 | grep inet | awk '$1=="inet" {print $2}')
docker run -it --name plant -e DISPLAY=$ip:0 -v /tmp/.X11-unix:/tmp/.X11-unix registry.gitlab.com/plant/plant bash
```

Docker instructions for Linux and Windows machines are in progress. Note that docker allows to mount multiple devices on the container, thus one can create a container with both a shared file system and the XServer.
