import plant
import numpy as np

lat_beg = 34.3808
lon_beg = -118.8470
lat_end = 33.4911
lon_end = -117.5070
step_y = -0.0001
step_x = 0.0002

width = int(np.round((lon_end - lon_beg) / step_x))
length = int(np.round((lat_end - lat_beg) / step_y))


def test_get_coordinates_1():

    plant_geogrid_obj = plant.get_coordinates(x0=lon_beg,
                                              y0=lat_beg,
                                              length=length,
                                              width=width,
                                              step_y=step_y,
                                              step_x=step_x)
    assert lon_beg == plant_geogrid_obj.x0
    assert lat_beg == plant_geogrid_obj.y0
    assert length == plant_geogrid_obj.length
    assert width == plant_geogrid_obj.width
    assert step_y == plant_geogrid_obj.step_y
    assert step_x == plant_geogrid_obj.step_x
    assert np.isclose(lon_end, plant_geogrid_obj.xf)
    assert np.isclose(lat_end, plant_geogrid_obj.yf)


def test_get_coordinates_2():
    plant_geogrid_obj = plant.get_coordinates(x0=lon_beg,
                                              xf=lon_end,
                                              y0=lat_beg,
                                              yf=lat_end,
                                              step_y=step_y,
                                              step_x=step_x)
    assert lon_beg == plant_geogrid_obj.x0
    assert lat_beg == plant_geogrid_obj.y0
    assert length == plant_geogrid_obj.length
    assert width == plant_geogrid_obj.width
    assert step_y == plant_geogrid_obj.step_y
    assert step_x == plant_geogrid_obj.step_x
