#!/usr/bin/env python3
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Federico Croce, Gustavo H. X. Shiroma, Marco Lavalle
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import pytest
import plant
from osgeo import gdal

from config_tests import \
    JPG_IMAGES, \
    VRT_IMAGES, \
    ENVI_IMAGES, \
    S3_IMAGES


def image_comparison_gdal(pl_img, gdal_img):
    # Checking coherence
    assert gdal_img is not None
    assert pl_img is not None

    # Checking dimensions
    assert gdal_img.RasterCount == pl_img.getNBands()
    assert gdal_img.RasterXSize == pl_img.getWidth()
    assert gdal_img.RasterYSize == pl_img.getLength()

    # Loading images
    gdal_arr = gdal_img.ReadAsArray()
    pl_arr = pl_img.image_list
    assert gdal_arr.shape == (pl_img.getNBands(), pl_img.shape[0], pl_img.shape[1])

    # Checking bands stats
    for i in range(pl_img.getNBands()):
        g_band = gdal_arr[i]
        p_band = pl_arr[i]
        assert round(g_band.max()) == round(p_band.max())
        assert round(g_band.min()) == round(p_band.min())
        assert g_band.size == p_band.size
        assert g_band.shape == p_band.shape


def image_comparison_gdal_light(pl_img, gdal_img):
    # Checking coherence
    assert gdal_img is not None
    assert pl_img is not None

    # Checking dimensions
    assert gdal_img.RasterCount == pl_img.getNBands()
    assert gdal_img.RasterXSize == pl_img.getWidth()
    assert gdal_img.RasterYSize == pl_img.getLength()


def check_band_selection(pl_img, pl_img_sel):
    assert pl_img is not None
    assert pl_img_sel is not None
    pl_img_band = pl_img.image_list[pl_img.get_nbands() - 1]
    pl_img_sel_band = pl_img_sel.image_list[0]
    assert round(pl_img_band.max()) == round(pl_img_sel_band.max())
    assert round(pl_img_band.min()) == round(pl_img_sel_band.min())
    assert pl_img_band.size == pl_img_sel_band.size
    assert pl_img_band.shape == pl_img_sel_band.shape


@pytest.mark.parametrize("img", JPG_IMAGES)
def test_read_jpg(img):
    pl_img = plant.read_image(img)
    gdal_img = gdal.Open(img)
    image_comparison_gdal(pl_img, gdal_img)
    n_bands = pl_img.get_nbands()
    pl_img_sel = plant.read_image(img + ':' + str(n_bands - 1))
    check_band_selection(pl_img, pl_img_sel)


@pytest.mark.parametrize("img", VRT_IMAGES)
def test_read_vrt(img):
    pl_img = plant.read_image(img)
    gdal_img = gdal.Open(img)
    image_comparison_gdal(pl_img, gdal_img)
    n_bands = pl_img.get_nbands()
    pl_img_sel = plant.read_image(img + ':' + str(n_bands - 1))
    check_band_selection(pl_img, pl_img_sel)


@pytest.mark.parametrize("img", ENVI_IMAGES)
def test_read_envi(img):
    pl_img = plant.read_image(img)
    gdal_img = gdal.Open(img)
    image_comparison_gdal(pl_img, gdal_img)
    n_bands = pl_img.get_nbands()
    pl_img_sel = plant.read_image(img + ':' + str(n_bands - 1))
    check_band_selection(pl_img, pl_img_sel)


@pytest.mark.parametrize("img", S3_IMAGES)
def test_read_s3(img):
    pl_img = plant.read_image(img)
    gdal_img = gdal.Open(img)
    image_comparison_gdal_light(pl_img, gdal_img)
    n_bands = pl_img.get_nbands()
    pl_img_sel = plant.read_image(img + ':' + str(n_bands - 1))
    check_band_selection(pl_img, pl_img_sel)


@pytest.mark.parametrize("img", VRT_IMAGES)
def test_app(img):
    pl_img = plant.info(img)
    assert pl_img is not None
