#!/usr/bin/env python3
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Gustavo H. X. Shiroma, Marco Lavalle
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import sys
import numpy as np
import os
import pytest
import plant

y0 = 34.380885
x0 = -118.847848
yf = 33.491165
xf = -117.506953
# lat_arr = [yf, y0]
# lon_arr = [x0, xf]
step_y = 0.0001
step_x = 0.0002


execution_dir = os.getcwd()
root_dir = os.path.dirname(os.path.realpath(__file__))
os.chdir(root_dir)
pytest.main(['-v'])
os.chdir(execution_dir)


def get_parser():
    '''
    Command line parser.
    '''
    descr = 'Test'
    epilog = ''
    parser = plant.argparse(epilog=epilog,
                            flag_debug=1,
                            description=descr)
    return parser


class PlantTest(plant.PlantScript):

    def __init__(self, parser, argv=None):
        '''
        class initialization
        '''
        super().__init__(parser, argv)

    def run(self):
        '''
        Run tests
        '''
        self.error_list = []
        # self.test_geo('plant_weather.py')

        script_list = plant.glob(
            plant.path.join(plant.plant_config.home_dir,
                            'script', 'plant*.py'))
        not_execute_list = ['plant_test.py']
        if script_list is not None:
            for script in script_list:
                if plant.path.basename(script) in not_execute_list:
                    continue
                flag_script = True
                self.module_name = script
                # self.print('testing: %s' %script)
                try:
                    plant.execute(script,
                                  verbose=False)
                except KeyboardInterrupt:
                    raise plant.PlantExceptionKeyboardInterrupt
                except:
                    flag_script = False
                    tb = sys.exc_info()[1]
                    self.print_test(flag_script,
                                    'script execution without input arguments '
                                    '(error message: %s)'
                                    % tb)
                if not flag_script:
                    continue
                self.print_test(flag_script, 'script execution without '
                                'input arguments')
        # self.test_plant_geo()
        self.test_geo('plant_dem.py')
        self.test_geo('plant_dem.py', option=' --no-geoid-correction')
        self.test_geo('plant_landcover.py')
        self.test_geo('plant_jaxafnf.py')
        self.test_geo('plant_jaxafnf.py', option=' -m HH')
        self.test_geo('plant_jaxafnf.py', option=' -m HV')
        self.test_geo('plant_jaxafnf.py', option=' -m POL')
        self.test_geo('plant_jaxafnf.py', option=' -e ')
        self.test_geo('plant_jaxafnf.py', option=' -e -m HH')
        self.test_geo('plant_jaxafnf.py', option=' -e -m HV')
        self.test_geo('plant_jaxafnf.py', option=' -e -m POL')
        self.test_geo('plant_jaxafnf.py', option=' -e -m HH_SIGMA')
        self.test_geo('plant_jaxafnf.py', option=' -e -m HV_SIGMA')
        self.test_geo('plant_jaxafnf.py', option=' -e -m DATE')
        self.test_geo('plant_jaxafnf.py', option=' -e -m INC')
        self.test_geo('plant_jaxafnf.py', option=' -e -m MASK')

        # self.test_geo('plant_lidar_glas.py')
        self.print('Total number of errors: %d'
                   % len(self.error_list))
        for i, current_error in enumerate(self.error_list):
            self.print('    %d: module: %s error: %s'
                       % (i+1, current_error[0],
                          current_error[1]))

        # plant.execute('python -m pytest -v')

    def test_geo(self, module_name, option=''):
        self.module_name = module_name
        self.print('')
        message = 'Testing: %s' % self.module_name
        if option:
            message += ' (option:%s)' % option
        self.print(message)
        input_geotransform = plant.get_geotransform(y0=y0,
                                                    yf=yf,
                                                    x0=x0,
                                                    xf=xf,
                                                    step_y=step_y,
                                                    step_x=step_x)
        temp_file = ('plant.isfile_%s.bin'
                     % (self.module_name.replace('.py', '')))
        flag_script = True
        try:
            plant.execute('%s -b %f %f %f %f '
                          '-o %s --step-lat %f '
                          '--step-lon %f -f %s'
                          % (self.module_name,
                             y0,
                             yf,
                             x0,
                             xf,
                             temp_file,
                             step_y,
                             step_x,
                             option),
                          verbose=False)
        except KeyboardInterrupt:
            raise plant.PlantExceptionKeyboardInterrupt
        except:
            flag_script = False
            # traceback.print_exc()
            tb = sys.exc_info()[1]
            # tb = str(sys.exc_info())
            self.print_test(flag_script,
                            'script execution (error message: %s)'
                            % tb)
        # plant.append_temporary_file(temp_file)
        if not flag_script:
            return
        self.print_test(flag_script, 'script execution')

        flag_file_created = plant.isfile(temp_file)
        self.print_test(flag_file_created, 'output file created')

        dem = self.read_image(temp_file, verbose=False)
        if dem is None:
            self.print_test(False, 'script return image_obj')
            return
        if dem.geotransform is None:
            self.print_test(False, 'script return image_obj.geotransform')
            return
        input_geotransform[3] = 0
        dem.geotransform[3] = 0
        input_geotransform[7] = 0
        dem.geotransform[7] = 0
        vmean = np.nanmean(dem.image)
        flag_geotransform = input_geotransform != dem.geotransform
        self.print_test(flag_geotransform, 'geotransform test')

        flag_mean = plant.isvalid(vmean) and vmean != 0
        self.print_test(flag_mean, 'valid data test')

    def print_test(self, flag, message):
        flag_str = '[OK]' if flag else '[FAIL]'
        if not flag:
            self.error_list.append([self.module_name, message])
        self.print('%s %s: %s'
                   % (flag_str,
                      self.module_name,
                      message))


def main(argv=None):
    with plant.PlantLogger():
        parser = get_parser()
        self_obj = PlantTest(parser, argv)
        ret = self_obj.run()
        return ret


if __name__ == '__main__':
    main()
