from os import path
from multiprocessing import cpu_count

# Global
N_PROCESSES = cpu_count()

options = dict()
options['mute'] = True
options['force'] = True
options['output_skip_if_existent'] = False

dat_dir = '/home/croce/ml/dat'

output_ext = 'tiff'

# N1
field_dir = path.join(dat_dir, 'field')
field_output_dir = path.join(field_dir, 'output')
file_agb_vector = path.join(field_output_dir, 'agb_vect.pickle')
agb_vect_file = path.join(field_output_dir, f'agb_vect.{output_ext}')
uavsar_dir = path.join(dat_dir, 'lband', 'rtc')
uavsar_output_dir = path.join(uavsar_dir, 'output')
pband_dir = path.join(dat_dir, 'pband')
pband_output_dir = path.join(pband_dir, 'output')

coeffs_output_dir = path.join(uavsar_dir, 'coeffs')
coeffs_file = path.join(coeffs_output_dir, 'coeffs_wcm.bin')

tdt_dir = path.join(field_dir, 'lope_tdt')
tdt_ref = path.join(tdt_dir, 'Plot_coord_AfriSAR_NL_np.txt')
e_file = path.join(tdt_dir, 'E.tif')
map_file = path.join(dat_dir, 'products', f'map_Lmodel.{output_ext}')

bbox = [-0.2532022168685209, -0.1483230396743310, 11.5629090686080751, 11.6635721785520605]
step_lat = 0.0000180173814111
step_lon = 0.0000180173814111

file_LHV_40 = path.join(uavsar_output_dir, f'LHV_40m.{output_ext}')
file_LHV_gamma_in = path.join(uavsar_dir, 'lopenp_14043_16015_001_160308_L090HV_CX_01_mag.bin')
file_LHV_crop = path.join(uavsar_output_dir, f'LHV.{output_ext}')
file_LHH_gamma_in = path.join(uavsar_dir, 'lopenp_14043_16015_001_160308_L090HH_CX_01_mag.bin')
file_LHH_crop = path.join(uavsar_output_dir, f'LHH.{output_ext}')
file_LVV_gamma_in = path.join(uavsar_dir, 'lopenp_14043_16015_001_160308_L090VV_CX_01_mag.bin')
file_LVV_crop = path.join(uavsar_output_dir, f'LVV.{output_ext}')

# N2
file_pband_inc = path.join(pband_dir, f'afrisar_dlr_T2-0_inc.{output_ext}')
file_pband_rg = path.join(pband_dir, f'afrisar_dlr_T2-0_rg.{output_ext}')
file_pband_az = path.join(pband_dir, f'afrisar_dlr_T2-0_az.{output_ext}')
file_PHV_slant_range = path.join(pband_dir, f'afrisar_dlr_T2-0_SLC_HV.{output_ext}')

file_PHV_sigma = path.join(pband_output_dir, f'afrisar_dlr_T2-0_SLC_hv_sigma.{output_ext}')
file_PHV_gamma = path.join(pband_output_dir, f'afrisar_dlr_T2-0_SLC_HV_gamma.{output_ext}')
file_PHV_geocoded = path.join(pband_output_dir, f'afrisar_dlr_T2-0_SLC_HV_geocoded.{output_ext}')
file_PHV_warp_geocoded = path.join(pband_output_dir, f'warped_afrisar_dlr_T2-0_SLC_HV_geocoded.{output_ext}')
file_PHV_warp_geocoded_40 = path.join(pband_output_dir, f'warped_afrisar_dlr_T2-0_SLC_HV_geocoded_40.{output_ext}')

lband_dir = path.join(dat_dir, 'lband', 'rtc')
lband_output_dir = path.join(lband_dir, 'output')
file_LHV_gamma = path.join(lband_output_dir, f'LHV.{output_ext}')
file_LHH_gamma = path.join(lband_output_dir, f'LHH.{output_ext}')
file_LVV_gamma = path.join(lband_output_dir, f'LVV.{output_ext}')
file_LHV_gamma_colo = path.join(lband_output_dir, f'LHV_colo.{output_ext}')
file_LHH_gamma_colo = path.join(lband_output_dir, f'LHH_colo.{output_ext}')
file_LVV_gamma_colo = path.join(lband_output_dir, f'LVV_colo.{output_ext}')
file_LHV_gamma_colo_40 = path.join(lband_output_dir, f'LHV_colo_40.{output_ext}')
file_LHH_gamma_colo_40 = path.join(lband_output_dir, f'LHH_colo_40.{output_ext}')
file_LVV_gamma_colo_40 = path.join(lband_output_dir, f'LVV_colo_40.{output_ext}')

file_html_slider = path.join(dat_dir, 'products', 'LP_slider.html')

# N3
uavsar_dir = path.join(dat_dir, 'lband', 'rtc')
uavsar_output_dir = path.join(uavsar_dir, 'output')

file_PHV = path.join(pband_output_dir, f'warped_afrisar_dlr_T2-0_SLC_HV_geocoded.{output_ext}')
file_LHH = path.join(lband_output_dir, f'LHH_colo.{output_ext}')
file_LHV = path.join(lband_output_dir, f'LHV_colo.{output_ext}')
file_LVV = path.join(lband_output_dir, f'LVV_colo.{output_ext}')

# N4
lband_training_dir = path.join(dat_dir, 'lband', 'rtc', 'output', 'n4')
pband_training_dir = path.join(dat_dir, 'pband', 'output', 'n4')
field_training_dir = path.join(dat_dir, 'field', 'output')

lband_target_dir = path.join(dat_dir, 'lband', 'rtc', 'output')
pband_target_dir = path.join(dat_dir, 'pband', 'output')

training_LHH_file = path.join(lband_training_dir, 'LHH_vect.tiff')
training_LHV_file = path.join(lband_training_dir, 'LHV_vect.tiff')
training_LVV_file = path.join(lband_training_dir, 'LVV_vect.tiff')
training_PHV_file = path.join(pband_training_dir, 'PHV_vect.tiff')

training_agb_file = path.join(field_training_dir, 'agb_vect.tiff')

LHH_file = path.join(lband_target_dir, 'LHH_colo_40.tiff')
LHV_file = path.join(lband_target_dir, 'LHV_colo_40.tiff')
LVV_file = path.join(lband_target_dir, 'LVV_colo_40.tiff')
PHV_file = path.join(pband_target_dir, 'warped_afrisar_dlr_T2-0_SLC_HV_geocoded_40.tiff')

map_file_lp = path.join(dat_dir, 'products', 'map_LPbands_192tr_rtc.tiff')
model_file_lp = path.join(dat_dir, 'models', 'rf_model_LP_192tr_rtc.joblib')

tree_image = path.join(dat_dir, 'models', 'tree.png')

# N5
lvis_dir = path.join(dat_dir, 'lvis')
lvis_output_dir = path.join(lvis_dir, 'output')
lvisL2Files = path.join(lvis_dir, '*.TXT')
lvis_file = path.join(lvis_output_dir, 'lvis_colo.tiff')
lvis_file_40 = path.join(lvis_output_dir, 'lvis_colo_40.tiff')
lvis_zt_file = path.join(lvis_output_dir, 'lvis_zt_colo.tiff')
lvis_zg_file = path.join(lvis_output_dir, 'lvis_zg_colo.tiff')

# N6
lvis_zt_m_zg_vect_file = path.join(lvis_output_dir, f'zt_m_zg_20_vect.{output_ext}')

# N7
lvis_training_dir = path.join(dat_dir, 'lvis', 'output')
lvis_target_dir = path.join(dat_dir, 'lvis', 'output')
training_lvis_file = path.join(lvis_training_dir, 'zt_m_zg_20_vect.tiff')

wcm_map_file = path.join(dat_dir, 'products', f'map_Lmodel.tiff')
lp_map_file = path.join(dat_dir, 'products', 'map_LPbands_192tr_rtc.tiff')
lplid_map_file = path.join(dat_dir, 'products', 'map_lvisLPbands_192tr.tiff')

model_file_lplid = path.join(dat_dir, 'models', 'rf_model_LPlidar_192tr_rtc.joblib')

# N8
# --