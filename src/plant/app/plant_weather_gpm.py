#!/usr/bin/env python3
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Gustavo H. X. Shiroma, Marco Lavalle
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import h5py
from os import path, makedirs
from requests import Session
from datetime import datetime, timedelta
from dateutil import parser
from dateutil.relativedelta import relativedelta

from osgeo import gdal
import plant

DEFAULT_STEP = 0.1

SERVER_ADDRESS = 'https://gpm1.gesdisc.eosdis.nasa.gov'
SERVER_DIR = 'data/GPM_L3'

DOWNLOAD_DIR_DEFAULT = 'GPM'


def get_parser():

    descr = ''
    epilog = ''
    parser = plant.argparse(input_files=1,
                            epilog=epilog,
                            description=descr,
                            geo=1,
                            date_all=1,
                            default_flags=1,
                            default_options=1,

                            date_epoch=1,
                            date_epoch_final=1,
                            date_year=1,
                            date_month=1,
                            date_day=1,
                            date_hour=1,
                            date_minute=1,
                            username=1,
                            password=1,
                            output_file=1,
                            null=1,
                            out_null=1,
                            geo_multilook=1)

    parser.add_argument('--download',
                        dest='download_dir',
                        type=str,
                        help='Directory containing '
                        'weather data',
                        default='')

    parser.add_argument('--n-images',
                        dest='n_images',
                        type=int,
                        help='Number of images')

    parser.add_argument('--accumulation-time',
                        dest='accumulation_time',
                        type=int,
                        help='Precipitation accumulation time')

    parser.add_argument('--hourly',
                        action='store_true',
                        dest='flag_hourly',
                        help='Use hourly observations')

    parser.add_argument('--monthly',
                        action='store_true',
                        dest='flag_monthly',
                        help='Use monthly observations')

    parser.add_argument('--version',
                        '--imerg-version',
                        type=int,
                        dest='imerg_version',
                        help='IMERG version number')

    parser.add_argument('--version-letter',
                        '--letter-version',
                        type=int,
                        dest='imerg_version_letter',
                        help='IMERG version letter (A or B)')

    return parser


def is_hdf5(input_file):
    try:
        _ = h5py.File(input_file, 'r')
        return True
    except BaseException:
        pass
    return False


class PlantWeatherGpm(plant.PlantScript):

    def __init__(self, parser, argv=None):

        super().__init__(parser, argv,
                         geo_default_step=DEFAULT_STEP)
        self.populate_parameters()

    def populate_parameters(self):

        self.flag_daily = None
        if self.output_file:
            output_dir = path.dirname(self.output_file)
            if not path.isdir(output_dir) and output_dir:
                self.print('WARNING Directory ' + output_dir +
                           ' does not exist. Creating it..', 1)
                makedirs(output_dir)
        else:
            if self.lat_arr is not None and self.lon_arr is not None:
                coordinates_string = \
                    plant.get_coordinates_string(lat_arr=self.lat_arr,
                                                 lon_arr=self.lon_arr)
            else:
                coordinates_string = ''
            self.output_file = ('weather_' + coordinates_string + '.bin')
        if self.imerg_version is None:
            self.imerg_version = 6

    def run(self):

        gdal_version = gdal.__version__
        gdal_version_major = int(gdal_version.split('.')[0])
        if gdal_version_major < 3:
            self.print('WARNING plant_weather_gpm.py may not work '
                       f' with the GDAL version {gdal_version}.'
                       ' It is suggested that a more recent'
                       ' version of GDAL be installed.')

        if self.input_files:

            out_image_obj = None
            flag_no_time_diff_selection = (not self.flag_daily and
                                           not self.flag_monthly and
                                           not self.flag_hourly)

            for i, current_file in enumerate(self.input_files):

                print(f'## input {i+1}: {current_file}')
                if '3B-HHR' in current_file and flag_no_time_diff_selection:
                    self.hourly = True
                    flag_no_time_diff_selection = False
                elif '3B-DAY' in current_file and flag_no_time_diff_selection:
                    self.daily = True
                    flag_no_time_diff_selection = False
                elif '3B-MO' in current_file and flag_no_time_diff_selection:
                    self.monthly = True
                    flag_no_time_diff_selection = False
                name = current_file
                temp_file = f'temp_sum_{self.start_time}_{i}.bin'

                ret = self.colocate(current_file, temp_file)
                plant.append_temporary_file(temp_file)

                if out_image_obj is None:
                    out_image_obj = ret
                    out_image_obj.band.name = name
                else:
                    out_image_obj.set_image(ret.image,
                                            band=i,
                                            name=name)

            if out_image_obj is None:
                self.print('ERROR no valid file was found for input'
                           ' parameters')
            self.save_image(out_image_obj, self.output_file)
            return out_image_obj

        self.download_dir = plant.test_download_dir(
            self.download_dir,
            DOWNLOAD_DIR_DEFAULT)

        if self.accumulation_time is None:
            self.accumulation_time = 1.0
        if self.hour is None:
            self.hour = 0
        if self.minute is None:
            self.minute = 0

        if self.epoch is not None:
            start_date = parser.parse(self.epoch)
        else:
            start_date = datetime(self.year, self.month, self.day,
                                  self.hour, self.minute)

        flag_no_time_diff_selection = (not self.flag_daily and
                                       not self.flag_monthly and
                                       not self.flag_hourly)
        if self.epoch_final:
            end_date = parser.parse(self.epoch_final)
            diff = end_date - start_date

            if flag_no_time_diff_selection and diff.days > 30:
                self.flag_monthly = True
                self.print('INFO no time spacing has been'
                           ' defined. Selecting monthy'
                           ' observations.')

            elif flag_no_time_diff_selection:
                self.flag_hourly = True
                self.print('INFO no time spacing has been'
                           ' defined. Selecting hourly'
                           ' observations.')

            if self.n_images is None and self.flag_monthly:
                self.n_images = diff.days // 30 + 1
            elif self.n_images is None and self.flag_daily:
                self.n_images = diff.days + 1
            elif self.n_images is None:
                self.n_images = diff.days * 24 + diff.seconds // 3600 + 1
        elif flag_no_time_diff_selection:
            self.flag_hourly = True

        out_image_obj = None

        if self.n_images is not None:

            self.print(f'number of images: {self.n_images}')
            for i in range(self.n_images):
                if self.flag_daily:

                    time_delta = timedelta(days=i * self.accumulation_time)
                elif self.flag_monthly:

                    time_delta = relativedelta(
                        months=i * self.accumulation_time)
                else:

                    time_delta = timedelta(hours=i * self.accumulation_time)
                current_date = start_date + time_delta

                print(f'processing date: {current_date}')
                name = f'{current_date}'
                with plant.PlantIndent():
                    temp_precip_sum_file = \
                        f'temp_precip_sum_{self.start_time}_{i}.bin'
                    ret = self.search_download_and_colocate(
                        current_date, temp_precip_sum_file)
                    plant.append_temporary_file(temp_precip_sum_file)
                    if ret is None:
                        continue
                    if out_image_obj is None:
                        out_image_obj = ret
                        out_image_obj.band.name = name
                    else:
                        out_image_obj.set_image(ret.image,
                                                band=i,
                                                name=name)
                    plant.debug(i)
                    plant.debug(name)
                    plant.debug(len(out_image_obj._band_orig))

            if out_image_obj is None:
                self.print('ERROR no valid file was found for input'
                           ' parameters')
            self.save_image(out_image_obj, self.output_file)
            return out_image_obj

        print(f'processing date: {start_date}')
        with plant.PlantIndent():
            ret = self.search_download_and_colocate(start_date,
                                                    self.output_file)
            return ret

    def colocate(self, current_file, colocated_weather_file):
        if not is_hdf5(current_file):
            file_alias = current_file
        else:
            subdataset = self.input_key
            if subdataset is None and '3B-MO' in current_file:
                subdataset = '//Grid/precipitation'
            elif subdataset is None and '3B-DAY' in current_file:
                subdataset = '//Grids/G1/surfPrecipLiqRateUn'
            else:
                subdataset = '//Grid/precipitationCal'

            driver = 'NETCDF'
            file_alias = (f'util:"{driver}:{current_file}'
                          f':{subdataset}"')

        argv = [file_alias] + ['-o'] + [colocated_weather_file]
        argv += ['--in-null', '-9999.9']
        argv += ['--gdalwarp-list']
        argv += ['--interp', 'cubic']
        argv += ['--transition', '0']
        argv += ['--no-average']

        ret = self.execute('plant_mosaic.py ' + ' '.join(argv))
        return ret

    def search_download_and_colocate(self, start_date,
                                     output_file):

        n_images = int(self.accumulation_time / 0.5)
        image_list = []
        colocated_weather_dir = 'temp_weather_colocated_' + \
            str(self.start_time)
        plant.append_temporary_file(colocated_weather_dir)

        version_str = f'{self.imerg_version:02}'
        if self.imerg_version_letter is None and self.flag_daily:
            version_letter_list = ['A']
        elif self.imerg_version_letter is None:
            version_letter_list = ['B']
        else:
            version_letter_list = self.imerg_version_letter
        print('number of images:', n_images)
        for i in range(n_images):
            for version_letter in version_letter_list:
                version_letter_str = f'{version_str}{version_letter}'
                if self.flag_monthly:
                    current_date = start_date + relativedelta(months=i)
                elif self.flag_daily:
                    current_date = start_date + timedelta(days=i)
                else:
                    current_date = start_date + timedelta(minutes=30 * i)

                year = current_date.year
                month = current_date.month
                day = current_date.day
                hour = current_date.hour
                minute = current_date.minute

                if minute < 30:
                    start_minute = 0
                    end_minute = 29
                else:
                    start_minute = 30
                    end_minute = 59
                second = 0
                end_second = 59
                time_diff = current_date - datetime(year, 1, 1, 0, 0, 0)
                day_number = int(time_diff.days) + 1
                subdataset = self.input_key
                if self.flag_monthly:
                    day = 1
                    remote_weather_dir = (f'GPM_3IMERGM.{version_str}/{year}/')
                    weather_file = (f'3B-MO.MS.MRG.3IMERG.'
                                    f'{year}{month:02}{day:02}-'
                                    f'S000000-E235959.{month:02}.'
                                    f'V{version_letter_str}.HDF5')

                elif self.flag_daily:

                    remote_weather_dir = (f'GPM_3CMB_DAY.{version_str}/{year}/'
                                          f'{month:02}/')
                    weather_file = (f'3B-DAY.GPM.DPRGMI.CORRAGD.'
                                    f'{year}{month:02}{day:02}-'
                                    f'S000000-E235959.{day_number:03}.'
                                    f'V{version_letter_str}.HDF5')

                else:

                    total_minutes = hour * 60 + start_minute
                    remote_weather_dir = (f'GPM_3IMERGHH.{version_str}/{year}/'
                                          f'{day_number:03}/')

                    weather_file = (f'3B-HHR.MS.MRG.3IMERG.'
                                    f'{year}{month:02}{day:02}-'
                                    f'S{hour:02}{start_minute:02}{second:02}-'
                                    f'E{hour:02}{end_minute:02}{end_second:02}'
                                    f'.{total_minutes:04}.'
                                    f'V{version_letter_str}.HDF5')
                remote_weather_file = path.join(remote_weather_dir,
                                                weather_file)
                output_weather_file = path.join(self.download_dir,
                                                weather_file)

                if (not plant.isfile(output_weather_file) and
                        (year != 2018 or (year == 2018 and day_number <= 59))):
                    self.download(remote_weather_file, output_weather_file)

                not_found_list = []

                if not plant.isfile(output_weather_file):
                    not_found_list.append(weather_file)

                    weather_file = weather_file.replace('HHR', 'HHR-L')
                    remote_weather_file = remote_weather_file.replace(
                        f'HH.{version_str}',
                        f'HHL.{version_str}')
                    remote_weather_file = remote_weather_file.replace('HHR',
                                                                      'HHR-L')
                    output_weather_file = path.join(self.download_dir,
                                                    weather_file)

                    if (not plant.isfile(output_weather_file) and
                            (year != 2018 or
                             (year == 2018 and day_number <= 152))):
                        self.download(remote_weather_file, output_weather_file)
                if not plant.isfile(output_weather_file):
                    not_found_list.append(weather_file)

                    weather_file = weather_file.replace('HHR-L', 'HHR-E')
                    remote_weather_file = remote_weather_file.replace(
                        f'HHL.{version_str}',
                        f'HHE.{version_str}')
                    remote_weather_file = remote_weather_file.replace('HHR-L',
                                                                      'HHR-E')
                    output_weather_file = path.join(self.download_dir,
                                                    weather_file)

                    if not plant.isfile(output_weather_file):
                        self.download(remote_weather_file, output_weather_file)

                if not plant.isfile(output_weather_file):
                    not_found_list.append(weather_file)
                    not_found_list_str = ", ".join(not_found_list)
                    self.print(
                        f'WARNING files not found: {not_found_list_str}')
                    continue

                remote_weather_header_file = remote_weather_file + '.xml'
                output_weather_header_file = output_weather_file + '.xml'

                colocated_weather_file = weather_file.replace('.HDF5', '.tif')
                colocated_weather_file = path.join(colocated_weather_dir,
                                                   colocated_weather_file)

                if not plant.isfile(output_weather_header_file):
                    self.download(remote_weather_header_file,
                                  output_weather_header_file)

                if not plant.isfile(colocated_weather_file):

                    try:
                        self.colocate(output_weather_file,
                                      colocated_weather_file)
                    except BaseException:
                        self.print('WARNING there was an error colocating'
                                   f' file {output_weather_file}. Skipping.')
                        return

                image_list.append(colocated_weather_file)
                break
        if len(image_list) == 0:
            return
        image_list_str = ' '.join(image_list)
        command = (f'plant_util.py {image_list_str}'
                   f' -o {output_file} -f --ge 0')
        if len(image_list) > 1:
            command += ' --nan-add'
        try:
            ret = plant.execute(command)
        except BaseException:
            self.print('WARNING there was an error executing command:'
                       f' "{command}". Skipping.')
            return
        for image_str in image_list:
            plant.append_temporary_file(image_str)

        return ret

    def download(self, remote_file, output_file,
                 n_attempts=None):
        url = f'{SERVER_ADDRESS}/{SERVER_DIR}/{remote_file}'
        s = CustomSession()
        self.print(f'INFO Downloading {remote_file} from'
                   f' {SERVER_ADDRESS}...')
        if self.username is not None and self.password is not None:
            resp = s.get(url, auth=(self.username, self.password),
                         stream=True)
        else:
            resp = s.get(url, stream=True)
        if resp.status_code == 404:

            self.print(f'WARNING file: {url} not found remotely')

            return
        if resp.status_code == 401:

            self.print(f'ERROR downloading {url}'
                       f' (error code: {resp.status_code}).'
                       ' Please, verify username (--username) and'
                       f' password (--password) for {SERVER_ADDRESS}')

        if resp.status_code != 200:

            self.print(f'ERROR downloading {url}'
                       f' (error code: {resp.status_code}).')
        with open(output_file, 'wb') as f:
            for chunk in resp.iter_content(chunk_size=1024):
                if chunk:
                    f.write(chunk)


class CustomSession(Session):
    def rebuild_auth(self, prepared_request, response):
        return


def main(argv=None):
    with plant.PlantLogger():
        parser = get_parser()
        self_obj = PlantWeatherGpm(parser, argv)
        ret = self_obj.run()
        return ret


if __name__ == '__main__':
    main()
