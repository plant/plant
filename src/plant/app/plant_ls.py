#!/usr/bin/env python3
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Gustavo H. X. Shiroma, Marco Lavalle
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from os import path, stat
import shutil

from osgeo import osr
import numpy as np
import plant
import h5py


def get_parser():

    descr = ('Emulate the result of the ls command'
             ' (Linux/Max) or dir (Windows), adding'
             ' image/header information (when available).'
             ' The list of files can be filtered by'
             ' images (--images), headers (--headers),'
             ' directories (--directories) or regular'
             ' files (--regular).')
    epilog = ('plant.ls\n'
              'plant.ls --image\n'
              'plant.ls --human\n'
              'plant.ls --header\n'
              'plant.ls --image --header\n'
              'plant.ls --directory\n')
    parser = plant.argparse(epilog=epilog,
                            description=descr,
                            input_files=1,
                            input_key=1,
                            flag_debug=1,
                            input_format=1,
                            output_file=1)
    parser.add_argument('--image',
                        '--images',
                        dest='image',
                        action='store_true',
                        help='Show images only.')
    parser.add_argument('--only-names',
                        '--only-filenames',
                        '--only-file-names',
                        '--filenames',
                        '--file-names',
                        dest='flag_only_filenames',
                        action='store_true',
                        help='Show only filenames.')
    parser.add_argument('--geo',
                        dest='flag_show_geogrid',
                        action='store_true',
                        help='Show geo-information.')
    parser.add_argument('--mean',
                        dest='flag_show_mean',
                        action='store_true',
                        help='Compute statistics.')
    parser.add_argument('--decimal',
                        '--decimal-places',
                        dest='decimal_places',
                        type=int,
                        help='Defines the number of decimal places')

    parser.add_argument('--nsig-figs',
                        '--n-significant-figures',
                        '--sig-figs',
                        '--significant-figures',
                        dest='n_significant_figures',
                        type=int,
                        help='Defines the number of significant figures')
    parser.add_argument('-e',
                        '--header',
                        '--headers',
                        dest='header',
                        action='store_true',
                        help='Show headers only.')
    parser.add_argument('-d',
                        '--directory',
                        '--directories',
                        dest='directory',
                        action='store_true',
                        help='Show directories only.')
    parser.add_argument('-r', '--regular',
                        dest='regular',
                        action='store_true',
                        help='Regular files only.')

    single_line_group = parser.add_mutually_exclusive_group(
        required=False)
    single_line_group.add_argument('--single-line',
                                   '--separate-lines',
                                   dest='single_line',
                                   help=('Print file attributes in separate'
                                         ' lines'),
                                   default=True,
                                   action='store_true')
    single_line_group.add_argument('--ns',
                                   '--nsl',
                                   '--no-single-line',
                                   '--no-separate-line',
                                   dest='single_line',
                                   default=True,
                                   help=('Print file attributes in separate'
                                         ' lines'),
                                   action='store_false')

    parser.add_argument('--human',
                        dest='human',
                        action='store_true',
                        help='Print sizes in human readable format.')
    parser.add_argument('--length',
                        dest='filter_length',
                        type=int,
                        help='Filter images by length.')
    parser.add_argument('--width',
                        dest='filter_width',
                        type=int,
                        help='Filter images by width.')
    parser.add_argument('--depth',
                        dest='filter_depth',
                        type=int,
                        help='Filter images by depth.')
    return parser


class PlantLS(plant.PlantScript):

    def __init__(self, parser, argv=None):

        super().__init__(parser, argv)

    def run(self):

        if len(self.input_files) == 0:
            self.input_files = ['*']

        filelist = []
        for current_input in self.input_files:
            input_expanded = path.expanduser(current_input)
            all_files_expanded = []
            plant.get_files_from_list(
                self,
                [input_expanded],
                flag_no_messages=True,
                all_files_expanded=all_files_expanded,
                verbose=False)

            filelist.extend(all_files_expanded)

        if len(filelist) == 0:
            self.print('ERROR file not found')
            return
        line_list = []
        ret_list = []

        for current_file in filelist:
            ret = self.list_file(current_file, self.flag_show_geogrid,
                                 self.flag_show_mean)
            if ret is None:

                continue
            type_str = ret.get('type', '')
            shape = ret.get('shape')
            flag_filter = (self.image or
                           self.header or
                           self.directory or
                           self.regular)
            if (flag_filter and
                not ((self.image and type_str == 'image') or
                     (self.header and type_str == 'header') or
                     (self.directory and type_str == 'directory') or
                     (self.regular and type_str == 'regular'))):
                continue
            if (shape is None and
                (self.filter_length or
                 self.filter_width or
                 self.filter_depth)):
                continue
            elif self.filter_length and self.filter_length != shape[0]:
                continue
            elif self.filter_width and self.filter_width != shape[1]:
                continue
            elif (self.filter_depth and
                    ((len(shape) <= 2 and self.filter_depth <= 1) or
                     (len(shape) > 2 and self.filter_depth != shape[2]))):
                continue
            ret_list.append(ret)

        column_size_filename = None
        column_size_file_size = None
        column_size_file_format = None
        column_size_dtype = None
        column_size_shape = None
        column_size_nbands = None
        column_size_geo = None
        column_size_geogrid = None
        column_size_mean = None
        for ret in ret_list:
            current_file = ret.get('filename', None)
            current_file_size = ret.get('file_size', None)
            current_file_format = ret.get('file_format', None)
            current_dtype = ret.get('dtype', None)
            current_shape_str = ret.get('shape_str', None)

            current_nbands = ret.get('nbands', None)
            current_geo = ret.get('geo', None)
            current_geogrid = ret.get('geogrid', None)
            current_mean = ret.get('mean', None)

            if (current_file is not None and
                    (column_size_filename is None or
                     len(current_file) > column_size_filename)):
                column_size_filename = len(current_file)
            if (current_file_size is not None and
                    (column_size_file_size is None or
                     len(current_file_size) > column_size_file_size)):
                column_size_file_size = len(current_file_size)
            if (current_file_format is not None and
                    (column_size_file_format is None or
                     len(current_file_format) > column_size_file_format)):
                column_size_file_format = len(current_file_format)
            if (current_dtype is not None and
                    (column_size_dtype is None or
                     len(current_dtype) > column_size_dtype)):
                column_size_dtype = len(current_dtype)
            if (current_shape_str is not None and
                    (column_size_shape is None or
                     len(current_shape_str) > column_size_shape)):
                column_size_shape = len(current_shape_str)
            if (current_nbands is not None and
                    (column_size_nbands is None or
                     len(current_nbands) > column_size_nbands)):
                column_size_nbands = len(current_nbands)
            if (current_geo is not None and
                    (column_size_geo is None or
                     len(current_geo) > column_size_geo)):
                column_size_geo = len(current_geo)
            if (current_geogrid is not None and
                    (column_size_geogrid is None or
                     len(current_geogrid) > column_size_geogrid)):
                column_size_geogrid = len(current_geogrid)
            if (current_mean is not None and
                    (column_size_mean is None or
                     len(current_mean) > column_size_mean)):
                column_size_mean = len(current_mean)
        if column_size_filename is None:
            column_size_filename = 0
        if column_size_file_size is None:
            column_size_file_size = 0
        if column_size_file_format is None:
            column_size_file_format = 0
        if column_size_dtype is None:
            column_size_dtype = 0
        if column_size_shape is None:
            column_size_shape = 0
        if column_size_nbands is None:
            column_size_nbands = 0
        if column_size_geo is None:
            column_size_geo = 0
        if column_size_geogrid is None:
            column_size_geogrid = 0
        if column_size_mean is None:
            column_size_mean = 0

        file_size_len = 14 if not self.human else 10

        if self.flag_only_filenames:
            self.flag_show_filename = True
            self.flag_show_file_size = False
            self.flag_show_file_format = False
            self.flag_show_dtype = False
            self.flag_show_shape = False
            self.flag_show_nbands = False
            self.flag_show_geo = False
            self.flag_show_size_len = False
        else:
            self.flag_show_filename = True
            self.flag_show_file_size = True
            self.flag_show_file_format = True
            self.flag_show_dtype = True
            self.flag_show_shape = True
            self.flag_show_nbands = True
            self.flag_show_geo = True
            self.flag_show_size_len = True

        column_length = 0
        if self.flag_show_filename:
            column_length += column_size_filename + 1
        if self.flag_show_file_size:
            column_length += column_size_file_size + 1
        if self.flag_show_file_format:
            column_length += column_size_file_format + 1
        if self.flag_show_dtype:
            column_length += column_size_dtype + 1
        if self.flag_show_shape:
            column_length += column_size_shape + 1
        if self.flag_show_nbands:
            column_length += column_size_nbands + 1
        if self.flag_show_geo:
            column_length += column_size_geo + 1
        if self.flag_show_geogrid:
            column_length += column_size_geogrid + 1
        if self.flag_show_mean:
            column_length += column_size_mean + 1
        if self.flag_show_size_len:
            column_length += file_size_len + 1

        terminal_width = shutil.get_terminal_size().columns
        if plant.plant_config.flag_debug:
            terminal_width = terminal_width - 28
        n_columns = int((terminal_width) / column_length)

        for i, ret in enumerate(ret_list):
            current_file = ret.get('filename', '')
            dtype_str = ret.get('dtype', '')
            file_type = ret.get('type', '')
            file_format_str = ret.get('file_format', '')
            file_size_str = ret.get('file_size', '')
            shape_str = ret.get('shape_str', '')
            shape = ret.get('shape', '')
            nbands_str = ret.get('nbands', '')
            geo_str = ret.get('geo', '')
            geogrid_str = ret.get('geogrid', '')
            mean_str = ret.get('mean', '')

            if ((path.islink(current_file) or
                 current_file.endswith('.vrt')) and
                    (file_type == 'image' or
                     file_type == 'header')):
                color_str = plant.bcolors.BCyan
            elif path.islink(current_file):
                color_str = plant.bcolors.Cyan
            elif (file_type is None or
                    file_type == 'regular'):
                color_str = plant.bcolors.ColorOff
            elif file_type == 'directory':
                color_str = plant.bcolors.BBlue
            elif file_type == 'header':
                color_str = plant.bcolors.Purple
            elif (file_type == 'image' and
                  file_format_str == 'ARRAY'):
                color_str = plant.bcolors.BYellow
            elif (file_type == 'image' and
                  file_format_str in plant.FIG_DRIVERS):
                color_str = plant.bcolors.BPurple
            elif file_type == 'image':
                color_str = plant.bcolors.BGreen
            else:
                color_str = plant.bcolors.ColorOff

            file_size_str = file_size_str.rjust(column_size_file_size)
            file_format_str = file_format_str.ljust(
                column_size_file_format)
            dtype_str = dtype_str.ljust(column_size_dtype)
            shape_str = shape_str.ljust(column_size_shape)
            nbands_str = nbands_str.ljust(column_size_nbands)
            geo_str = geo_str.ljust(column_size_geo)
            geogrid_str = geogrid_str.ljust(column_size_geogrid)
            mean_str = mean_str.ljust(column_size_mean)
            current_file_str = current_file.ljust(
                column_size_filename)
            current_file_str = (color_str + current_file_str +
                                plant.bcolors.ColorOff)

            column_list = []
            if self.flag_show_size_len and file_size_str:
                column_list.append(file_size_str)
            if self.flag_show_dtype and dtype_str:
                column_list.append(dtype_str)
            if self.flag_show_file_format and file_format_str:
                column_list.append(file_format_str)
            if self.flag_show_shape and shape_str:
                column_list.append(shape_str)
            if self.flag_show_nbands and nbands_str:
                column_list.append(nbands_str)
            if self.flag_show_geo and geo_str:
                column_list.append(geo_str)
            if self.flag_show_geogrid and geogrid_str:
                column_list.append(geogrid_str)
            if self.flag_show_mean and mean_str:
                column_list.append(mean_str)
            if self.flag_show_filename and current_file_str:
                column_list.append(current_file_str)
            column_str = ' '.join(column_list)
            line_list.append(column_str)
            if (self.single_line or
                len(line_list) == n_columns or
                    i == len(filelist) - 1):
                self.print(' '.join(line_list))
                line_list = []
        if len(line_list) != 0:
            self.print(''.join(line_list))

    def list_file(self, current_file, flag_show_geogrid, flag_show_mean):
        ret_dict = {}

        try:
            file_size = stat(current_file).st_size
            if self.human and file_size is not None:
                file_size = plant.get_file_size_string(file_size)
            elif file_size is not None:
                file_size = str(file_size)
        except FileNotFoundError:
            file_size = ''

        ret_dict['file_size'] = file_size

        ret_dict['filename'] = current_file
        if path.isdir(current_file):
            ret_dict['type'] = 'directory'
            return ret_dict
        image_obj = self.read_image(current_file,
                                    only_header=True,
                                    flag_exit_if_error=False,
                                    flag_no_messages=True,
                                    verbose=False)

        if image_obj is None:
            if not path.isfile(current_file):
                return
            if any([current_file.endswith(ext)
                    for ext in plant.HEADER_EXTENSIONS]):
                ret_dict['type'] = 'header'
            else:
                ret_dict['type'] = 'regular'
            return ret_dict

        if image_obj.file_format.upper() in ['HDF5', 'NETCDF', 'NISAR']:

            parsed_ret_dict = plant.parse_filename(current_file)
            if image_obj.file_format.upper() in ['HDF5', 'NETCDF']:
                h5_obj = h5py.File(parsed_ret_dict['filename'])
                h5_dataset = h5_obj[parsed_ret_dict['key']]
                file_size = h5_dataset.id.get_storage_size()

            if image_obj.file_format.upper() in ['NISAR']:

                file_size = 0
                with h5py.File(parsed_ret_dict['filename']) as h5_obj:
                    for band in range(image_obj.nbands):
                        band_obj = image_obj.get_band(band=band)
                        h5_dataset = h5_obj[band_obj.input_key]
                        this_file_size = h5_dataset.id.get_storage_size()
                        file_size += this_file_size

            if self.human and file_size is not None:
                file_size = plant.get_file_size_string(file_size)
                ret_dict['file_size'] = file_size
            elif file_size is not None:
                file_size = str(file_size)
                ret_dict['file_size'] = file_size

        flag_read_geo_info = None

        if (image_obj.geotransform and
                plant.is_projected(image_obj.projection) is True):
            proj = osr.SpatialReference(wkt=image_obj.projection)
            epsg_code = proj.GetAttrValue('AUTHORITY', 1)
            ret_dict['geo'] = f'projected (EPSG:{epsg_code})'
            flag_read_geo_info = True
        elif (image_obj.geotransform and
                plant.is_geographic(image_obj.projection) is True):
            ret_dict['geo'] = 'geographic (lat/lon)'
            flag_read_geo_info = True
        elif image_obj.geotransform:
            ret_dict['geo'] = 'geo'
            flag_read_geo_info = True
        elif image_obj.gcp_projection:
            ret_dict['geo'] = 'GCP'
        else:
            ret_dict['geo'] = ''

        if not flag_read_geo_info or not flag_show_geogrid:
            ret_dict['geogrid'] = ''
        else:

            x0, dx, _, yf, _, dy, *_ = image_obj.geotransform

            xf = x0 + dx * image_obj.width
            y0 = yf + dy * image_obj.length
            x0_str = str(x0)
            xf_str = str(xf)
            max_nx = max(len(x0_str), len(xf_str))
            y0_str = str(y0)
            yf_str = str(yf)
            max_ny = max(len(y0_str), len(yf_str))

            ret_dict['geogrid'] = f'dY:{dy}'
            ret_dict['geogrid'] += f' dX:{dx}'

            ret_dict['geogrid'] += f' Y:({y0:{max_ny}}, {yf:{max_ny}})'
            ret_dict['geogrid'] += f' X:({x0:{max_nx}}, {xf:{max_nx}})'

        if not flag_show_mean:
            ret_dict['mean'] = ''
        else:
            mean_value_list = []
            for band in range(image_obj.nbands):
                mean_value = np.nanmean(image_obj.get_image(band=band))
                mean_value_str = str(plant.format_number(
                    mean_value,
                    decimal_places=self.decimal_places,
                    sigfigs=self.n_significant_figures))

                mean_value_list.append(mean_value_str)
            ret_dict['mean'] = ' '.join(mean_value_list)

        ret_dict['dtype'] = plant.get_dtype_name(image_obj.dtype)
        ret_dict['file_format'] = image_obj.file_format

        if image_obj.length == 1:
            shape_str = ('length:%d' % image_obj.width)
        elif image_obj.depth == 1:
            shape_str = ('length:%d width:%d' % (image_obj.length,
                                                 image_obj.width))
        else:
            shape_str = ('depth:%d length:%d width:%d' %
                         (image_obj.depth, image_obj.length, image_obj.width))
        if image_obj.nbands > 1:
            ret_dict['nbands'] = ' %d bands' % image_obj.nbands
        if any([current_file.endswith(ext)
                for ext in plant.HEADER_EXTENSIONS]):
            shape_str = shape_str
            ret_dict['type'] = 'header'
        else:
            shape_str = shape_str
            ret_dict['type'] = 'image'
        ret_dict['shape_str'] = shape_str
        ret_dict['shape'] = image_obj.shape
        return ret_dict


def main(argv=None):
    with plant.PlantLogger():
        parser = get_parser()
        self_obj = PlantLS(parser, argv)
        ret = self_obj.run()
        return ret


if __name__ == '__main__':
    main()
