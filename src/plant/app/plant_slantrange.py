#!/usr/bin/env python3
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Gustavo H. X. Shiroma, Marco Lavalle
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import time
import sys
import numpy as np
from scipy.ndimage import map_coordinates
import plant


def get_parser():

    descr = ('Project geocoded images onto the slant-range geometry'
             ' using geolocation arrays.')
    epilog = ''
    parser = plant.argparse(epilog=epilog,
                            description=descr,
                            input_images=2,
                            output_file=1,
                            output_dir=1,
                            separate=1,

                            default_options=1,
                            topo_dir=2,
                            multilook=1,
                            pickle_dir=1,
                            pixel_size=1)
    parser.add_argument('--order',
                        type=int,
                        default=1,
                        help='Order of the the spline interpolation. '
                        'The order has to be in the range 0 to 5 '
                        '(default: %(default)s).',
                        dest='spline_order')

    parser.add_argument('--geo-index-width',
                        dest='geo_index_width',
                        type=int,
                        help=('Defines output width for geocoded'
                              ' slant-range indexes option'))

    parser.add_argument('--geo-index-length',
                        dest='geo_index_length',
                        type=int,
                        help=('Defines output length for geocoded'
                              ' slant-range indexes option'))

    parser.add_argument('--geo-index-x',
                        '--geo-index-rg',
                        dest='geo_index_x',
                        type=str,
                        help=('Defines geocoded slant-range x or'
                              ' range indexes'))

    parser.add_argument('--geo-index-y',
                        '--geo-index-az',
                        dest='geo_index_y',
                        type=str,
                        help=('Defines geocoded slant-range y or'
                              ' azimuth indexes'))
    return parser


class PlantSlantrange(plant.PlantScript):

    def __init__(self, parser, argv=None):

        super().__init__(parser, argv)

        self.dest_lat = None
        self.dest_lon = None
        self.dest_width = None
        self.dest_length = None
        self.geo_data = None
        self.geo_width = None
        self.geo_length = None
        self.geo_geotransform = None
        self.populate_parameters()

    def populate_parameters(self):

        self.flag_geo_index = (self.geo_index_x is not None or
                               self.geo_index_y is not None)
        if self.flag_geo_index:
            self.print('Considering geocoded X/Y indexes')
            return
        if self.lat_file is None or not plant.isfile(self.lat_file):
            self.print('ERROR please provide a directory containing lat.rdr'
                       ' or .llh file')
            return
        if self.lon_file is None or not plant.isfile(self.lon_file):
            self.print('ERROR please provide a directory containing lon.rdr'
                       ' or .llh file')
            return

    def run(self):

        if self.separate:
            input_images = self.input_images
            for i, current_file in enumerate(input_images):
                self.input_images = [current_file]
                self.output_file = self.output_files[i]
                if (self.output_skip_if_existent and
                        plant.isfile(self.output_file)):
                    print('INFO output file %s already exist, '
                          'skipping execution..' % self.output_file)
                    continue
                ret = self.project_slantrange()
            return ret
        elif (not self.output_file and
              not self.output_dir and
              not self.output_ext):
            self.print('ERROR one the following argument is required: '
                       '--output-file, --output-dir, --output-ext')
            sys.exit(1)
        elif (not self.output_file and
              (self.output_dir or
               self.output_ext)):
            self.print('ERROR this script only accepts --output-dir or '
                       '--output-ext in --separate mode')
            sys.exit(1)
        ret = self.project_slantrange()
        return ret

    def project_slantrange(self):

        if not self.flag_geo_index:
            image_obj = self.read_image(self.lat_file)
            self.dest_width = image_obj.width
            self.dest_length = image_obj.length
            self.dest_lat = image_obj.image
            image_obj = self.read_image(self.lon_file)
            self.dest_lon = image_obj.image
            self.dest_lon = plant.copy_shape(self.dest_lat,
                                             self.dest_lon)
        else:
            geo_index_y = self.read_image(self.geo_index_y).image.copy()
            null_ind = np.where(geo_index_y == 55537)
            geo_index_y[null_ind] = 0

            geo_index_x = self.read_image(self.geo_index_x).image.copy()
            null_ind = np.where(geo_index_x == 55537)
            geo_index_x[null_ind] = 0

            if self.geo_index_length is None:
                self.geo_index_length = np.nanmax(geo_index_y) + 1
            else:
                geo_index_y = np.clip(geo_index_y, 0, self.geo_index_length)
            if self.geo_index_width is None:
                self.geo_index_width = np.nanmax(geo_index_x) + 1
            else:
                geo_index_x = np.clip(geo_index_x, 0, self.geo_index_width)
            print('min x: ', np.nanmin(geo_index_x))
            print('max x: ', np.nanmax(geo_index_x))
            print('min y: ', np.nanmin(geo_index_y))
            print('max y: ', np.nanmax(geo_index_y))
            print('output length:', self.geo_index_length)
            print('output width:', self.geo_index_width)
        ret = None
        out_image = None
        for current_image in self.input_images:
            if not plant.isfile(current_image):
                self.print('ERROR file not found: ' + current_image)
                sys.exit(1)
            image_obj = self.read_image(current_image)
            if image_obj is None:
                self.print('ERROR opening file %s'
                           % current_image)
                sys.exit(1)
            for b in range(image_obj.nbands):
                self.geo_data = np.copy(image_obj.image)
                self.geo_width = image_obj.width
                self.geo_length = image_obj.length
                self.geo_geotransform = image_obj.geotransform
                if (image_obj.length == 1 or
                        image_obj.width == 1):
                    ret = self.slantrange_vector()
                elif self.geo_geotransform is None:
                    self.print(f'ERROR geotransform of'
                               f' {current_image} not found')
                    return
                elif self.flag_geo_index:
                    ret = self.slantrange_raster_geo_index(
                        geo_index_y, geo_index_x)
                else:
                    ret = self.slantrange_raster()
                if out_image is None:
                    out_image = plant.PlantImage(ret)
                else:
                    out_image.set_image(ret,
                                        band=out_image.nbands)
        self.save_image(out_image, output_file=self.output_file)
        return ret

    def slantrange_vector(self):

        lat_image_obj = self.read_image(self.lat_file)
        self.geo_data = np.ravel(self.geo_data)
        self.geo_lat = np.ravel(lat_image_obj.image)
        if self.geo_data.shape != self.geo_lat.shape:
            self.print('ERROR dimensions of input '
                       'data and lat.rdr do not match', 1)
            return

        lon_image_obj = self.read_image(self.lon_file)
        self.geo_lon = np.ravel(lon_image_obj.image)
        if self.geo_data.shape != self.geo_lon.shape:
            self.print('ERROR dimensions of input '
                       'data and lon.rdr do not match', 1)
            return

        min_lat = np.min(self.dest_lat)
        max_lat = np.max(self.dest_lat)
        min_lon = np.min(self.dest_lon)
        max_lon = np.max(self.dest_lon)
        ind = np.where(np.logical_and(np.logical_and(
            self.geo_lat > min_lat,
            self.geo_lat < max_lat),
            np.logical_and(
            self.geo_lon > min_lon,
            self.geo_lon < max_lon)))

        self.dest_lat = np.asarray(self.dest_lat,
                                   dtype=np.float32)
        self.dest_lon = np.asarray(self.dest_lon,
                                   dtype=np.float32)
        self.dest_lon = plant.copy_shape(self.dest_lat,
                                         self.dest_lon)

        self.geo_data = np.asarray(self.geo_data,
                                   dtype=np.float32)
        self.geo_lat = np.asarray(self.geo_lat,
                                  dtype=np.float32)
        self.geo_lon = np.asarray(self.geo_lon,
                                  dtype=np.float32)
        self.geo_lat = plant.copy_shape(self.geo_data,
                                        self.geo_lat)
        self.geo_lon = plant.copy_shape(self.geo_data,
                                        self.geo_lon)

        print('# elements inside destination area %d'
              % plant.get_indexes_len(ind))
        self.geo_lat = self.geo_lat[ind]
        self.geo_lon = self.geo_lon[ind]
        self.geo_data = self.geo_data[ind]

        min_geo_lat = np.nanmin(self.geo_lat)
        max_geo_lat = np.nanmax(self.geo_lat)
        min_geo_lon = np.nanmin(self.geo_lon)
        max_geo_lon = np.nanmax(self.geo_lon)

        lat_diff = np.zeros((self.dest_width))
        lon_diff = np.zeros((self.dest_width))
        sr_data = np.zeros((self.dest_length,
                            self.dest_width))
        sr_data = plant.insert_nan(sr_data,
                                   out_null=np.nan)
        for i in range(1, self.dest_length - 1):
            if i > 1:
                print('line %d/%d (sec=%s, total est.: %sh)'
                      % (i, self.dest_length,
                         str(int(time.time() -
                                 self.start_time)),
                         str(int((time.time() -
                                  self.start_time) *
                                 self.dest_length /
                                 ((3600 - 2) * (i - 1))))))

            lat_ind = np.logical_and(self.dest_lat[i, :] >=
                                     min_geo_lat,
                                     self.dest_lat[i, :] <=
                                     max_geo_lat)
            ind = np.where(lat_ind)
            if plant.get_indexes_len(ind) == 0:
                continue
            ind = np.where(np.logical_and(
                lat_ind,
                np.logical_and(self.dest_lon[i, :] >=
                               min_geo_lon,
                               self.dest_lon[i, :] <=
                               max_geo_lon)))
            if plant.get_indexes_len(ind) == 0:
                continue
            lat_diff = np.abs((self.dest_lat[i + 1, :] -
                               self.dest_lat[i - 1, :])) / 2
            lon_diff[1:-1] = np.abs((self.dest_lon[i, 2:] -
                                     self.dest_lon[i, :-2])) / 2
            min_lat = self.dest_lat[i, :] - lat_diff
            max_lat = self.dest_lat[i, :] + lat_diff
            min_lon = self.dest_lon[i, :] - lon_diff
            max_lon = self.dest_lon[i, :] + lon_diff
            for j in range(1, self.dest_width - 1):
                ind_lat = np.logical_and(self.geo_lat >= min_lat[j],
                                         self.geo_lat <= max_lat[j])
                ind_lon = np.logical_and(self.geo_lon >= min_lon[j],
                                         self.geo_lon <= max_lon[j])
                ind = np.where(np.logical_and(ind_lat, ind_lon))
                if plant.get_indexes_len(ind) > 0:
                    sr_data[i, j] = ind[0].shape[0]

        return sr_data

    def slantrange_raster_geo_index(self,
                                    geo_index_y, geo_index_x):
        if self.output_dtype is None:
            self.output_dtype = np.float64
        sr_data = np.zeros((self.geo_index_length,
                            self.geo_index_width),
                           dtype=self.output_dtype)
        sr_data[geo_index_y,
                geo_index_x] = self.geo_data

        return sr_data

    def slantrange_raster(self):

        plant_geogrid_obj = plant.get_coordinates(
            geotransform=self.geo_geotransform,
            length=self.geo_length,
            width=self.geo_width,
            verbose=self.verbose)
        if plant_geogrid_obj is None:
            self.print('ERROR reading geo-information from: %s'
                       % self.input_file, 1)
            return
        self.geo_data_type = self.geo_data.dtype.name
        self.geo_y0 = plant_geogrid_obj.y0
        self.geo_yf = plant_geogrid_obj.yf
        self.geo_x0 = plant_geogrid_obj.x0
        self.geo_xf = plant_geogrid_obj.xf
        self.geo_step_y = plant_geogrid_obj.step_y
        self.geo_step_x = plant_geogrid_obj.step_x
        self.geo_length = plant_geogrid_obj.length
        self.geo_width = plant_geogrid_obj.width

        if 'float' not in self.geo_data_type.lower():
            self.geo_data = np.asarray(self.geo_data,
                                       dtype=np.float64)
        if self.in_null is not None:
            self.geo_data = plant.apply_null(self.geo_data,
                                             self.in_null,
                                             out_null=np.nan)

        self.dest_lat = ((self.dest_lat - self.geo_y0) / self.geo_step_y)
        self.dest_lon = ((self.dest_lon - self.geo_x0) / self.geo_step_x)
        coordinates = (self.dest_lat, self.dest_lon)

        self.print('spline interpolation order: %d'
                   % self.spline_order)

        if 'complex' in self.geo_data_type.lower():
            sr_data = np.zeros((self.dest_length, self.dest_width),
                               dtype=np.complex64)

            sr_data_temp = np.zeros((self.dest_length, self.dest_width))
            map_coordinates(np.real(self.geo_data),
                            coordinates,
                            output=sr_data_temp,
                            order=self.spline_order,

                            mode='constant',
                            cval=np.nan,
                            prefilter=False)
            sr_data += sr_data_temp

            map_coordinates(np.imag(self.geo_data),
                            coordinates,
                            output=sr_data_temp,
                            order=self.spline_order,

                            mode='constant',
                            cval=np.nan,
                            prefilter=False)
            sr_data += 1j * sr_data_temp

        else:
            sr_data = np.zeros((self.dest_length, self.dest_width),
                               dtype=self.geo_data_type)
            map_coordinates(self.geo_data,
                            coordinates,
                            output=sr_data,
                            order=self.spline_order,

                            mode='constant',
                            cval=np.nan,
                            prefilter=False)
        sr_data = np.asarray(sr_data,
                             dtype=self.geo_data_type)

        if self.out_null is not None:

            indexes = plant.isnan(sr_data)
            sr_data = plant.insert_nan(sr_data,
                                       indexes,
                                       out_null=self.out_null)

        return sr_data


def main(argv=None):
    with plant.PlantLogger():
        parser = get_parser()
        self_obj = PlantSlantrange(parser, argv)
        ret = self_obj.run()
        return ret


if __name__ == '__main__':
    main()
