#!/usr/bin/env python3
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Gustavo H. X. Shiroma, Marco Lavalle
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import sys
from os import remove, path, makedirs
import numpy as np
import copy
import time
import shutil
import plant

DEFAULT_EXT = '.cal'
DEFAULT_SIM_EXT = '.sim'


def get_parser():

    descr = ''
    epilog = ''

    parser = plant.argparse(epilog=epilog,
                            description=descr,
                            input_images=1,

                            topo_dir=1,
                            output_file=1,
                            output_dir=1,
                            separate=1,
                            default_options=1,
                            pixel_size=1)

    parser.add_argument('-a', '--action',
                        dest='action',
                        type=str,
                        help='list of radiometric corrections'
                        ' to perform. Options: abs, terrain, polcal, fr, '
                        'azslope (default: "%(default)s")',
                        default='terrain')

    parser.add_argument('--terrain', '--terrain-type',
                        '--rtc',
                        dest='terrain_correction_type',
                        type=str,
                        help="type of radiometric terrain correction: "
                        "'beta-naught', "
                        "'sigma-naught'"
                        "'sigma-naught-norlim', "
                        "'sigma-naught-ahmed', "
                        "'sigma-naught-ulander', "
                        "'gamma-naught-norlim', "
                        "'gamma-naught', "
                        "'gamma-naught-ulander' "
                        "(default: %(default)s)",
                        default='gamma-naught-ulander')

    parser.add_argument('--inc',
                        dest='input_inc_file',
                        type=str,
                        help='Incidence angle (single-band'
                        ' file)')

    parser.add_argument('--lia',
                        dest='input_lia_file',
                        type=str,
                        help='Local incidence angle (single'
                        '-band file)')

    parser.add_argument('--psi',
                        dest='input_psi_file',
                        type=str,
                        help='Local incidence angle (single'
                        '-band file)')

    parser.add_argument('--dem',
                        dest='dem_file',
                        type=str,
                        help='Reference DEM (z.rdr) file')

    parser.add_argument('--mask',
                        dest='mask_file',
                        type=str,
                        help='Layover/shadow mask file'
                        ' (mask.rdr) file')

    parser.add_argument('--az-slope',
                        dest='in_azslope_angle',
                        type=str,
                        help='Input save az slope angle (only '
                        'for azslope mode)')
    parser.add_argument('--out-az-slope',
                        dest='out_azslope_angle',
                        type=str,
                        help='Save az slope angle (only for '
                        'azslope mode)')
    parser.add_argument('--out-lia',
                        dest='out_lia',
                        type=str,
                        help='Save estimated lia angle. ')
    parser.add_argument('--out-psi',
                        dest='out_psi',
                        type=str,
                        help='Save estimated psi angle. ')

    parser.add_argument('--cal-factor',
                        '--calibration-factor',
                        dest='calibration_factor',
                        type=float,
                        help='Calibration factor')

    parser.add_argument('-c', '--calibration-factor-db',
                        '--cal-factor-db',
                        dest='calibration_factor_db',
                        type=float,
                        help='Calibration factor in dB')

    parser.add_argument('--faraday-rotation-deg', '--fr-deg', '--fr',
                        dest='fr_angle',
                        type=float,
                        help='Faraday rotation angle (in '
                        'degrees)',
                        default=np.nan)

    parser.add_argument('--method',
                        dest='method',
                        type=float,
                        help='Local inc. angle method. '
                        'Options: 1 - calculate from DEM Lee200; 2 - '
                        'calculate from DEM test; 3 - Read from file'
                        "(default: %(default)s)",
                        default=3)

    parser.add_argument('--input-radiometry',
                        dest='input_radiometry',
                        type=str,
                        help='Input data radiometry. Options:'
                        'beta, sigma-inc (or sigma-ellipsoid), sigma-lia')

    parser.add_argument('--rtc-min-value-db',
                        dest='rtc_min_value_db',
                        type=float,
                        help='DEM upsample factor.')

    parser.add_argument('--sim', '--simulate-backscatter', '--simamp',
                        '--sim-amp', '--simulate',
                        dest='simulate',
                        action='store_true',
                        help='Simulate backscatter '
                        '(no input backscatter image is needed).')

    parser.add_argument('--line-by-line',
                        dest='fast',
                        action='store_false',
                        help='Process line by line')

    parser.add_argument('-b', '--out-db', '--out-dB', dest='output_in_dB',
                        action='store_true',
                        help='Output in dB')

    in_intensity_group = parser.add_mutually_exclusive_group(required=False)
    in_intensity_group.add_argument('--in-intensity', '--in-int',
                                    dest='in_intensity',
                                    action='store_true',
                                    help='Input image is intensity')
    in_intensity_group.add_argument('--in-amplitude', '--in-amp',
                                    dest='in_amplitude',
                                    action='store_true',
                                    help='Input image is amplitude')

    parser.add_argument('--out-magnitude',
                        dest='out_magnitude',
                        action='store_true',
                        help='Output image saved as real')
    return parser


class PlantRadiometricCorrection(plant.PlantScript):

    def __init__(self, parser, argv=None):

        super().__init__(parser, argv)
        self.image_obj = None
        self.geotransform = None
        self.projection = None
        self.dt = None
        self.length = None
        self.width = None
        self.scheme = None
        self.phase = 0
        self.hh_file_orig = ''
        self.hv_file_orig = ''
        self.vh_file_orig = ''
        self.vv_file_orig = ''
        self.receive = None
        self.transmit = None
        self.convert_inc_to_rad = False
        self.convert_lia_to_rad = False
        self.convert_psi_to_rad = False
        self.rtc_min_value = 0.0
        self.step_abs = False
        self.step_terrain_correction = False
        self.step_polcal = False
        self.step_fr = False
        self.step_azslope = False
        self.step_area = False
        self.input_inc = None
        self.input_lia = None
        self.input_psi = None
        self.input_dem = None

        self.mask_file_str = ''

    def run(self):

        if self.separate:

            self_dict = {}
            prevent_copy_list = ['parser']
            for key, value in self.__dict__.items():
                if key in prevent_copy_list:
                    continue
                self_dict[key] = copy.deepcopy(value)

            input_images = self.input_images
            print('input_images:', input_images)
            print('output_files:', self.output_files)

            ret_list = []
            for i, current_file in enumerate(input_images):
                if i != 0:
                    plant.plant_config.logger_obj.flush_temporary_files()
                    self.__dict__.update(self_dict)
                self.input_images = [current_file]
                self.output_file = self.output_files[i]
                if (self.output_skip_if_existent and
                        plant.isfile(self.output_file)):
                    print('INFO output file %s already exist, '
                          'skipping execution..' % self.output_file)
                    continue
                self.populate_parameters()
                self.loop_non_polarimetric_channels()
                ret = self.realize_changes()
                ret_list.append(ret)

            return ret_list

        self.populate_parameters()
        self.loop_polarimetric_channels()
        self.loop_non_polarimetric_channels()
        ret = self.realize_changes()
        return ret

    def populate_parameters(self):

        if not self.in_intensity and self.in_amplitude:
            self.in_intensity = False
        elif not self.in_intensity and not self.in_amplitude:
            self.in_intensity = None

        if not self.output_file:
            self.print('ERROR invalid output filename')

        if (not self.simulate and self.input_images is None or
                len(self.input_images) == 0):
            self.parser.print_help()
            self.print('EXIT please provide an input image')
            return

        if not self.simulate:
            self.hh_file_orig = self.hh_file
            self.hv_file_orig = self.hv_file
            self.vh_file_orig = self.vh_file
            self.vv_file_orig = self.vv_file

        self.current_images = self.input_images[:]

        if not self.simulate:
            self.image_obj = self.read_image(self.input_images[0],
                                             only_header=1)

            self.width = self.image_obj.width
            self.length = self.image_obj.length

            if not self.output_ext:
                if '.' in self.input_images[0]:
                    self.output_ext = '.' + \
                        (self.input_images[0].split('.'))[-1]
                else:
                    self.output_ext = DEFAULT_EXT
        else:
            self.output_ext = DEFAULT_SIM_EXT
            self.identified_inputs = []

        self.number_of_files = len(self.identified_inputs)
        self.print(f'number of input files (pol): {self.number_of_files}')
        self.print(f'number of input files (total):'
                   f' {len(self.current_images)}')

        self.print('selected steps (action): ' + self.action)

        self.step_abs = (('abs' in self.action or 'area' in self.action) or
                         self.calibration_factor is not None or
                         self.calibration_factor_db is not None)
        self.step_terrain_correction = 'terrain' in self.action
        self.step_polcal = 'polcal' in self.action
        self.step_azslope = 'azslope' in self.action
        self.step_fr = 'fr' in self.action or plant.isvalid(self.fr_angle)

        if self.calibration_factor_db is not None and self.calibration_factor is None:
            self.calibration_factor = 10**(self.calibration_factor_db / 10.0)
            self.print('calibration factor [dB]: ' +
                       str(self.calibration_factor_db))
            self.print('calibration factor [linear]: ' +
                       str(self.calibration_factor))

        self.step_area = self.step_abs and self.calibration_factor is None

        if self.topo_dir is None and len(self.input_images) >= 1:
            self.topo_dir = plant.get_common_directory(self.input_images)
        elif self.topo_dir is None:
            self.topo_dir = '.'

        if self.step_abs:
            self.print('verifying inputs for absolute radiometric '
                       'correction...')
            if self.step_area:
                if self.pixel_size_az is None or self.pixel_size_rg is None:
                    self.print('ERROR pixel sizes could not be '
                               'determined for radiometric area correction. ')
                    return
                else:
                    self.pixel_area = self.pixel_size_az * self.pixel_size_rg
                    self.print('image intensity will be divided by pixel-'
                               'area (%f[m^2])...' % (self.pixel_area))
            elif self.calibration_factor is None:
                self.print('ERROR The calibration factor is'
                           ' necessary for absolute radiometric'
                           ' correction.')
                return

        if self.step_terrain_correction:
            self.print('verifying inputs for terrain correction (mode %s)...'
                       % (self.terrain_correction_type))
            self.terrain_correction_type = \
                self.terrain_correction_type.replace('_',
                                                     '-')
            if self.terrain_correction_type == 'sigma-naught':
                self.terrain_correction_type = 'sigma-naught-ulander'
            if self.terrain_correction_type == 'gamma-naught':
                self.terrain_correction_type = 'gamma-naught-ulander'
            if (self.terrain_correction_type not in
                ['sigma-naught-ellipsoid',
                 'sigma-naught-norlim'
                 'gamma-naught-norlim',
                 'sigma-naught-ahmed',
                 'sigma-naught-ulander',
                 'gamma-naught-ulander']):
                self.print('ERROR terrain_correction_type not '
                           'recognized: %s'
                           % self.terrain_correction_type)

            if self.input_radiometry is None:
                self.print('WARNING the parameter --input-radiometry'
                           ' is not set. Considering input as'
                           ' beta/beta-naught')
                self.input_radiometry = 'beta'

            else:
                if 'beta' in self.input_radiometry.lower():
                    self.input_radiometry = 'beta'
                elif ('sigma' in self.input_radiometry.lower() and
                      ('inc' in self.input_radiometry.lower() or
                       'ell' in self.input_radiometry.lower())):
                    self.input_radiometry = 'sigma-inc'
                elif 'sigma' in self.input_radiometry.lower():
                    self.input_radiometry = 'sigma-lia'
                else:
                    self.print('ERROR not recognized input radiometry: '
                               f' {self.input_radiometry}')
                    return
                self.print('INFO input radiometry:'
                           f' {self.input_radiometry}')

            self._check_inc_dict = None

            if not self.input_inc_file:

                self.input_inc_file = path.join(self.topo_dir, 'los.rdr:0')
                if not plant.isfile(self.input_inc_file):
                    ret = self._check_inc_file(self.topo_dir)
                    if ret is not None and 'inc' in ret.keys():
                        self.input_inc_file = ret['inc']

            if not self.input_lia_file:
                self.input_lia_file = path.join(self.topo_dir,
                                                'localInc.rdr')
                if not plant.isfile(self.input_lia_file):
                    self.input_lia_file = path.join(self.topo_dir,
                                                    'incLocal.rdr')
                if not plant.isfile(self.input_lia_file):
                    ret = self._check_inc_file(self.topo_dir)
                    if ret is not None and 'lia' in ret.keys():
                        self.input_lia_file = ret['lia']
                if not plant.isfile(self.input_lia_file):
                    self.input_lia_file = None

            if not self.input_psi_file:
                self.input_psi_file = path.join(self.topo_dir,
                                                'localPsi.rdr')
                if not plant.isfile(self.input_psi_file):
                    ret = self._check_inc_file(self.topo_dir)
                    if ret is not None and 'psi' in ret.keys():
                        self.input_psi_file = ret['psi']
                if not plant.isfile(self.input_psi_file):
                    self.input_psi_file = None

            if not self.mask_file:
                self.mask_file = path.join(self.topo_dir, 'mask.rdr')
            if not self.dem_file:
                self.dem_file = path.join(self.topo_dir, 'z.rdr')

            if self.method < 3:
                flag_open_input_dem = (self.terrain_correction_type in
                                       ['sigma-naught-norlim',
                                        'gamma-naught-norlim',
                                        'sigma-naught-ahmed',
                                        'sigma-naught-ulander',
                                        'gamma-naught-ulander'])

            else:
                flag_open_input_dem = False
            flag_open_input_psi = (self.terrain_correction_type in
                                   ['sigma-naught-ahmed',
                                    'sigma-naught-ulander',
                                    'gamma-naught-ulander'])
            if ('sigma-inc' in self.input_radiometry or
                    'sigma-ellipsoid' in self.input_radiometry or

                    self.method < 3):
                flag_open_input_inc = (self.terrain_correction_type in
                                       ['sigma-naught-ellipsoid',

                                        'sigma-naught-norlim',
                                        'gamma-naught-norlim',
                                        'sigma-naught-ahmed',
                                        'sigma-naught-ulander',
                                        'gamma-naught-ulander'])
            else:
                flag_open_input_inc = (self.terrain_correction_type in
                                       ['sigma-naught-ellipsoid',

                                        'sigma-naught-ahmed'])
            flag_open_input_lia = ('sigma-lia' in self.input_radiometry or
                                   self.terrain_correction_type in
                                   ['sigma-naught-norlim',
                                    'gamma-naught-norlim',
                                    'sigma-naught-ahmed',
                                    'sigma-naught-ulander',
                                    'gamma-naught-ulander'])

            self.print('Files needed for selected terrain correction (%s):'
                       % self.terrain_correction_type)
            with plant.PlantIndent():
                if flag_open_input_dem:
                    self.print(f'reference DEM: {self.dem_file}')
                if flag_open_input_inc:
                    self.print(f'incidence angle: {self.input_inc_file}')
                if flag_open_input_lia:
                    self.print(f'local-incidence angle: {self.input_lia_file}')
                if flag_open_input_psi:
                    self.print(f'projection angle: {self.input_psi_file}')

            if (not plant.isfile(self.input_inc_file) and
                    flag_open_input_inc):
                self.print(f'ERROR file not found: {self.input_inc_file}')
                return

            dem_file_str = self.dem_file
            if (not plant.isfile(dem_file_str) and
                    flag_open_input_dem):
                self.print(f'ERROR file not found: {dem_file_str}')
                return

            if (self.method >= 3 and
                not plant.isfile(self.input_lia_file) and
                    flag_open_input_lia):
                self.print(f'ERROR File not found: {self.input_lia_file}')
                return

            self.mask_file_str = self.mask_file

            if flag_open_input_inc:
                self.input_inc = self.read_file(self.input_inc_file)

                if plant.is_deg(self.input_inc):
                    self.print('inc. angle will be converted to radians...')
                    self.convert_inc_to_rad = True

                else:
                    self.print('inc. angle is given in radians...')
            if flag_open_input_dem:
                self.input_dem = self.read_file(dem_file_str)
            if flag_open_input_lia and self.input_lia_file:
                self.input_lia = self.read_file(self.input_lia_file)

                if plant.is_deg(self.input_lia):
                    self.print('local-inc. angle will be converted to'
                               ' radians...')
                    self.convert_lia_to_rad = True

                else:
                    self.print('local-inc. angle psi is given in radians...')
            elif flag_open_input_lia:
                if self.pixel_size_az is None or self.pixel_size_rg is None:
                    self.populate_pixel_sizes(self.pixel_size_az,
                                              self.pixel_size_rg)
                self.input_lia = plant.get_local_inc_angle(
                    self.input_dem,
                    self.input_inc,
                    self.pixel_size_az,
                    self.pixel_size_rg,
                    method=self.method,
                    degrees=self.convert_inc_to_rad)
            if self.out_lia:
                self.save_image(self.convert_lia(self.input_lia), self.out_lia)

            if (flag_open_input_psi and self.input_psi_file and
                    plant.isfile(self.input_psi_file)):
                self.input_psi = self.read_file(self.input_psi_file)

            elif flag_open_input_psi:
                self.print('ERROR the projection angle (psi) is'
                           ' required for the selected radiometric'
                           ' terrain correction (RTC).')
                return

            if flag_open_input_psi:

                if plant.is_deg(self.input_psi):
                    self.print('Projection angle psi will be converted to'
                               ' radians...')
                    self.convert_psi_to_rad = True
                else:
                    self.print('Projection angle psi is given in radians...')

            if self.out_psi and flag_open_input_psi and self.input_psi_file:
                self.save_image(self.input_psi, self.out_psi)

            if self.rtc_min_value_db is not None:
                self.rtc_min_value = 10. ** (self.rtc_min_value_db / 10.0)
                self.print(f'RTC min. value: {self.rtc_min_value_db} dB = '
                           f' {self.rtc_min_value} ')

            if self.geotransform is None or self.projection is None:
                print('=============================')
                print('geotransform:', self.geotransform)
                print('projection:', self.projection)
                input_angle_file_dict = {
                    self.input_inc_file: flag_open_input_inc,
                    self.input_lia_file: flag_open_input_lia,
                    self.input_psi_file: flag_open_input_psi}
                for input_angle_file, flag_open_file in input_angle_file_dict.items():
                    if not input_angle_file or not flag_open_file:
                        continue
                    input_inc_obj = self.read_image(input_angle_file)
                    self.geotransform = input_inc_obj.geotransform
                    self.projection = input_inc_obj.projection
                    if self.geotransform is not None and self.projection is not None:
                        break

        if self.step_fr:

            if np.isnan(self.fr_angle):
                self.print('ERROR the Faraday Rotation angle is'
                           ' necessary for Faraday Rotation'
                           ' correction.')

                return
            else:
                self.fr_angle = float(self.fr_angle)

        if self.step_polcal:
            self.print('verifying inputs for polarimetric correction...')
        if self.step_polcal and self.number_of_files != 4:
            self.print('ERROR all polarimetric channels are necessary'
                       ' for polarimetric correction (polcal).')
            return

        elif self.step_polcal:

            self.transmit = None
            self.receive = None
            if self.transmit is None or self.receive is None:

                self.print('ERROR The polarimetric constants are necessary'
                           ' for polarimetric correction (polcal).')

                return

        if self.step_azslope:
            self.print('verifying inputs for azimuth slope correction...')
        if self.step_azslope and self.number_of_files != 4:
            self.print('ERROR all polarimetric channels are'
                       ' necessary for azimuth slope correction (azslope).')
            return
        elif self.step_azslope:
            if self.pixel_size_az is None or self.pixel_size_rg is None:
                self.populate_pixel_sizes(self.pixel_size_az,
                                          self.pixel_size_rg)

            if self.input_inc is None:
                if not self.input_inc_file:
                    self.input_inc_file = path.join(self.topo_dir, 'los.rdr')
                self.input_inc_file = self.input_inc_file
                if not plant.isfile(self.input_inc_file):
                    self.print('ERROR File not found: ' +
                               self.input_inc_file)

                    return
                self.input_inc = self.read_file(self.input_inc_file)

                if plant.is_deg(self.input_inc):
                    self.print('inc. angle will be converted to radians...')
                    self.convert_inc_to_rad = True

            if self.input_dem is None:
                if not self.dem_file:
                    self.dem_file = path.join(self.topo_dir, 'z.rdr')
                dem_file_str = self.dem_file
                if not plant.isfile(dem_file_str):
                    self.print('ERROR File not found: ' +
                               dem_file_str)

                    return
                self.input_dem = self.read_file(dem_file_str)

            if self.pixel_size_az is None or self.pixel_size_rg is None:
                self.print('WARNING Pixel sizes could not be '
                           'determined. ')
            elif self.pixel_size_az is None:
                self.print('WARNING Pixel size in azimuth could not be '
                           'determined. ')
            elif self.pixel_size_rg is None:
                self.print('WARNING Pixel size in range could not be '
                           'determined. ')
            if self.pixel_size_az is None or self.pixel_size_rg is None:
                plant.prompt_continue('Pixel sizes are necessary for'
                                      ' azimuth slope correction.'
                                      ' Do you want to continue without'
                                      ' performing this correction?'
                                      ' ([y]es/[n]o) ', force=self.force)
                self.step_azslope = False

        self.print('INFO radiometric correction steps:')
        if self.step_polcal:
            self.print('    - polarimetric correction.')
        if self.step_fr:
            self.print('    - Faraday Rotation correction.')
        if self.step_azslope:
            self.print('    - azimuth slope correction.')
        if self.step_abs:
            self.print('    - absolute radiometric correction.')
        if self.step_terrain_correction:
            self.print('    - terrain correction.')

    def _check_inc_file(self, topo_dir):
        if self._check_inc_dict is not None:
            return self._check_inc_dict
        self._check_inc_dict = {}
        ext_list = ['.rdr', '.bin']
        inc_name_list = ['inc', 'localInc', 'incLocal']
        for ext in ext_list:
            for inc_name in inc_name_list:
                inc_file = path.join(topo_dir, inc_name + ext)
                if not path.isfile(inc_file):
                    continue
                inc_obj = self.read_image(inc_file, verbose=False)

                if inc_name == 'inc' and inc_obj is not None and inc_obj.nbands == 1:
                    self._check_inc_dict['inc'] = inc_file
                elif inc_name == 'localInc' and inc_obj is not None and inc_obj.nbands == 1:
                    self._check_inc_dict['lia'] = inc_file
                elif inc_name == 'incLocal' and inc_obj is not None and inc_obj.nbands == 1:
                    self._check_inc_dict['lia'] = inc_file

                elif inc_obj is not None and inc_obj.nbands == 2:
                    self._check_inc_dict['psi'] = inc_file + ':0'
                    self._check_inc_dict['lia'] = inc_file + ':1'
                if ('inc' in self._check_inc_dict and
                    'lia' in self._check_inc_dict and
                        'psi' in self._check_inc_dict):
                    break
        return self._check_inc_dict

    def convert_inc(self, input_data):

        if self.convert_inc_to_rad:
            return np.radians(input_data)
        return input_data

    def convert_lia(self, input_data):
        if self.convert_lia_to_rad:
            return np.radians(input_data)
        return input_data

    def convert_psi(self, input_data):

        if (input_data.shape[0] != self.length or
                input_data.shape[1] != self.width):
            print('*** cropping angle...', input_data.shape)
            input_data = plant.copy_shape_from_tuple((self.length, self.width),
                                                     input_data)
            print('*** new shape: ', input_data.shape)
        if self.convert_psi_to_rad:
            return np.radians(input_data)
        return input_data

    def populate_pixel_sizes(self, args_pixel_size_az, args_pixel_size_rg):
        self.pixel_size_az = args_pixel_size_az
        self.pixel_size_rg = args_pixel_size_rg
        if self.pixel_size_az:
            self.print('azimuth pixel size: ' +
                       str(self.pixel_size_az))
        else:
            self.print('ERROR azimuth pixel size could not be determined.')
            return
        if self.pixel_size_rg:
            self.print('range pixel size: ' +
                       str(self.pixel_size_rg))
        else:
            self.print('ERROR range pixel size could not be determined.')
            return

    def read_file(self, input_images):

        image_obj = self.read_image(input_images,

                                    verbose=self.verbose)

        if self.image_obj is None:
            self.image_obj = image_obj
        if self.width is None:
            self.width = image_obj.width
        if self.length is None:
            self.length = image_obj.length
        if image_obj.width != self.width or image_obj.length != self.length:
            self.print('input data shape: %d x %d ' % (self.length,
                                                       self.width))
            self.print(input_images + ' shape: %d x %d '
                       % (image_obj.length,
                          image_obj.width))
            if (abs(image_obj.width - self.width) > 1 or
                    abs(image_obj.length - self.length) > 1):
                self.print('ERROR input image(s) and ' + input_images +
                           f' dimensions do not match ({image_obj.shape})'
                           f' ({self.length}, {self.width}).')
                return

            self.print(
                'WARNING input image(s) and ' +
                input_images +
                ' dimensions'
                ' do not match. Resizing input image...')
            image = plant.copy_shape_from_tuple((self.length, self.width),
                                                image_obj.image)
            image_obj.set_image(image)
        return image_obj.image

    def apply_abs_and_terrain_correction(self, current_file,
                                         output_file=None):

        if not output_file:
            output_file = self.get_filename(current_file, temp=True)
        if not plant.isfile(current_file) and not self.simulate:
            self.print('WARNING File not found: ' + current_file)
            return -1

        out_ret = plant.overwrite_file_check(output_file, force=self.force)
        if not out_ret:
            self.print('operation cancelled.')
            sys.exit(0)

        if self.step_terrain_correction and self.step_abs:
            self.print('INFO applying radiometric terrain and absolute '
                       'corrections (mode: ' +
                       self.terrain_correction_type + ').')
            self.print('INFO calibration factor [linear]: ' +
                       str(self.calibration_factor))
        elif self.step_terrain_correction:
            self.print('INFO applying radiometric terrain correction (mode: ' +
                       self.terrain_correction_type + ').')
        elif self.step_abs:
            self.print('INFO applying absolute radiometric correction...')
            self.print('INFO calibration factor [linear]: ' +
                       str(self.calibration_factor))

        if not self.simulate:

            image_obj = self.read_image(current_file, verbose=self.verbose)

            if self.in_intensity is None:
                self.in_intensity = 'complex' not in plant.get_dtype_name(
                    image_obj.dtype).lower()

            self.dt = image_obj.dtype

            if self.output_in_dB or self.out_magnitude:
                self.dt = np.float32
            band_range = image_obj.nbands
        else:
            self.dt = np.float32
            band_range = 1
            if self.in_intensity is None:
                self.in_intensity = True

        if self.in_intensity:
            print('INFO input image as intensity/power')
        else:
            print('INFO input image as amplitude')

        image = None

        if self.fast is None or self.fast:
            try:
                if not self.simulate:
                    image_tc_obj = image_obj.soft_copy()
                for b in range(band_range):
                    if not self.simulate:
                        image = image_obj.get_image(band=b)

                    image_tc = self.run_terrain_correction(image,
                                                           inc=self.input_inc,
                                                           lia=self.input_lia,
                                                           psi=self.input_psi)
                    if self.simulate:
                        self.save_image(image_tc, output_file)
                        return
                    image_tc_obj.set_image(image_tc, band=b)
                self.save_image(image_tc_obj, output_file)
                return
            except MemoryError:
                image = None

        with open(output_file, 'w') as out_file:
            for b in range(band_range):
                print(f'*** band (2): {b}')
                if not self.simulate:
                    image_orig = image_obj.get_image(band=b)
                for i in range(self.length):
                    if not self.simulate:
                        image = np.copy(image_orig[i, :])
                    if self.input_mask is not None:
                        image = plant.insert_nan(
                            image,
                            np.where(self.input_mask[i, :] != 1),
                            out_null=np.nan)
                    if self.input_inc is not None:
                        inc = self.input_inc[i, :]
                    if self.input_lia is not None:
                        lia = self.input_lia[i, :]
                    if self.input_psi is not None:
                        psi = self.input_psi[i, :]
                    self.run_terrain_correction(image,
                                                out_file=out_file,
                                                inc=inc,
                                                lia=lia,
                                                psi=psi)
        if self.image_obj.nbands > 1:
            scheme = 'BSQ'
            self.image_obj.scheme = scheme
        else:
            scheme = None
        plant.create_isce_header(output_file,
                                 scheme=scheme,
                                 image_obj=self.image_obj)
        self.save_image(self.image_obj, output_file, save_header_only=True)

    def run_terrain_correction(self, image, out_file=None,
                               inc=None, lia=None, psi=None):

        if not self.step_terrain_correction:
            image_tc = self.save_to_file(image, out_file)
            return image_tc

        if ('sigma-inc' in self.input_radiometry or
                'sigma-ellipsoid' in self.input_radiometry):
            factor = 1.0 / np.sin(self.convert_inc(inc))
        elif 'sigma-lia' in self.input_radiometry:
            factor = 1.0 / np.sin(self.convert_lia(lia))
        elif image is not None:
            factor = np.ones_like(image, dtype=np.float32)
        elif inc is not None:
            factor = np.ones_like(inc, dtype=np.float32)
        elif lia is not None:
            factor = np.ones_like(lia, dtype=np.float32)
        elif psi is not None:
            factor = np.ones_like(psi, dtype=np.float32)

        if (factor.shape[0] != self.length or
                factor.shape[1] != self.width):
            factor = plant.copy_shape_from_tuple((self.length, self.width),
                                                 factor)
        if self.terrain_correction_type == 'beta_naught':
            image_tc = self.save_to_file(image, out_file, factor=factor)
            return image_tc
        elif self.terrain_correction_type == 'sigma-naught-ellipsoid':
            factor *= np.sin(self.convert_inc(inc))
            image_tc = self.save_to_file(image, out_file, factor=factor)
        elif self.terrain_correction_type == 'sigma-naught-norlim':
            factor *= np.sin(self.convert_lia(lia))
            image_tc = self.save_to_file(image, out_file, factor=factor)
        elif self.terrain_correction_type == 'sigma-naught-ahmed':
            factor *= np.cos(self.convert_inc(inc))
            factor *= np.absolute(np.cos(self.convert_psi(psi)))
            factor /= np.cos(self.convert_lia(lia))
            image_tc = self.save_to_file(image, out_file, factor=factor)
        elif self.terrain_correction_type == 'sigma-naught-ulander':
            factor *= np.absolute(np.cos(self.convert_psi(psi)))
            image_tc = self.save_to_file(image, out_file, factor=factor)

        elif self.terrain_correction_type == 'gamma-naught-norlim':
            factor *= np.tan(self.convert_lia(lia))
            image_tc = self.save_to_file(image, out_file, factor=factor)
        elif self.terrain_correction_type == 'gamma-naught-ulander':
            factor *= np.absolute(np.cos(self.convert_psi(psi)))
            factor /= np.cos(self.convert_lia(lia))
            image_tc = self.save_to_file(image, out_file, factor=factor)
        return image_tc

    def save_to_file(self, image, out_file, factor=None):

        if factor is not None:

            invalid_ind = np.where(factor < 0)
            factor[invalid_ind] = np.nan

            if self.rtc_min_value is not None and self.rtc_min_value > 0.0:
                invalid_ind = np.where(factor > 1.0 / self.rtc_min_value)
                factor[invalid_ind] = np.nan

            if not self.in_intensity:

                factor = np.sqrt(factor)
            if image is not None:
                image = image * factor
            else:
                image = 1.0 / factor
        elif image is None:
            self.print('ERROR no input image or no terrain correction'
                       ' selected')

            return

        if self.step_abs:
            if self.step_area:
                image = image / self.pixel_area

            if self.in_intensity:
                image *= self.calibration_factor

            else:
                image *= np.sqrt(self.calibration_factor)

            if self.output_in_dB:
                indexes = np.where(image == 0)
                image = plant.insert_nan(image,
                                         indexes,
                                         out_null=np.nan)

                if self.in_intensity:
                    image = 10 * np.log10(image)

                else:
                    image = 20 * np.log10(image)

        elif self.output_in_dB:
            if self.in_intensity:
                image = 10 * np.log10(image)
            else:
                image = 20 * np.log10(image)

        if (not self.in_intensity and not self.output_in_dB):
            image = image**2

        if self.out_magnitude:
            image = np.absolute(image)

        image = np.asarray(image, dtype=self.dt)
        if out_file is None:
            return image
        image.tofile(out_file)

    def apply_polarimetric_correction(self, transmit=None, receive=None):

        from polcal import polcal_py

        hhOut_file = self.get_filename(self.hh_file, temp=True)
        hvOut_file = self.get_filename(self.hv_file, temp=True)
        vhOut_file = self.get_filename(self.vh_file, temp=True)
        vvOut_file = self.get_filename(self.vv_file, temp=True)

        if self.step_polcal:
            self.print('INFO applying polarimetric correction...')
        if self.step_fr:
            self.print('INFO applying Faraday rotation correction...')
        if self.step_azslope:
            self.print(
                'INFO applying azimuth terrain orientation correction...')

        self.print('temporary files:')
        self.print('%s' % hhOut_file)
        self.print('%s' % hvOut_file)
        self.print('%s' % vhOut_file)
        self.print('%s' % vvOut_file)

        hh_file_c = path.abspath(self.hh_file)
        hv_file_c = path.abspath(self.hv_file)
        vh_file_c = path.abspath(self.vh_file)
        vv_file_c = path.abspath(self.vv_file)
        hhOut_file_c = path.abspath(hhOut_file)
        hvOut_file_c = path.abspath(hvOut_file)
        vhOut_file_c = path.abspath(vhOut_file)
        vvOut_file_c = path.abspath(vvOut_file)

        if self.step_polcal:

            transmitCrossTalk1Real_c = transmit.getCrossTalk1().real
            transmitCrossTalk1Imag_c = transmit.getCrossTalk1().imag
            transmitCrossTalk2Real_c = transmit.getCrossTalk2().real
            transmitCrossTalk2Imag_c = transmit.getCrossTalk2().imag
            transmitChannelImbalanceReal_c = \
                transmit.getChannelImbalance().real
            transmitChannelImbalanceImag_c = \
                transmit.getChannelImbalance().imag
            receiveCrossTalk1Real_c = receive.getCrossTalk1().real
            receiveCrossTalk1Imag_c = receive.getCrossTalk1().imag
            receiveCrossTalk2Real_c = receive.getCrossTalk2().real
            receiveCrossTalk2Imag_c = receive.getCrossTalk2().imag
            receiveChannelImbalanceReal_c = \
                receive.getChannelImbalance().real
            receiveChannelImbalanceImag_c = \
                receive.getChannelImbalance().imag
        else:
            transmitCrossTalk1Real_c = 0
            transmitCrossTalk1Imag_c = 0
            transmitCrossTalk2Real_c = 0
            transmitCrossTalk2Imag_c = 0
            transmitChannelImbalanceReal_c = 1
            transmitChannelImbalanceImag_c = 0
            receiveCrossTalk1Real_c = 0
            receiveCrossTalk1Imag_c = 0
            receiveCrossTalk2Real_c = 0
            receiveCrossTalk2Imag_c = 0
            receiveChannelImbalanceReal_c = 1
            receiveChannelImbalanceImag_c = 0

        if self.step_azslope:
            azslope_file = self.in_azslope_angle
            if not azslope_file and not self.out_azslope_angle:
                azslope_file = 'azslope.temp' + str(int(self.start_time))
            elif not azslope_file:
                azslope_file = self.out_azslope_angle
            plant.get_azslope_angle(self.input_dem,
                                    self.convert_inc(
                                        self.input_inc),
                                    self.pixel_size_az,
                                    self.pixel_size_rg,
                                    azslope_file,
                                    force=self.force)
            azslope_file_c = path.abspath(azslope_file)
        else:
            azslope_file_c = ''

        image_obj = self.read_image(self.input_images[0], only_header=True)
        dataType = plant.get_isce_dtype(image_obj.dtype)
        flag_is_complex = np.asarray(('CFLOAT' in dataType.upper() or
                                      'COMPLEX' in dataType.upper()),
                                     dtype=np.byte)

        time_1 = time.time()

        flag_c_call = False
        if flag_c_call:
            command = '/home/shiroma/dev/plant/polcal.abi3.so '

            command_vect = [hh_file_c, hv_file_c, vh_file_c, vv_file_c,
                            hhOut_file_c, hvOut_file_c, vhOut_file_c,
                            vvOut_file_c,
                            str(self.width), str(self.length),
                            str(flag_is_complex),
                            str(transmitCrossTalk1Real_c),
                            str(transmitCrossTalk2Real_c),
                            str(transmitChannelImbalanceReal_c),
                            str(transmitCrossTalk1Imag_c),
                            str(transmitCrossTalk2Imag_c),
                            str(transmitChannelImbalanceImag_c),
                            str(receiveCrossTalk1Real_c),
                            str(receiveCrossTalk2Real_c),
                            str(receiveChannelImbalanceReal_c),
                            str(receiveCrossTalk1Imag_c),
                            str(receiveCrossTalk2Imag_c),
                            str(receiveChannelImbalanceImag_c),
                            str(azslope_file_c)]
            command += ' '.join(command_vect)
            plant.execute(command, verbose=True)
        else:
            from isceobj.Image import createImage
            hh_fileImage = createImage()
            hh_fileImage.load(hh_file_c + '.xml')
            hh_fileImage.createImage()
            hh_fileAccessor = hh_fileImage.getAccessor()

            hv_fileImage = createImage()
            hv_fileImage.load(hv_file_c + '.xml')
            hv_fileImage.createImage()
            hv_fileAccessor = hv_fileImage.getAccessor()

            vh_fileImage = createImage()
            vh_fileImage.load(vh_file_c + '.xml')
            vh_fileImage.createImage()
            vh_fileAccessor = vh_fileImage.getAccessor()

            vv_fileImage = createImage()
            vv_fileImage.load(vv_file_c + '.xml')
            vv_fileImage.createImage()
            vv_fileAccessor = vv_fileImage.getAccessor()

            hhOut_fileImage = createImage()
            hhOut_fileImage.initImage(hhOut_file_c, 'write', self.width)
            hhOut_fileImage.dataType = hh_fileImage.dataType
            hhOut_fileImage.createImage()
            hhOut_fileAccessor = hhOut_fileImage.getAccessor()

            hvOut_fileImage = createImage()
            hvOut_fileImage.initImage(hvOut_file_c, 'write', self.width)
            hvOut_fileImage.dataType = hv_fileImage.dataType
            hvOut_fileImage.createImage()
            hvOut_fileAccessor = hvOut_fileImage.getAccessor()

            vhOut_fileImage = createImage()
            vhOut_fileImage.initImage(vhOut_file_c, 'write', self.width)
            vhOut_fileImage.dataType = vh_fileImage.dataType
            vhOut_fileImage.createImage()
            vhOut_fileAccessor = vhOut_fileImage.getAccessor()

            vvOut_fileImage = createImage()
            vvOut_fileImage.initImage(vvOut_file_c, 'write', self.width)
            vvOut_fileImage.dataType = vv_fileImage.dataType
            vvOut_fileImage.createImage()
            vvOut_fileAccessor = vvOut_fileImage.getAccessor()

            if azslope_file_c:
                azslope_image = createImage()
                azslope_image.set_filename(azslope_file_c)
                azslope_image.load(azslope_file_c + '.xml')
                azslope_image.initImage(azslope_file_c, 'read', self.width)
                azslope_image.createImage()
                azslope_file_accessor = azslope_image.getAccessor()
            else:
                azslope_file_accessor = 0

            if plant.isvalid(self.fr_angle) and self.fr_angle != 0:
                self.print('Faraday rotation angle (deg): ' +
                           str(self.fr_angle))
            elif plant.isnan(self.fr_angle):
                self.fr_angle = 0

            polcal_py(hh_fileAccessor, hv_fileAccessor, vh_fileAccessor,
                      vv_fileAccessor, hhOut_fileAccessor, hvOut_fileAccessor,
                      vhOut_fileAccessor, vvOut_fileAccessor,
                      self.width, self.length, flag_is_complex,
                      transmitCrossTalk1Real_c, transmitCrossTalk2Real_c,
                      transmitChannelImbalanceReal_c, transmitCrossTalk1Imag_c,
                      transmitCrossTalk2Imag_c, transmitChannelImbalanceImag_c,
                      receiveCrossTalk1Real_c, receiveCrossTalk2Real_c,
                      receiveChannelImbalanceReal_c, receiveCrossTalk1Imag_c,
                      receiveCrossTalk2Imag_c, receiveChannelImbalanceImag_c,
                      azslope_file_accessor,
                      self.fr_angle)

            hh_fileImage.finalizeImage()
            hv_fileImage.finalizeImage()
            vh_fileImage.finalizeImage()
            vv_fileImage.finalizeImage()
            if self.step_azslope and not self.out_azslope_angle:
                if plant.isfile(azslope_file):
                    remove(azslope_file)
                if plant.isfile(azslope_file + '.vrt'):
                    remove(azslope_file + '.vrt')
                if plant.isfile(azslope_file + '.xml'):
                    remove(azslope_file + '.xml')

            descr = ''
            hhOut_fileImage.setImageType('bil')
            hhOut_fileImage.addDescription(descr)
            hhOut_fileImage.finalizeImage()
            hhOut_fileImage.renderHdr()
            hvOut_fileImage.setImageType('bil')
            hvOut_fileImage.addDescription(descr)
            hvOut_fileImage.finalizeImage()
            hvOut_fileImage.renderHdr()
            vhOut_fileImage.setImageType('bil')
            vhOut_fileImage.addDescription(descr)
            vhOut_fileImage.finalizeImage()
            vhOut_fileImage.renderHdr()
            vvOut_fileImage.setImageType('bil')
            vvOut_fileImage.addDescription(descr)
            vvOut_fileImage.finalizeImage()
            vvOut_fileImage.renderHdr()

        time_diff = (self.output_file +
                     ' --- ' +
                     'polcal time: ' + plant.hms_string(time.time() - time_1))

        plant.execute('echo "' + time_diff + '" > ' +
                      self.output_file + '.txt')

        shutil.copyfile(self.hh_file + '.xml', hhOut_file + '.xml')
        shutil.copyfile(self.hv_file + '.xml', hvOut_file + '.xml')
        shutil.copyfile(self.vh_file + '.xml', vhOut_file + '.xml')
        shutil.copyfile(self.vv_file + '.xml', vvOut_file + '.xml')

        self.hh_file = hhOut_file
        self.hv_file = hvOut_file
        self.vh_file = vhOut_file
        self.vv_file = vvOut_file

    def loop_non_polarimetric_channels(self):

        if (not self.step_abs and not self.step_terrain_correction
            and not ((self.out_intensity and not self.in_intensity) or
                     (self.output_in_dB))):
            return

        for i, current_file in enumerate(self.current_images):
            output_file = self.get_filename(current_file, temp=True)
            self.apply_abs_and_terrain_correction(current_file,
                                                  output_file)

            self.current_images[i] = output_file

        if self.simulate:
            output_file = self.get_filename(self.output_file,
                                            temp=True)
            self.apply_abs_and_terrain_correction(None,
                                                  output_file)
            self.current_images = [output_file]

    def loop_polarimetric_channels(self):

        if (self.number_of_files == 4 and self.step_polcal or
                (self.step_fr and self.fr_angle != 0) or self.step_azslope):
            self.apply_polarimetric_correction(self.transmit, self.receive)

    def get_filename(self, filename_orig, temp=False):

        ret_dict = plant.parse_filename(filename_orig)
        filename = ret_dict['filename']
        timestamp = str(float(time.time()))

        if temp:
            str_ext = '_temp_' + timestamp
        else:
            str_ext = self.output_ext
        if str_ext not in filename or not temp:
            file_temp_splitted = filename.split('.')
            if len(file_temp_splitted) > 1:
                file_temp = '.'.join(file_temp_splitted[:-1])
            else:
                file_temp = file_temp_splitted[0]
            file_temp = file_temp + str_ext
        elif str_ext + '(2)' in filename:
            file_temp = filename.replace(str_ext + '(2)',
                                         str_ext + '(3)')
        else:
            file_temp = filename.replace(str_ext,
                                         str_ext + '(2)')
        if temp:
            plant.append_temporary_file(path.basename(file_temp))
        return path.basename(file_temp)

    def get_output_name(self, output_file, filename_orig, output_dir, pol):

        if (path.isdir(output_file) or
                output_file.endswith('/')):
            output_dir = output_file
            output_file = ''
        if not path.isdir(output_dir):
            makedirs(output_dir)

        if output_dir and not (output_file):
            output_file = path.join(output_dir, path.basename(filename_orig))
        elif (not (output_dir) and output_file and self.number_of_files > 1 and
              pol):
            output_file = output_file + '_' + pol
        elif (not (output_dir) and output_file and self.number_of_files > 1):
            output_file = self.get_filename(output_file)

        return output_file

    def realize_changes(self):

        self.print('INFO output files:')

        for i, current_file in enumerate(self.current_images):
            if self.simulate or len(self.current_images) == 1:
                output_file = self.output_file
            else:
                output_file = self.output_files[i]
                if current_file != self.input_images[i]:
                    plant.rename_image(current_file,
                                       output_file,
                                       verbose=self.verbose,
                                       force=self.force)
                    continue

            plant.plant.copy_image(current_file,
                                   output_file,
                                   verbose=self.verbose,
                                   force=self.force)


def main(argv=None):
    with plant.PlantLogger():
        parser = get_parser()
        self_obj = PlantRadiometricCorrection(parser, argv)
        ret = self_obj.run()
        return ret


if __name__ == '__main__':
    main()
