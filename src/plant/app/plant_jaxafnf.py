#!/usr/bin/env python3
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Gustavo H. X. Shiroma, Marco Lavalle
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import os
import sys
from os import path
from ftplib import FTP
import numpy as np
import plant

CALIBRATION_FACTOR = 83
DEFAULT_STEP_25 = 0.000225
DEFAULT_STEP_100 = 0.0009
DOWNLOAD_DIR_DEFAULT = 'JAXA_FNF'
MAX_N_DOWNLOADS_ATTEMPTS = 1


def get_parser():

    descr = ("Download and mosaic JAXA's FNF maps with a desired --bbox \n"
             'and lat/lon steps. 25m and 100m resolutions are available. \n'
             'For 100m resolution, possible products are: HH, HV, DATE, \n'
             'INC, MASK, FNF. For 25m, only FNF is available.')

    epilog = ('')

    parser = plant.argparse(epilog=epilog,
                            description=descr,

                            input_files=1,
                            geo=1,
                            default_flags=1,
                            default_output_options=1,
                            default_geo_output_options=1,
                            output_file=2,
                            null=1,
                            out_null=1,
                            geo_multilook=1)

    parser.add_argument('-d', '--download',
                        dest='download_dir',
                        type=str,
                        help='Directory containing FNF data'
                        ' (default: jaxafnf).',
                        default='')

    parser.add_argument('--res-025deg',
                        dest='res_025deg',
                        action='store_true',
                        help='1km resolution')

    parser.add_argument('--res-1km',
                        dest='res_1km',
                        action='store_true',
                        help='1km resolution')

    parser.add_argument('--res-100',
                        dest='res_100',
                        action='store_true', required=False,
                        help='100m resolution (only 2007,'
                        ' 2008, 2009, and 2010)',
                        default=False)

    parser.add_argument('-y', '--year', dest='year_str', type=str,
                        required=False, help='Acquisition year. Options: '
                        '2007 (only 25m), 2008, 2009, '
                        '2010 (default for 100m), 2015, 2016,'
                        ' 2017 (default for 25m)',
                        default=None)
    parser.add_argument('-m', '--mode', dest='mode', type=str,
                        required=False, default='FNF',
                        help='Options 25m: FNF, HH, HV, POL. '
                        '100m: FNF, HH, HV, HH_SIGMA, HV_SIGMA, '
                        'DATE, INC, MASK, POL. Where POL '
                        'is HH and HV '
                        '(default: %(default)s)')
    parser.add_argument('--natural',
                        action='store_true',
                        required=False,
                        help='Backscatter in natural unit'
                        '(instead of dB)',
                        default=False)
    return parser


class PlantJaxafnf(plant.PlantScript):

    def __init__(self, parser, argv=None):

        super().__init__(parser, argv,
                         geo_default_step=DEFAULT_STEP_25)
        self.downloads_subdir_list = []
        self.downloads_size_list = []
        self.step_fnf_data = 0.0
        self.step_fnf_data_sub = 0.0
        self.fnf_suffix = ''
        self.fnf_ftp_suffix = ''
        self.fnf_extr_prefix = ''
        self.fnf_extr_suffix = ''
        self.fnf_dtype = np.byte
        self.lon_convention_0_360 = False

        if ((self.lat_arr is None or self.lon_arr is None or
                any(plant.isnan(self.lat_arr)) or
                any(plant.isnan(self.lon_arr))) and
                self.input_files is not None):

            compressed_file_list = [f for f in self.input_files
                                    if f.endswith('.tar.gz')]

            if len(compressed_file_list) > 0:
                lat_min = lat_max = lon_min = lon_max = None
                for f in compressed_file_list:
                    current_lat_min = None
                    current_lat_max = None
                    current_lon_min = None
                    current_lon_max = None
                    f_name = path.basename(str(f))

                    if f_name.startswith('S'):
                        current_lat_max = -int(f_name[1:3])
                    elif f_name.startswith('N'):
                        current_lat_max = int(f_name[1:3])

                    if current_lat_max is not None:
                        current_lat_min = current_lat_max - 5
                    if f_name[3] == 'E':
                        current_lon_min = int(f_name[4:7])
                    elif f_name[3] == 'W':
                        current_lon_min = -int(f_name[4:7])

                    if current_lon_min is not None:
                        current_lon_max = current_lon_min + 5
                    if lat_min is None or current_lat_min < lat_min:
                        lat_min = current_lat_min
                    if lat_max is None or current_lat_max > lat_max:
                        lat_max = current_lat_max
                    if lon_min is None or current_lon_min < lon_min:
                        lon_min = current_lon_min
                    if lon_max is None or current_lon_max > lon_max:
                        lon_max = current_lon_max
                self.lat_arr = [lat_min, lat_max]
                self.lon_arr = [lon_min, lon_max]

                if self.res_100:
                    DEFAULT_STEP = DEFAULT_STEP_100
                else:
                    DEFAULT_STEP = DEFAULT_STEP_25
                if self.step_y is None:
                    self.step_y = DEFAULT_STEP
                if self.step_x is None:
                    self.step_x = DEFAULT_STEP
            uncompressed_file_list = [f for f in self.input_files
                                      if not f.endswith('.tar.gz')]

            plant_grid = plant.PlantGeogrid(
                bbox=self.bbox,
                step=self.step,
                lat_arr=self.lat_arr,
                lon_arr=self.lon_arr,
                bbox_file=uncompressed_file_list,
                verbose=False,
                default_step=None,
                step_x=self.step_x,
                step_y=self.step_y,
                projection=self.output_projection,

                plant_transform_obj=self.plant_transform_obj)

            plant_grid.projection = self.output_projection
            plant_grid.print()

            if plant_grid.projection is not None:
                self.projection = plant_grid.projection

            if (plant_grid.lat_arr is not None and
                    plant_grid.lon_arr is not None):
                self.bbox = plant.get_bbox(plant_grid.lat_arr,
                                           plant_grid.lon_arr)
            if plant_grid.step_y is not None:
                self.step_y = plant_grid.step_y
            if plant_grid.step_x is not None:
                self.step_x = plant_grid.step_x
            if plant_grid.geotransform is not None:
                self.geotransform = plant_grid.geotransform

            self.lat_arr = plant_grid.lat_arr

            self.lon_arr = plant_grid.lon_arr
            if (self.lon_arr is not None and
                plant.isvalid(self.lon_arr[0]) and
                    plant.isvalid(self.lon_arr[1])):
                self.lon_convention_0_360 = np.nansum(
                    np.asarray(self.lon_arr) > 180)

        if (self.lat_arr is None or self.lon_arr is None or
                any(plant.isnan(self.lat_arr)) or
                any(plant.isnan(self.lon_arr))):
            self.print('ERROR bounding box could not be'
                       ' determined')
            return

    def run(self):
        self._check_output_file()
        input_dict = self.__dict__
        mode = self.mode
        modes = mode.split(',')

        new_modes_list = []
        inc_file = None
        flag_sigma = False
        for i, current_mode in enumerate(modes):
            current_mode_upper = current_mode.upper()
            if 'SIGMA' in current_mode_upper:
                new_modes_list.append('INC')
                flag_sigma = True
            if 'POL' not in current_mode_upper:
                new_modes_list.append(current_mode)
                continue
            new_modes_list.append(current_mode.replace('POL', 'HH'))
            new_modes_list.append(current_mode.replace('POL', 'HV'))
        current_band = 0
        for current_mode in new_modes_list:
            self.__dict__.update(input_dict)
            self.mode = current_mode
            temp_file = plant.get_temporary_file(
                suffix=f'_{current_mode}.tif',
                append=True)
            image_obj = self.generate_jaxafnf_product(temp_file, inc_file)
            if current_mode == 'INC' and flag_sigma:
                inc_file = temp_file
                flag_sigma = False
                continue
            if current_band == 0:
                out_image_obj = image_obj
            else:
                out_image_obj.set_band(image_obj.band, band=current_band)
            current_band += 1
        self.save_image(out_image_obj, self.output_file, force=True)

    def _populate_parameters(self):

        self.download_dir = plant.test_download_dir(self.download_dir,
                                                    DOWNLOAD_DIR_DEFAULT)

        self.mode = self.mode.upper()

        if self.year_str is None and not self.res_100:
            self.year = ['17']
        elif self.year_str is None:
            self.year = ['10']
        elif 'ALL' in self.year_str.upper():
            self.year = ['07', '08', '09', '10', '15', '16', '17']
        else:
            self.year = self.year_str.split(',')
            self.year = ['%02d' % (int(year) % 100) for year in self.year]

        if self.res_100 and any([int(year) > 10 for year in self.year]):
            self.print('ERROR 100m resolution are not available for datasets'
                       ' after 2010')
            return

        if (('HH' in self.mode or 'HV' in self.mode) and
                (self.res_100 or self.res_1km)):
            self.print('ERROR low resolution backscatter are not'
                       ' available')
            return

        self.print('year: %s' % (','.join(self.year)))
        if self.res_100:
            self.step_fnf_data = 20.0
            self.step_fnf_data_sub = 10.0
        else:
            self.step_fnf_data = 5.0
            self.step_fnf_data_sub = 1.0

        self.background_value = 0
        if self.res_100:
            self.fnf_suffix = '100'
        elif self.res_1km:
            self.fnf_suffix = '1km'
        elif self.res_025deg:
            self.fnf_suffix = '025deg'
        else:
            self.fnf_suffix = '25'
            self.background_value = 3
            self.fnf_dtype = np.byte

        if 'HH' in self.mode:
            self.fnf_dtype = np.uint16
        elif 'HV' in self.mode:
            self.fnf_dtype = np.uint16
        elif 'DATE' in self.mode:
            self.fnf_dtype = np.uint16
        elif 'INC' in self.mode:
            self.fnf_dtype = np.uint16
        elif 'MASK' in self.mode:
            self.fnf_dtype = np.byte
        elif 'FNF' in self.mode:
            self.fnf_dtype = np.byte
            self.background_value = 1
        else:
            self.print('ERROR mode not available: ' + self.mode)
            return

    def download_subdir(self, arg):

        arg = " ".join(arg.split())
        filename = str.split(arg, ' ')
        if len(filename) == 9:
            self.downloads_subdir_list.append(filename[-1])
            self.downloads_size_list.append(filename[-5])

    def download_jaxafnf_file(self,
                              file_to_download,
                              n_attempts=MAX_N_DOWNLOADS_ATTEMPTS):

        download_dir = self.download_dir
        server = 'ftp.eorc.jaxa.jp'
        year = path.basename(file_to_download).split('_')[1]
        if self.res_1km or self.res_025deg:
            directory = path.join('pub', 'ALOS-2', 'ext1', 'PALSAR-2_MSC',
                                  'FNFlowreso', self.fnf_suffix)
        elif int(year) < 15:
            directory = path.join('pub', 'ALOS', 'ext1', 'PALSAR_MSC',
                                  self.fnf_suffix + 'm_MSC', '20' + year)
        else:
            directory = path.join('pub', 'ALOS-2', 'ext1', 'PALSAR-2_MSC',
                                  self.fnf_suffix + 'm_MSC', '20' + year)
        print(f'connecting to {server} (via FTP)...')
        try:
            ftp = FTP(server)
        except BaseException:
            print(f'ERROR connecting to {server} via FTP.')
            return
        ftp.login()
        ftp.cwd(directory)
        try:
            os.makedirs(path.join(download_dir, self.fnf_suffix))
        except BaseException:
            pass
        current_file = path.basename(file_to_download)
        destination_file = path.join(download_dir, current_file)
        self.downloads_subdir_list = []
        self.downloads_size_list = []
        ftp.retrlines('LIST', self.download_subdir)
        filelist = self.downloads_subdir_list
        if current_file in filelist:
            for i in range(n_attempts):
                if plant.isfile(destination_file):
                    os.remove(destination_file)
                if i == 0:
                    self.print('downloading %s ... ' % current_file, 1)
                else:
                    self.print('downloading (attempt %d) %s ... ' %
                               (i + 1, current_file), 0)
                i += 1
                try:
                    ftp.retrbinary('RETR ' + current_file,
                                   open(destination_file,
                                        'wb').write)
                except BaseException:
                    pass

                plant.extract_to(destination_file,
                                 path.join(download_dir, self.fnf_suffix),
                                 root=self.fnf_extr_suffix[1:],
                                 verbose=self.verbose)

        else:
            self.print('    file ' + current_file +
                       ' not found in remote server')
        try:
            ftp.quit()
        except BaseException:
            pass

    def _set_jaxafnf_parameters(self, year=None):

        if year is None:
            year = 15
        if self.res_100:
            self.fnf_extr_prefix = 'MSC_'
            self.fnf_ftp_suffix = 'FNF100.tar.gz'
        elif not self.res_1km and not self.res_025deg:
            self.fnf_extr_prefix = ''
            if 'FNF' not in self.mode and int(year) < 15:
                self.fnf_ftp_suffix = 'MOS.tar.gz'
            elif 'FNF' not in self.mode:
                self.fnf_ftp_suffix = 'MOS_F02DAR.tar.gz'
            elif int(year) < 15:
                self.fnf_ftp_suffix = 'FNF.tar.gz'
            else:
                self.fnf_ftp_suffix = 'FNF_F02DAR.tar.gz'
        if (self.res_1km or self.res_025deg) and int(year) <= 10:
            self.fnf_ftp_suffix = f'.tar.gz'
        elif self.res_1km or self.res_025deg:
            self.fnf_ftp_suffix = f'F02DAR.tar.gz'

        if 'HH' in self.mode:
            self.fnf_extr_suffix = '_sl_HH'
        elif 'HV' in self.mode:
            self.fnf_extr_suffix = '_sl_HV'
        elif 'DATE' in self.mode:
            self.fnf_extr_suffix = '_date'
        elif 'INC' in self.mode:
            self.fnf_extr_suffix = '_linci'
        elif 'MASK' in self.mode:
            self.fnf_extr_suffix = '_mask'
        elif 'FNF' in self.mode:
            self.fnf_extr_suffix = '_C'
        else:
            self.print('ERROR mode not available: ' + self.mode)
            return

        if self.res_100:
            self.fnf_extr_suffix += '_100m'
        elif self.res_1km:
            self.fnf_extr_suffix += '_1km'
        elif self.res_025deg:
            self.fnf_extr_suffix += '_025deg'
        if int(year) >= 15:
            self.fnf_extr_suffix += '_F02DAR'

    def generate_jaxafnf_product(self, output_file, inc_file=None):

        print('## mode: ', self.mode)
        temporary_files = []
        with plant.PlantIndent():
            self._populate_parameters()

            if len(self.input_files) == 0:

                self._check_output_size()
                output_dir = path.join(self.download_dir, self.fnf_suffix)

                self._download_and_extract_all(
                    output_dir,
                    flag_save_extracted_list=True)
                rad_corrected_file_list = \
                    self._correct_abs_radiometry(output_dir)
            else:
                print('*** ==========================================')
                print('*** input_files:', self.input_files)

                self._set_jaxafnf_parameters()

                file_list = self.input_files
                output_dir = path.dirname(self.output_file)
                if not output_dir:
                    output_dir = os.getcwd()

                user_extracted_file_list = \
                    [f for f in file_list if not f.endswith('tar.gz')]
                if any([f.endswith('tar.gz') for f in file_list]):
                    extracted_file_list = self._download_and_extract_all(
                        output_dir,
                        compressed_file_list=file_list,
                        flag_save_extracted_list=False)
                    extracted_file_list = [f for f in extracted_file_list
                                           if not f.endswith('.hdr')]

                    print('*** extracted_file_list:', extracted_file_list)
                    print(
                        '*** user_extracted_file_list:',
                        user_extracted_file_list)
                    user_extracted_file_list.extend(extracted_file_list)
                    print(
                        '*** extracted_file_list (all):',
                        user_extracted_file_list)
                    temporary_files.extend(extracted_file_list)
                rad_corrected_file_list = \
                    self._correct_abs_radiometry(
                        output_dir,
                        user_extracted_file_list=user_extracted_file_list)
            image_obj = self.generate_output(
                rad_corrected_file_list, output_file, inc_file)
            plant.plant_config.temporary_files.extend(temporary_files)
        return image_obj

    def _check_output_file(self):

        output_dir = path.dirname(self.output_file)
        if not path.isdir(output_dir) and output_dir:
            os.makedirs(output_dir)

        if not plant.isfile(self.output_file):
            return
        while not self.force:
            res = plant.get_keys('The file ' + self.output_file +
                                 ' already exists. Do you want to'
                                 ' overwrite it?'
                                 ' ([y]es/[n]o) ')
            if res.startswith('n'):
                self.print('Operation cancelled.', 1)
                return
            elif res.startswith('y'):
                break

    def _check_output_size(self):
        if self.output_dtype:
            datatype_size = plant.get_dtype_size(self.output_dtype)
        else:
            datatype_size = plant.get_dtype_size(self.fnf_dtype)
        data_size = (self.lon_size * self.lat_size *
                     datatype_size)

        if self.nlooks_lat is None:
            self.nlooks_lat = 1
        if self.nlooks_lon is None:
            self.nlooks_lon = 1

        if data_size > 5e8:
            while not self.force:
                res = plant.get_keys(
                    'WARNING the output file seems to be too'
                    ' large (%s) for step=%f and given coordinates.'
                    'Continue anyway? ([y]es/[n]o) '
                    % (plant.get_file_size_string(data_size), self.step))
                if res.startswith('y'):
                    break
                else:
                    self.print('Operation cancelled.', 1)
                    sys.exit(0)
        else:
            self.print('expected output size: %s' %
                       plant.get_file_size_string(data_size))

    def _download_and_extract_all(self,
                                  output_dir,
                                  compressed_file_list=None,
                                  flag_download=True,
                                  flag_save_extracted_list=True):

        if compressed_file_list is None:
            self.print('downloading and extracting files...')
            compressed_file_list = self._get_compressed_file_list()
        else:
            self.print('extracting files:', compressed_file_list)

        n_compressed_files = len(compressed_file_list)
        self.print(f'number of compressed files: {n_compressed_files}')
        extracted_file_list_all = []
        for i, current_tar_file in enumerate(compressed_file_list):
            if not current_tar_file.endswith('tar.gz'):

                continue

            area = path.basename(current_tar_file).split('_')[0]
            self.print(area + ' (%d/%d)' % (i + 1, n_compressed_files))
            with plant.PlantIndent():
                search_root = self.fnf_extr_suffix[1:]
                print(f'*** search_root:', search_root)
                extracted_file_list = self._extract_single_file(
                    current_tar_file,
                    output_dir,
                    search_root,
                    flag_download=flag_download,
                    flag_save_extracted_list=flag_save_extracted_list)
                extracted_file_list_all.extend(extracted_file_list)

                if len(extracted_file_list) != 0 or 'F02DAR' not in search_root:
                    continue

                search_root = search_root.replace('F02DAR', 'FP6QAR')
                extracted_file_list = self._extract_single_file(
                    current_tar_file,
                    output_dir,
                    search_root,
                    flag_download=flag_download,
                    flag_save_extracted_list=flag_save_extracted_list)
                extracted_file_list_all.extend(extracted_file_list)

        return extracted_file_list_all

    def _correct_abs_radiometry(self, output_dir,
                                user_extracted_file_list=None):

        extracted_file_list = self._get_extracted_file_list(
            output_dir,
            user_extracted_file_list=user_extracted_file_list)
        print('*** extracted_file_list:', extracted_file_list)

        if 'HH' in self.mode or 'HV' in self.mode:
            for i, current_file in enumerate(extracted_file_list):

                if current_file.endswith('_intensity'):
                    continue
                intensity_file = \
                    self._convert_digital_number_to_intensity(current_file)

                extracted_file_list[i] = intensity_file
        n_extracted_files = len(extracted_file_list)

        self.print(f'number of extracted files: {n_extracted_files}')
        if n_extracted_files == 0:
            self.print(
                'ERROR no extracted data was found for the given region '
                'and available FNF files')
            return
        return extracted_file_list

    def _get_compressed_file_list(self):
        compressed_file_list = []
        if self.res_1km or self.res_025deg:

            res_str = '1km' if self.res_1km else '025deg'
            for year in self.year:
                self._set_jaxafnf_parameters(year)
                if int(year) <= 10:
                    alos_number_str = '1'
                else:
                    alos_number_str = '2'
                area = f'PSR{alos_number_str}_{year}_C_{res_str}'
                tar_file = path.join(self.download_dir,
                                     area + '_' +
                                     self.fnf_ftp_suffix)
                compressed_file_list.append(tar_file)
            return compressed_file_list

        start_lat_index = ((self.lat_arr[0] + 90) // self.step_fnf_data)

        end_lat_index = np.ceil((self.lat_arr[1] + 90.0) /
                                self.step_fnf_data)
        if self.lon_convention_0_360:
            start_lon_index = (self.lon_arr[0] // self.step_fnf_data)
            end_lon_index = np.ceil(np.float32(self.lon_arr[1]) /
                                    self.step_fnf_data)
        else:
            start_lon_index = ((self.lon_arr[0] + 180) // self.step_fnf_data)
            end_lon_index = np.ceil(((self.lon_arr[1] + 180.0) /
                                     self.step_fnf_data))
        count = -1
        for year in self.year:
            self._set_jaxafnf_parameters(year)
            for j in range(0, int(end_lat_index - start_lat_index)):
                for i in range(0, int(end_lon_index - start_lon_index)):
                    count += 1
                    current_lat = ((j + start_lat_index + 1) *
                                   self.step_fnf_data - 90)
                    current_lon = ((i + start_lon_index) *
                                   self.step_fnf_data - 180)
                    coordinates_string = plant.get_coordinate_string(
                        lat=current_lat, lon=current_lon)
                    area = coordinates_string + '_' + year
                    current_tar_file = path.join(self.download_dir,
                                                 area + '_' +
                                                 self.fnf_ftp_suffix)
                    compressed_file_list.append(current_tar_file)
        return compressed_file_list

    def _convert_digital_number_to_intensity(self, current_file):
        intensity_file = current_file + '_intensity'
        if (plant.isfile(current_file)):
            self.print(f'file: {current_file} [OK]')
        if (plant.isfile(current_file) and
                not plant.isfile(intensity_file)):
            image_obj = plant.read_image(current_file,
                                         verbose=False)
            image = np.asarray(image_obj.image,
                               dtype=np.float32)
            image = (image**2) / (10**(CALIBRATION_FACTOR / 10))
            image_obj.set_image(image)
            plant.save_image(image_obj, intensity_file, force=True)
            plant.append_temporary_file(current_file)
        if plant.isfile(intensity_file):
            self.print(f'file: {intensity_file} (intensity) [OK]')
            return intensity_file
        self.print(f'ERROR generating intensity file: {intensity_file}')
        return

    def _get_extracted_file_list(self, output_dir,
                                 user_extracted_file_list=None):

        file_list = []
        if self.res_1km or self.res_025deg:
            for year in self.year:
                self._set_jaxafnf_parameters(year)
                if int(year) <= 10:
                    alos_number_str = '1'
                else:
                    alos_number_str = '2'
                area = (f'PSR{alos_number_str}_{year}'
                        f'{self.fnf_extr_suffix}')
                extracted_file = path.join(output_dir, area)
                file_list.append(extracted_file)
            return file_list

        start_lat_index = ((self.lat_arr[0] + 90) // self.step_fnf_data_sub)
        end_lat_index = np.ceil((self.lat_arr[1] + 90.0) /
                                self.step_fnf_data_sub)
        if self.lon_convention_0_360:
            start_lon_index = (self.lon_arr[0] // self.step_fnf_data_sub)
            end_lon_index = np.ceil(np.float32(self.lon_arr[1]) /
                                    self.step_fnf_data_sub)
        else:
            start_lon_index = ((self.lon_arr[0] + 180) //
                               self.step_fnf_data_sub)
            end_lon_index = np.ceil(((self.lon_arr[1] + 180.0) /
                                     self.step_fnf_data_sub))
        self.print('extracted files:')
        print(f'*** user_extracted_file_list:', user_extracted_file_list)
        with plant.PlantIndent():
            for year in self.year:
                self._set_jaxafnf_parameters(year)
                for j in range(0, int(end_lat_index - start_lat_index)):
                    for i in range(0, int(end_lon_index - start_lon_index)):
                        current_lon = ((start_lon_index + i) *
                                       self.step_fnf_data_sub - 180)
                        current_lat = ((start_lat_index + j + 1) *
                                       self.step_fnf_data_sub - 90)
                        coordinates_string = plant.get_coordinate_string(
                            lat=current_lat, lon=current_lon)
                        area = self.fnf_extr_prefix + coordinates_string + \
                            '_' + year + self.fnf_extr_suffix
                        if int(year) >= 19:
                            area += '.tif'

                        print(f'*** area:', area)
                        if user_extracted_file_list:
                            current_file = None
                            for f in user_extracted_file_list:
                                if area not in path.basename(f):

                                    continue
                                current_file = f
                                break
                            if current_file is None and 'F02DAR' in area:
                                area = area.replace('F02DAR', 'FP6QAR')
                                print(f'*** new area:', area)
                                for f in user_extracted_file_list:
                                    if area not in path.basename(f):

                                        continue
                                    current_file = f
                                    break
                            print(f'*** current_file (1):', current_file)
                            if current_file is None:
                                continue
                        else:
                            current_file = path.join(output_dir, area)
                        print(f'*** current_file:', current_file)
                        intensity_file = current_file + '_intensity'
                        if (('HH' in self.mode or 'HV' in self.mode) and
                                plant.isfile(intensity_file)):
                            file_list.append(intensity_file)
                            self.print(f'file: {intensity_file} [OK]')

                        elif (('HH' in self.mode or 'HV' in self.mode) and
                                plant.isfile(intensity_file.replace('F02DAR', 'FP6QAR'))):
                            file_list.append(
                                intensity_file.replace(
                                    'F02DAR', 'FP6QAR'))
                            self.print(f'file: {intensity_file} [OK]')

                        elif plant.isfile(current_file):
                            file_list.append(current_file)
                            self.print(f'file: {current_file} [OK]')

                        elif plant.isfile(current_file.replace('F02DAR', 'FP6QAR')):
                            file_list.append(
                                current_file.replace(
                                    'F02DAR', 'FP6QAR'))
                            self.print(f'file: {current_file} [OK]')
                        else:
                            self.print(f'file: {current_file} [NOT FOUND]')

        return file_list

    def _extract_single_file(self, tar_file, output_dir,
                             search_root,
                             flag_download=True,
                             flag_save_extracted_list=True):

        try:
            year = int(path.basename(tar_file).split('_')[1])
        except BaseException:
            self.print('ERROR extracting year from file name:'
                       f' {tar_file}')

        self._set_jaxafnf_parameters(str(year))

        n_attempts = 0
        res = ''
        extracted_file_list = []
        while (not plant.verify_extraction(
                tar_file, output_dir,
                root=search_root,
                verbose=self.verbose,
                extracted_file_list=extracted_file_list,
                flag_save_extracted_list=flag_save_extracted_list)):
            if plant.isfile(tar_file):
                self.print(f'extracting: {tar_file} to {output_dir}')
                if plant.extract_to(
                        tar_file,
                        output_dir,
                        root=search_root,
                        verbose=self.verbose,
                        extracted_file_list=extracted_file_list,
                        flag_save_extracted_list=flag_save_extracted_list):
                    break
                else:
                    self.print(f'WARNING error extracting: {tar_file}')
            else:
                self.print(f'file not found: {tar_file}')
            if not flag_download:
                return

            if not res and not (plant.plant_config.flag_all or
                                plant.plant_config.flag_never or
                                self.force):
                while (not self.force and
                       (not (res.startswith('A') or
                             res.startswith('N') or
                             res.startswith('y') or
                             res.startswith('n')))):
                    if not plant.isfile(tar_file):
                        self.print(f'WARNING File not found: {tar_file}')
                        res = plant.get_keys(
                            'Would you like to download'
                            ' this file? ([y]es/[n]o/[A]ll/[N]one) ')
                    else:
                        res = plant.get_keys(
                            'Would you like to re-download'
                            ' this file? ([y]es/[n]o/[A]ll/[N]one) ')
            if res.startswith('A'):
                plant.plant_config.flag_all = True
            elif res.startswith('N'):
                plant.plant_config.flag_never = True
            if (res.startswith('y') or
                    plant.plant_config.flag_all or
                    self.force):
                self.download_jaxafnf_file(tar_file)
            elif (res.startswith('n') or
                  plant.plant_config.flag_never or
                  n_attempts + 1 >= MAX_N_DOWNLOADS_ATTEMPTS):
                break
            n_attempts += 1

        return extracted_file_list

    def generate_output(self, file_list, output_file, inc_file=None):
        argv = file_list + ['-o'] + [output_file]
        if 'HH' in self.mode.upper() or 'HV' in self.mode.upper():
            argv += ['--interp'] + ['average']
        argv += ['--gdalwarp-list']
        argv += ['--transition'] + ['0']

        argv += ['--no-average']
        self.execute('plant_mosaic.py ' + ' '.join(argv))
        image_obj = plant.read_image(output_file)
        if image_obj is None:
            self.print(f'ERROR opening {output_file}')
            return
        flag_update_image = (('HH' in self.mode or
                              'HV' in self.mode) and
                             ('SIGMA' in self.mode.upper() or
                              not self.natural))
        if not flag_update_image:
            image_obj.name = self.mode
            return image_obj

        image = np.asarray(image_obj.image,
                           dtype=np.float32)

        pol = 'HH' if 'HH' in self.mode else 'HV'
        if 'SIGMA' in self.mode.upper():
            inc = plant.read_matrix(inc_file)
            image = image * np.cos(np.radians(inc))
            radiometry_str = 'sigma-naught'
        else:
            radiometry_str = 'gamma-naught'
        image = plant.insert_nan(image, np.where(image <= 0), out_null=np.nan)
        if self.natural:
            radiometry_str += ' [natural]'
        else:
            ind = np.where(plant.isvalid(image))
            image[ind] = 10 * np.log10(image[ind])
            radiometry_str += ' [dB]'
        self.print(f'## file saved: {radiometry_str}')
        image_obj.image = image
        image_obj.name = f'{pol} {radiometry_str}'
        return image_obj


def main(argv=None):
    with plant.PlantLogger():
        parser = get_parser()
        self_obj = PlantJaxafnf(parser, argv)
        ret = self_obj.run()
        return ret


if __name__ == '__main__':
    main()
