#!/usr/bin/env python3
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Gustavo H. X. Shiroma, Marco Lavalle
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import sys
import csv
from os import path
import numpy as np
from scipy.optimize import curve_fit
from plant.modules.polygon import Polygon
import plant

FLAG_SAVE_EDGES = False


def get_parser():

    descr = ('Estimate the above-ground biomass (AGB)'
             ' from in-situ field plots.'
             ' The expected inputs are CSV/TDT files'
             " containing tree parameters"
             ' such as tree height, wood density (WD) and'
             ' diameter at breast height (DBH). The'
             ' AGB estimates are calculated from'
             ' chave2014, therefore the stress-factor'
             ' parameter E also need to be provided.')
    epilog = ''
    parser = plant.argparse(epilog=epilog,
                            description=descr,
                            input_files=2,
                            geo=1,
                            output_file=2,
                            output_ext=1,
                            default_flags=1,
                            default_output_options=1,

                            default_lidar=1,
                            step=1,
                            step_y=1,
                            step_x=1,
                            null=1,
                            out_null=1,

                            separator=1,
                            default_vector=1)
    parser.add_argument('--first-line',
                        dest='first_line',
                        type=int,
                        help='First CSV line'
                        ' (starting from 1)',
                        default=1)

    parser.add_argument('--meters-to-deg',
                        dest='meters_to_deg',
                        action='store_true',
                        default=False,
                        help='Convert input from meters to deg (lat/lon)'
                        ' - only available for lat/lon from ref. file')

    parser.add_argument('--agb-per-area',
                        dest='agb_per_area',
                        action='store_true',
                        default=False,
                        help='Output in AGB/area')

    parser.add_argument('--ref',
                        dest='ref_file',
                        type=str,
                        help='Reference CSV file')

    parser.add_argument('-E', '--stress-factor',
                        dest='chave2014_E',
                        type=str,
                        help='Chave2014 Environmental '
                        'stress factor',
                        default=None)

    parser.add_argument('--ref-first-line',
                        dest='ref_file_first_line',
                        type=int,
                        help='Ref. file first CSV line ('
                        'starting from 1)',
                        default=1)

    parser.add_argument('--area-id',
                        dest='area_id',
                        type=str,
                        help='Area ID (if ref. file is provided)',
                        default=None)

    parser.add_argument('--subplot-length',
                        '--subplot-length-m',
                        '--subplot-width',
                        '--subplot-width-m',
                        dest='subplot_size',
                        type=int,
                        help='Subplot length/width in meters')

    parser.add_argument('--subplot-id',
                        dest='subplot_id',
                        type=int,
                        help='Subplot ID')

    parser.add_argument('--subplot-id-list',
                        dest='subplot_id_list',
                        type=int,
                        nargs='*',
                        help='Subplot list')

    parser.add_argument('--footprint',
                        dest='footprint',
                        type=float,
                        help='AGB footprint size'
                        ' (for --image, --gdal-image).'
                        ' Default: %(default)s',
                        default=40e-6)

    parser.add_argument('--lvis',
                        dest='lvis_files',
                        type=str,
                        nargs='*',
                        default=None,
                        help='LVIS CSV file')

    parser.add_argument('--lvis-height',
                        dest='lvis_height_percentile',
                        type=str,
                        default='ZT',
                        help='LVIS height percentile'
                        '(for --lvis mode). Default: %(default)s')

    parser.add_argument('--lvis-footprint',
                        dest='lvis_footprint',
                        type=float,
                        help='LVIS footprint size'
                        '(for --lvis mode).'
                        ' Default: %(default)s',

                        default=200e-6)

    parser.add_argument('--in-null',
                        dest='in_null',
                        type=float,
                        help='Input null value '
                        '(default: %(default)s)',
                        default=-999)

    return parser


def func_chave2014_3(dbh_vect, a, b, c):
    return (np.exp(a + b * np.log(dbh_vect) + c * np.log(dbh_vect)**2))


class PlantAGBSamples(plant.PlantScript):

    def __init__(self, parser, argv=None):

        super().__init__(parser, argv)

        self.lat_0 = [None for t in range(len(self.input_files))]
        self.lat_f_x = [None for t in range(len(self.input_files))]
        self.lat_f_y = [None for t in range(len(self.input_files))]
        self.lat_f_xy = [None for t in range(len(self.input_files))]
        self.lat_x_step = [None for t in range(len(self.input_files))]
        self.lat_y_step = [None for t in range(len(self.input_files))]

        self.lon_0 = [None for t in range(len(self.input_files))]
        self.lon_x_step = [None for t in range(len(self.input_files))]
        self.lon_y_step = [None for t in range(len(self.input_files))]
        self.lon_f_x = [None for t in range(len(self.input_files))]
        self.lon_f_y = [None for t in range(len(self.input_files))]
        self.lon_f_xy = [None for t in range(len(self.input_files))]

        self.n_subplots_y = [None for t in range(len(self.input_files))]
        self.dim_length_x = [np.nan for t in range(len(self.input_files))]
        self.dim_length_y = [np.nan for t in range(len(self.input_files))]

        self.subplot_id_list_all = [self.subplot_id_list
                                    for t in range(len(self.input_files))]

        if self.lvis_files is not None:
            lvis_files = []
            for f in self.lvis_files:
                lvis_files.extend(plant.glob(f))
            self.lvis_files = lvis_files
            if self.lvis_files is not None:
                self.valid_lvis_indexes = [
                    None for t in range(len(self.lvis_files))]

        self.lat_dict = {}
        self.lon_dict = {}

        if not self.output_ext:
            self.output_ext = '.bin'

    def run(self):

        np.seterr(divide='ignore', invalid='ignore', over='ignore')

        if not self.ref_file:
            self.print('ERROR reference file (--ref) not found')
            return
        self.get_coordinates_from_ref_file()
        if self.chave2014_E is not None:
            if plant.isnumeric(self.chave2014_E):
                self.E = np.float64(self.chave2014_E)
            else:
                self.E = plant.read_image(self.chave2014_E)

                plant_geogrid_obj = plant.get_coordinates(
                    bbox_file=self.E,
                    plant_transform_obj=self.plant_transform_obj)
                if plant_geogrid_obj is None:
                    self.E = None
                else:

                    self.E_y0 = plant_geogrid_obj.y0
                    self.E_x0 = plant_geogrid_obj.x0

                    self.E_step_y = plant_geogrid_obj.step_y
                    self.E_step_x = plant_geogrid_obj.step_x
                    self.E_lat_size = plant_geogrid_obj.length
                    self.E_lon_size = plant_geogrid_obj.width
        else:
            self.E = None

        file_id_vect = []
        lat_vect = []
        lon_vect = []
        agb_4_vect = []

        agb_vect = []
        wd_vect = []
        dbh_vect = []
        area_vect = []
        height_vect = []
        crown_diameter_vect = []
        columns_dict = {}

        lines = None
        ret = None
        agb = None
        height = None
        crown_diameter = None
        dbh = None
        wd = None
        area = None
        for file_count, current_file in enumerate(self.input_files):
            self.print(f'processing input file {file_count+1}:'
                       f' {current_file}')

            self.populate_file_info(current_file, file_count)

            self.initiate_columns_dict(columns_dict)
            if not plant.isfile(current_file):
                self.print(f'ERROR file not found {current_file}')
            self.print('opening: ' + current_file)
            with open(current_file, 'r', encoding="ISO-8859-1") as f:
                lines = f.readlines()
                for i, line in enumerate(lines):
                    if i < self.first_line - 1:
                        continue
                    line_splitted = \
                        ['{}'.format(x)
                         for x in list(csv.reader([line],
                                                  delimiter=self.separator,
                                                  quotechar='"'))[0]]
                    if i == self.first_line - 1:
                        self.populate_columns_dict(line_splitted,
                                                   columns_dict)
                        continue
                    ret = self.get_lat_lon(line_splitted,
                                           columns_dict,
                                           file_count)
                    if ret is None:
                        continue
                    lat, lon = ret
                    ret = self.get_forest_parameters(line_splitted,
                                                     columns_dict,
                                                     lat=lat)
                    agb, height, crown_diameter, dbh, wd, area = ret
                    file_id_vect.append(file_count)
                    lat_vect.append(lat)
                    lon_vect.append(lon)
                    agb_vect.append(agb)
                    height_vect.append(height)
                    crown_diameter_vect.append(crown_diameter)
                    dbh_vect.append(dbh)
                    wd_vect.append(wd)
                    area_vect.append(area)
        del lines
        del ret
        del agb
        del height
        del crown_diameter
        del dbh
        del wd
        del area
        if all(plant.isnan(height_vect)):
            if str(self.separator):
                separator_str = f', separator: "{self.separator}"'
            else:
                separator_str = ''
            self.print(f'ERROR no valid samples were found in the'
                       f' input files. Please, check input paramterers.'
                       f' first line: {self.first_line}{separator_str}')

            return
        file_id_vect = np.asarray(file_id_vect)
        lat_vect = np.asarray(lat_vect)
        lon_vect = np.asarray(lon_vect)
        agb_vect = np.asarray(agb_vect)
        height_vect = np.asarray(height_vect)
        tree_crown_factor = 1.0
        crown_diameter_vect = np.asarray(
            crown_diameter_vect) * tree_crown_factor
        dbh_vect = np.asarray(dbh_vect)
        wd_vect = np.asarray(wd_vect)
        area_vect = np.asarray(area_vect)
        valid_ind = np.where(np.logical_and(
            plant.isvalid(height_vect),
            np.logical_and(plant.isvalid(dbh_vect),
                           dbh_vect != 0)))
        try:
            function_fit, pcov = curve_fit(func_chave2014_3,
                                           dbh_vect[valid_ind],
                                           height_vect[valid_ind])
        except TypeError:

            print('WARNING there was an error while fitting the AGB model'
                  ' chave2014 (3)')
            function_fit = None
        if function_fit is None:
            new_height_vect = None
        else:
            new_height_vect = func_chave2014_3(dbh_vect,
                                               function_fit[0],
                                               function_fit[1],
                                               function_fit[2])
            new_height_vect[valid_ind] = height_vect[valid_ind]

        if (any(plant.isvalid(wd_vect)) or
                any(plant.isvalid(dbh_vect)) or
                any(plant.isvalid(height_vect))):
            agb_4_vect = 0.0673 * (wd_vect * (dbh_vect**2) *
                                   height_vect)**0.976
            ind_invalid = np.where(plant.isnan(agb_vect))
            agb_vect[ind_invalid] = agb_4_vect[ind_invalid]
        else:
            agb_4_vect = None

        output_file = (self.output_file + '_tree_height_field' +
                       self.output_ext)
        self.save_vector(height_vect,
                         output_file,
                         lat_vect,
                         lon_vect,
                         footprint_array=crown_diameter_vect)
        del height_vect

        if (new_height_vect is not None and
            (any(plant.isvalid(wd_vect)) or
             any(plant.isvalid(dbh_vect)) or
             any(plant.isvalid(new_height_vect)))):
            agb_4n_vect = 0.0673 * (wd_vect * (dbh_vect**2) *
                                    new_height_vect)**0.976
            ind_invalid = np.where(plant.isnan(agb_vect))
            agb_vect[ind_invalid] = agb_4n_vect[ind_invalid]
        else:
            agb_4n_vect = None

        if (any(plant.isnan(agb_vect)) and
                (any(plant.isvalid(wd_vect)) or
                 any(plant.isvalid(dbh_vect)))):
            ind_invalid = np.where(plant.isnan(agb_vect))
            if self.E is None:
                E = 2
                self.print('WARNING chave2014 (7) method was used and '
                           ' the environmental stress factor was not '
                           ' provided. Using default value: ' + str(E))
            elif plant.isnumeric(self.E):
                E = self.E
            else:
                lat_ind = [(self.E.image.shape[0] -
                            ((lat_vect[i] - self.E_y0) /
                             self.E_step_y)) for i in ind_invalid]
                lon_ind = [((lon_vect[i] - self.E_x0) /
                            self.E_step_x) for i in ind_invalid]
                lat_ind = np.asarray(lat_ind, dtype=int).ravel()
                lon_ind = np.asarray(lon_ind, dtype=int).ravel()
                E = []
                for i, ind in enumerate(zip(lat_ind, lon_ind)):
                    E.append(self.E.image[ind])
                E = np.asarray(E)

            agb_vect[ind_invalid] = np.exp(
                -1.803 - 0.976 * E +
                0.976 * np.log(wd_vect[ind_invalid]) +
                2.673 * np.log(dbh_vect[ind_invalid]) -
                0.0299 * (np.log(dbh_vect[ind_invalid])**2))
            del ind_invalid
            del E
            del self.E

        agb_vect /= 1e3

        if self.lvis_files is not None:

            if self.plant_geogrid_obj is not None:

                self.step_y = self.plant_geogrid_obj.step_y
                self.step_x = self.plant_geogrid_obj.step_x
                self.lat_size = self.plant_geogrid_obj.length
                self.lon_size = self.plant_geogrid_obj.width

            self.get_edges()
            self.save_as_seen_by_lvis(agb_vect,
                                      new_height_vect,
                                      wd_vect,
                                      lat_vect,
                                      lon_vect,
                                      crown_diameter_vect,

                                      str(self.lvis_height_percentile),
                                      calculate_rmse=False)
        crown_diameter_vect = np.asarray(crown_diameter_vect)
        output_file = (self.output_file + '_tree_height_field_estimated' +
                       self.output_ext)
        if new_height_vect is not None:
            self.save_vector(new_height_vect,
                             output_file,
                             lat_vect,
                             lon_vect,
                             footprint_array=crown_diameter_vect)
            output_file = (self.output_file + '_agb_field' +
                           self.output_ext)
        self.save_vector(agb_vect,
                         output_file,
                         lat_vect,
                         lon_vect,
                         footprint_array=crown_diameter_vect)

        if len(self.input_files) == 1 and FLAG_SAVE_EDGES:
            file_count = 0
            edges_lat = [self.lat_0[file_count],
                         self.lat_f_y[file_count],
                         self.lat_f_xy[file_count],
                         self.lat_f_x[file_count]]
            edges_lon = [self.lon_0[file_count],
                         self.lon_f_y[file_count],
                         self.lon_f_xy[file_count],
                         self.lon_f_x[file_count]]

            edges_obj = plant.PlantImage(edges_lat)
            edges_obj.set_image(edges_lon, band=1)
            output_file = (self.output_file + '_edges' +
                           self.output_ext)
            self.save_image(edges_obj, output_file)

        agb_radar_vect = np.zeros((len(self.input_files)))

        for file_count, current_file in enumerate(self.input_files):
            ind = np.where(file_id_vect == file_count)
            current_agb = np.nansum(agb_vect[ind])
            if (plant.isvalid(self.dim_length_x[file_count]) and
                    plant.isvalid(self.dim_length_y[file_count])):

                print('*** **********************')

                print('*** self.n_subplots_y: ',
                      self.n_subplots_y[file_count])

                SUBPLOT_ID_HALF_HA_HALF_AREA_LIST = [3, 4, 9, 10, 15]
                if self.subplot_id_list_all[file_count] is not None:
                    area = 0
                    for subplot_id in self.subplot_id_list_all[file_count]:
                        subplot_area = 0
                        if (self.n_subplots_y[file_count] == 3 and
                            subplot_id
                                in SUBPLOT_ID_HALF_HA_HALF_AREA_LIST):
                            subplot_area += 1.0 / 50.0
                        else:
                            subplot_area += 1.0 / 25.0
                        print(f'*** subplot ID {subplot_id} area: '
                              f'{subplot_area}')
                        area += subplot_area
                else:
                    area = (self.dim_length_x[file_count] *
                            self.dim_length_y[file_count] / 1e4)

                agb_radar_vect[file_count] = current_agb / area
                self.print('AGB (%s): %.1f Mg/ha (#samples: %d, '
                           'AGB: %.1f Mg, area: %.4f ha)'
                           % (path.basename(current_file),
                              current_agb / area,
                              ind[0].shape[0],
                              current_agb,
                              area))

                print('*** **********************')

            else:
                agb_radar_vect[file_count] = current_agb
                self.print('AGB (%s): %.1f Mg/ha (#samples: %d, '
                           'AGB: %.1f Mg)'
                           % (path.basename(current_file),
                              current_agb,
                              ind[0].shape[0],
                              current_agb))

        if (plant.isvalid(self.dim_length_x[file_count]) and
                plant.isvalid(self.dim_length_y[file_count])):
            output_file = (self.output_file + '_agb_plot_ha' +
                           self.output_ext)
            self.save_vector(agb_radar_vect,
                             output_file,
                             save_as_text=False,
                             save_as_raster=False,
                             save_as_raster_gdal=False,
                             save_as_vector=True)
        else:
            output_file = (self.output_file + '_agb_plot' +
                           self.output_ext)
            self.save_vector(agb_radar_vect,
                             output_file,
                             save_as_text=False,
                             save_as_raster=False,
                             save_as_raster_gdal=False,
                             save_as_vector=True)

    def get_edges(self):
        for file_count in range(len(self.input_files)):
            p1 = np.asarray([self.lon_0[file_count], self.lat_0[file_count]])
            p2 = np.asarray([self.lon_f_x[file_count],
                             self.lat_f_x[file_count]])
            p3 = np.asarray([self.lon_f_xy[file_count],
                             self.lat_f_xy[file_count]])
            p4 = np.asarray([self.lon_f_y[file_count],
                             self.lat_f_y[file_count]])

            current_polygon = Polygon([p1, p2, p3, p4])

            if file_count == 0:
                min_lon = np.min(current_polygon.x)
                max_lon = np.max(current_polygon.x)
                min_lat = np.min(current_polygon.y)
                max_lat = np.max(current_polygon.y)
            else:
                min_lon = min([min_lon, np.min(current_polygon.x)])
                max_lon = min([max_lon, np.max(current_polygon.x)])
                min_lat = min([min_lat, np.min(current_polygon.y)])
                max_lat = min([max_lat, np.max(current_polygon.y)])

        delta_lat, delta_lon = plant.m_to_deg(
            100, lat=np.nanmean((min_lat + max_lat) / 2))

        if plant.isnan(self.y0):
            self.y0 = max_lat - delta_lat
        if plant.isnan(self.yf):
            self.yf = min_lat = delta_lat
        if plant.isnan(self.x0):
            self.x0 = min_lon - delta_lon
        if plant.isnan(self.xf):
            self.xf = max_lon + delta_lon

    def initiate_columns_dict(self, columns_dict):
        columns_dict['lat'] = None
        columns_dict['lon'] = None
        columns_dict['subplot_id'] = None
        columns_dict['x'] = None
        columns_dict['y'] = None
        columns_dict['dbh'] = None
        columns_dict['wd'] = None
        columns_dict['height'] = None
        columns_dict['cn'] = None
        columns_dict['agb'] = None
        columns_dict['area'] = None
        columns_dict['area_id'] = None

    def populate_columns_dict(self, line_splitted, columns_dict):
        if len(line_splitted) <= 1:
            self.print('ERROR separating line ' +
                       str(self.ref_file_first_line) +
                       ' using input separator')
            return
        for j, element in enumerate(line_splitted):
            if ('lat' in element.lower() and
                    columns_dict['lat'] is None):
                columns_dict['lat'] = j
                self.print(f'latitude in column: {j}' +
                           f'   field: {element}')
            elif ('lon' in element.lower() and
                    columns_dict['lon'] is None):
                self.print(f'longitude in column: {j}' +
                           f'   field: {element}')
                columns_dict['lon'] = j
            elif ('t1' == element.lower() and
                  columns_dict['subplot_id'] is None):
                columns_dict['subplot_id'] = j
                self.print(f't1 (subplot) in column: {j}' +
                           f'   field: {element}')
            elif ('x' == element.lower() and
                    columns_dict['x'] is None):
                columns_dict['x'] = j
                self.print(f'x in column: {j}' +
                           f'   field: {element}')
            elif ('y' == element.lower() and
                    columns_dict['y'] is None):
                self.print(f'y in column: {j}' +
                           f'   field: {element}')
                columns_dict['y'] = j
            elif ('dbh' in element.lower() and
                    columns_dict['dbh'] is None):
                self.print(f'DBH in column: {j}' +
                           f'   field: {element}')
                columns_dict['dbh'] = j
            elif ('wd' in element.lower() and
                    columns_dict['wd'] is None):
                self.print(f'WD in column: {j}' +
                           f'   field: {element}')
                columns_dict['wd'] = j
            elif ('height' in element.lower() and
                    columns_dict['height'] is None):
                self.print(f'height in column: {j}' +
                           f'   field: {element}')
                columns_dict['height'] = j
            elif ('census notes' in element.lower() and
                    columns_dict['cn'] is None):
                self.print(f'census notes in column: {j}' +
                           f'   field: {element}')
                columns_dict['cn'] = j
            elif (('Kd ' in element or
                    'Total P' in element or

                    'agb' in element.lower()) and
                    columns_dict['agb'] is None):
                self.print(f'agb in column: {j}' +
                           f'   field: {element}')
                columns_dict['agb'] = j
            elif ('area' in element.lower() and
                    columns_dict['area'] is None):
                self.print(f'area in column: {j}' +
                           f'   field: {element}')
                columns_dict['area'] = j
            elif ('plotviewname' in element.lower() and
                    columns_dict['area_id'] is None):
                self.print(f'area_id in column: {j}')
                columns_dict['area_id'] = j

    def get_lat_lon(self, line_splitted, columns_dict, file_count):
        if (columns_dict['subplot_id'] is not None):
            t1 = float(line_splitted[columns_dict['subplot_id']])
            if (self.subplot_id_list_all[file_count] is not None and
                    t1 not in self.subplot_id_list_all[file_count]):

                return
            t1_offset_x_temp = (np.floor((t1 - 1) /
                                         self.n_subplots_y[file_count]))
            t1_offset_x = t1_offset_x_temp * 20
            if np.mod(t1_offset_x_temp, 2) == 0:
                t1_offset_y = np.mod((t1 - 1),
                                     self.n_subplots_y[file_count]) * 20
            else:
                t1_offset_y = (self.n_subplots_y[file_count] - 1 -
                               np.mod((t1 - 1),
                                      self.n_subplots_y[file_count])) * 20
            t1_offset_y = int(t1_offset_y)

        elif self.subplot_id_list_all[file_count] is not None:
            self.print('*** subplot_id not found, columns_dict: ',
                       columns_dict)
            self.print('ERROR the subplot ID parameter was set with'
                       f' value "{self.subplot_id}", but the '
                       ' respective fields "t1" or "T1" were not found'
                       ' in the input file.')
            return
        else:
            t1_offset_x = 0
            t1_offset_y = 0
        if (columns_dict['x'] is not None and
                columns_dict['y'] is not None):
            try:
                x = float(line_splitted[columns_dict['x']])
            except BaseException:
                return
            try:
                y = float(line_splitted[columns_dict['y']])
            except BaseException:
                return
            lat = (self.lat_0[file_count] +
                   (x + t1_offset_x) * self.lat_x_step[file_count] +
                   (y + t1_offset_y) * self.lat_y_step[file_count])
            lon = (self.lon_0[file_count] +
                   (x + t1_offset_x) * self.lon_x_step[file_count] +
                   (y + t1_offset_y) * self.lon_y_step[file_count])
        elif (columns_dict['lat'] is not None and
                columns_dict['lon'] is not None):
            try:
                lat = float(line_splitted[columns_dict['lat']])
            except BaseException:
                return
            try:
                lon = float(line_splitted[columns_dict['lon']])
            except BaseException:
                return
        elif (columns_dict['area_id'] is not None):

            try:
                area_id = line_splitted[columns_dict['area_id']]
            except BaseException:
                pass
            lat = self.lat_dict[area_id]
            lon = self.lon_dict[area_id]
        else:
            return
        return lat, lon

    def get_forest_parameters(self, line_splitted, columns_dict,
                              lat=0):
        agb = np.nan
        height = np.nan
        crown_diameter = self.footprint
        dbh = np.nan
        wd = np.nan
        area = np.nan
        if columns_dict['agb'] is not None:
            try:
                agb = float(line_splitted[columns_dict['agb']])
            except BaseException:
                pass
        if columns_dict['height'] is not None:
            try:
                height = float(line_splitted[columns_dict['height']])
            except BaseException:
                pass
        if columns_dict['cn'] is not None:
            dx = np.nan
            dy = np.nan
            cn = line_splitted[columns_dict['cn']].replace('/n', '').lower()
            if ('dx' in cn and 'dy' in cn):
                cn_splitted = cn.split(';')
                for cn in cn_splitted:
                    if 'dx' in cn:
                        try:
                            dx = float(cn.split('=')[1])
                        except BaseException:
                            pass
                    if 'dy' in cn:
                        try:
                            dy = float(cn.split('=')[1])
                        except BaseException:
                            pass
                if any(plant.isvalid([dx, dy])):
                    crown_diameter = np.mean(
                        plant.m_to_deg(np.nanmean([dx, dy]),
                                       lat=lat))
        if columns_dict['dbh'] is not None:
            try:
                dbh = float(line_splitted[columns_dict['dbh']])
                dbh /= 10

            except BaseException:
                pass
        if columns_dict['wd'] is not None:
            try:
                wd = float(line_splitted[columns_dict['wd']])
            except BaseException:
                pass
        if (columns_dict['area'] is not None and
                self.agb_per_area):
            try:
                area = float(line_splitted[columns_dict['area']])
            except BaseException:
                pass
        return (agb, height, crown_diameter, dbh, wd, area)

    def populate_file_info(self, current_file, file_count):

        if self.area_id is None:
            area_id = (path.basename(current_file))[0:6].replace('_', '-')
        else:
            area_id = self.area_id
        if area_id + ' "100,0m"' in self.lat_dict.keys():
            self.dim_length_x[file_count] = 100
        elif area_id + ' "50,0m"' in self.lat_dict.keys():
            self.dim_length_x[file_count] = 50
        else:
            self.dim_length_x[file_count] = None
        if area_id + ' "0,100m"' in self.lat_dict.keys():
            self.dim_length_y[file_count] = 100
        elif area_id + ' "0,50m"' in self.lat_dict.keys():
            self.dim_length_y[file_count] = 50
        else:
            self.dim_length_y[file_count] = None
        if self.dim_length_y[file_count] is not None:
            self.n_subplots_y[file_count] = int(np.ceil(
                float(self.dim_length_y[file_count]) / 20))

        if ((self.subplot_size is None or self.subplot_size == 20) and
            self.subplot_id is not None and
                self.subplot_id_list_all[file_count] is None):
            self.subplot_id_list_all[file_count] = [self.subplot_id]
        elif (self.subplot_size is not None and
              self.subplot_id is not None):
            subplot_width = self.subplot_size // 20
            n_subplots_y = self.n_subplots_y[file_count]
            print('*** subplot_width: ', subplot_width)
            print('*** n_subplots_y: ', n_subplots_y)

            subplot_array = np.arange(1, n_subplots_y * 5 + 1)
            subplot_array = subplot_array.reshape(5, n_subplots_y)
            subplot_array = subplot_array.transpose()
            for i in range(0, 5, 2):
                subplot_array[:, i] = subplot_array[::-1, i]
            print('*** original plot array:')
            print(subplot_array)
            index_array = subplot_array[subplot_width - 1:n_subplots_y,
                                        0:5 + 1 - subplot_width].copy()
            for i in range(1, 5 + 1 - subplot_width, subplot_width):

                index_array[:, i:] = (index_array[:, i:] +
                                      index_array[0, i - 1] -
                                      index_array[0, i] + 1)
            print('*** subplot indexes array:')
            print(index_array)
            if self.subplot_id > np.max(index_array):
                print('*** ERROR subplot ID greater than number of subplots')

            ind = np.where(index_array == self.subplot_id)
            subplot_id_list = []
            for i in range(subplot_width):
                for j in range(subplot_width):
                    subplot_id = int(subplot_array[ind[0] + i, ind[1] + j])
                    subplot_id_list.append(subplot_id)
            self.subplot_id_list_all[file_count] = sorted(subplot_id_list)
            print(f'*** self.subplot_id_list_all[{file_count}]: ',
                  self.subplot_id_list_all[file_count])

        if not self.meters_to_deg:
            return

        self.lat_0[file_count] = self.lat_dict[area_id + ' "0,0m"']
        self.lon_0[file_count] = self.lon_dict[area_id + ' "0,0m"']

        if area_id + ' "100,0m"' in self.lat_dict.keys():
            self.lat_f_x[file_count] = self.lat_dict[area_id + \
                ' "' + str(self.dim_length_x[file_count]) + ',0m"']
            self.lon_f_x[file_count] = self.lon_dict[area_id + \
                ' "' + str(self.dim_length_x[file_count]) + ',0m"']
            self.lat_x_step[file_count] = ((self.lat_f_x[file_count] -
                                            self.lat_0[file_count]) /
                                           self.dim_length_x[file_count])
            self.lon_x_step[file_count] = ((self.lon_f_x[file_count] -
                                            self.lon_0[file_count]) /
                                           self.dim_length_x[file_count])
        elif area_id + ' "50,0m"' in self.lat_dict.keys():
            self.lat_f_x[file_count] = self.lat_dict[area_id + \
                ' "' + str(self.dim_length_x[file_count]) + ',0m"']
            self.lon_f_x[file_count] = self.lon_dict[area_id + \
                ' "' + str(self.dim_length_x[file_count]) + ',0m"']
            self.lat_x_step[file_count] = ((self.lat_f_x[file_count] -
                                            self.lat_0[file_count]) /
                                           self.dim_length_x[file_count])
            self.lon_x_step[file_count] = ((self.lon_f_x[file_count] -
                                            self.lon_0[file_count]) /
                                           self.dim_length_x[file_count])
        else:
            self.print('ERROR lat/lon not found for X in: ' +
                       str(self.ref_file))
            sys.error(1)
        if area_id + ' "0,100m"' in self.lat_dict.keys():
            self.lat_f_y[file_count] = \
                self.lat_dict[area_id + ' "0,' +
                              str(self.dim_length_y[file_count]) + 'm"']
            self.lon_f_y[file_count] = \
                self.lon_dict[area_id + ' "0,' +
                              str(self.dim_length_y[file_count]) + 'm"']
            self.lat_y_step[file_count] = ((self.lat_f_y[file_count] -
                                            self.lat_0[file_count]) /
                                           self.dim_length_y[file_count])
            self.lon_y_step[file_count] = ((self.lon_f_y[file_count] -
                                            self.lon_0[file_count]) /
                                           self.dim_length_y[file_count])
        elif area_id + ' "0,50m"' in self.lat_dict.keys():
            self.lat_f_y[file_count] = \
                self.lat_dict[area_id + ' "0,' +
                              str(self.dim_length_y[file_count]) + 'm"']
            self.lon_f_y[file_count] = \
                self.lon_dict[area_id + ' "0,' +
                              str(self.dim_length_y[file_count]) + 'm"']
            self.lat_y_step[file_count] = ((self.lat_f_y[file_count] -
                                            self.lat_0[file_count]) /
                                           self.dim_length_y[file_count])
            self.lon_y_step[file_count] = ((self.lon_f_y[file_count] -
                                            self.lon_0[file_count]) /
                                           self.dim_length_y[file_count])
        else:
            self.print('ERROR lat/lon not found for Y in: ' +
                       str(self.ref_file))
            return

        if self.lvis_files is None:
            return

        if len(self.lvis_files) >= 1:
            area_id_xy = (area_id + ' "' +
                          str(self.dim_length_x[file_count]) + ',' +
                          str(self.dim_length_y[file_count]) + 'm"')
            if area_id_xy in self.lat_dict.keys():
                self.dim_length = 100
                self.lat_f_xy[file_count] = self.lat_dict[area_id_xy]
                self.lon_f_xy[file_count] = self.lon_dict[area_id_xy]
            else:
                self.print('ERROR lat/lon not found for XY in: ' +
                           str(self.ref_file))
                return
        self.print('meters to lat/lon transformation:')
        self.print('    start lat,lon: %f,%f'
                   % (self.lat_0[file_count], self.lon_0[file_count]))
        self.print('    step x lat,lon: %f,%f'
                   % (self.lat_x_step[file_count],
                      self.lon_x_step[file_count]))
        self.print('    step y lat,lon: %f,%f'
                   % (self.lat_y_step[file_count],
                      self.lon_y_step[file_count]))

    def save_as_seen_by_lvis(self,
                             agb_vect,
                             height_vect,
                             wd_vect,
                             lat_vect,
                             lon_vect,
                             crown_diameter_vect,
                             lvis_height,
                             calculate_rmse=False):

        self.print('-----------------------------------------', 0)
        self.print('AGB vs LVIS')
        self.lvis_separator = ' '
        lvis_footprint = self.lvis_footprint

        height_vect_lvis = []
        lat_vect_lvis = []
        lon_vect_lvis = []
        valid_files_count = 0

        if self.step_y is None and self.footprint is not None:
            if plant.isvalid(self.footprint) and self.footprint != 0:
                self.step_y = self.footprint / 2
        if self.step_x is None and self.footprint is not None:
            if plant.isvalid(self.footprint) and self.footprint != 0:
                self.step_x = self.footprint / 2

        for i, current_file in enumerate(self.lvis_files):
            self.print(f'processing LVIS file {i+1}/{len(self.lvis_files)}:'
                       f' {current_file}')

            ret = plant.read_csv_data_lat_lon(current_file,
                                              lat_text='TLAT',
                                              lon_text='TLON',
                                              first_line=self.first_line,
                                              data_text_list=[lvis_height],
                                              separator=self.lvis_separator,
                                              verbose=False)
            if ret is None:
                continue
            height_vect_t_nan, lat_vect_t_nan, lon_vect_t_nan = ret
            height_vect_t = []
            lat_vect_t = []
            lon_vect_t = []

            for j, x in enumerate(height_vect_t_nan):
                if plant.isvalid(x, self.in_null):
                    height_vect_t.append(height_vect_t_nan[j])
                    lat_vect_t.append(lat_vect_t_nan[j])
                    lon_vect_t.append(lon_vect_t_nan[j])
            height_vect_t = np.asarray(height_vect_t)
            lat_vect_t = np.asarray(lat_vect_t)
            lon_vect_t = np.asarray(lon_vect_t)

            if self.valid_lvis_indexes[i] is None:
                self.valid_lvis_indexes[i] = self.get_overlapping_indexes(
                    lat_vect_t, lon_vect_t, self.lvis_footprint,
                    verbose=self.verbose)
            if self.valid_lvis_indexes[i] == 0:
                continue
            height_vect_t = height_vect_t[self.valid_lvis_indexes[i]]
            lat_vect_t = lat_vect_t[self.valid_lvis_indexes[i]]
            lon_vect_t = lon_vect_t[self.valid_lvis_indexes[i]]
            height_vect_lvis += height_vect_t.tolist()
            lat_vect_lvis += lat_vect_t.tolist()
            lon_vect_lvis += lon_vect_t.tolist()
            valid_files_count += 1
            self.print('    # points: ' +
                       str(self.valid_lvis_indexes[i][0].shape[0]), 0)
        if valid_files_count == 0:
            self.print('-------------------------------------', 0)
            self.print('WARNING. No valid points were found for '
                       'input files.', 1)
            sys.exit(0)

        self.print('-------------------------------------', 0)
        self.print('number of files with valid data: %d' %
                   valid_files_count, 1)
        self.print('# total points: %d' %
                   len(height_vect_lvis), 1)
        self.print('-------------------------------------', 0)

        lat_vect_lvis = np.asarray(lat_vect_lvis)
        lon_vect_lvis = np.asarray(lon_vect_lvis)
        height_vect_lvis = np.asarray(height_vect_lvis)

        if lvis_footprint == 0:
            lvis_footprint_vect = np.arange(1e-6, 400e-6, 1e-6)
            r2_vect_1 = np.zeros((lvis_footprint_vect.shape[0]))
            rmse_vect_1 = np.zeros((lvis_footprint_vect.shape[0]))
            min_rmse_1 = 9999
            min_i_1 = None
            r2_vect_2 = np.zeros((lvis_footprint_vect.shape[0]))
            rmse_vect_2 = np.zeros((lvis_footprint_vect.shape[0]))
            min_rmse_2 = 9999
            min_i_2 = None
            r2_vect_3 = np.zeros((lvis_footprint_vect.shape[0]))
            rmse_vect_3 = np.zeros((lvis_footprint_vect.shape[0]))
            min_rmse_3 = 9999
            min_i_3 = None
            for i in range(lvis_footprint_vect.shape[0]):
                valid_lvis_indexes = self.get_overlapping_indexes(
                    lat_vect_lvis, lon_vect_lvis, lvis_footprint_vect[i],
                    verbose=False)
                if valid_lvis_indexes is None:
                    continue
                height_vect_lvis_temp = height_vect_lvis[valid_lvis_indexes]
                lat_vect_lvis_temp = lat_vect_lvis[valid_lvis_indexes]
                lon_vect_lvis_temp = lon_vect_lvis[valid_lvis_indexes]
                ret = self.join_data(lat_vect_lvis_temp,
                                     lon_vect_lvis_temp,
                                     lat_vect,
                                     lon_vect,
                                     agb_vect,
                                     height_vect,
                                     wd_vect,
                                     crown_diameter_vect,
                                     lvis_footprint_vect[i])
                agb_sum, agb_max, height_avg, height_loreys, height_max = ret
                current_r2, current_rmse = plant.calculate_r2_rmse(
                    height_vect_lvis_temp, height_avg, verbose=False)
                if current_rmse < min_rmse_1:
                    min_rmse_1 = current_rmse
                    min_i_1 = i
                r2_vect_1[i] = current_r2
                rmse_vect_1[i] = current_rmse
                current_r2, current_rmse = plant.calculate_r2_rmse(
                    height_vect_lvis_temp, height_loreys, verbose=False)
                if current_rmse < min_rmse_2:
                    min_rmse_2 = current_rmse
                    min_i_2 = i
                r2_vect_2[i] = current_r2
                rmse_vect_2[i] = current_rmse
                current_r2, current_rmse = plant.calculate_r2_rmse(
                    height_vect_lvis_temp, height_max, verbose=False)
                if current_rmse < min_rmse_3:
                    min_rmse_3 = current_rmse
                    min_i_3 = i
                r2_vect_3[i] = current_r2
                rmse_vect_3[i] = current_rmse
            print('%s minimum RMSE [avg]: footprint=%f, r2=%f, RMSE=%f'
                  % (lvis_height, lvis_footprint_vect[min_i_1],
                     r2_vect_1[min_i_1], rmse_vect_1[min_i_1]))
            print("%s minimum RMSE [Lorey's]: footprint=%f, r2=%f, RMSE=%f"
                  % (lvis_height, lvis_footprint_vect[min_i_2],
                     r2_vect_2[min_i_2], rmse_vect_2[min_i_2]))
            print('%s minimum RMSE [max]: footprint=%f, r2=%f, RMSE=%f'
                  % (lvis_height, lvis_footprint_vect[min_i_3],
                     r2_vect_3[min_i_3], rmse_vect_3[min_i_3]))
            lvis_footprint = lvis_footprint_vect[min_i_3]

        ret = self.join_data(lat_vect_lvis,
                             lon_vect_lvis,
                             lat_vect,
                             lon_vect,
                             agb_vect,
                             height_vect,
                             wd_vect,
                             crown_diameter_vect,
                             lvis_footprint)

        agb_sum, agb_max, height_avg, height_loreys, height_max = ret

        if calculate_rmse:
            __, rmse_avg = plant.calculate_r2_rmse(height_vect_lvis,
                                                   height_avg,
                                                   verbose=False)
            __, rmse_loreys = plant.calculate_r2_rmse(height_vect_lvis,
                                                      height_loreys,
                                                      verbose=False)
            __, rmse_max = plant.calculate_r2_rmse(height_vect_lvis,
                                                   height_max,
                                                   verbose=False)
            return (rmse_avg, rmse_loreys, rmse_max)

        output_file = (self.output_file + '_agb_sum' +
                       self.output_ext)
        self.save_vector(agb_sum,
                         output_file,
                         lat_vect_lvis,
                         lon_vect_lvis,
                         footprint=lvis_footprint)
        output_file = self.output_file + '_agb_max' + self.output_ext
        self.save_vector(agb_max,
                         output_file,
                         lat_vect_lvis,
                         lon_vect_lvis,
                         footprint=lvis_footprint)

        output_file = (self.output_file + '_tree_height_field_avg' +
                       self.output_ext)
        self.save_vector(height_avg,
                         output_file,
                         lat_vect_lvis,
                         lon_vect_lvis,
                         footprint=lvis_footprint)

        output_file = (self.output_file + '_tree_height_field_loreys' +
                       self.output_ext)
        self.save_vector(height_loreys,
                         output_file,
                         lat_vect_lvis,
                         lon_vect_lvis,
                         footprint=lvis_footprint)
        output_file = (self.output_file + '_tree_height_field_max' +
                       self.output_ext)
        self.save_vector(height_max,
                         output_file,
                         lat_vect_lvis,
                         lon_vect_lvis,
                         footprint=lvis_footprint)
        output_file = (self.output_file + '_tree_height_lvis_' +
                       lvis_height + self.output_ext)
        self.save_vector(height_vect_lvis,
                         output_file,
                         lat_vect_lvis,
                         lon_vect_lvis,
                         footprint=lvis_footprint)

    def get_overlapping_indexes(self,
                                lat_vect_lvis,
                                lon_vect_lvis,
                                footprint,
                                verbose):
        flag_vector_lvis_inside_polygon = \
            np.zeros((len(lat_vect_lvis)))

        for file_count in range(len(self.input_files)):
            vx = (footprint * 0.5 *
                  np.asarray([self.lon_x_step[file_count],
                              self.lat_x_step[file_count]]) /
                  np.sqrt(self.lon_x_step[file_count]**2 +
                          self.lat_x_step[file_count]**2))
            vy = (footprint * 0.5 *
                  np.asarray([self.lon_y_step[file_count],
                              self.lat_y_step[file_count]]) /
                  np.sqrt(self.lon_x_step[file_count]**2 +
                          self.lat_x_step[file_count]**2))
            p1 = np.asarray([self.lon_0[file_count],
                             self.lat_0[file_count]]) + vx + vy
            p2 = np.asarray([self.lon_f_x[file_count],
                             self.lat_f_x[file_count]]) - vx + vy
            p3 = np.asarray([self.lon_f_xy[file_count],
                             self.lat_f_xy[file_count]]) - vx - vy
            p4 = np.asarray([self.lon_f_y[file_count],
                             self.lat_f_y[file_count]]) + vx - vy

            current_polygon = Polygon([p1, p2, p3, p4])

            min_lon = np.min(current_polygon.x)
            max_lon = np.max(current_polygon.x)
            valid_points_lon = np.logical_and(lon_vect_lvis >= min_lon,
                                              lon_vect_lvis <= max_lon)
            if not (np.any(valid_points_lon)):
                continue
            min_lat = np.min(current_polygon.y)
            max_lat = np.max(current_polygon.y)
            valid_points_ind = np.where(
                np.logical_and(valid_points_lon,
                               np.logical_and(lat_vect_lvis >= min_lat,
                                              lat_vect_lvis <= max_lat)))
            n_valid = plant.get_indexes_len(valid_points_ind)
            if n_valid == 0:
                continue
            self.print('current area: ' + str(current_polygon.path))
            self.print('    # shots: ' + str(n_valid))
            for count in range(n_valid):
                i = valid_points_ind[0][count]
                if not flag_vector_lvis_inside_polygon[i]:
                    flag_vector_lvis_inside_polygon[i] = \
                        current_polygon.pnpoly(lon_vect_lvis[i],
                                               lat_vect_lvis[i])
        n_lvis_shots_inside_polygon = np.sum(flag_vector_lvis_inside_polygon)
        if n_lvis_shots_inside_polygon == 0:
            return 0
        if verbose:
            print('# lvis shots inside areas: ' +
                  str(n_lvis_shots_inside_polygon))
        valid_lvis_indexes = np.where(flag_vector_lvis_inside_polygon)
        return valid_lvis_indexes

    def join_data(self,
                  lat_vect_lvis,
                  lon_vect_lvis,
                  lat_vect,
                  lon_vect,
                  agb_vect,
                  height_vect,
                  wd_vect,
                  crown_diameter_vect,
                  footprint):

        agb = np.zeros((len(lat_vect_lvis)))
        agb_max = np.zeros((len(lat_vect_lvis)))
        height_avg = np.zeros((len(lat_vect_lvis)))
        height_loreys = np.zeros((len(lat_vect_lvis)))
        height_max = np.zeros((len(lat_vect_lvis)))

        for i in range(len(lat_vect_lvis)):
            lat_diff_sq = (lat_vect_lvis[i] - lat_vect)**2
            lon_diff_sq = (lon_vect_lvis[i] - lon_vect)**2
            dist = np.sqrt(lat_diff_sq + lon_diff_sq)
            valid_indexes = np.where(dist < (footprint / 2.0 +
                                             crown_diameter_vect))
            if plant.get_indexes_len(valid_indexes) == 0:
                continue
            agb[i] = np.nansum(agb_vect[valid_indexes])
            agb_max[i] = np.nanmax(agb_vect[valid_indexes])
            valid_indexes = np.where(
                np.logical_and(dist < (footprint / 2.0 + crown_diameter_vect),
                               np.logical_and(plant.isvalid(wd_vect),
                                              plant.isvalid(height_vect))))
            if plant.get_indexes_len(valid_indexes) == 0:
                continue
            height_avg[i] = np.nanmean(height_vect[valid_indexes])
            height_loreys[i] = np.nansum((wd_vect[valid_indexes]**2) *
                                         height_vect[valid_indexes])
            height_loreys[i] /= np.nansum(wd_vect[valid_indexes]**2)
            height_max[i] = np.nanmax(height_vect[valid_indexes])

        delta_lat_m, delta_lon_m = plant.deg_to_m(footprint,
                                                  lat=np.nanmean(lat_vect))

        factor = 1e-4 * np.pi * (delta_lat_m * delta_lon_m) / 4

        self.print('Scaling AGB values from lidar shot area to hectare..')
        self.print('    shot axis: %f %f' % (delta_lat_m, delta_lon_m))
        self.print('    factor: %f' % factor)
        agb /= factor
        agb_max /= factor

        return agb, agb_max, height_avg, height_loreys, height_max

    def get_coordinates_from_ref_file(self):
        self.print(f'ref. file: {self.ref_file}')
        self.lat_dict = {}
        self.lon_dict = {}
        columns_dict = {}
        position_column = None
        if not plant.isfile(self.ref_file):
            self.print(f'ERROR file not found: {self.ref_file}')
        with open(self.ref_file, 'r', encoding="ISO-8859-1") as f:
            lines = f.readlines()
            for i, line in enumerate(lines):

                line_splitted = \
                    ['{}'.format(x)
                     for x in list(csv.reader([line],
                                              delimiter=self.separator,
                                              quotechar='"'))[0]]
                if i == self.ref_file_first_line - 1:
                    if len(line_splitted) <= 1:
                        self.print('line ' +
                                   str(self.ref_file_first_line) +
                                   ' of %s'
                                   ': %s' % (self.ref_file, line))
                        self.print('ERROR separating line ' +
                                   str(self.ref_file_first_line) +
                                   ' using input separator')
                        return

                    for j, element in enumerate(line_splitted):
                        if 'lat' in element.lower():
                            columns_dict['lat'] = j
                            self.print('latitude in column ' +
                                       '(ref. file): ' + str(j) +
                                       '   field: ' + element)
                        elif 'lon' in element.lower():
                            self.print('longitude in column: ' +
                                       '(ref. file): ' + str(j) +
                                       '   field: ' + element)
                            columns_dict['lon'] = j
                        elif ('plotviewname' in element.lower() or
                              'plot' in element.lower()):
                            self.print('area id. (ref. file):  ' + str(j) +
                                       '   field: ' + element)
                            columns_dict['area_id'] = j
                        elif ('position' in element.lower()):
                            self.print('position (ref. file):  ' + str(j) +
                                       '   field: ' + element)
                            position_column = j
                            self.meters_to_deg = True
                    continue

                try:
                    lat = float(line_splitted[columns_dict['lat']])
                except BaseException:
                    break
                try:
                    lon = float(line_splitted[columns_dict['lon']])
                except BaseException:
                    break
                try:
                    area_id = line_splitted[columns_dict['area_id']]
                    if (position_column is not None):
                        try:
                            position = line_splitted[position_column]
                            if '"' not in position:
                                position = f'"{position}"'

                            area_id += ' ' + position
                        except BaseException:
                            break
                except BaseException:
                    break
                self.lat_dict[area_id] = lat
                self.lon_dict[area_id] = lon


def main(argv=None):
    with plant.PlantLogger():
        parser = get_parser()
        self_obj = PlantAGBSamples(parser, argv)
        ret = self_obj.run()
        return ret


if __name__ == '__main__':
    main()
