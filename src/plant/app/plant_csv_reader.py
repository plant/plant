#!/usr/bin/env python3
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Gustavo H. X. Shiroma, Marco Lavalle
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import sys
import plant

DEFAULT_FOOTPRINT = plant.m_to_deg_lat(18.25)


def get_parser():

    descr = ('Rasterize comma-separated values (CSV) data.'
             ' Required parameters are the CSV files and'
             ' the location of the fields containing the data'
             ' and X and Y position in the raster.'
             ' The location of the fields are defined by'
             ' the field label/name or index'
             ' (column number). For example: '
             ' --y-index 0 will use the first'
             ' column to represent the position in the'
             ' Y direction (northing or latitude) and '
             ' --data-label "height" represents the'
             ' respective field "height" of the CSV file')
    epilog = ''
    parser = plant.argparse(epilog=epilog,
                            description=descr,
                            input_files=2,
                            geo=1,
                            default_flags=1,
                            default_options=1,
                            default_output_options=1,
                            separator=1,
                            output_file=2,
                            null=1,
                            out_null=1,
                            default_lidar=1)

    parser.add_argument('--footprint', '--footprint_size',
                        dest='footprint', type=float,
                        required=False, help='Footprint size '
                        '(default: %(default)s)',
                        default=DEFAULT_FOOTPRINT)

    parser_lat = parser.add_mutually_exclusive_group(required=False)
    parser_lat.add_argument('--y-index',
                            '--y-column',
                            '--lat-index',
                            '--lat-column',
                            dest='lat_column',
                            type=int,
                            required=False,
                            help='Latitude column index')

    parser_lat.add_argument('--y-label',
                            '--lat-label',
                            dest='lat_text',
                            type=str,
                            required=False,
                            help='Latitude column label')

    parser_lon = parser.add_mutually_exclusive_group(required=False)
    parser_lon.add_argument('--x-index',
                            '--x-column',
                            '--lon-index',
                            '--lon-column',
                            dest='lon_column',
                            type=int,
                            required=False,
                            help='Longitude column index')

    parser_lon.add_argument('--x-label',
                            '--lon-label',
                            dest='lon_text',
                            type=str,
                            required=False,
                            help='Longitude column label')

    parser_data = parser.add_mutually_exclusive_group(required=False)
    parser_data.add_argument('--data-index',
                             '--data-column',
                             dest='data_column_list',
                             type=int,
                             nargs='+',
                             required=False,
                             help='Data column index')

    parser_data.add_argument('--data-label',
                             dest='data_text_list',
                             type=str,
                             nargs='+',
                             required=False,
                             help='Data column label')

    parser.add_argument('--first-line',
                        dest='first_line',
                        type=int,
                        required=False,
                        help='First CSV line')

    parser.add_argument('--first-header-index',
                        '--first-header-column',
                        dest='first_header_column',
                        type=int,
                        required=False,
                        help='First header column')

    parser.add_argument('--trim-whitespaces',
                        dest='trim_whitespaces',
                        action='store_true',
                        help='Sort inputs by name',
                        required=False,
                        default=False)

    parser.add_argument('--lvis',
                        dest='lvis',
                        action='store_true',
                        help='Input is a LVIS L2 CSV file',
                        required=False, default=False)

    parser.add_argument('-n',
                        '--nlines',
                        dest='n_lines',
                        type=int,
                        required=False,
                        help='Number of lines to read')

    return parser


class PlantCSVReader(plant.PlantScript):

    def __init__(self, parser, argv=None):

        super().__init__(parser, argv)

        if self.in_null is None or plant.isnan(self.in_null):
            self.in_null = -999
        self.image_list = []
        self.populate_parameters()

    def populate_parameters(self):

        self.print('output file: %s' % self.output_file, 1)
        if (plant.isvalid(self.plant_geogrid_obj.length) and
                plant.isvalid(self.plant_geogrid_obj.width)):
            if self.output_dtype:
                datatype_size = plant.get_dtype_size(self.output_dtype)
            else:
                datatype_size = 4
            data_size = (plant.isvalid(self.plant_geogrid_obj.length) *
                         plant.isvalid(self.plant_geogrid_obj.width) *
                         datatype_size)
            if data_size > 5e8:
                self.print('WARNING this file seems to be too large (%s) '
                           'for step=%f and given coordinates.' %
                           (plant.get_file_size_string(data_size), self.step))
                while 1:
                    res = plant.get_keys('Continue anyway? ([y]es/[n]o) ')
                    if res.startswith('y'):
                        break
                    elif res.startswith('n'):
                        self.print('Operation cancelled.', 1)
                        sys.exit(0)
            else:
                self.print('expected output size: %s' %
                           plant.get_file_size_string(data_size), 1)

        if self.null is not None:
            print('output null value: %s' % str(self.null))
        if self.lvis:
            self.trim_whitespaces = True
            if self.first_header_column is None:
                self.first_header_column = 1
            if self.first_line is None:
                self.first_line = 12
            if self.separator is None:
                self.separator = ' '
            if self.canopy:
                if self.lat_text is None:
                    self.lat_text = 'TLAT'
                if self.lon_text is None:
                    self.lon_text = 'TLON'
                if self.data_text_list is None:
                    self.data_text_list = ['ZT']
            if self.ground:
                if self.lat_text is None:
                    self.lat_text = 'GLAT'
                if self.lon_text is None:
                    self.lon_text = 'GLON'
                if self.data_text_list is None:
                    self.data_text_list = ['ZG']

        if self.first_header_column is None:
            self.first_header_column = 0

        if self.first_line is None:
            self.first_line = 0

        if self.separator is None:
            self.separator = ','
        elif self.separator == '':
            self.separator = ' '

    def run(self):

        if (self.data_column_list is not None and
                self.data_text_list is None):
            str_data_list = ['COL_%d' % x for x in self.data_column_list]
            nbands = len(self.data_column_list)
        elif (self.data_text_list is not None):
            str_data_list = self.data_text_list
            nbands = len(self.data_text_list)
        else:
            self.print('ERROR please identify the label or the column '
                       'number of the input data in the CSV file')
            return
        data_vect_all = [[] for i in range(nbands)]
        lat_vect_all = []
        lon_vect_all = []
        valid_files_count = 0
        for i, current_file in enumerate(self.input_files):
            self.print('-----------------------------------------')
            self.print(current_file + ' (%d/%d)'
                       % (i + 1, len(self.input_files)))

            with plant.PlantIndent():
                ret = plant.read_csv_data_lat_lon(
                    current_file,
                    verbose=self.verbose,
                    lat_column=self.lat_column,
                    lon_column=self.lon_column,
                    data_column_list=self.data_column_list,
                    lat_text=self.lat_text,
                    lon_text=self.lon_text,
                    data_text_list=self.data_text_list,
                    separator=self.separator,

                    y0=self.plant_geogrid_obj.y0,
                    yf=self.plant_geogrid_obj.yf,
                    x0=self.plant_geogrid_obj.x0,
                    xf=self.plant_geogrid_obj.xf,
                    first_line=self.first_line,
                    trim_whitespaces=self.trim_whitespaces,
                    first_header_column=self.first_header_column,
                    n_lines=self.n_lines)
                if ret is None:
                    continue

                valid_files_count += 1
                data_vect, lat_vect, lon_vect = ret

                width = len(data_vect[0])
                self.print(f'# valid points: {width}')

                for j in range(width):
                    flag_valid = True
                    for i in range(len(data_vect)):
                        if plant.isnan(data_vect[i][j], self.in_null):
                            flag_valid = False
                            break
                    if flag_valid:
                        for b in range(nbands):
                            data_vect_all[b].append(data_vect[b][j])

                        lat_vect_all.append(lat_vect[j])
                        lon_vect_all.append(lon_vect[j])
                self.print('# valid points (cumulative):'
                           f' {len(data_vect_all[0])}')

        valid_lines_count = len(data_vect_all[0])
        self.print('# valid points (total): ' + str(valid_lines_count))

        if valid_lines_count == 0:
            self.print('ERROR. No valid lines were found for the '
                       'selected region.')
            return

        self.save_vector(data_vect_all,
                         self.output_file,
                         lat_vect_all,
                         lon_vect_all,
                         nbands=nbands,
                         str_data=str_data_list,
                         lat_text=self.lat_text,
                         lon_text=self.lon_text,
                         footprint=self.footprint)

        if plant.isfile(self.output_file):
            return plant.read_image(self.output_file)

        out_image_obj = plant.PlantImage(data_vect_all)
        out_image_obj.set_image(lat_vect_all, band=1,
                                name='lat')
        out_image_obj.set_image(lon_vect_all, band=1,
                                name='lon')
        return out_image_obj


def main(argv=None):
    with plant.PlantLogger():
        parser = get_parser()
        self_obj = PlantCSVReader(parser, argv)
        ret = self_obj.run()
        return ret


if __name__ == '__main__':
    main()
