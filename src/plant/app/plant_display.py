#!/usr/bin/env python3
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Giangi Sacco, Gustavo H. X. Shiroma and Marco Lavalle
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import sys
import math

import plant


def get_parser():
    return plant.plant_display_lib.get_parser()


class Display(object):

    def _set_if_not_present(self, opList, option, default):

        if option not in opList:
            opList.append(option)
            opList.append(default)

    def _get_if_present(self, opList, option):

        if opList is None:
            return
        ret = None
        try:
            indx = opList.index(option)
        except ValueError:
            return
        except AttributeError:
            return
        try:
            ret = opList[indx + 1]
        except IndexError:
            return

        opList.pop(indx)

        opList.pop(indx)
        return ret

    def _create_command(self, options):
        ext = options['ext']
        dataType = options['dataType']
        image = options['image']
        width = int(options['width']) if 'width' in options else None
        length = int(options['length']) if 'length' in options else None
        argv = options['other']
        band_indexes = self._get_if_present(argv, '--band')

        if '--mdx' in argv:
            argv.remove('--mdx')
        if band_indexes is None:
            if 'band' in options.keys():
                band_indexes = options['band']
        if band_indexes is not None:
            band_indexes = plant.get_int_list(plant.read_matrix(
                band_indexes,
                verbose=False))
        nbands = None

        if 'plant_image' in options.keys():
            plant_image = options['plant_image']
            if isinstance(plant_image, plant.PlantImage):

                if ext is None and plant_image.scheme is not None:
                    ext = plant_image.scheme.lower()

                if band_indexes is None:
                    band_indexes = plant.get_int_list(plant_image.band_orig)

                if nbands is None:
                    nbands = plant_image.nbands_orig

                if width is None:
                    width = plant_image.width_orig

                if length is None:
                    length = plant_image.length
                image = plant_image.filename_orig
        else:
            plant_image = image

        if width is not None:
            width_suffix = ' -s ' + str(width)
        else:
            width_suffix = ''

        if band_indexes is None:
            band_indexes = [0]
            nbands = 1
        if nbands is None:
            nbands = plant.read_image(image, only_header=True).nbands_orig

        flag_include_dtype = True
        for x in self._dataTypes:
            if x in argv:
                flag_include_dtype = False
        if flag_include_dtype:
            dtype_suffix = ' ' + dataType
        else:
            dtype_suffix = ''

        self._displayPlot_arg.append([plant_image, [image] + argv, None])
        command = ''

        band_indexes = [b + 1 for b in band_indexes]

        if ext is None:
            ext = 'bsq'
        ext = ext.lower()
        if (ext in self._ext['cpx'] or
                ext in self._ext['scor'] or
                ext in self._ext['byt']):
            command = image
            command += width_suffix
            command += dtype_suffix
            command += ' ' + ' '.join(argv)
        elif ext in self._ext['fr']:
            command = image
            command += width_suffix
            command += dtype_suffix
            command += ' ' + ' '.join(argv)
        elif ext in self._ext['rmg']:
            command = image
            command = (' ' + ' -rmg -RMG-Mag -CW -RMG-Hgt ' +
                       ' '.join(argv))
        elif ext in self._ext['unw']:
            tpi = str(2. * math.pi)
            self._set_if_not_present(argv, '-wrap', tpi)
            command = image
            command += width_suffix
            command += '  -amp'
            command += dtype_suffix
            command += ' -rtlr '
            command += str(width * int(dataType[2:]))
            command += ' -CW -unw'
            command += dtype_suffix
            command += ' -rhdr '
            command += str(width * int(dataType[2:]))
            command += ' -cmap cmy ' + ' '.join(argv)
        elif ext in self._ext['cor']:
            self._set_if_not_present(argv, '-wrap', '1.2')
            command = image
            command += width_suffix
            command += '  -rmg -RMG-Mag -CW -RMG-Hgt ' + ' '.join(argv)
        elif ext in self._ext['dem']:
            self._set_if_not_present(argv, '-wrap', '100')
            self._set_if_not_present(argv, '-cmap', 'cmy')
            command = image + ' -slope'
            command += width_suffix
            command += dtype_suffix
            command += ' ' + image
            command += width_suffix
            command += dtype_suffix
            command += ' ' + ' '.join(argv)
        elif ext in self._ext['msk']:
            self._set_if_not_present(argv, '-wrap', '1.2')
            command = image
            command += width_suffix
            command += ' -rmg -RMG-Mag -CW -RMG-Hgt ' + ' '.join(argv)
        elif ext in self._ext['amp']:

            chdr = dataType[2:]
            ctlr = dataType[2:]
            newChdr = self._get_if_present(argv, '-chdr')
            if newChdr is not None:
                chdr = newChdr
            newCtlr = self._get_if_present(argv, '-ctlr')
            if newCtlr is not None:
                ctlr = newCtlr
            command = image
            command += width_suffix
            command += ' -CW '
            command += ' -amp1 '
            command += dtype_suffix
            command += ' -ctlr '
            command += ctlr + ' -amp2'
            command += dtype_suffix
            command += '  -chdr '
            command += chdr + ' ' + ' '.join(argv)

        elif ext in self._ext['bil']:
            sizeof = self._get_data_size(dataType)
            command = image
            command += width_suffix
            for i in band_indexes:

                rhdr = (i - 1) * width * sizeof
                rtlr = (nbands - i) * width * sizeof
                if ('-set' not in argv and
                        nbands > 1):
                    command += ' -ch' + str(i)
                command += dtype_suffix
                command += (' -rhdr ' + str(rhdr)) if rhdr else ''
                command += (' -rtlr ' + str(rtlr)) if rtlr else ''
                command += ' ' + ' '.join(argv)

        elif ext in self._ext['bip']:

            sizeof = self._get_data_size(dataType)
            command = image
            command += width_suffix
            for i in band_indexes:

                chdr = (i - 1) * sizeof
                ctlr = (nbands - i) * sizeof
                if ('-set' not in argv and
                        nbands > 1):
                    command += ' -ch' + str(i)
                command += dtype_suffix
                command += (' -chdr ' + str(chdr)) if chdr else ''
                command += (' -ctlr ' + str(ctlr)) if ctlr else ''
                command += ' ' + ' '.join(argv)

        elif ext in self._ext['bsq']:

            sizeof = self._get_data_size(dataType)
            command = image
            command += width_suffix
            if nbands > 1:
                for i in band_indexes:

                    shdr = (i - 1) * width * length * sizeof
                    stlr = (nbands - i) * width * length * sizeof
                    if ('-set' not in argv and
                            nbands > 1):
                        command += ' -ch' + str(i)
                    command += dtype_suffix
                    command += (' -shdr ' + str(shdr)) if shdr else ''
                    command += (' -stlr ' + str(stlr)) if stlr else ''
                    command += ' ' + ' '.join(argv)

            else:
                command += dtype_suffix
                command += ' ' + ' '.join(argv)
        else:
            command = image
            command += width_suffix
            command += ' ' + ' '.join(argv)
            if flag_include_dtype:
                command += ' ' + dataType
        return command

    def parse(self, argv):
        ret = {}
        if '-z' in argv:
            indx = argv.index('-z')
            if len(argv) >= indx + 2:
                ret['-z'] = argv[indx + 1]
                argv.pop(indx + 1)
            argv.pop(indx)

        if '-P' in argv:
            indx = argv.index('-P')
            ret['-P'] = '-P'
            argv.pop(indx)

        self.imgOpt = []
        self.parOpt = []
        self.allImgParOpt = []
        pos = 0
        flag_found_first_image = False
        self.flag_current_alias = False
        flag_alias = False

        self.alias_filelist = []

        new_argv = []
        flag_skip_next = False
        for i, arg in enumerate(argv):
            if flag_skip_next:
                flag_skip_next = False
                continue
            if '-ch' not in arg:
                new_argv.append(arg)
                continue
            mdx_band = None
            if ((arg == '-ch' or arg == '--ch') and
                    i + 1 < len(argv) and
                    plant.isnumeric(argv[i + 1])):
                mdx_band = int(argv[i + 1])
                flag_skip_next = True
            elif arg.startswith('-ch') and plant.isnumeric(arg[3:]):
                mdx_band = int(arg[3:])
            elif arg.startswith('--ch') and plant.isnumeric(arg[4:]):
                mdx_band = int(arg[4:])
            else:
                new_argv.append(arg)
            if mdx_band is not None:
                new_argv += ['--band', str(mdx_band - 1)]
        argv = new_argv

        while (True):

            if (pos >= len(argv)):
                if self.flag_current_alias:
                    self._append_alias_to_img_opt()
                    self.flag_current_alias = False
                elif flag_found_first_image:
                    self.imgOpt.append(self.parOpt + self.allImgParOpt)
                break

            flag_alias = self._check_if_alias(argv[pos])

            if (len(plant.search_image(argv[pos])) != 0) or flag_alias:

                filelist = plant.search_image(argv[pos])
                for current_file in filelist:

                    if not flag_found_first_image:
                        flag_found_first_image = True
                        if not flag_alias:
                            self.parOpt.append(current_file)

                    elif not self.flag_current_alias:
                        self.imgOpt.append(self.parOpt + self.allImgParOpt)
                        if not flag_alias:
                            self.parOpt = [current_file]

                    else:
                        self._append_alias_to_img_opt()
                        self.flag_current_alias = False
                        if not flag_alias:
                            self.parOpt = [current_file]

            if flag_alias:
                self.parOpt = []
                flag_found_first_image = True
                self.flag_current_alias = flag_alias
                self.alias_filelist = []
                flag_found_input = False
                pointer = 1
                localParOpt = []
                localAllImgParOpt = []
                tempParOpt = []
                while (pos + pointer) < len(argv):

                    if (not argv[pos + pointer].startswith('-') and
                            not plant.isnumeric(
                                argv[pos + pointer].startswith('-'))):

                        if flag_found_input:
                            localParOpt.append(tempParOpt)
                        else:

                            localAllImgParOpt = tempParOpt

                        self.alias_filelist.append(argv[pos + pointer])
                        flag_found_input = True
                        tempParOpt = []

                    elif self._check_if_alias(argv[pos + pointer]):
                        break

                    else:
                        tempParOpt.append(argv[pos + pointer])
                        nargs = self.get_nargs(argv[pos + pointer])
                        for i in range(nargs):
                            pointer += 1
                            if pos + pointer < len(argv):
                                tempParOpt.append(argv[pos + pointer])

                    pointer += 1
                if not flag_found_input:
                    print('ERROR input(s) not recognized for --RGB')
                    return
                localParOpt.append(tempParOpt)
                self.parOpt = [localAllImgParOpt + x for x in localParOpt]
                pos += pointer - 1

            elif (argv[pos].startswith('-') and
                  not plant.isnumeric(argv[pos].startswith('-'))):

                if flag_found_first_image:
                    self.parOpt.append(argv[pos])
                    ref_list = self.parOpt

                else:
                    self.allImgParOpt.append(argv[pos])
                    ref_list = self.allImgParOpt

                nargs = self.get_nargs(argv[pos])
                for i in range(nargs):
                    pos += 1
                    try:
                        ref_list.append(argv[pos])
                    except IndexError:
                        pass

            elif len(plant.search_image(argv[pos])) == 0:
                print('ERROR file not found: ' + argv[pos])
                return
            else:
                self.flag_force_execute_DisplayLib = \
                    (self.flag_force_execute_DisplayLib or
                     not plant.isfile(argv[pos]))
            pos += 1

        flag_input_sort = False
        for img in self.imgOpt:
            for index, img_index in enumerate(img):
                next_index = index + 1
                flag_input_sort |= img_index.strip() == '--sort'
                while next_index <= (len(img) - 1):
                    if img_index == img[next_index]:
                        nargs = self.get_nargs(img_index)
                        try:
                            img = img[0:next_index] + \
                                img[next_index + 1 + nargs:]
                        except IndexError:
                            pass
                    else:
                        next_index += 1

        if flag_input_sort:
            for index_1 in range(len(self.imgOpt)):
                for index_2 in range(len(self.imgOpt)):
                    if (self.imgOpt[index_1][0] <
                            self.imgOpt[index_2][0]):
                        self.imgOpt[index_1], self.imgOpt[index_2] = \
                            self.imgOpt[index_2], self.imgOpt[index_1]
        ret['imageArgs'] = self.imgOpt

        return ret

    def get_nargs(self, parameter):
        single_parameters_list = self._dataTypes + \
            self._displayPlotSinglePar + self._single_parameters

        if parameter in single_parameters_list:
            return 0
        multi_par = [parameter in par[0]
                     for par in self._multi_parameters_list]

        if any(multi_par):
            multi_par_pos = multi_par.index(True)
            n_args = self._multi_parameters_list[multi_par_pos][1]

        else:
            n_args = 1
        return n_args

    def _check_if_alias(self, arg):
        return arg.upper() in self._alias_RGB

    def _append_alias_to_img_opt(self):
        str_hh_file = None
        parameters_list = [x + self.allImgParOpt for x in self.parOpt]
        master_file = None
        hv_file = None
        alpha_file = None

        self.flag_rgb = True
        self.flag_alpha = False

        mix_type = '+' if self.flag_rgb else 'x'
        if self.flag_alpha:
            if len(self.alias_filelist) == 2:
                master_file = self.alias_filelist[0]
                master_parameters = parameters_list[0]
                alpha_file = self.alias_filelist[1]
                alpha_parameters = parameters_list[1]
            elif len(self.alias_filelist) > 2:
                master_file = [f for f in self.alias_filelist
                               if 'entropy.bin' in f]
                master_file = master_file[0] if len(master_file) >= 1 \
                    else None
                alpha_file = [f for f in self.alias_filelist if 'alpha.bin'
                              in f]
                alpha_file = alpha_file[0] if len(alpha_file) >= 1 \
                    else None
            if master_file is None or alpha_file is None:
                print('ERROR incorrect inputs for -entropy.')
                print('Please, enter the entropy and alpha files.')
                return
            if (len(plant.search_image(master_file)) == 0):
                print('ERROR file not found: ' + master_file)
                return
            if (len(plant.search_image(alpha_file)) == 0):
                print('ERROR file not found: ' + alpha_file)
                return
            if master_file is not None:
                ind = [i for i, x in enumerate(self.alias_filelist)
                       if x == master_file][0]
                master_parameters = parameters_list[ind]
            if alpha_file is not None:
                ind = [i for i, x in enumerate(self.alias_filelist)
                       if x == alpha_file][0]
                alpha_parameters = parameters_list[ind]
        elif self.flag_rgb:
            if len(self.alias_filelist) > 1:
                ret = plant.search_pol(self.alias_filelist,
                                       sym=True,
                                       exact_str_vv_search=False)
            else:
                multiband_filename = self.alias_filelist[0]
                multiband_parameters = parameters_list[0][:]
                ret = None
                if ('--band' not in multiband_parameters and
                        plant.IMAGE_NAME_SEPARATOR not in multiband_filename):
                    image_obj = plant.read_image(multiband_filename)
                    for b in range(image_obj.nbands):
                        if b != 0:
                            self.alias_filelist.append(multiband_filename)
                            parameters_list.append(multiband_parameters[:])
                        parameters_list[b] += ['--band', str(b)]

            if ret is not None and len(ret['identified_inputs']) != 1:
                master_file = ret['hh_file']
                str_hh_file = ret['str_hh_file']
                hv_file = ret['hv_file']
                vv_file = ret['vv_file']
                if vv_file is None:
                    vv_file = master_file
                    str_vv_file = str_hh_file
                else:
                    str_vv_file = ret['str_vv_file']
                str_hv_file = ret['str_hv_file']
                if master_file is not None:
                    ind = [i for i, x in enumerate(self.alias_filelist)
                           if x == master_file][0]
                    master_parameters = parameters_list[ind]
                if hv_file is not None:
                    ind = [i for i, x in enumerate(self.alias_filelist)
                           if x == hv_file][0]
                    hv_parameters = parameters_list[ind]
                if vv_file is not None:
                    ind = [i for i, x in enumerate(self.alias_filelist)
                           if x == vv_file][0]
                    vv_parameters = parameters_list[ind]
            ret = plant.get_images_from_list(self.alias_filelist,
                                             parameters_list)

            alias_imagelist, parameters_list = ret
            if (master_file is None and
                    len(alias_imagelist) == 1):
                master_file = alias_imagelist[0]
                master_parameters = parameters_list[0]
            if ((master_file is None or hv_file is None)
                    and (len(self.alias_filelist) >= 3)
                    and (len(alias_imagelist) >= 3)):

                vv_file = self.alias_filelist[2]
                vv_parameters = parameters_list[2]
                str_vv_file = vv_file

                hv_file = self.alias_filelist[1]
                hv_parameters = parameters_list[1]
                str_hv_file = hv_file

                master_file = self.alias_filelist[0]
                master_parameters = parameters_list[0]
                str_hh_file = master_file
                if len(self.alias_filelist) >= 4:
                    alpha_file = self.alias_filelist[3]
                    alpha_parameters = parameters_list[3]
                    str_alpha_file = alpha_file
                if len(self.alias_filelist) > 4:
                    print('WARNING mdx RGB mode accepts at most 4 images '
                          '(RGB and alpha). Ignoring: %s' %
                          str((self.alias_filelist[4:] + parameters_list[4:])))
            if ((master_file is None or hv_file is None)
                    and (len(self.alias_filelist) == 2)
                    and (len(alias_imagelist) == 2)):

                vv_file = self.alias_filelist[0]
                vv_parameters = parameters_list[0]
                str_vv_file = vv_file

                hv_file = self.alias_filelist[1]
                hv_parameters = parameters_list[1]
                str_hv_file = hv_file

                master_file = self.alias_filelist[0]
                master_parameters = parameters_list[0]
                str_hh_file = master_file
            elif master_file is None:
                print('ERROR opening %s' % str(self.alias_filelist))
                return
        ret = self._get_mdx_parameters(master_file,
                                       master_parameters,
                                       mix_type,
                                       str_hh_file)
        master_file, master_parameters = ret
        if self.flag_rgb:
            if (master_file is not None and
                    hv_file is None):
                self.imgOpt.append([master_file] + master_parameters)
                return
            ret = self._get_mdx_parameters(hv_file,
                                           hv_parameters,
                                           mix_type,
                                           str_hv_file)
            hv_file, hv_parameters = ret
            ret = self._get_mdx_parameters(vv_file,
                                           vv_parameters,
                                           mix_type,
                                           str_vv_file)
            vv_file, vv_parameters = ret
            self.imgOpt.append([master_file,
                                '-cmap', 'red'] +
                               master_parameters)
            self.imgOpt.append([hv_file,
                                '-cmap', 'green'] +
                               hv_parameters)
            self.imgOpt.append([vv_file,
                                '-cmap', 'blue'] +
                               vv_parameters)
            if alpha_file is not None:
                ret = self._get_mdx_parameters(alpha_file,
                                               alpha_parameters,
                                               'x',
                                               str_alpha_file)
                alpha_file, alpha_parameters = ret
                self.imgOpt.append([alpha_file,
                                    '-a', '0', '-m', '1'] +
                                   alpha_parameters)
        elif self.flag_alpha:
            self.imgOpt.append([master_file,
                                '-a', '0', '-m', '1'] +
                               master_parameters)
            ret = self._get_mdx_parameters(alpha_file,
                                           alpha_parameters,
                                           mix_type)
            alpha_file, alpha_parameters = ret
            self.imgOpt.append([alpha_file,
                                '-cmap', 'cmy',
                                '-a', '0', '-m', '90',
                                '-clpmax', '70']
                               + alpha_parameters)
        else:
            sys.exit('ERROR alias not identified')

    def _get_mdx_parameters(self,
                            current_file,
                            parameters,
                            mix_type=None,
                            label=None):

        new_parameters = parameters[:]

        if ((not plant.test_gdal_open(current_file)) and
                (not plant.test_other_drivers(current_file)) and
                plant.IMAGE_NAME_SEPARATOR in current_file):
            current_file_splitted = \
                current_file.split(plant.IMAGE_NAME_SEPARATOR)
            current_file = current_file_splitted[0]
            new_parameters.append(['--band',
                                   str(int(current_file_splitted[1]))])
        if (label is not None and
                '-set' not in new_parameters and
                '--band' not in new_parameters):

            new_parameters.append('-set')
            new_parameters.append(label)

        image_dict = self._get_info(image=current_file)
        if ('-s' not in new_parameters and
                image_dict['width'] is not None):
            new_parameters.append('-s')
            new_parameters.append(str(image_dict['width']))
        dataType = None
        for x in self._dataTypes:
            if x in new_parameters:
                dataType = x
        if dataType is None:
            if image_dict['dataType'] is not None:
                dataType = image_dict['dataType']
            else:
                print('ERROR dataType could not be identified')
                return
        if dataType == '-c8':
            dataType = '-c8mag'
        new_parameters.append(dataType)
        if ('-mix' not in new_parameters and
                mix_type is not None):
            new_parameters.append('-mix')
            new_parameters.append(mix_type)
        return (current_file, new_parameters)

    def _get_info(self, listOp=None, image=None):
        mask_parser = plant.argparse(input_image=1,
                                     default_input_options=1,
                                     default_flags=1,
                                     in_null=1,
                                     null=1,
                                     default_output_options=1,
                                     mask=1,
                                     flag_use_ctable=1,
                                     transform=1)
        self._get_if_present(listOp, '-s')

        parsed_args = None
        listOp = plant.pre_parser(listOp, parser=mask_parser,
                                  verbose=False)

        try:
            parsed_args, extra = mask_parser.parse_known_args(listOp)
        except BaseException:
            pass
        if parsed_args is None:
            try:
                ret = mask_parser.parse_known_args(listOp,
                                                   allow_abbrev=False)
                ret = parsed_args, extra
            except BaseException:
                pass

        if not image:
            image = listOp[0]
            ret = None

        if parsed_args is not None:
            script_obj = plant.PlantScript(parsed_args=parsed_args)

            if script_obj.flag_use_ctable is None:
                script_obj.flag_use_ctable = True

            image_obj = script_obj.read_image(image,
                                              only_header=True,
                                              verbose=False)

            flag_apply_transformation = \
                script_obj.plant_transform_obj.flag_apply_transformation()
        else:
            image_obj = plant.read_image(image,

                                         only_header=True,
                                         verbose=False)
            flag_apply_transformation = False

        if image_obj is not None:

            ret = {'plant_image': image_obj,
                   'image': image_obj.filename,
                   'ext': image_obj.scheme,
                   'width': image_obj.width,
                   'length': image_obj.length,
                   'dataType': image_obj.dtype,
                   'nbands': image_obj.nbands,
                   'nbands_orig': image_obj.nbands_orig}

        if listOp is not None:
            ret_commandline = self._get_info_from_command_line(listOp)
            if ret is None:
                ret = ret_commandline
            else:
                if (not ret_commandline['image'] is None and
                        ret['image'] is None):
                    ret['image'] = ret_commandline['image']
                if not ret_commandline['ext'] is None and ret['ext'] is None:
                    ret['ext'] = ret_commandline['ext']

                if not ret_commandline['width'] is None:
                    ret['width'] = ret_commandline['width']

                if not ret_commandline['dataType'] is None:
                    ret['dataType'] = ret_commandline['dataType']
            if ret['width'] is None:
                print('ERROR could not determine image width.')
                return

            if (image_obj.file_format is None or
                    image_obj.file_format.upper() in
                plant.MDX_NOT_SUPPORTED_FORMATS or
                    image_obj.length <= 1 or
                    flag_apply_transformation or
                    (image_obj.band is not None and
                     image_obj.band.ctable is not None)):
                self.flag_force_execute_DisplayLib = True
            if not (ret['image']):
                ret['image'] = image

        if ret is None:
            return ({'image': None,
                    'ext': None,
                     'width': None,
                     'length': None,
                     'dataType': None,
                     'nbands': None,
                     'nbands_orig': None})
        if ret['image'] is None and not plant.isfile(image):
            print('ERROR file not found: %s'
                  % image)
            return
        elif ret['image'] is None:
            print('ERROR opening %s'
                  % image)
            return
        ret['dataType'] = plant.get_mdx_dtype(ret['dataType'])
        if ret['dataType'] == '-c8':
            ret['ext'] = 'cpx'

        return ret

    def _is_ext(self, ext):
        found = False
        for k, v in self._ext.items():
            if ext in v:
                found = True
                break
        return found

    def _get_isce_ext(self, info, imagename):
        try:
            from isceobj.Util import key_of_same_content
        except BaseException:
            return
        ext = None

        try:
            ext = key_of_same_content('image_type', info)[1]

            if (not self._is_ext(ext)):
                raise Exception
        except BaseException:

            try:
                nameSplit = imagename.split('.')
                if len(nameSplit) > 1:
                    ext = nameSplit[-1]
                if (not self._is_ext(ext)):
                    raise Exception
            except BaseException:

                try:
                    scheme = key_of_same_content('scheme', info)[1]
                    ext = scheme
                    if ext is not None:
                        ext = ext.lower()
                    if (not self._is_ext(ext)):
                        raise Exception
                except BaseException:
                    ext = None
        return ext

    def _get_info_from_command_line(self, opList):

        image = opList[0]
        dataType = None
        ext = self._get_isce_ext('', image)
        for x in self._dataTypes:
            if x in opList:
                dataType = x
        if dataType is not None:
            if ext is None:
                for key, val in self._mapDataType.items():
                    for new_key, new_val in val.items():
                        if dataType in new_val:
                            ext = key
        try:
            width = (opList[opList.index('-s') + 1])
        except BaseException:
            width = None
        return {'image': image,
                'ext': ext,
                'width': width,
                'dataType': dataType}

    def _get_command(self, options):
        command = 'mdx'
        if '-z' in options:
            command += ' -z ' + options['-z']

        for listOp in options['imageArgs']:

            opDict = self._get_info(listOp)
            if not (opDict is None):
                try:

                    opDict['other'] = listOp[1:]
                except BaseException:

                    pass
            command += ' ' + self._create_command(opDict)
        return command

    def run(self, argv=None):
        if argv is None:
            argv = sys.argv[1:]
        argv_orig = argv[:]
        self._displayPlot_obj = plant.PlantDisplayLib(
            argv=argv, mdx_mode=True)

        options = self.parse(argv)
        command = self._get_command(options)

        flag_force_mdx = '--mdx' in argv
        if self._flagExecuteDisplayLib(argv_orig) and flag_force_mdx:
            print('WARNING mdx may not accept one of the input'
                  ' parameters')
        elif self._flagExecuteDisplayLib(argv_orig) or not flag_force_mdx:
            ret = self._execute_display_lib(argv_orig)
            return ret

        flag_error = False

        try:
            return plant.execute(command)
        except KeyboardInterrupt:
            raise plant.PlantExceptionKeyboardInterrupt
        except plant.PlantExceptionError:
            flag_error = True
            error_message = plant.get_error_message()
        if flag_error and flag_force_mdx:
            print(error_message + '\n')
        if flag_error and not flag_force_mdx:

            ret = self._execute_display_lib(argv_orig)
            return ret

    def _execute_display_lib(self, argv):

        for args in self._displayPlot_arg:

            self._displayPlot_obj.insert_data_with_arg(*args)
        ret = self._displayPlot_obj.show()
        return ret

    def _get_data_size(self, dataType):
        try:
            size = int(dataType[2:])
        except BaseException:
            size = 0
        return size

    def _flagExecuteDisplayLib(self, argv):
        if self.flag_force_execute_DisplayLib:
            return True
        for x in self._displayPlotOptions:
            if x in argv or x.replace('--', '-') in argv:
                return True
        return False

    def __init__(self, argv=None):

        if argv is None:
            argv = sys.argv[1:]

        self.flag_force_execute_DisplayLib = False

        self._displayPlot_arg = []

        self._alias_RGB = ['-RGB', '--RGB']

        self._single_parameters = ['-wrap', '-d', '-STD', '-PER', '-CW',
                                   '-WRAP', '-ON', '-OFF', '-P', '-ponly',
                                   '-LE', '-le', '-little', '-BE', '-be',
                                   '-big', '-BS', '-bs', '-bswap', '-NM',
                                   '-C', '-CLOSE', '-NC', '-NOCLOSE',
                                   '--mdx',

                                   '-slope']

        self._displayPlotOptions = ['--scatterplot',
                                    '--scatter',
                                    '--scatter-plot',
                                    '--trendplot',
                                    '--trend',
                                    '--trend-plot',
                                    '--plot',
                                    '--print',
                                    '--hist-2d',
                                    '--histogram-2d',
                                    '--hist2d',
                                    '--histogram2d',
                                    '--hist',
                                    '--histogram',
                                    '--profilex',
                                    '--xprofile',
                                    '--profile-x',
                                    '--x-profile',
                                    '--profiley',
                                    '--yprofile',
                                    '--profile-y',
                                    '--y-profile',
                                    '--profiley-horizontal',
                                    '--profile-y-horizontal',
                                    '--yprofile-horizontal',
                                    '--y-profile-horizontal',
                                    '--profiley-h',
                                    '--profileyh',
                                    '--profile-y-h',
                                    '--yprofile-h',
                                    '--y-profile-h',

                                    '--image',
                                    '--no-im-rgb',
                                    '-no-im-rgb',
                                    '--no-rgb',
                                    '--im-geo',
                                    '--image-geo',
                                    '--geo',
                                    '--im-rgb',
                                    '--image-rgb',
                                    '--imshow',
                                    '--animate',
                                    '--animated',

                                    '--multiplot',
                                    '--multi-plot',
                                    '--barplot',
                                    '--bar-plot',
                                    '--table',

                                    '-h',
                                    '--help']
        self._displayPlotSinglePar = \
            plant.get_args_from_argparser(plant.get_parser(),
                                          store_true_action=True,
                                          store_false_action=True,
                                          store_action=False)
        self._multi_parameters_list = [['--extent', 4]]
        self._dataTypes = ['-b1', '-byte', '-b2', '-byte2', '-i1',
                           '-integer*1', '-i2', '-integer*2', '-i4',
                           '-integer*4', '-r4', '-real*4',
                           '-r8', '-real*8',
                           '-c2', '-complex*2', '-c8', '-complex*8',
                           '-c8mag', '-cmag', '-c8pha', '-cpha', '-c2mag',
                           '-c2pha', '-rmg', '-vfmt', '-val_frmt']

        try:
            from iscesys.ImageApi import DataAccessor as DA
            dtype_long_size = DA.getTypeSize('LONG')
        except BaseException:
            dtype_long_size = 8

        self._mapDataType = {'xml': {'BYTE': '-b1', 'SHORT': '-i2',
                                     'CFLOAT': '-c8', 'FLOAT': '-r4',
                                     'INT': '-i4', 'LONG': '-i' +
                                     str(dtype_long_size),
                                     'DOUBLE': '-r8', 'CDOUBLE': '-c16'},
                             'rsc': {'cpx': '-c8', 'fr': '-r4', 'rmg': '-r4',
                                     'scor': '-r4', 'dem': '-i2', 'byt': '-i1',
                                     'amp': '-r4', 'unw': '-r4', 'msk': '',
                                     'cor': ''}}

        self._metaExtensions = ['.xml', '.rsc', '.hdr']
        self._ext = {}
        self._ext['fr'] = ['fr', 'tec', 'phase']
        self._ext['cpx'] = ['slc', 'int', 'flat', 'mph', 'cpx']
        self._ext['rmg'] = ['hgt', 'hgt_holes', 'rect', 'rmg']
        self._ext['scor'] = ['scor']
        self._ext['dem'] = ['dem', 'dte', 'dtm']
        self._ext['unw'] = ['unw']
        self._ext['cor'] = ['cor']
        self._ext['msk'] = ['msk']
        self._ext['byt'] = ['byt', 'flg']
        self._ext['amp'] = ['amp']
        self._ext['bil'] = ['bil']
        self._ext['bip'] = ['bip']
        self._ext['bsq'] = ['bsq']

        self._startLat = []
        self._deltaLat = []
        self._startLon = []
        self._deltaLon = []
        self._length = []
        self._width = []
        self._names = []


def main(argv=None):
    with plant.PlantLogger():
        self_obj = Display(argv)
        return self_obj.run(argv)


if __name__ == '__main__':
    main()
