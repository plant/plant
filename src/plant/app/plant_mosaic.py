#!/usr/bin/env python3
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Gustavo H. X. Shiroma, Marco Lavalle
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import time
import sys
import numpy as np
from os import path, makedirs

import copy
import plant

FLAG_REPROJECT_WITH_GDALWARP = True


def get_parser():

    descr = ('Mosaic or geographically relocate input data.'
             ' If a single input is used, the input can be'
             ' relocated using one of the bounding box/step'
             ' options. If multiple inputs are used'
             ' the files are mosaicked to a single-output.')

    epilog = ''
    parser = plant.argparse(epilog=epilog,
                            description=descr,
                            geo=1,
                            input_images=2,
                            default_geo_input_options=1,
                            default_geo_output_options=1,
                            output_file=1,
                            output_dir=1,
                            separate=1,
                            default_options=1)

    parser.add_argument('--factor', '--factors', '--multiplier',
                        '--multipliers',
                        dest='factors',
                        type=float,
                        nargs='+',
                        help='Factors to be applied '
                        'on inputs')

    parser.add_argument('--transition',
                        dest='transition',
                        type=float,
                        help='Defines transition width in overlapping'
                        'areas (default: %(default)s).',
                        default=0)
    parser.add_argument('--interp',
                        '--interp-method',
                        '--resampl',
                        dest='interp_method',
                        type=str,
                        help='Interpolation method '
                        'for geocoding (Options: near, bilinear, cubic, '
                        'cubicspline, lanczos, '
                        'average, mode, max, min, med, q1, q3)')

    parser.add_argument('--srtm',
                        dest='srtm',
                        action='store_true',
                        help='Use SRTM conventions (NULL etc.)')

    parser.add_argument('--any-null', '--any-nan',
                        dest='any_null',
                        action='store_true',
                        help='Accumulate NaNs to'
                        'the output')
    parser.add_argument('--no-average',
                        dest='no_average',
                        action='store_true',
                        help='Do not average in overlapping '
                        'area (choose the first data).')
    parser.add_argument('--src-no-geotransform',
                        dest='no_geotransform_src',
                        action='store_true',
                        help='Set GDAL keyword '
                        '-to SRC_METHOD=NO_GEOTRANSFORM.')
    parser.add_argument('--dst-no-geotransform',
                        dest='no_geotransform_dst',
                        action='store_true',
                        help='Set GDAL keyword '
                        '-to DST_METHOD=NO_GEOTRANSFORM.')

    return parser


class PlantMosaic(plant.PlantScript):

    def __init__(self, parser, argv=None):

        self.projection = None
        super().__init__(parser, argv)
        self.populate_parameters()

    def populate_parameters(self):

        if self.srtm and self.in_null is None:
            self.in_null = -32768

        if 'topo_dir' not in self.__dict__.keys():
            self.topo_dir = None
        if 'backward_geocoding_x' not in self.__dict__.keys():
            self.backward_geocoding_x = None
        if 'backward_geocoding_y' not in self.__dict__.keys():
            self.backward_geocoding_y = None

        if not self.output_file and not self.output_dir:
            self.parser.print_usage()
            self.print('ERROR please select an output file or directory')
            sys.exit(1)

        if path.isdir(self.output_file) or self.output_file.endswith('/'):
            self.output_file = ''

        output_projection_ref_file = None
        if self.bbox_file:
            geo_file = self.bbox_file
            output_projection_ref_file = geo_file
        elif self.backward_geocoding_x:
            geo_file = [self.backward_geocoding_x]
            output_projection_ref_file = geo_file[0]
        elif self.topo_dir:
            geo_file = None
            output_projection_ref_file = None
        else:
            geo_file = self.input_images
            output_projection_ref_file = geo_file

        if self.topo_dir:
            print('topo directory:', self.topo_dir)

        if (self.output_projection is None and
                self.projection is not None):
            self.output_projection = self.projection
        elif (self.output_projection is None and
              isinstance(output_projection_ref_file, list)):
            projection = None
            for f in output_projection_ref_file:
                projection = plant.read_image(f, only_header=True).projection
                if projection is not None:
                    break
            self.output_projection = projection
        elif (self.output_projection is None and
              output_projection_ref_file):
            projection = plant.read_image(output_projection_ref_file,
                                          only_header=True).projection
            self.output_projection = projection

        if not self.output_projection:
            self.output_projection = plant.PROJECTION_REF

        self.plant_geogrid_obj = plant.get_coordinates(
            bbox_file=geo_file,
            verbose=False,

            geo_center=self.geo_center,
            geo_search_str=self.geo_search_str,
            bbox=self.bbox,
            step=self.step,
            step_x=self.step_x,
            step_y=self.step_y,
            step_m=self.step_m,
            step_m_x=self.step_m_x,
            step_m_y=self.step_m_y,
            projection=self.output_projection,
            bbox_topo=self.bbox_topo,

            plant_transform_obj=self.plant_transform_obj)

        self.plant_geogrid_obj.projection = self.output_projection

        self.use_gdal_bbox = (
            not self.plant_geogrid_obj.has_valid_coordinates() and
            len(self.input_images) <= 1 and not self.backward_geocoding_x)
        self.use_gdal_step = ((plant.isnan(self.plant_geogrid_obj.step_x) or
                               plant.isnan(self.plant_geogrid_obj.step_y)) and
                              len(self.input_images) <= 1 and
                              not self.backward_geocoding_x)
        self.plant_geogrid_obj.print()

        geogrid_width = self.plant_geogrid_obj.width
        geogrid_length = self.plant_geogrid_obj.length

        if self.factors is None:
            self.factors = 1

        if self.use_gdal_bbox or self.use_gdal_step:

            return
        if self.output_dtype:
            max_datatype_size = plant.get_dtype_size(self.output_dtype)
        else:
            max_datatype_size = 0
            for current_file in self.input_images:
                image_obj = self.read_image(current_file,
                                            verbose=False,
                                            only_header=True)
                if image_obj is not None:
                    current_dtype_size = plant.get_dtype_size(image_obj.dtype)

                    if current_dtype_size > max_datatype_size:
                        max_datatype_size = current_dtype_size
            if max_datatype_size == 0:
                max_datatype_size = 8
        if (geogrid_length is None or not np.isfinite(geogrid_length) or
                geogrid_width is None or not np.isfinite(geogrid_width)):
            return

        print('output width:', geogrid_width)
        print('output length:', geogrid_length)
        data_size = geogrid_width * geogrid_length * max_datatype_size
        if data_size > 5e8:
            while not self.force:
                self.print('WARNING this file seems to be too large (%s) '
                           'for step=(%f,%f) and given coordinates. '
                           % (plant.get_file_size_string(data_size),
                              self.plant_geogrid_obj.step_y,
                              self.plant_geogrid_obj.step_x))
                res = plant.get_keys('Continue anyway? ([y]es/[n]o) ')
                if res.startswith('y'):
                    break
                elif res.startswith('n'):
                    self.print('operation cancelled by the user.', 1)
                    sys.exit(0)
        else:

            self.print('expected output size: %s' %
                       plant.get_file_size_string(data_size))

    def run(self):

        self_mosaic_method = self.mosaic_geo_gdalwarp
        self.print('transition width: ' + str(self.transition))
        if self.separate:
            self_dict = {}
            prevent_copy_list = ['parser']
            for key, value in self.__dict__.items():
                if key in prevent_copy_list:
                    continue
                self_dict[key] = copy.deepcopy(value)
            input_images = self.input_images
            image_obj = None
            current_band = 0
            for i, current_file in enumerate(input_images):
                if i != 0:
                    plant.plant_config.logger_obj.flush_temporary_files()
                    self.__dict__.update(self_dict)
                self.input_images = [current_file]
                if self.output_dir_orig is None:
                    temp_file = plant.get_temporary_file(
                        current_file + '.geo_sep_' + str(i),
                        dirname=plant.dirname(self.output_file),
                        append=True)
                    self.output_file = temp_file
                else:
                    self.output_file = self.output_files[i]
                if (self.output_skip_if_existent and
                        plant.isfile(self.output_file)):
                    print(f'INFO output file {self.output_file}'
                          ' already exist, skipping execution..')
                    continue
                if image_obj is None:
                    image_obj = self_mosaic_method()
                    current_band = image_obj.nbands
                else:
                    current_image_obj = self_mosaic_method()
                    for band in range(current_image_obj.nbands):
                        image = current_image_obj.get_image(band=band)
                        image_obj.set_image(image, band=current_band)
                        current_band += 1
            plant.plant_config.logger_obj.flush_temporary_files()
            self.__dict__.update(self_dict)
            if self.output_dir_orig is None:
                self.save_image(image_obj, self.output_file)
            return image_obj
        if not self.output_file:
            self.output_file = path.join(self.output_dir, 'mosaic.tif')
        return self_mosaic_method()

    def mosaic_geo_gdalwarp(self):
        if not path.isdir(plant.dirname(self.output_file)):
            makedirs(plant.dirname(self.output_file))
        gdal_filelist = []
        bands_list = []
        self.input_format_orig = self.input_format
        self.include_alpha_band = False
        for i, current_file in enumerate(self.input_images):
            self.print(f'## file {i+1}: {current_file}')
            with plant.PlantIndent():
                self._prepare_file(
                    i, current_file, gdal_filelist,
                    bands_list,
                    flag_exit_if_error=len(self.input_images) == 1)

        self.plant_transform_obj = None

        if len(gdal_filelist) == 0:
            if self.input_images is None:
                self.input_images = 'None'
            if len(self.input_images) >= 1:
                self.print('ERROR invalid input(s): ' +
                           ', '.join(self.input_images))
            else:
                self.print('ERROR input file(s) not found')
            return

        geo_file = plant.get_temporary_file(
            path.basename(self.output_file + '.geo_temp_3'),
            dirname=plant.dirname(self.output_file),
            append=True)
        geo_file = self.gdalwarp_binding(gdal_filelist,
                                         geo_file,
                                         include_mask_band=False)
        gdal_filelist = [geo_file]

        if self.use_gdal_bbox or self.use_gdal_step:
            data_obj = self.read_image(gdal_filelist[0])
            bands = data_obj.nbands
            self.geotransform = data_obj.geotransform
            self.projection = data_obj.projection
            if self.geotransform is not None:
                self.plant_geogrid_obj = plant.get_coordinates(
                    geotransform=self.geotransform,
                    projection=self.projection,
                    verbose=True)

            if bands == 1 and self.include_alpha_band:
                data_obj.nbands = 1
                self.save_image(data_obj, self.output_file)
                return data_obj

            if self.use_gdal_step and self.geotransform is not None:
                self.step_y = self.plant_geogrid_obj.step_y
                self.step_x = self.plant_geogrid_obj.step_x

        data_obj = self.read_image(gdal_filelist[0],
                                   input_format=plant.DEFAULT_GDAL_FORMAT)

        if self.include_alpha_band:
            data_obj.nbands = data_obj.nbands - 1
        self.geotransform = data_obj.geotransform
        self.projection = data_obj.projection

        plant.save_image(data_obj, self.output_file,
                         in_null=self.in_null,
                         out_null=self.out_null,
                         force=self.force,
                         verbose=self.verbose)
        return data_obj

    def _prepare_file(self, i, current_file, gdal_filelist, bands_list,
                      flag_exit_if_error=False):
        self.input_format = self.input_format_orig
        band = None
        flag_test_gdal_open = plant.test_gdal_open(current_file,
                                                   geo=True)
        if ((not flag_test_gdal_open) and
                (not plant.test_other_drivers(current_file)) and
                plant.IMAGE_NAME_SEPARATOR in current_file):
            current_file_splitted = \
                current_file.split(plant.IMAGE_NAME_SEPARATOR)
            current_file = current_file_splitted[0]
            band = int(current_file_splitted[1])
        suffix = str(time.time()) + '_' + str(i)
        if current_file.endswith('.xml'):
            return
        if len(self.input_images) > 1:
            self.print('current file: ' + path.basename(current_file))

        temp_file = plant.get_temporary_file(
            current_file + '.geo_temp_' + suffix,
            dirname=plant.dirname(self.output_file),
            append=True)

        if self.backward_geocoding_x or self.backward_geocoding_y:
            with plant.PlantIndent():
                if not self.backward_geocoding_x:
                    self.print('ERROR backward geocoding image X not provided'
                               f' for {current_file}')
                if not self.backward_geocoding_y:
                    self.print('ERROR backward geocoding image Y not provided'
                               f' for {current_file}')
                self.print('backward geocoding image X:'
                           f' {self.backward_geocoding_x}')
                image_x_obj = plant.read_image(self.backward_geocoding_x)
                plant.apply_crop(image_x_obj,
                                 plant_transform_obj=self.plant_transform_obj,
                                 verbose=False)
                self.print('backward geocoding image y:'
                           f' {self.backward_geocoding_y}')
                image_y_obj = plant.read_image(self.backward_geocoding_y)
                plant.apply_crop(image_y_obj,
                                 plant_transform_obj=self.plant_transform_obj,
                                 verbose=False)
                image_obj = image_x_obj.copy()
                image_sr_obj = plant.read_image(current_file)
                valid_ind = np.where(
                    np.logical_and(image_y_obj.image != 55537,
                                   image_x_obj.image != 55537))
                x_ind = image_x_obj.image[valid_ind]
                y_ind = image_y_obj.image[valid_ind]
                for b in range(image_sr_obj.nbands):
                    image = image_sr_obj.get_image(band=b)
                    new_image = np.full((image_x_obj.shape), np.nan,
                                        dtype=image.dtype)
                    new_image[valid_ind] = image[y_ind, x_ind]
                    image_obj.set_image(new_image, band=b)

                plant.apply_null(image_obj,
                                 in_null=self.in_null,
                                 out_null=self.out_null)
                plant.apply_mask(image_obj,
                                 plant_transform_obj=self.plant_transform_obj,
                                 verbose=self.verbose,
                                 force=self.force)
        else:
            image_obj = self.read_image(current_file,
                                        verbose=False,
                                        only_header=True)
            if (image_obj.geotransform is None and
                    image_obj.gcp_projection is None and
                    not self.topo_dir):
                self.print(f'ERROR georeference of {current_file} not found')
                return
        flag_apply_transformation = \
            image_obj.plant_transform_obj.flag_apply_transformation()

        if not self.projection and image_obj.projection:
            self.projection = image_obj.projection

        if self.backward_geocoding_x and self.backward_geocoding_y:
            self.save_image(image_obj,
                            temp_file,
                            output_format=plant.DEFAULT_GDAL_FORMAT,
                            flag_temporary=True,
                            force=True)

            current_file = temp_file

        elif self.topo_dir:
            current_file = self._make_gdal_readable(
                current_file,
                image_obj,
                flag_apply_transformation,
                flag_test_gdal_open,
                temp_file,
                band)
            self.print('assigning geolocation to image...')

            plant_transform_topo_obj = plant.PlantTransform(
                select_row=self.plant_transform_obj.select_row,
                select_col=self.plant_transform_obj.select_col,
                srcwin=self.plant_transform_obj.srcwin,
                polygon=self.plant_transform_obj.polygon,
                length_orig=self.plant_transform_obj._length_orig,
                width_orig=self.plant_transform_obj._width_orig)

            with plant.PlantIndent():
                current_file = plant.assign_georeferences(
                    image_obj,
                    topo_dir=self.topo_dir,
                    lat_file=self.lat_file,
                    lon_file=self.lon_file,
                    output_file=current_file,
                    plant_transform_obj=plant_transform_topo_obj,

                    force=self.force,
                    verbose=self.verbose,
                    suffix=suffix)
                plant.append_temporary_file(current_file)
                self.print('geolocated file: %s' % current_file)

        elif (not self.test_overlap(
                image_2_obj=image_obj) and
                not image_obj.gcp_list and flag_exit_if_error):

            plant.print_geoinformation(bbox_file=current_file)

            self.print('ERROR file outside limits: %s' % current_file)
            return

        elif (not self.test_overlap(
                image_2_obj=image_obj) and
                not image_obj.gcp_list):
            self.print('WARNING file outside limits: %s. Skipping'
                       % current_file)
            return

        elif image_obj.gcp_list:
            self.print('file %s has GCPs' % current_file)
            pass
        else:
            current_file = self._make_gdal_readable(
                current_file,
                image_obj,
                flag_apply_transformation,
                flag_test_gdal_open,
                temp_file,
                band)

        geo_file = plant.get_temporary_file(
            current_file + '.geo_temp_2',
            dirname=plant.dirname(self.output_file))
        if (band is not None and not self.topo_dir):
            image_obj = self.read_image(current_file, band=band)

            plant.append_temporary_file(geo_file)
            self.save_image(image_obj,
                            geo_file,
                            flag_temporary=True,
                            output_format=plant.DEFAULT_GDAL_FORMAT,
                            force=True)
            image_obj = 0
            projected_file = self._fix_projection(geo_file)
            gdal_filelist.append(projected_file)
        elif band is not None:
            print('ERROR cannot geocode images'
                  ' with multiple bands')
            return

        projected_file = self._fix_projection(current_file)
        gdal_filelist.append(projected_file)

    def _make_gdal_readable(self,
                            current_file,
                            image_obj,
                            flag_apply_transformation,
                            flag_test_gdal_open,
                            temp_file,
                            band):

        if (plant.test_for_gdal_convention_error(current_file) or
                flag_apply_transformation or
                image_obj.input_key is not None or

                (not flag_test_gdal_open and
                 not plant.is_isce_image(current_file))):
            if plant.test_for_gdal_convention_error(current_file):
                self.print('updating image to follow GDAL '
                           'convention..')
            else:
                self.print('updating image for gdalwarp..')
            with plant.PlantIndent():
                if band is None:
                    image_obj = self.read_image(current_file)
                else:
                    image_obj = self.read_image(current_file,
                                                band=band)
                    band = None
                self.save_image(image_obj,
                                temp_file,
                                flag_temporary=True,
                                output_format=plant.DEFAULT_GDAL_FORMAT,
                                force=True)

            current_file = temp_file

        elif (not flag_test_gdal_open and
              plant.is_isce_image(current_file)):
            self.print('rendering VRT file for ISCE image..')
            current_file = self.render_vrt(current_file,
                                           temp_file + '.vrt',

                                           verbose=self.verbose)
            plant.append_temporary_file(current_file)
            self.input_format = 'VRT'
        return current_file

    def _fix_projection(self, input_file,
                        projection=None,
                        output_projection=None):

        return input_file

    def _get_gdalwarp_grid(self):
        outputBounds = None
        xRes = None
        yRes = None

        if not self.use_gdal_bbox:
            outputBounds = self.plant_geogrid_obj.gdal_output_bounds

        if (not self.use_gdal_step and
                plant.isvalid(self.plant_geogrid_obj.step_y)):
            yRes = self.plant_geogrid_obj.step_y

        if (not self.use_gdal_step and
                plant.isvalid(self.plant_geogrid_obj.step_x)):
            xRes = self.plant_geogrid_obj.step_x

        return outputBounds, yRes, xRes

    def gdalwarp_binding(self, input_files,
                         output_file,
                         topo_dir=None,
                         include_mask_band=False):

        flag_ctable = False
        if not isinstance(input_files, list):
            input_files_loop = [input_files]
        else:
            input_files_loop = input_files
        flag_all_int_image = True
        for current_file in input_files_loop:
            image_obj = plant.read_image(current_file,
                                         only_header=True)
            flag_ctable = flag_ctable or image_obj.ctable is not None
            dtype = plant.get_dtype_name(image_obj.dtype).lower()
            if 'int' not in dtype:
                flag_all_int_image = False

        output_format = plant.DEFAULT_GDAL_FORMAT

        outputBounds, yRes, xRes = self._get_gdalwarp_grid()

        kwargs = {}

        dstSRS = plant.get_srs_from_projection(self.output_projection)
        if dstSRS:
            kwargs['dstSRS'] = dstSRS

        dstAlpha = include_mask_band

        if self.output_dtype is not None:
            kwargs['outputType'] = plant.get_gdal_dtype_from_np(
                self.output_dtype)

        if self.interp_method is None and flag_ctable:
            resampleAlg = 'near'
        elif self.interp_method is None:
            resampleAlg = 'cubic'

        else:
            resampleAlg = self.interp_method

        if self.in_null is not None:
            kwargs['srcNodata'] = self.in_null

        if self.out_null is not None:
            kwargs['dstNodata'] = self.out_null
        elif self.output_dtype and 'int' in plant.get_dtype_name(
                self.output_dtype).lower():
            int_null = plant.get_int_nan_by_dtype(
                self.output_dtype)
            print(f'updating no data to: {self.output_dtype}')
            kwargs['dstNodata'] = int_null
        elif self.output_dtype or not flag_all_int_image:
            kwargs['dstNodata'] = 'nan'

        geoloc = bool(topo_dir)

        transformerOptions = []
        if self.no_geotransform_src:
            transformerOptions.append('SRC_METHOD=NO_GEOTRANSFORM')
        if self.no_geotransform_dst:
            transformerOptions.append('DST_METHOD=NO_GEOTRANSFORM')
        error_message = None
        try:
            image_obj = plant.Warp(output_file,
                                   input_files,

                                   format=output_format,
                                   outputBounds=outputBounds,
                                   xRes=xRes,
                                   yRes=yRes,
                                   dstAlpha=dstAlpha,

                                   resampleAlg=resampleAlg,

                                   geoloc=geoloc,
                                   transformerOptions=transformerOptions,
                                   **kwargs)
        except RuntimeError:
            error_message = plant.get_error_message()
            flag_update_output_bounds = outputBounds is None and self.topo_dir
        if error_message is not None and flag_update_output_bounds:
            plant_geogrid_obj = plant.PlantGeogrid(
                plant_geogrid_obj=self.plant_geogrid_obj,

                verbose=True,

            )
            plant_geogrid_obj.projection = self.output_projection
            outputBounds = plant_geogrid_obj.gdal_output_bounds
            image_obj = plant.Warp(output_file,
                                   input_files,

                                   format=output_format,
                                   outputBounds=outputBounds,
                                   xRes=xRes,
                                   yRes=yRes,

                                   dstAlpha=dstAlpha,

                                   resampleAlg=resampleAlg,

                                   errorThreshold=0,

                                   geoloc=geoloc,
                                   transformerOptions=transformerOptions,
                                   **kwargs)
        elif error_message is not None:
            print(f'ERROR {error_message}')
            return

        return output_file


def main(argv=None):
    with plant.PlantLogger():
        parser = get_parser()
        self_obj = PlantMosaic(parser, argv)
        ret = self_obj.run()
        return ret


if __name__ == '__main__':
    main()
