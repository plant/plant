#!/usr/bin/env python3
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Gustavo H. X. Shiroma, Marco Lavalle
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import plant
import time
import glob


def get_parser():

    descr = ('Execute a command (e.g. bash script) using plant.execute().')
    epilog = ('plant_execute <command>')
    parser = plant.argparse(epilog=epilog,
                            description=descr,
                            flag_debug=1,
                            flag_force=1,

                            output_file=1)
    parser.add_argument('command',
                        type=str,
                        nargs='*',
                        action='extend',
                        help='Input command')
    parser.add_argument('-i',
                        dest='command',
                        type=str,
                        nargs='*',
                        action='extend',
                        help='Input files')
    parser.add_argument('-a',
                        '--var-a',
                        dest='var_a',
                        type=str,
                        help='Variable a')
    parser.add_argument('--var-f',
                        '--files',
                        dest='var_f',
                        type=str,
                        help='File list (variable f)')
    parser.add_argument('--time',
                        dest='time',
                        action='store_true',
                        help='Return/save elapsed time.')

    parser.add_argument('-n',
                        '--number-of-executions',
                        '--repeat',
                        dest='repeat',
                        default=1,
                        type=int,
                        help='Number of executions.')

    return parser


class PlantExecute(plant.PlantScript):

    def __init__(self, parser, argv=None):

        super().__init__(parser, argv)

    def run(self):

        ret_list = []
        if self.var_a is None:
            var_a_iter = [None]
        else:
            var_a_iter = self.read_image(self.var_a).image.ravel().tolist()
        if self.var_f is None:
            var_f_iter = ['']
        else:
            var_f_iter = glob.glob(self.var_f)
        for _ in range(self.repeat):
            for var_f in var_f_iter:
                for var_a in var_a_iter:
                    if var_a is not None:
                        var_a_str = str(int(var_a))
                    for command in self.command:
                        if var_a is not None:
                            command = command.replace('{a}', var_a_str)
                            command = command.replace('$a', var_a_str)
                        if var_f is not None:
                            command = command.replace('{f}', var_f)
                            command = command.replace('$f', var_f)
                        if self.time:
                            time_before = time.time()
                        ret = plant.execute(command)
                        if self.time:
                            elapsed_time = time.time() - time_before
                            self.print(f'elapsed time: {elapsed_time}')
                            ret_list.append(float(elapsed_time))
                            continue
                        ret_list.append(ret)
        if len(ret_list) == 1:
            out_image_obj = ret_list[0]
            if self.output_file:
                self.save_image(out_image_obj, self.output_file)
            return out_image_obj
        if self.output_file:

            self.save_image(ret_list, self.output_file)
        return ret_list


def main(argv=None):
    with plant.PlantLogger():
        parser = get_parser()
        self_obj = PlantExecute(parser, argv)
        ret = self_obj.run()
        return ret


if __name__ == '__main__':
    main()
