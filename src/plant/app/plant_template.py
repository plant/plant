#!/usr/bin/env python3
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Gustavo H. X. Shiroma, Marco Lavalle
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import plant


def get_parser():

    descr = ('This is a template file for algorithm developers'
             ' to generate custom PLAnT tools using the framework. For'
             ' more information or instructions, please'
             ' open this script in a text editor'
             ' of your preference.')
    epilog = ''
    parser = plant.argparse(epilog=epilog,
                            description=descr,
                            input_images=1,
                            default_options=1,
                            multilook=1,
                            output_dir=1,

                            separate=1,
                            output_file=1)

    parser.add_argument('--my-float',
                        dest='my_float_parameter',
                        type=float,

                        help='Insert here your float')

    parser.add_argument('--set-bool',
                        dest='my_bool_parameter',
                        action='store_true',

                        help='Insert here your boolean parameter')

    return parser


class PlantTemplate(plant.PlantScript):

    def __init__(self, parser, argv=None):

        super().__init__(parser, argv)

    def run(self):

        out_image_obj = None

        print('')
        print('********************************')
        print(f'PLAnT Template - Hello World!')
        print('********************************')
        print('')
        print('## my_float (--my-float):', self.my_float_parameter)
        print('## my_bool (--set-bool):', self.my_bool_parameter)
        print('')
        print('********************************')
        print('')

        for current_file in self.input_images:

            image_obj = self.read_image(current_file, verbose=False)
            out_image_obj = image_obj

            print('## file: ', current_file)
            with plant.PlantIndent():
                print('format: ', image_obj.file_format)
                print('nbands: ', image_obj.nbands)
                print('width: ', image_obj.width)
                print('length: ', image_obj.length)
                print('content: ', image_obj.image)
                print('')

        import time
        max_count = 200
        print()
        print()
        print('counter 1')
        for i in plant.PrintProgress(range(max_count)):
            time.sleep(0.01)
        plant.print_progress(max_count, max_count)
        print()
        print()
        print('counter 1')
        for i in range(max_count):
            plant.print_progress(i, max_count)
            time.sleep(0.01)
        plant.print_progress(max_count, max_count)

        print()
        print()
        print('counter 2')
        for i in range(4):
            for j in range(max_count):
                plant.print_progress(i, 4,
                                     j, max_count)
                time.sleep(0.002)
        plant.print_progress(max_count, max_count)

        print()
        print()
        print('counter 3')
        with plant.PrintProgress(max_count) as progress_obj:
            for i in range(max_count):
                progress_obj.print_progress(i)
                time.sleep(0.01)

        print()
        print()
        print('counter 4')
        with plant.PrintProgress(10,
                                 sub_total=max_count) as progress_obj:
            for i in range(10):
                for j in range(max_count):
                    progress_obj.print_progress(i, j)
                    time.sleep(0.001)

        return out_image_obj


def main(argv=None):
    with plant.PlantLogger():
        parser = get_parser()
        self_obj = PlantTemplate(parser, argv)
        ret = self_obj.run()
        return ret


if __name__ == '__main__':
    main()
