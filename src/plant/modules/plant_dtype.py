#!/usr/bin/env python3
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Gustavo H. X. Shiroma, Marco Lavalle
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import numpy as np
from osgeo import gdal
import h5py
import plant


def read_c4_dataset_as_c8(ds: h5py.Dataset, key=np.s_[...]):

    complex32 = np.dtype([('r', np.float16), ('i', np.float16)])
    z = ds.astype(complex32)[key]
    complex64 = np.dtype([("r", np.float32), ("i", np.float32)])
    return z.astype(complex64).view(np.complex64)


def get_dtype_name(data):
    class_name = data.__class__.__name__
    if (class_name == 'list'):
        return get_dtype_name(data[0])
    elif (class_name == 'memmap'):
        return data.dtype.name
    elif (class_name == 'type'):
        return str(data.__name__)
    elif ((class_name == 'ndarray' or
           class_name == 'MaskedArray') and
          data.dtype.name == 'object'):
        return get_dtype_name(data.item(0))
    elif (class_name == 'ndarray' or
          class_name == 'MaskedArray'):
        return data.dtype.name
    elif class_name == 'str':

        return data
    elif class_name == 'dtype':
        return str(data)

    else:
        return class_name


def get_gdal_dtype_from_np(dtype):

    if isinstance(dtype, str):
        dtype = dtype.lower()
    else:
        dtype = np.dtype(dtype).name.lower()
    if 'byte' in dtype:
        return gdal.GDT_Byte
    elif 'float64' in dtype:
        return gdal.GDT_Float64
    elif 'float' in dtype:
        return gdal.GDT_Float32
    elif 'byte' in dtype or 'int8' in dtype:
        return gdal.GDT_Byte
    elif 'uint16' in dtype:
        return gdal.GDT_UInt16
    elif 'uint32' in dtype:
        return gdal.GDT_UInt32
    elif 'uint64' in dtype:
        try:
            return gdal.GDT_UInt64
        except AttributeError:
            return gdal.GDT_Uint32
    elif 'int16' in dtype:
        return gdal.GDT_Int16
    elif 'int32' in dtype:
        return gdal.GDT_Int32
    elif 'int64' in dtype:
        try:
            return gdal.GDT_Int64
        except AttributeError:
            return gdal.GDT_Int32
    elif 'complex128' in dtype:
        return gdal.GDT_CFloat64
    elif 'complex' in dtype:
        return gdal.GDT_CFloat32

    return gdal.GDT_Unknown


def apply_srf_dtype(image):
    dtype = get_dtype_name(image)
    if 'int' in dtype:
        new_dtype = np.byte
    elif 'complex' in dtype:
        new_dtype = np.complex64
    else:
        new_dtype = np.float32
    image = np.asarray(image, dtype=new_dtype)
    return image


def get_np_dtype_from_srf(dtype):
    for srf_dtype, np_dtype in plant.SRF_DTYPE:
        if dtype == srf_dtype:
            return np_dtype

    return


def get_srf_dtype(dtype):
    dtype = dtype.lower()
    if 'int' in dtype or 'byte' in dtype:
        return 1
    if 'complex' in dtype:
        return 30
    return 20


def get_np_dtype_from_gdal(dtype):

    if dtype == gdal.GDT_Byte:
        return np.uint8
    elif dtype == gdal.GDT_Float64:
        return np.float64
    elif dtype == gdal.GDT_Float32:
        return np.float32
    elif dtype == gdal.GDT_UInt16:
        return np.uint16
    elif dtype == gdal.GDT_UInt32:
        return np.uint32
    elif dtype == gdal.GDT_Int16:
        return np.int16
    elif dtype == gdal.GDT_Int32:
        return np.int32
    elif dtype == gdal.GDT_CInt16:

        print('WARNING Casting unsupported format CInt16 to CFloat32')
        return np.complex64
    elif dtype == gdal.GDT_CInt32:

        print('WARNING Casting unsupported format CInt32 to CFloat32')
        return np.complex64
    elif dtype == gdal.GDT_CFloat64:
        return np.complex128
    elif dtype == gdal.GDT_CFloat32:
        return np.complex64

    try:
        if dtype == gdal.GDT_Int64:
            return np.int64
        elif dtype == gdal.GDT_UInt64:
            return np.uint64
    except AttributeError:
        pass
    return np.float32


def get_vrt_dtype(dtype):

    dtype_upper = get_isce_dtype(dtype).upper()

    if 'BYTE' in dtype_upper or 'CHAR' in dtype_upper:
        return 'Byte'
    elif 'UINT16' in dtype_upper:
        return 'UInt16'
    elif ('SHORT' in dtype_upper or
          'CIQBYTE' in dtype_upper or
          'INT16' in dtype_upper):
        return 'Int16'
    elif 'UINT64' in dtype_upper:
        return 'UInt64'
    elif 'UINT' in dtype_upper:
        return 'UInt32'
    elif 'INT64' in dtype_upper or 'LONG' in dtype_upper:
        return 'Int64'
    elif 'INT' in dtype_upper:
        return 'Int32'
    elif 'FLOAT' in dtype_upper:
        return 'Float32'
    elif 'DOUBLE' in dtype_upper:
        return 'Float64'
    elif 'CFLOAT' in dtype_upper:
        return 'CFloat32'
    elif 'CDOUBLE' in dtype_upper:
        return 'CFloat64'
    else:
        return


def get_isce_dtype(dtype):

    if dtype is None:
        return
    if not isinstance(dtype, str):
        dtype = np.dtype(dtype).name
    dtype_lower = dtype.lower()
    if dtype_lower == 'float32':
        return 'FLOAT'
    elif dtype_lower == 'float64':
        return 'DOUBLE'
    elif dtype_lower == 'byte' or 'int8' in dtype_lower:
        return 'BYTE'
    elif 'int16' in dtype_lower:
        return 'SHORT'
    elif 'int64' in dtype_lower:
        return 'LONG'
    elif 'int' in dtype_lower:
        return 'INT'
    elif (dtype_lower == 'complex64' or
          dtype_lower == 'complex'):
        return 'CFLOAT'
    elif dtype_lower == 'complex128':
        return 'CDOUBLE'
    else:
        return dtype


def get_dtype_size(input_data_type):

    if input_data_type is None:
        return

    if not isinstance(input_data_type, str):
        try:
            dtype_size = np.dtype(input_data_type).itemsize
            return dtype_size
        except BaseException:
            pass
    multiplier = 1
    if isinstance(input_data_type, list):
        dtype_upper = input_data_type[0].upper()
    else:
        if '[' in input_data_type and ']' in input_data_type:
            value = input_data_type.split('[')[1].split(']')[0]
            if plant.isnumeric(value):
                multiplier = int(value)
        dtype_upper = input_data_type.upper()

    if 'CFLOAT' in dtype_upper:
        return multiplier * 8
    elif 'CDOUBLE' in dtype_upper:
        return multiplier * 16
    elif 'FLOAT64' in dtype_upper:
        return multiplier * 8
    elif 'FLOAT' in dtype_upper:
        return multiplier * 4
    elif 'DOUBLE' in dtype_upper:
        return multiplier * 8
    elif 'BYTE' in dtype_upper or 'CHAR' in dtype_upper:
        return multiplier * 1
    elif 'SHORT' in dtype_upper:
        return multiplier * 2

    elif 'LONG' in dtype_upper:
        return multiplier * 8
    elif 'INT16' in dtype_upper:
        return multiplier * 2
    elif 'INT32' in dtype_upper:
        return multiplier * 4
    elif 'INT64' in dtype_upper:
        return multiplier * 8
    elif 'INT' in dtype_upper:
        return multiplier * 4
    else:
        dtype = get_isce_dtype(input_data_type)
        if dtype.upper() != dtype_upper:
            return multiplier * get_dtype_size(dtype)

        return


def get_int_nan_by_dtype(dtype):

    if not (isinstance(dtype, str)):
        dtype = plant.get_dtype_name(dtype)

    if dtype == 'uint8' or dtype == 'byte':
        return 255

    if dtype == 'int8':
        return -128

    if dtype == 'uint16':
        return 65535

    return -32768


def get_int_nan(image):
    return plant.get_int_nan_by_dtype(plant.get_dtype_name(image))


def get_mdx_dtype(dtype):

    if not isinstance(dtype, str):
        dtype = np.dtype(dtype).name
    dtypes = ['-b1', '-byte', '-b2', '-byte2', '-i1',
              '-integer*1', '-i2', '-integer*2', '-i4',
              '-integer*4', '-r4', '-real*4',
              '-r8', '-real*8',
              '-c2', '-complex*2', '-c8', '-complex*8',
              '-c8mag', '-cmag', '-c8pha', '-cpha', '-c2mag',
              '-c2pha', '-rmg', '-vfmt', '-val_frmt', '-c16']
    if dtype in dtypes:
        return dtype
    if 'byte' in dtype:
        return '-b1'
    elif 'float64' in dtype:
        return '-r8'
    elif 'float' in dtype:
        return '-r4'
    elif 'byte' in dtype or dtype in 'int8':
        return '-b1'
    elif 'int8' in dtype:
        return '-i1'
    elif 'int16' in dtype:
        return '-i2'
    elif 'int32' in dtype:
        return '-i4'
    elif 'int64' in dtype:
        return '-i8'
    elif 'complex128' in dtype:
        return '-c16'
    elif 'complex' in dtype:
        return '-c8'
    else:
        return ''


def get_np_dtype(isceType):

    if isceType is None:
        return
    try:
        isceType_upper = isceType.upper()
    except AttributeError:
        isceType_upper = get_dtype_name(isceType).upper()

    if ('CFLOAT' in isceType_upper or
            'COMPLEX64' in isceType_upper):
        dtype = np.complex64
    elif ('CDOUBLE' in isceType_upper or
          'COMPLEX128' in isceType_upper):
        dtype = np.complex128
    elif 'UINT16' in isceType_upper:
        dtype = np.uint16
    elif 'UINT32' in isceType_upper:
        dtype = np.uint32
    elif 'UINT64' in isceType_upper:
        dtype = np.uint64
    elif 'FLOAT64' in isceType_upper:
        dtype = np.float64
    elif 'FLOAT' in isceType_upper:
        dtype = np.float32
    elif 'DOUBLE' in isceType_upper:
        dtype = np.float64
    elif ('BYTE' in isceType_upper or
          'INT8' in isceType_upper):
        dtype = np.byte
    elif 'SHORT' in isceType_upper or 'INT16' in isceType_upper:
        dtype = np.int16

    elif 'LONG' in isceType_upper:
        dtype = np.int64
    elif 'INT64' in isceType_upper:
        dtype = np.int64
    elif 'INT' in isceType_upper:
        dtype = np.int32
    else:
        dtype = None
    return dtype
