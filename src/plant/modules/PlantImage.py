#!/usr/bin/env python3
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Gustavo H. X. Shiroma, Marco Lavalle
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import numpy as np
import os
from osgeo import gdal
from collections.abc import Sequence

import numbers
import plant

PLANT_IMAGE_IS_MUTABLE = False

_IMAGE_OBJ_ATTRS_TO_UPDATE_AFTER_LOADING_IMAGE = [
    '_geotransform',
    '_applied_transform_obj_crop_list',
    '_gcp_list',
    '_gcp_projection',
    '_width',
    '_length',
    '_depth',
    '_width_orig',
    '_length_orig',
    '_depth_orig',
    '_projection',
    '_metadata',
    '_null',
    '_offset_x',
    '_offset_y']

_BAND_OBJ_ATTRS_TO_KEEP_AFTER_LOADING_IMAGE = [
    '_name']


def _create_PlantBandOperation(image_ref, inputs,
                               inputs_kwargs, plant_transform_obj=None):
    max_nbands = None
    if image_ref is None:
        for arg in inputs:
            if (not isinstance(arg, plant.PlantImage) or
                (max_nbands is not None and
                 max_nbands >= arg.nbands)):
                continue
            max_nbands = arg.nbands
            image_ref = arg
        if image_ref is None:
            image_ref = PlantBand(inputs[0])
            max_nbands = image_ref.nbands
    else:
        max_nbands = image_ref.nbands
    out_image_obj = image_ref.soft_copy()
    if plant_transform_obj is not None:
        out_image_obj.plant_transform_obj = plant_transform_obj
    for b in range(max_nbands):
        band_args = []
        for input in inputs:
            if isinstance(input, plant.PlantImage):
                band_args.append(input.get_band(band=b))
            else:
                band_args.append(input)
        band_operation_obj = PlantBandOperation(*band_args,
                                                **inputs_kwargs)
        for band_obj in band_args:
            if not isinstance(band_obj, PlantBand):
                continue
            band_obj._notify_changes_list.append(
                band_operation_obj)
        out_image_obj.set_band(band_operation_obj, band=b,
                               realize_changes=False)
    plant.prepare_image(out_image_obj)
    return out_image_obj


def algebraic_decorator(operation):
    def decorated(f):
        def wrapper(*args, **kwargs):
            if isinstance(args[1], str) and operation == '__add__':
                return args[0].name + args[1]
            elif isinstance(args[1], str) and operation == '__radd__':
                return args[1] + args[0].name

            kwargs = {}
            kwargs['operation'] = operation
            out_image_obj = _create_PlantBandOperation(
                None, args, kwargs)

            return out_image_obj
        return wrapper
    return decorated


class ExtendedNdarray(np.ndarray):

    def __new__(cls, parent_obj, band, *args, **kwargs):
        cls.parent_obj = parent_obj
        cls.band = band

        obj = np.ndarray.__new__(cls, *args, **kwargs)
        return obj

    def __getitem__(self, key):

        if (isinstance(key, slice) and key.start is None and
                key.stop is None and key.step is None):
            image_obj = self.parent_obj
        else:
            image_obj = self.parent_obj.__getitem__(key)
        image = image_obj.get_array(band=self.band)
        new_ndarray = np.ndarray(
            buffer=image,
            shape=image.shape,
            dtype=image.dtype)
        return new_ndarray


class PlantBandBase():

    def initialize_parameters(self):
        self._applied_transform_obj_crop_header_list = []
        self._applied_transform_obj_crop_image_list = []
        self._applied_transform_obj_list = None

        self._notify_changes_list = []
        self._notify_changes_locker = False
        self._image = None

        self._name = None
        self._ctable = None
        self._dtype = None
        self._null = np.nan
        self._stats = None
        self._parent_list = None
        self._parent_orig_band_index = None
        self._parent_orig = None
        self._parent_orig_band_orig_index = None

    def notify_changes(self):
        if self._notify_changes_locker:
            return

        self._notify_changes_locker = True
        for band_obj in self._notify_changes_list:

            _ = band_obj.get_image()
        self._notify_changes_list = []
        self._notify_changes_locker = False

    def copy(self):
        new = PlantBand()
        for key, value in self.__dict__.items():
            new.__dict__[key] = plant.get_immutable_value(value)
        return new

    def soft_copy(self):
        new = PlantBand()
        for key, value in self.__dict__.items():
            if key == '_image':
                continue
            new.__dict__[key] = plant.get_immutable_value(value)
        return new

    def to_array(self):
        return self._image

    @property
    def image_loaded(self):
        flag_image_loaded = self._image is not None
        return flag_image_loaded

    @property
    def dataType(self):
        return self.dtype

    @property
    def dtype(self):
        if self.image_loaded:
            self._dtype = plant.get_dtype_name(self._dtype)
        return self._dtype

    @dtype.setter
    def dtype(self, val):
        self._dtype = val
        if self.image_loaded:
            self._image = np.asarray(self._image,
                                     dtype=val)

    @property
    def ctable(self):
        return self._ctable

    @ctable.setter
    def ctable(self, val):
        if isinstance(val, dict):
            ctable = gdal.ColorTable()
            for key, value in val.items():
                ctable.SetColorEntry(int(key), value)
            self._ctable = ctable
        elif val is None:
            self._ctable = None
        else:
            self._ctable = val.Clone()

    @property
    def name(self, default=None):
        if self._name:
            return self._name
        if default is not None:
            return default
        parent_orig_obj = self.parent_orig
        name = os.path.basename(parent_orig_obj.name)
        if parent_orig_obj.nbands != 1:
            b = self.parent_band_index
            name += f' (band {b})'

        return self._name

    @name.setter
    def name(self, val):
        self._name = val

    @property
    def null(self):
        return self._null

    @null.setter
    def null(self, val):
        self._null = val

    @property
    def stats(self):
        return self._stats

    @stats.setter
    def stats(self, val):
        self._stats = val

    @property
    def parent_list(self):
        return self._parent_list

    @parent_list.setter
    def parent_list(self, val):
        self._parent_list = val

    @property
    def parent_band_index(self):
        for parent_obj in reversed(self.parent_list):
            for b in range(parent_obj.nbands):
                if parent_obj.get_band(band=b) is self:
                    return b
        return None

    @property
    def parent_orig_band_index(self):
        return self._parent_orig_band_index

    @parent_orig_band_index.setter
    def parent_orig_band_index(self, val):
        self._parent_orig_band_index = val

    @property
    def parent_orig(self):
        return self._parent_orig

    @parent_orig.setter
    def parent_orig(self, val):
        self._parent_orig = val

    @property
    def parent_orig_band_orig_index(self):
        return self._parent_orig_band_orig_index

    @parent_orig_band_orig_index.setter
    def parent_orig_band_orig_index(self, val):
        self._parent_orig_band_orig_index = val

    def __getattr__(self, name):

        if name in self.__dir__():
            return
        if not name.startswith('__'):
            wrapper_obj = plant.ModuleWrapper(name, self)
            if wrapper_obj._module_obj:
                return wrapper_obj
        try:
            ret = getattr(self.parent_orig, name)
            flag_attribute_error = False
        except AttributeError:
            flag_attribute_error = True
        if flag_attribute_error:
            raise AttributeError(
                f'{self.__class__.__name__}.{name} is invalid.')
        return ret

    getImageType = dtype.fget
    getCTable = ctable.fget
    getName = name.fget

    getNull = null.fget
    getStats = stats.fget

    get_image_type = dtype.fget
    get_ctable = ctable.fget

    get_name = name.fget
    get_null = null.fget
    get_stats = stats.fget

    setImageType = dtype.fset
    setCTable = ctable.fset
    setName = name.fset
    setNull = null.fset
    setStats = stats.fset

    set_image_type = dtype.fset
    set_ctable = ctable.fset
    set_name = name.fset
    set_null = null.fset
    set_stats = stats.fset


class PlantImage():

    def __new__(cls, *args, **kwargs):
        input_data = args[0] if len(args) > 0 else None
        input_data_kw = kwargs.pop('input_data', None)
        ref_image_obj = kwargs.pop('ref_image_obj', None)

        if isinstance(ref_image_obj, PlantImage):
            ref_image_obj.init(*args, **kwargs)
            return ref_image_obj

        if isinstance(input_data, PlantImage):
            input_data.init(**kwargs)
            return input_data

        if input_data is None:
            input_data = input_data_kw
        if isinstance(input_data, PlantImage):
            input_data.init(**kwargs)
            return input_data
        new = super(PlantImage, cls).__new__(cls)

        new.initialize_parameters()
        new.init(input_data, **kwargs)
        return new

    def __len__(self):
        return self.nbands

    @algebraic_decorator('__add__')
    def __add__(self, image_2_obj):
        pass

    @algebraic_decorator('__sub__')
    def __sub__(self, image_2_obj):
        pass

    @algebraic_decorator('__mul__')
    def __mul__(self, image_2_obj):
        pass

    @algebraic_decorator('__div__')
    def __div__(self, image_2_obj):
        pass

    @algebraic_decorator('__pow__')
    def __pow__(self, *args):
        pass

    @algebraic_decorator('__sin__')
    def sin(input_data, **kwargs):
        pass

    @algebraic_decorator('__cos__')
    def cos(input_data, **kwargs):
        pass

    @algebraic_decorator('__radd__')
    def __radd__(self, image_2_obj):
        pass

    @algebraic_decorator('__rsub__')
    def __rsub__(self, image_2_obj):
        pass

    @algebraic_decorator('__rmul__')
    def __rmul__(self, image_2_obj):
        pass

    @algebraic_decorator('__rdiv__')
    def __rdiv__(self, image_2_obj):
        pass

    @algebraic_decorator('__rpow__')
    def __rpow__(self, *args):
        pass

    def initialize_parameters(self):
        self._plant_transform_obj = None
        self._applied_transform_obj_crop_list = []

        self._realize_changes_locked = False

        self._filename_orig = None
        self._filename = None
        self._header_file = None
        self._band_list = []
        self._file_format = None
        self._geotransform = None
        self._gcp_list = None
        self._gcp_projection = None
        self._scheme = None
        self._nbands = None
        self._nbands_orig = None
        self._band_orig = None

        self._input_key = None

        self._width = None
        self._length = None
        self._depth = None

        self._name = None
        self._projection = None
        self._metadata = None

        self._null = np.nan

        self._offset_x = None
        self._offset_y = None
        self._width_orig = None
        self._length_orig = None
        self._depth_orig = None
        self._selected_band = 0

    def init(self, *args, **kwargs):

        input_data = args[0] if len(args) > 0 else None
        if input_data is None:
            input_data = kwargs.pop('input_data', None)
        filename_orig = kwargs.pop('filename_orig', None)
        filename = kwargs.get('filename', None)
        header_file = kwargs.get('header_file', None)

        image = kwargs.get('image', None)

        name = kwargs.get('name', None)
        projection = kwargs.get('projection', None)
        file_format = kwargs.get('file_format', None)
        geotransform = kwargs.get('geotransform', None)
        gcp_list = kwargs.get('gcp_list', None)
        gcp_projection = kwargs.get('gcp_projection', None)

        scheme = kwargs.get('scheme', None)
        nbands = kwargs.pop('nbands', None)
        nbands_orig = kwargs.get('nbands_orig', None)
        band_orig = kwargs.get('band_orig', None)

        input_key = kwargs.get('input_key', None)

        metadata = kwargs.get('metadata', None)

        null = kwargs.get('null', np.nan)

        width_orig = kwargs.get('width_orig', None)
        length_orig = kwargs.pop('length_orig', None)
        depth_orig = kwargs.pop('depth_orig', None)
        verbose = kwargs.get('verbose', False)

        plant_transform_obj = kwargs.get('plant_transform_obj', None)
        realize_changes_locked = kwargs.get('realize_changes_locked', None)

        width = kwargs.get('width', None)
        length = kwargs.get('length', None)
        depth = kwargs.get('depth', None)
        offset_x = kwargs.get('offset_x', None)
        offset_y = kwargs.get('offset_y', None)

        update = kwargs.get('update', False)

        if (plant_transform_obj is not None and
                isinstance(plant_transform_obj, plant.PlantTransform)):

            plant_transform_obj = plant.PlantTransform(
                plant_transform_obj=plant_transform_obj,
                verbose=verbose,
                width=width,
                length=length,
                depth=depth,
                offset_x=offset_x,
                offset_y=offset_y)

            self._plant_transform_obj = plant_transform_obj
        elif plant_transform_obj is not None:
            self._plant_transform_obj = plant_transform_obj

        if isinstance(input_data, PlantImage):
            if update:
                input_data.realize_changes()
            self.__dict__.update(input_data.__dict__)
        elif isinstance(input_data, PlantBand):
            input_data.parent_orig.soft_copy(ref_image_obj=self)
            image = input_data.image
        elif isinstance(input_data, str):
            _ = self.read_image(
                input_data,
                method_kwargs_dict=plant.get_kwargs_dict_read_image(),
                ref_image_obj=self,
                **kwargs)

        elif input_data is not None:
            image = input_data

        if filename_orig is not None:
            self._filename_orig = filename_orig

        if filename is not None:
            self._filename = filename

        if header_file is not None:
            self._header_file = header_file

        if (self._filename is None and
                self._filename_orig is not None):
            self._filename = self._filename_orig
        if (self._filename_orig is None and
                self._filename is not None):
            self._filename_orig = self._filename

        if self._name is None:
            self._name = name

        if nbands is not None:
            self._nbands = nbands
        elif len(self._band_list) != 0 and self._nbands is None:
            self._nbands = len(self._band_list)

        if nbands_orig is not None:
            self._nbands_orig = nbands_orig
        elif len(self._band_list) != 0 and self._nbands_orig is None:
            self._nbands_orig = len(self._band_list)

        if self._nbands is None and self._nbands_orig is not None:
            self._nbands = self._nbands_orig
        elif self._nbands_orig is None and self._nbands is not None:
            self._nbands_orig = self._nbands

        if (self._band_orig is None and band_orig is not None and
            (isinstance(band_orig, Sequence) or
             isinstance(band_orig, np.ndarray))):
            self._band_orig = band_orig
        elif self._band_orig is None and band_orig is not None:
            self._band_orig = [band_orig]
        elif (self._band_orig is None and
              self._nbands_orig is not None and
              self._nbands_orig == 1):
            self._band_orig = [0]

        if self._input_key is None:
            self._input_key = input_key

        if plant.isvalid(null, null=np.nan):
            self._null = null
        elif (not plant.isvalid(self._null, null=np.nan) and
              (self.dtype is not None and
               'int' in plant.get_dtype_name(self.dtype))):
            if self.dtype:
                self._null = plant.get_int_nan_by_dtype(self.dtype)
            else:
                self._null = plant.INT_NAN

        if (image is not None and hasattr(image, "__getitem__") and

                isinstance(image, Sequence) and
                len(image) > 0 and
                ((self._nbands is not None and
                  self._nbands == len(image)) or
                 isinstance(image[0], np.ndarray) or
                 isinstance(image[0], plant.PlantImage) or
                 isinstance(image[0], plant.PlantBand) or
                 (len(image) > 1 and
                  plant.get_sequence_depth(image) > 2))):
            self._band_list = [PlantBand(band, **kwargs)
                               for band in image]
        elif (image is not None and hasattr(image, "__getitem__") and

              isinstance(image, Sequence) and
              len(image) > 0):
            self._band_list = [PlantBand(image, **kwargs)]
        elif image is not None:
            self._band_list = [PlantBand(image, **kwargs)]

        if (len(self._band_list) == 0 and
                self._nbands is not None):
            self._band_list = []
            for current_band in range(self._nbands):
                self._band_list.append(PlantBand(**kwargs))
        elif image is not None and len(self._band_list) == 0:
            self._band_list = [PlantBand(**kwargs)]
        if file_format is not None:
            self._file_format = file_format
        elif (self._file_format is None and
              self._filename_orig is not None):
            self._file_format = plant.get_output_format(
                self._filename_orig, file_format)
        elif (self._file_format is None and
              self._filename is not None):
            self._file_format = plant.get_output_format(
                self._filename, file_format)
        if geotransform is not None:
            self._geotransform = geotransform
        if gcp_list is not None:
            self._gcp_list = gcp_list
        if gcp_projection is not None:
            self._gcp_projection = gcp_projection

        if scheme is not None:
            self._scheme = scheme
        if self._nbands is None:
            self._nbands = len(self._band_list)

        if self._nbands_orig is None:
            self._nbands_orig = len(self._band_list)
        if self._band_orig is None and self._nbands == self._nbands_orig:
            self._band_orig = np.arange(self._nbands_orig)

        if width is not None:
            self._width = int(width)
        else:
            for b in range(self._nbands):
                if (len(self._band_list) != 0 and
                        self._band_list[b].image_loaded and
                        len(self._band_list[b].image.shape) >= 1):
                    self._width = self._band_list[b].image.shape[-1]
                elif (len(self._band_list) != 0 and
                      self._band_list[b].image_loaded):
                    self._width = 1
                if self._width is not None:
                    break

        if length is not None:
            self._length = int(length)
        else:
            for b in range(self._nbands):
                if (len(self._band_list) != 0 and
                        self._band_list[b].image_loaded and
                        len(self._band_list[b].image.shape) >= 2):
                    self._length = self._band_list[b].image.shape[-2]
                elif (len(self._band_list) != 0 and
                      self._band_list[b].image_loaded):
                    self._length = 1
                if self._length is not None:
                    break

        if (width is not None and
                length is not None and
                depth is None):
            depth = 1

        if depth is not None:
            self._depth = int(depth)
        else:
            for b in range(self._nbands):
                if (len(self._band_list) != 0 and
                        self._band_list[b].image_loaded and
                        len(self._band_list[b].image.shape) >= 3):
                    self._depth = self._band_list[b].image.shape[-3]
                elif (len(self._band_list) != 0 and
                      self._band_list[b].image_loaded):
                    self._depth = 1
                if self._depth is not None:
                    break

        if projection:
            self._projection = projection
        if metadata:
            self._metadata = metadata

        if offset_x is not None:
            self._offset_x = offset_x
        if self._offset_x is None:
            self._offset_x = 0
        if offset_y is not None:
            self._offset_y = offset_y
        if self._offset_y is None:
            self._offset_y = 0
        if width_orig is not None:
            self._width_orig = width_orig
        elif self._width_orig is None:
            self._width_orig = self._width

        if length_orig is not None:
            self._length_orig = length_orig
        elif self._length_orig is None:
            self._length_orig = self._length

        if depth_orig is not None:
            self._depth_orig = depth_orig
        elif self._depth_orig is None:
            self._depth_orig = self._depth

        if realize_changes_locked is not None:

            self._realize_changes_locked = realize_changes_locked

        if self._nbands == 0 or len(self._band_list) == 0:
            return
        for i, band in enumerate(self._band_list):
            if i > self._nbands:
                break
            if (band.parent_orig_band_orig_index is None and
                    (self._band_orig is None or
                     i >= len(self._band_orig))):
                band.parent_orig_band_orig_index = i
            elif band.parent_orig_band_orig_index is None:
                band.parent_orig_band_orig_index = self._band_orig[i]
            if band.parent_orig is None:
                band.parent_orig = self
            if band.parent_orig_band_index is None:
                band.parent_orig_band_index = i
            if band.parent_list is None:
                band.parent_list = [self]

    def __repr__(self):

        ret = f"PlantImage('{self.name}')"
        return ret

    def __str__(self):
        if self.filename_orig:
            return self.filename_orig

        if self.filename:
            return self.filename

        id_str = plant.get_obj_id(self)
        return f'MEM:{id_str}'

    def deepcopy(self):
        return self.deep_copy()

    def deep_copy(self, ref_image_obj=None):
        if ref_image_obj is None:
            ref_image_obj = PlantImage()
        for key, value in self.__dict__.items():
            ref_image_obj.__dict__[key] = \
                plant.get_immutable_value(value)
        ref_image_obj._band_list = [self.get_band(band=b).copy()
                                    for b in range(self.nbands)]
        return ref_image_obj

    def copy(self, ref_image_obj=None):
        if ref_image_obj is None:
            ref_image_obj = PlantImage()
        for key, value in self.__dict__.items():
            ref_image_obj.__dict__[key] = \
                plant.get_immutable_value(value)
        return ref_image_obj

    def soft_copy(self, ref_image_obj=None):
        if ref_image_obj is None:
            ref_image_obj = PlantImage()
        for key, value in self.__dict__.items():
            if key == '_band_list':
                continue
            ref_image_obj.__dict__[key] = \
                plant.get_immutable_value(value)
        ref_image_obj._band_list = \
            [PlantBand(None) for b in range(self.nbands)]

        return ref_image_obj

    def to_array(self):

        args = []
        for b in range(self.nbands):
            args.append(self.get_image(band=b))
        stacked_image = np.dstack(args)
        return stacked_image

    def realize_changes(self, band=None):
        if self._realize_changes_locked:

            return
        self.lock_realize_changes()

        if band is None:
            band_list = range(self.nbands)
        else:
            band_list = [band]
        for b in band_list:
            band = self.get_band(band=b)

            if (band.parent_orig is not None and
                    band.parent_orig is not self and
                    band.parent_orig._realize_changes_locked):

                continue

            self.get_image(band=b)

        self.unlock_realize_changes()

    def lock_realize_changes(self):

        self._realize_changes_locked = True

    def unlock_realize_changes(self, unlock_children=False):

        self._realize_changes_locked = False
        if not unlock_children:
            return
        band_list = range(self.nbands)
        for b in band_list:
            band = self.get_band(band=b)
            if not band.parent_orig._realize_changes_locked:
                continue
            band.parent_orig.unlock_realize_changes()

    @property
    def plant_transform_obj(self):
        return self._plant_transform_obj

    @plant_transform_obj.setter
    def plant_transform_obj(self, val):

        self._plant_transform_obj = val

    @property
    def crop_window(self):

        default_crop_window = [0, 0, self.width, self.length]
        if self._plant_transform_obj is None:
            return default_crop_window
        crop_window = self._plant_transform_obj.crop_window
        if crop_window is None:
            return default_crop_window
        for i, value in enumerate(crop_window):
            if plant.isvalid(value):
                continue
            crop_window[i] = default_crop_window[i]
        return crop_window

    @property
    def name(self, band=None, default=None):
        if self._name is not None:
            return self._name
        if default is not None:
            return default
        if band is not None:
            band_obj = self.get_band(band=band)
            return band_obj.get_name(default='')
        if self.nbands == 1:
            band = self._selected_band
            band_obj = self.get_band(band=band)
            name = self._band_list[band].get_name(default='')
            if name:
                return name
        name = os.path.basename(self.filename)
        return name

    @name.setter
    def name(self, val, band=None):
        if band is None:
            self._name = val
            if self.nbands > 1:
                return
            band = 0
        band = self.get_band(band=band)
        band.set_name(val)

    @property
    def filename_orig(self):
        if self._filename_orig:

            return self._filename_orig
        arg_id = str(id(self))

        value_str = f'MEM:{arg_id}'
        return value_str

    @filename_orig.setter
    def filename_orig(self, val):

        self.realize_changes()

        self._filename_orig = val

    @property
    def filename(self):
        if self._filename:
            return self._filename

        ret_dict = plant.parse_filename(self.filename_orig)
        self._filename = ret_dict['filename']
        return self._filename

    @filename.setter
    def filename(self, val):

        self.realize_changes()
        self._filename = val

    @property
    def header_file(self):
        if self._header_file is not None:
            return self._header_file
        filename = self.filename
        if not filename:
            return
        if (self.file_format is None or
                'ENVI' in self.file_format.upper()):
            header_file = plant.get_envi_header(filename)
            if plant.isfile(header_file):
                self._header_file = header_file
        if (self.file_format is None or
                ('ISCE' in self.file_format.upper() and
                 plant.isfile(filename + '.xml'))):
            self._header_file = filename + '.xml'
        if (self.file_format is None or
                ('ISCE' in self.file_format.upper() and
                 filename.endswith('.xml'))):
            self._header_file = filename
        if (self.file_format is None or
                ('ANN' in self.file_format.upper())):
            header_dict = plant.parse_uavsar_filename(filename)
            header_file = plant.get_annotation_file(
                header_dict, dirname=os.path.dirname(filename))
            if plant.isfile(header_file):
                self._header_file = header_file
        return self._header_file

    @header_file.setter
    def header_file(self, val):
        self.realize_changes()
        self._header_file = val

    def add_band(self, new_band, **kwargs):
        self.set_image(new_band, band=self.nbands, **kwargs)

    @property
    def band(self, band=None):
        if band is None:
            band = self._selected_band
        if band < 0:
            print(f'ERROR invalid band: {band}')
            return
        elif len(self._band_list) == 0:
            print(f'ERROR image object has {len(self._band_list)}'
                  ' band(s)')
            return
        elif band > self.nbands - 1:
            print(f'ERROR error reading band: {band}.'
                  f' Image object has only {self.nbands} band(s)')
            return

        band_obj = self._band_list[band]

        return band_obj

    @band.setter
    def band(self, new_band, band=None, realize_changes=None):

        if new_band is None:
            print('WARNING invalid argument for band creation: None')
            return
        if realize_changes is None:

            realize_changes = False
            for b in range(self.nbands):
                if b > len(self._band_list) - 1:
                    break
                current_band = self._band_list[b]
                if current_band.parent_orig is self:
                    realize_changes = True
                    break

        if not isinstance(new_band, PlantBand):

            self.set_image(new_band, band=band)
            return

        if band is None:
            band = self._selected_band

        if (band <= len(self._band_list) - 1 and
                self._band_list[band].image_loaded):

            self._band_list[band].notify_changes()

        if realize_changes:
            self.realize_changes()

        if realize_changes and not new_band.image_loaded:
            new_band.get_image()

        if new_band.parent_orig is None:
            new_band.parent_orig = self

        if (new_band.parent_orig_band_orig_index is None and
                self._band_orig is not None):

            new_band.parent_orig_band_orig_index = \
                self._band_orig[band]

        if new_band.parent_list is None:
            new_band.parent_list = [self]
        else:
            new_band.parent_list.append(self)

        if new_band.parent_orig_band_index is None:
            new_band.parent_orig_band_index = band

        if (band <= len(self._band_list) - 1):
            self._band_list[band] = new_band

        else:
            for i in range(len(self._band_list), band + 1):
                self._band_list.append(PlantBand(None))
            self._band_list[band] = new_band

        if new_band.image_loaded:
            self.update_parameters_with_image(new_band.image)
        elif (new_band.parent_orig is not None and
              new_band.parent_orig is not self):
            self.update_parameters_with_image_obj(new_band.parent_orig)
        nbands_observed = max([0] + [b for b, current_band in
                                     enumerate(self._band_list)
                                     if current_band._image is not None]) + 1
        self._nbands = max([self._nbands, nbands_observed, band + 1])

    def update_parameters_with_image(self, image):

        new_shape = plant.get_image_dimensions(image)
        self._depth, self._length, self._width = new_shape

        if self._width_orig is None:
            self._width_orig = self._width
        if self._length_orig is None:
            self._length_orig = self._length
        if self._depth_orig is None:
            self._depth_orig = self._depth

    def update_parameters_with_image_obj(self, image_obj):
        if image_obj._width is not None:
            self._width = image_obj._width
        if image_obj._length is not None:
            self._length = image_obj._length
        if image_obj._depth is not None:
            self._depth = image_obj._depth
        if image_obj._width_orig is not None:
            self._width_orig = image_obj._width_orig
        if image_obj._length_orig is not None:
            self._length_orig = image_obj._length_orig
        if image_obj._depth_orig is not None:
            self._depth_orig = image_obj._depth_orig
        if image_obj.geotransform is not None:
            self._geotransform = image_obj.geotransform

    def __array_ufunc__(self, ufunc, method, *inputs, **kwargs_orig):

        kwargs = {}
        kwargs['ufunc_kwargs'] = kwargs_orig

        kwargs['operation'] = f'{ufunc.__name__}'
        kwargs['ufunc'] = ufunc
        kwargs['method'] = method
        out_image_obj = _create_PlantBandOperation(
            self, inputs, kwargs)
        return out_image_obj

    def __getitem__(self, key):
        if (isinstance(key, slice) and key.start is None and
                key.stop is None and key.step is None):
            return self
        if (isinstance(key, slice) or
                isinstance(key, numbers.Number) or
                len(key) == 1):
            if isinstance(key, numbers.Number):
                band = [key]
            else:
                band = plant.expand_slice(key, default_stop=self.nbands)
            out_image_obj = self.soft_copy()
            out_image_obj.nbands = 0
            for i, b in enumerate(band):
                out_image_obj.set_band(self.get_band(band=b).copy(),
                                       band=i)
            return out_image_obj
        print('WARNING __getitem_ from PlantImage is not fully'
              ' implemented. Please use image_obj.image[key]'
              ' instead')

        key_y, key_x = key
        key_y_updated = plant.update_slice_cut(
            key_y, self.offset_y, size_cut=self.length)
        key_x_updated = plant.update_slice_cut(
            key_x, self.offset_x, size_cut=self.width)
        key = [key_y_updated, key_x_updated]

        flag_load_all_image = None
        if not self.image_loaded:
            flag_load_all_image = False
            n_elements = self.n_elements()
            n_elements_slice = self.n_elements(key=key)
            rate = float(n_elements_slice) / n_elements
            flag_load_all_image = rate > 0.5

        if (flag_load_all_image is False and
                not self.plant_transform_obj.flag_apply_crop()):
            out_image_obj = self.soft_copy()

            out_image_obj.plant_transform_obj.select_row = key[0]
            out_image_obj.plant_transform_obj.select_col = key[1]

            out_image_obj.plant_transform_obj.update_crop_window(
                geotransform=out_image_obj.geotransform,
                projection=out_image_obj.projection,
                length_orig=out_image_obj.length_orig,
                width_orig=out_image_obj.width_orig)

            plant.prepare_image(out_image_obj)
            return out_image_obj

        kwargs = {}
        kwargs['operation'] = '__getitem__'
        kwargs['key'] = key
        transform_kwargs = {}
        if len(key) > 0:
            transform_kwargs['select_row'] = key[0]
        if len(key) > 1:
            transform_kwargs['select_col'] = key[1]
        plant_transform_obj = plant.PlantTransform(**transform_kwargs)
        out_image_obj = _create_PlantBandOperation(
            self, [self], kwargs, plant_transform_obj=plant_transform_obj)
        return out_image_obj

    def __setitem__(self, key, value):

        try:
            self.image.__setitem__(key, value)
        except ValueError:

            self.image = self.image.copy()
            self.image.flags.writeable = True
            self.image.__setitem__(key, value)

    def __delitem__(self, key):
        raise NotImplementedError

    @property
    def image(self, *args, **kwargs):

        image = self._get_image(*args, **kwargs)
        if image is not None and not PLANT_IMAGE_IS_MUTABLE:

            image.flags.writeable = False

        return image

    @property
    def new_image(self, *args, **kwargs):

        band = kwargs.pop('band', None)
        if band is None:
            band = self._selected_band
        if band < 0:
            print(f'ERROR invalid band: {band}')
            return
        elif len(self._band_list) == 0:
            print(f'ERROR image object has {len(self._band_list)}'
                  ' band(s)')
            return
        elif band > self.nbands - 1:
            print(f'ERROR error reading band: {band}.'
                  f' Image object has only {self.nbands} band(s)')
            return
        if (band < len(self._band_list) and
                self._band_list[band].image_loaded):
            image = self._band_list[band].array
            return image
        new_image = ExtendedNdarray(self,
                                    band,

                                    shape=self.shape,
                                    dtype=self.dtype)
        return new_image

    @property
    def array(self, *args, **kwargs):
        return self.get_image(*args, **kwargs)

    def _get_image(self, band=None):

        if band is None:
            band = self._selected_band
        if band < 0:
            print(f'ERROR invalid band: {band}')
            return
        elif len(self._band_list) == 0:
            print(f'ERROR image object has {len(self._band_list)}'
                  ' band(s)')
            return
        elif band > self.nbands - 1:
            print(f'ERROR error reading band: {band}.'
                  f' Image object has only {self.nbands} band(s)')
            return
        if (band < len(self._band_list) and
                self._band_list[band].image_loaded):
            image = self._band_list[band].array
            return image

        if (self._band_list[band].parent_orig is not None and
                self._band_list[band].parent_orig is not self):
            parent_orig = self._band_list[band].parent_orig
            band_orig = self._band_list[band].parent_orig_band_index

            image = parent_orig.get_array(band=band_orig)
            new_band = parent_orig.get_band(band=band_orig)

            if (self._plant_transform_obj is not None and
                    new_band._applied_transform_obj_list is not None and
                    self._plant_transform_obj.id_orig not in new_band._applied_transform_obj_list):
                new_image_obj = plant.PlantImage(new_band)

                plant.prepare_image(
                    new_image_obj,
                    plant_transform_obj=self._plant_transform_obj,
                    verbose=False)
                image = new_image_obj.image
                new_band = new_image_obj.band

            parent_ref = parent_orig

            self._band_list[band] = new_band

            for key, value in parent_ref.__dict__.items():

                if key not in _IMAGE_OBJ_ATTRS_TO_UPDATE_AFTER_LOADING_IMAGE:
                    continue

                self.__dict__[key] = value

            new_band.parent_list.append(self)
            if image is None:
                print(f'ERROR loading band {band} from'
                      f' {self._band_list[band].parent_orig.filename}')
            return image
        if self._band_orig is None or band > len(self._band_orig) - 1:
            band_orig = band
        else:

            band_orig = self._band_orig[band]

        if self.filename_orig is None:
            return

        if (band_orig < len(self._band_list) and
            isinstance(self._band_list[band_orig],
                       plant.PlantBandOperation)):
            band_obj = self._band_list[band_orig]
            return band_obj.array

        image_obj = self.read_image(
            self.filename_orig,
            band=band_orig,
            method_kwargs_dict=plant.get_kwargs_dict_read_image(),

            flag_private_attributes=True,
            flag_exit_if_error=False,
            only_header=False)

        if image_obj is None:
            print(f'ERROR reading band {band_orig} from {self}')
            return

        for key, value in image_obj.__dict__.items():

            if key not in _IMAGE_OBJ_ATTRS_TO_UPDATE_AFTER_LOADING_IMAGE:
                continue

            self.__dict__[key] = value

        image_obj.band.parent_orig = self
        image_obj.band.parent_orig_band_orig_index = band_orig
        image_obj.band.parent_orig_band_index = band
        if (band < len(self._band_list)):
            band_obj = self._band_list[band]
            for key in _BAND_OBJ_ATTRS_TO_KEEP_AFTER_LOADING_IMAGE:
                if band_obj.__dict__[key] is None:
                    continue
                image_obj.band.__dict__[key] = band_obj.__dict__[key]
        self.set_band(image_obj.band, band=band, realize_changes=False)

        image = self._band_list[band]._image

        return image

    def read_image(self, *args, **kwargs):

        method_to_execute = plant.read_image
        kwargs['method_to_execute'] = method_to_execute

        kwargs = plant.populate_kwargs(self, *args, **kwargs)

        ret = method_to_execute(*args, **kwargs)
        return ret

    @image.setter
    def image(self, val, band=None, name=None, realize_changes=None):

        if band is None:
            band = self._selected_band

        if (band <= len(self._band_list) - 1 and
                self._band_list[band].image_loaded):

            self._band_list[band].notify_changes()

        if val is not None:
            if (isinstance(val, plant.PlantImage) or
                    isinstance(val, plant.PlantBand)):
                val = val.image
            elif not isinstance(val, np.ndarray):
                val = np.asarray(val)
            self.update_parameters_with_image(val)

            val = plant.shape_image(val)

        if (band <= len(self._band_list) - 1):
            self._band_list[band].image = val
        else:
            for i in range(len(self._band_list), band + 1):
                self._band_list.append(PlantBand(val))
        if self._band_list[band].parent_orig is None:
            self._band_list[band].parent_orig = self
        if (self._band_list[band].parent_orig_band_orig_index is None and
                self._band_orig is not None):

            self._band_list[band].parent_orig_band_orig_index = \
                self._band_orig
        if self._band_list[band].parent_list is None:
            self._band_list[band].parent_list = [self]
        if self._band_list[band].parent_orig_band_index is None:
            self._band_list[band].parent_orig_band_index = band
        nbands_observed = max([0] + [b for b, current_band in
                                     enumerate(self._band_list)
                                     if current_band.image_loaded]) + 1
        self._nbands = max([self._nbands, nbands_observed, band + 1])

        if name is not None:
            self._band_list[band].name = name

    @image.deleter
    def image(self, band=None):
        if band is None:
            band = self._selected_band

        if (band <= self._nbands - 1):
            self._band_list.pop(band)
        self._selected_band = 0

    @property
    def image_list(self):
        return [band.image for band in self._band_list]

    @property
    def image_loaded(self):

        if self._selected_band > len(self._band_list) - 1:
            print(f'ERROR invalid band {self._selected_band}')
            return False
        flag_image_loaded = self._band_list[self._selected_band].image_loaded
        return flag_image_loaded

    def n_elements(self, flag_all=None, key=None):
        n_elements = 1
        if (isinstance(key, slice) and key.start is None and
                key.stop is None and key.step is None):
            key = None
        elif isinstance(key, slice):
            key = [key]

        if (self.length is not None and
                key is not None and len(key) >= 1 and
                key[0] is not None and plant.is_numeric(key[0]) and
                key[0] > self.length):
            print(f'ERROR invalid line {key[0]} for array with length'
                  f' {self.length}')
        elif (key is not None and len(key) >= 1 and
                key[0] is not None and plant.is_numeric(key[0])):
            n_elements *= 1

        elif (self.length is not None and
                key is not None and len(key) >= 1 and
                key[0] is not None):
            expanded = plant.expand_slice(
                key[0], default_stop=self.length)
            n_elements *= len(expanded)
        elif self.length is not None:
            n_elements *= self.length

        if (self.width is not None and
                key is not None and len(key) >= 2 and
                key[1] is not None and plant.is_numeric(key[1]) and
                key[1] > self.width):
            print(f'ERROR invalid column {key[1]} for array with width'
                  f' {self.length}')
        elif (key is not None and len(key) >= 2 and
                key[1] is not None and plant.is_numeric(key[1])):
            n_elements *= 1

        elif (self.width is not None and
                key is not None and len(key) >= 2 and
                key[1] is not None):
            n_elements *= len(plant.expand_slice(
                key[1], default_stop=self.width))
        elif self.width is not None:
            n_elements *= self.width

        if (self.depth is not None and
                key is not None and len(key) >= 3 and
                key[2] is not None and plant.is_numeric(key[2]) and
                key[2] > self.depth):
            print(f'ERROR invalid depth index {key[2]} for array with depth'
                  f' {self.depth}')
            return
        elif (key is not None and len(key) >= 3 and
                key[2] is not None and plant.is_numeric(key[2])):
            n_elements *= 1

        elif (self.depth is not None and
                key is not None and len(key) >= 3 and
                key[2] is not None):
            n_elements *= len(plant.expand_slice(
                key[2], default_stop=self.depth))
        elif self.depth is not None:
            n_elements *= self.depth

        if flag_all:
            n_elements *= self.nbands
        return n_elements

    @property
    def width(self):
        if self.image_loaded:
            return self.image.shape[-1]
        if self._width is not None:
            return self._width
        return self._width_orig

    @width.setter
    def width(self, val):

        for b in range(self.nbands):
            band = self.get_band(band=b)
            parent_orig = band.parent_orig
            if (parent_orig is not None and
                    parent_orig.width is not None and
                    val == parent_orig.width):
                continue
            self.realize_changes(band=b)
            if self.nbands == 1:
                print(f'WARNING changing image'
                      f' width from {parent_orig.width}'
                      f' to {val}')
            else:
                print(f'WARNING changing band {b}'
                      f' width from {parent_orig.width}'
                      f' to {val}')
            new_shape = list(band.image.shape)

            new_shape[-1] = val
            image = plant.cut_image_with_shape(
                band.image, new_shape)
            band.image = image

        self._width = val
        if self._width_orig is None:
            self._width_orig = self._width

    @property
    def length(self):
        if self.image_loaded:
            return self.image.shape[-2]
        if self._length is not None:
            return self._length
        return self._length_orig

    @length.setter
    def length(self, val):
        for b in range(self.nbands):
            band = self.get_band(band=b)
            parent_orig = band.parent_orig
            if (parent_orig.length is not None and
                    val == parent_orig.length):
                continue
            self.realize_changes(band=b)
            if self.nbands == 1:
                print(f'WARNING changing image'
                      f' length from {parent_orig.length}'
                      f' to {val}')
            else:
                print(f'WARNING changing band {b}'
                      f' length from {parent_orig.length}'
                      f' to {val}')
            new_shape = list(band.image.shape)
            new_shape[-2] = val
            image = plant.cut_image_with_shape(
                band.image, new_shape)
            band.image = image
        self._length = val
        if self._length_orig is None:
            self._length_orig = self._length

    @property
    def depth(self):
        if self.image_loaded and len(self.image.shape) > 2:
            return self.image.shape[-3]
        elif self.image_loaded:
            return 1
        if self._depth is not None:
            return self._depth
        if self._depth_orig is not None:
            return self._depth_orig
        return 1

    @depth.setter
    def depth(self, val):
        for b in range(self.nbands):
            band = self.get_band(band=b)
            parent_orig = self
            if (parent_orig.depth is None or
                    (parent_orig.depth is not None and
                     val == parent_orig.depth)):
                continue
            self.realize_changes(band=b)
            if self.nbands == 1:
                print(f'WARNING changing image'
                      f' depth from {parent_orig.depth}'
                      f' to {val}')
            else:
                print(f'WARNING changing band {b}'
                      f' depth from {parent_orig.depth}'
                      f' to {val}')
            new_shape = list(parent_orig.shape)
            if len(new_shape) >= 3:
                new_shape[-3] = val
            else:
                new_shape.insert(0, val)
            image = plant.cut_image_with_shape(
                band.image, new_shape)
            band.image = image
        self._depth = val
        if self._depth_orig is None:
            self._depth_orig = self._depth

    @property
    def shape(self):
        if self.depth is None or self.depth <= 1:
            return (self.length, self.width)
        else:
            return (self.depth, self.length, self.width)

    @shape.setter
    def shape(self, val):
        if (not isinstance(val, list) and
                not isinstance(val, tuple)):
            val = [val]
        for i in range(max([len(self.shape), len(val)])):
            if i == 0:
                self.width = val[-1]
            elif i == 1 and len(val) >= 2:
                self.length = val[-2]
            elif i == 1:
                self.length = 1
            elif i == 2 and len(val) >= 3:
                self.depth = val[-3]
            elif i == 2:
                self.depth = 1

    @property
    def dataType(self, band=None):
        return self.get_dtype(band=band)

    @property
    def dtype(self, band=None):
        if band is None:
            band = self._selected_band
        if band >= len(self._band_list):
            return
        if band > len(self._band_list) - 1:
            print(f'ERROR invalid band {band}')
            return
        return self._band_list[band].dtype

    @dtype.setter
    def dtype(self, val, band=None, realize_changes=True):
        if realize_changes:
            self.realize_changes()
        if band is None:

            for band in range(self.nbands):
                self._band_list[band].dtype = val
            return
        if band > len(self._band_list) - 1:
            print(f'ERROR invalid band {band}')
            return
        self._band_list[band].dtype = val

    @property
    def null(self, band=None):
        if band is None:
            return self._null
        current_band = self._band_list[band]
        if current_band is None:
            return self._null
        return current_band.null

    @null.setter
    def null(self, val, band=None, realize_changes=True):
        if band is None:

            self._null = band
            return
        self.realize_changes()
        if band > len(self._band_list) - 1:
            print(f'ERROR invalid band {band}')
            return
        self._band_list[band].null = val

    @property
    def bands(self):
        return self._nbands

    @property
    def numBands(self):
        return self._nbands

    @property
    def nbands(self):
        return self._nbands

    @nbands.setter
    def nbands(self, val, realize_changes=None):
        if val < len(self._band_list):
            self._band_list = self._band_list[:val]

        self._nbands = val
        if realize_changes is False:
            return
        for b in range(min([self.nbands, val])):
            self.realize_changes(band=b)

    @property
    def nbands_orig(self):
        return self._nbands_orig

    @nbands_orig.setter
    def nbands_orig(self, val, realize_changes=None):

        if realize_changes is True:
            self.realize_changes()
        self._nbands_orig = val
        if len(self._band_list) > val:
            self._band_list = self._band_list[:val]
        else:
            for i in range(len(self._band_list), val):
                self._band_list.append(PlantBand(None))

    @property
    def band_orig(self):
        return self._band_orig

    @band_orig.setter
    def band_orig(self, val, realize_changes=None):

        if realize_changes is True:
            self.realize_changes()
        if plant.is_sequence(val):
            self._band_orig = val
        else:
            self._band_orig = [val]

    @property
    def input_key(self):
        return self._input_key

    @input_key.setter
    def input_key(self, val, realize_changes=None):

        if realize_changes is True:
            self.realize_changes()
        self._input_key = val

    @property
    def ctable(self, band=None):
        if band is None:
            band = self._selected_band
        if band > len(self._band_list) - 1:
            print(f'ERROR invalid band {band}')
            return
        return self._band_list[band].ctable

    @ctable.setter
    def ctable(self, val, band=None, realize_changes=None):

        if realize_changes is True:
            self.realize_changes()
        if band is None:
            band = self._selected_band
        if band > len(self._band_list) - 1:
            print(f'ERROR invalid band {band}')
            return
        self._band_list[band].ctable = val

    @property
    def projection(self):
        return self._projection

    @projection.setter
    def projection(self, val, realize_changes=None):
        if realize_changes is True:
            self.realize_changes()
        self._projection = val

    @property
    def metadata(self):
        return self._metadata

    @metadata.setter
    def metadata(self, val, realize_changes=None):
        if realize_changes is True:
            self.realize_changes()
        self._metadata = val

    @property
    def stats(self, band=None):
        if band is None:
            band = self._selected_band
        if band > len(self._band_list) - 1:
            print(f'ERROR invalid band {band}')
            return
        return self._band_list[band].stats

    @stats.setter
    def stats(self, val, band=None):
        if band is None:
            band = self._selected_band
        if band > len(self._band_list) - 1:
            print(f'ERROR invalid band {band}')
            return
        self._band_list[band].stats = val

    @property
    def offset_x(self):
        return self._offset_x

    @offset_x.setter
    def offset_x(self, val):
        self._offset_x = val

    @property
    def offset_y(self):
        return self._offset_y

    @offset_y.setter
    def offset_y(self, val):
        self._offset_y = val

    @property
    def width_orig(self):
        return self._width_orig

    @width_orig.setter
    def width_orig(self, val):
        self._width_orig = val

    @property
    def length_orig(self):
        return self._length_orig

    @length_orig.setter
    def length_orig(self, val):
        self._length_orig = val

    @property
    def depth_orig(self):
        if self._depth_orig is not None:
            return self._depth_orig
        return 1

    @depth_orig.setter
    def depth_orig(self, val):
        self._depth_orig = val

    @property
    def file_format(self):
        return self._file_format

    @file_format.setter
    def file_format(self, val):
        self._file_format = val

    @property
    def geotransform(self):
        return self._geotransform

    @geotransform.setter
    def geotransform(self, val, realize_changes=None):
        if realize_changes is not False:
            self.realize_changes()
        self._geotransform = val

    @property
    def gcp_list(self):
        return self._gcp_list

    @gcp_list.setter
    def gcp_list(self, val, realize_changes=None):
        if realize_changes is not False:
            self.realize_changes()
        self._gcp_list = val

    @property
    def gcp_projection(self):
        return self._gcp_projection

    @gcp_projection.setter
    def gcp_projection(self, val, realize_changes=None):
        if realize_changes is not False:
            self.realize_changes()
        self._gcp_projection = val

    @property
    def scheme(self):
        if self._scheme:
            return self._scheme
        if not self.header_file:
            return
        if (not self.file_format or
                'ENVI' in self.file_format.upper()):
            image_obj = plant.get_info_from_envi_header(
                self.header_file)
            if image_obj is not None:
                self._scheme = image_obj.scheme

        if (not self.file_format or
                'ISCE' in self.file_format.upper()):
            try:
                image_obj = plant.get_info_from_xml(
                    self.header_file)
                if image_obj is not None:
                    self._scheme = image_obj.scheme
            except KeyError:
                pass

        return self._scheme

    @scheme.setter
    def scheme(self, val, realize_changes=None):
        if realize_changes is not False:
            self.realize_changes()
        self._scheme = val

    isImageLoaded = image_loaded.fget

    getPlantTransformObj = plant_transform_obj.fget
    getFilenameOrig = filename_orig.fget
    getFilename = filename.fget
    getName = name.fget
    getHeaderFile = header_file.fget
    getImage = image.fget
    getBand = band.fget
    getWidth = width.fget
    getLength = length.fget

    getDepth = depth.fget
    getShape = shape.fget
    getImageType = dtype.fget
    getDType = dtype.fget
    getDataType = dtype.fget
    getFileFormat = file_format.fget
    getGeotransform = geotransform.fget
    getGcpList = gcp_list.fget
    getGcpProjection = gcp_projection.fget
    getProjection = projection.fget
    getScheme = scheme.fget
    getNumBands = nbands.fget
    getBands = nbands.fget
    getNBands = nbands.fget
    getNBandsOrig = nbands_orig.fget
    getBandOrig = band_orig.fget

    getInputKey = input_key.fget
    getCTable = ctable.fget
    getMetadata = metadata.fget

    getNull = null.fget
    getStats = stats.fget
    getOffsetX = offset_x.fget
    getOffsetY = offset_y.fget
    getWidthOrig = width_orig.fget
    getLengthOrig = length_orig.fget
    getDepthOrig = depth_orig.fget

    is_image_loaded = image_loaded.fget
    get_plant_transform_obj = plant_transform_obj.fget
    get_filename_orig = filename_orig.fget
    get_filename = filename.fget
    get_name = name.fget
    get_header_file = header_file.fget
    get_array = array.fget
    get_new_image = new_image.fget
    get_image = image.fget
    get_band = band.fget
    get_width = width.fget
    get_length = length.fget

    get_depth = depth.fget
    get_shape = shape.fget
    get_image_type = dtype.fget
    get_dtype = dtype.fget
    get_datatype = dtype.fget
    get_file_format = file_format.fget
    get_geotransform = geotransform.fget
    get_gcp_projection = gcp_projection.fget
    get_projection = projection.fget
    get_gcp_list = gcp_list.fget
    get_scheme = scheme.fget
    get_num_bands = nbands.fget
    get_bands = nbands.fget
    get_nbands = nbands.fget
    get_nbands_orig = nbands_orig.fget
    get_band_orig = band_orig.fget

    get_input_key = input_key.fget
    get_ctable = ctable.fget
    get_metadata = metadata.fget

    get_null = null.fget
    get_stats = stats.fget
    get_offset_x = offset_x.fget
    get_offset_y = offset_y.fget
    get_width_orig = width_orig.fget
    get_length_orig = length_orig.fget
    get_depth_orig = depth_orig.fget

    setPlantTransformObj = plant_transform_obj.fset
    setFilenameOrig = filename_orig.fset
    setFilename = filename.fset
    setName = name.fset
    setHeaderFile = header_file.fset
    setImage = image.fset
    setBand = band.fset
    setWidth = width.fset
    setLength = length.fset

    setDepth = depth.fset
    setShape = shape.fset
    setImageType = dtype.fset
    setDType = dtype.fset
    setDataType = dtype.fset
    setFileFormat = file_format.fset
    setGeotransform = geotransform.fset
    setGcpProjection = gcp_projection.fset
    setGcpList = gcp_list.fset
    setProjection = projection.fset
    setScheme = scheme.fset
    setNumBands = nbands.fset
    setBands = nbands.fset
    setNBands = nbands.fset
    setNBandsOrig = nbands_orig.fset
    setBandOrig = band_orig.fset

    setInputKey = input_key.fset
    setCTable = ctable.fset
    setMetadata = metadata.fset

    setNull = null.fset
    setStats = stats.fset
    setOffsetX = offset_x.fset
    setOffsetY = offset_y.fset
    setWidthOrig = width_orig.fset
    setLengthOrig = length_orig.fset
    setDepthOrig = depth_orig.fset

    set_plant_transform_obj = plant_transform_obj.fset
    set_filename_orig = filename_orig.fset
    set_filename = filename.fset
    set_name = name.fset
    set_header_file = header_file.fset
    set_image = image.fset
    set_band = band.fset
    set_width = width.fset
    set_length = length.fset

    set_depth = depth.fset
    set_shape = shape.fset
    set_image_type = dtype.fset
    set_dtype = dtype.fset
    set_datatype = dtype.fset
    set_file_format = file_format.fset
    set_geotransform = geotransform.fset
    set_gcp_list = gcp_list.fset
    set_gcp_projection = gcp_projection.fset
    set_projection = projection.fset
    set_scheme = scheme.fset
    set_num_bands = nbands.fset
    set_bands = nbands.fset
    set_nbands = nbands.fset
    set_nbands_orig = nbands_orig.fset
    set_band_orig = band_orig.fset

    set_input_key = input_key.fset
    set_ctable = ctable.fset
    set_metadata = metadata.fset

    set_null = null.fset
    set_stats = stats.fset
    set_offset_x = offset_x.fset
    set_offset_y = offset_y.fset
    set_width_orig = width_orig.fset
    set_length_orig = length_orig.fset
    set_depth_orig = depth_orig.fset

    def __getattr__(self, name):

        if name in self.__dir__():
            return
        if name.startswith('__'):
            raise AttributeError
        wrapper_obj = plant.ModuleWrapper(name, self)

        if not wrapper_obj._module_obj:
            raise AttributeError(
                f'{self.__class__.__name__}.{name} is invalid.')

        return wrapper_obj


class PlantBand(PlantBandBase):

    def __new__(cls, *args, **kwargs):
        input_data = args[0] if len(args) > 0 else None
        input_data_kw = kwargs.pop('input_data', None)
        if input_data is None:
            input_data = input_data_kw
        if isinstance(input_data, PlantBand):
            input_data.init(**kwargs)
            return input_data
        new = super(PlantBand, cls).__new__(cls)

        new.initialize_parameters()
        new.init(input_data, **kwargs)
        return new

    def init(self, *args, **kwargs):
        input_data = args[0] if len(args) > 0 else None
        image = kwargs.pop('image', None)
        name = kwargs.pop('name', None)
        dtype = kwargs.pop('dtype', None)
        ctable = kwargs.pop('ctable', None)
        null = kwargs.pop('null', np.nan)
        stats = kwargs.pop('stats', None)
        parent_orig = kwargs.pop('parent_orig', None)
        parent_orig_band_orig_index = kwargs.pop(
            'parent_orig_band_orig_index', None)
        parent_list = kwargs.pop('parent', None)
        parent_orig_band_index = kwargs.pop('parent_orig_band_index', None)

        if isinstance(input_data, PlantBand):
            self.__dict__.update(input_data.__dict__)
        elif isinstance(input_data, PlantImage):
            self._image = input_data.to_array()

        elif input_data is not None:
            self._image = input_data

        if image is not None:
            self._image = image
        self._image = plant.shape_image(self._image)

        if name:
            self._name = name

        if ctable is not None:
            self._ctable = ctable.Clone()

        if dtype is not None:
            self._dtype = dtype
        elif self._image is not None:
            self._dtype = plant.get_dtype_name(self._image)

        if stats is not None:
            self._stats = stats
        if plant.isvalid(null, null=np.nan):
            self._null = null
        elif (not plant.isvalid(self._null, null=np.nan) and
              (self.dtype is not None and
               'int' in plant.get_dtype_name(self.dtype))):
            self._null = plant.INT_NAN

        if parent_orig is not None:
            self._parent_orig = parent_orig
        if parent_orig_band_orig_index is not None:
            self._parent_orig_band_orig_index = parent_orig_band_orig_index
        if parent_list is not None and len(parent_list) > 1:
            self._parent_list = parent_list
        if parent_orig_band_index is not None:
            self._parent_orig_band_index = parent_orig_band_index

    def __repr__(self):

        ret = f"PlantBand('{self.name}')"
        return ret

    def __str__(self):
        if self.name:
            return self.name
        max_n_elements = 10
        if self.image_loaded:
            if (len(self.image.shape) <= 2 and
                    self.image.shape[0] == 1 and
                    self.image.shape[1] == 1):
                return f'{str(self.image[0, 0])}'
            if (len(self.image.shape) <= 2 and
                    self.image.shape[0] == 1 and
                    self.image.shape[1] < max_n_elements):
                return f'{str(self.image[0, :])}'
            n_elements = 1
            for dim in self.image.shape:
                n_elements = n_elements * dim
            if n_elements < max_n_elements:
                return f'{str(self.image)}'
        id_str = plant.get_obj_id(self)
        return f'MEM:{id_str}'

    @property
    def new_image(self):

        if self.image_loaded:
            return self._image
        new_image = ExtendedNdarray(self.parent_orig,
                                    self.parent_orig_band_index,

                                    shape=self.shape,
                                    dtype=self.dtype)
        return new_image

    @property
    def image(self):

        if (not self.image_loaded and
                self.parent_orig is not None and
                self.parent_orig_band_index is not None):

            self._image = self.parent_orig.get_image(
                band=self.parent_orig_band_index)
        return self._image

    @property
    def array(self):
        return self.get_image()

    @image.setter
    def image(self, val):

        if self.image_loaded:
            self.notify_changes()
        if (isinstance(val, plant.PlantImage) or
                isinstance(val, plant.PlantBand)):
            val = val.image
        elif not isinstance(val, np.ndarray):
            val = np.asarray(val)
        self._image = plant.shape_image(val)
        self._dtype = plant.get_dtype_name(val)

    getImage = image.fget
    get_image = image.fget
    get_new_image = new_image.fget
    get_array = array.fget
    setImage = image.fset
    set_image = image.fset


class PlantBandOperation(PlantBandBase):

    def init(self, *args, **kwargs):

        self.operation = kwargs.pop('operation', None)
        self.args = args
        self.kwargs = kwargs

        PlantBand.initialize_parameters(self)

    def __new__(cls, *args, **kwargs):

        new = super(PlantBandOperation, cls).__new__(cls)

        new.init(*args, **kwargs)

        return new

    @property
    def __class__(self):
        return plant.PlantBand

    @property
    def array(self):
        return self.get_image()

    @property
    def image(self):

        if self.image_loaded:

            return self._image

        if self.kwargs and 'ufunc' in self.kwargs.keys():
            ufunc = self.kwargs.pop('ufunc')
            ufunc_kwargs = self.kwargs.pop('ufunc_kwargs')
            new_args = []
            for band_obj in self.args:
                new_args.append(band_obj.image)
                band_obj._notify_changes_list.remove(self)
            result = ufunc(*new_args, **ufunc_kwargs)
            self._image = result
            return result

        band_1_obj = self.args[0]
        if self.operation == '__getitem__':
            result = band_1_obj.image.__getitem__(self.kwargs['key'])
            self._image = result
            return result

        if len(self.args) > 1:
            band_2_obj = self.args[1]
        else:
            band_2_obj = None

        if isinstance(band_2_obj, PlantBand):
            geotransform_1 = band_1_obj.geotransform
            geotransform_2 = band_2_obj.geotransform
            if (geotransform_1 is not None and
                    geotransform_2 is not None and
                    not plant.compare_geotransforms(geotransform_1,
                                                    geotransform_2)):
                print('WARNING inputs have different geometries', 1)
        elif band_2_obj is not None:
            band_2_obj = PlantBand(band_2_obj)

        if self in band_1_obj._notify_changes_list:
            band_1_obj._notify_changes_list.remove(self)

        image_1 = band_1_obj.image
        if band_2_obj is not None:
            if self in band_2_obj._notify_changes_list:
                band_2_obj._notify_changes_list.remove(self)
            image_2 = band_2_obj.image
            if image_1.shape != (1, 1) and image_2.shape != (1, 1):
                image_1, image_2 = plant.get_intersection(image_1,
                                                          image_2)

        flag_constant = False
        if image_1.shape == (1, 1):
            flag_constant = True
            image_1 = float(image_1[0, 0])
        print(f'executing operation: {self.operation}')
        print(f'    argument 1: {band_1_obj} (id: {id(band_1_obj)})')
        if band_2_obj is not None:
            print(f'    argument 1: {band_2_obj} (id: {id(band_2_obj)})')
        if band_2_obj is not None and image_2.shape == (1, 1):
            flag_constant = True
            image_2 = float(image_2[0, 0])
        if self.operation == '__sin__':
            result = np.sin(image_1)
        elif self.operation == '__cos__':
            result = np.cos(image_1)
        elif ((self.operation == '__add__' or
              self.operation == '__radd__') and
              flag_constant):
            result = image_1 + image_2
        elif (self.operation == '__add__' or
              self.operation == '__radd__'):
            result = np.nansum([image_1, image_2], axis=0)
        elif self.operation == '__sub__' and flag_constant:
            result = image_1 - image_2
        elif self.operation == '__sub__':
            result = np.nansum([image_1, -image_2], axis=0)
        elif self.operation == '__rsub__' and flag_constant:
            result = image_2 - image_1
        elif self.operation == '__rsub__':
            result = np.nansum([image_2, - image_1], axis=0)
        elif ((self.operation == '__mul__' or
               self.operation == '__rmul__')
              and flag_constant):
            result = image_1 * image_2
        elif (self.operation == '__mul__' or
              self.operation == '__rmul__'):
            result = np.nanprod([image_1, image_2], axis=0)
        elif self.operation == '__div__' and flag_constant:
            result = image_1 / image_2
        elif self.operation == '__div__' or self.operation == 'true_divide':
            result = np.nanprod([image_1, 1.0 / image_2], axis=0)
        elif self.operation == '__rdiv__' and flag_constant:
            result = image_2 / image_1
        elif self.operation == '__rdiv__':
            result = np.nanprod([image_2, 1.0 / image_1], axis=0)
        elif self.operation == '__pow__':
            result = np.power(image_1, image_2)
        elif self.operation == '__rpow__':
            result = np.power(image_2, image_1)
        else:
            print(f'ERROR operation not implemented: {self.operation}')
            return

        self._image = result

        return result

    @image.setter
    def image(self, val):

        if self.image_loaded:
            self.notify_changes()
        if (isinstance(val, plant.PlantImage) or
                isinstance(val, plant.PlantBand)):
            val = val.image
        elif not isinstance(val, np.ndarray):
            val = np.asarray(val)
        self._image = plant.shape_image(val)
        self._dtype = plant.get_dtype_name(val)

    def __repr__(self):

        args_str = ', '.join([arg.__repr__() for arg in self.args])
        if args_str:
            args_str = ', ' + args_str
        ret = f"PlantBandOperation('{self.operation}'{args_str})"
        return ret

    def __str__(self):
        ret = f'PLAnT band operation: {self.operation}'
        return ret

    set_image = image.fset
    get_image = image.fget
