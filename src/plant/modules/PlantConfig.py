#!/usr/bin/env python3
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Gustavo H. X. Shiroma, Marco Lavalle
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

from plant.modules.plant_constants import PLANT_CONFIG_FILE
import inspect
import getpass
import sys
import os
import time
import plant


def read_parameters_text_file(filename):

    if not os.path.isfile(filename):
        return
    parameters = {}
    with open(filename, 'r', encoding='ISO-8859-1') as f:
        lines = f.readlines()
        for i, current_line in enumerate(lines):
            try:
                parameter, value = current_line.split('=')
                if '{' in value:
                    j = i
                    while ('}' not in lines[j]):
                        value += lines[i + 1]
                        j += 1
                value = value.replace('"', '')
                parameter = parameter.strip()
                value = value.strip()
                parameters[parameter] = value
            except BaseException:
                pass
    return parameters


class PlantConfig(metaclass=plant.Singleton):
    def __new__(cls):
        new = super(PlantConfig, cls).__new__(cls)

        return new

    def get_variable(self,
                     variable_name,
                     default=None,
                     verbose=True):

        download_dir = None
        user_config_file = self.user_config_file
        if os.path.isfile(user_config_file):
            ret = read_parameters_text_file(user_config_file)
            if ret is not None:
                if variable_name in ret.keys():
                    value = ret[variable_name]
                else:
                    value = None
                if (value is not None and
                        variable_name.upper().endswith('DIR') and
                        value.startswith('~')):
                    value = os.path.expanduser(value)
                if value is not None:
                    return value

        if (variable_name == 'DOWNLOAD_DIR'):
            home_directory = os.path.expanduser('~')
            suggested_download_dir = os.path.join(home_directory,
                                                  plant.DOWNLOAD_DIR)
            while 1:
                res = plant.get_keys('Please, type your PLAnT download'
                                     ' directory (ENTER to use %s): '
                                     % suggested_download_dir)
                if res == '':
                    download_dir = suggested_download_dir
                else:
                    download_dir = res
                if not os.path.isdir(download_dir):
                    res = plant.get_keys('WARNING directory %s does not exist.'
                                         ' Would you like to create it?'
                                         ' ([y]es/[n]o) ' % download_dir)
                    if res.startswith('y'):
                        print('creading diretory: %s'
                              % download_dir)
                        os.makedirs(download_dir)
                        break
                else:
                    break
                _write_default_config(user_config_file,
                                      download_dir=download_dir)
            return download_dir
        elif not os.path.isfile(user_config_file):
            _write_default_config(user_config_file)
            try:
                plant.touch(user_config_file)
            except BaseException:
                pass
        return default

    def reset(self):
        self.main_script = None
        self.current_script = None

        self.logger_obj = None
        self.script_list = []

        self._download_dir = None
        self.temporary_files_dict = {}
        self.flag_keep_temporary = False
        self.output_files = []
        self.cache_dict = {}
        self.command_line = None
        self.flag_never = None
        self.flag_all = None

        _calframe = inspect.stack()
        _script_path = _calframe[0][1]
        _, home_dir = os.path.splitdrive(_script_path)
        for i in range(2):
            home_dir, _ = os.path.split(home_dir)
        self.home_dir = home_dir
        self.user_config_file = os.path.join(home_dir,
                                             PLANT_CONFIG_FILE)

        self.debug_level = int(self.get_variable('DEBUG_LEVEL', '0'))
        self.flag_debug = (self.get_variable(
            'DEBUG', 'False').title() == 'True')

        self.log_dir = self.get_variable('LOG_DIR', None)

        self.flag_show_elapsed_time = \
            self.get_variable('FLAG_SHOW_ELAPSED_TIME',
                              'False').title() == 'True'
        self.variables = {}

    def _debug(self):
        print('*********************************')
        print('## plant.config debug')
        print('*********************************')
        print('## ID: ', id(self))
        print('## main script: ', self.main_script)
        print('## current script: ', self.current_script)
        print('## command line: ', self.command_line)
        if self.logger_obj is not None:
            print('## Logger object ID: ', id(self.logger_obj))
        print('## temporary files:',
              len(self.temporary_files_dict.keys()))
        with plant.PlantIndent():
            for f in self.temporary_files_dict.keys():
                print(f)
        print('## output files:', len(self.output_files))
        with plant.PlantIndent():
            for f in self.output_files:
                print(f)
        print('## script list:', len(self.script_list))
        with plant.PlantIndent():
            for f in self.script_list:
                print(f)
        print('## variables:',
              len(self.variables.keys()))
        with plant.PlantIndent():
            for f in self.variables.keys():
                print(f)
        import psutil
        proc = psutil.Process()
        print('## list of open files:', len(proc.open_files()))
        print(proc.open_files())

    @property
    def temporary_files(self):
        if self.current_script not in self.temporary_files_dict:
            self.temporary_files_dict[self.current_script] = []
        return self.temporary_files_dict[self.current_script]

    @property
    def download_dir(self):
        if self._download_dir is None:
            self._download_dir = self.get_variable('DOWNLOAD_DIR')
        return self._download_dir

    @temporary_files.setter
    def temporary_files(self, val):
        if self.current_script not in self.temporary_files_dict:
            self.temporary_files_dict[self.current_script] = []
        self.temporary_files_dict[self.current_script] = val

    @property
    def flag_ipython(self):

        return self.in_ipython()

    def in_ipython(self):
        try:
            _ = get_ipython()

        except NameError:
            return False
        return True


if 'plant_config' not in locals():
    plant_config = PlantConfig()


def get_variable(*args, **kwargs):
    return plant_config.get_variable(*args, **kwargs)


class PlantLogger():

    def __init__(self, filename=None):

        self.calframe = None
        self.traceback = None
        self.flag_active = True
        self.filename = None
        self.start_time = time.time()
        self.last_message_time = self.start_time
        self.flag_message_error = False
        self.flag_message_exit = False
        self.flag_message_debug = False

        self.terminal = sys.stdout
        self.filename = filename
        self.log_error = False
        self.original_stdout = None
        self.original_stderr = None
        self.original_stdin = None
        self.flag_mute = None
        self.flag_color_text = True
        self.prefix = ''
        if (sys.platform == 'win32' or
                sys.platform == 'cygwin'):
            self.comment_char = ':: '
        else:
            self.comment_char = '# '
        self.log_file = None
        self.current_line = ''
        self.current_line_log = ''
        self.current_line_non_decorated = ''

        self.flag_cancel_new_line = False

    def __enter__(self):

        if not self.flag_active:
            self.__init__()

        if (not isinstance(sys.stdout, PlantLogger) and
                not isinstance(sys.stdout, PlantStdout)):

            plant_config.logger_obj = self

            self.original_stdout = sys.stdout
            self.original_stderr = sys.stderr
            self.original_stdin = sys.stdin
            sys.stdout = PlantStdout(self)
            sys.stderr = PlantStderr(self)
            sys.stdin = PlantStdin(sys.stdin)

        return self

    def fileno(self):

        return 1

    def __del__(self):

        if self.flag_active:

            self.__exit__()

        if self.log_file is not None:
            self.log_file.close()

    def __exit__(self, error_type=None, error_value=None, traceback=None):

        if not self.flag_active:
            self.close()
            return

        self.flag_active = False
        self.flush_temporary_files()
        plant_config.output_files = list(
            set(plant_config.output_files) -
            set(plant_config.temporary_files))
        if len(plant_config.output_files) != 0:
            if sys.stdout == self.original_stdout:
                prefix = ''
            else:
                prefix = '## '
            print(f'{prefix}output file(s):')
            for current_file in plant_config.output_files:
                if not plant.isfile(current_file):
                    print(f'    {current_file}'
                          ' (validation error: file may not exist)')
                    continue
                if not os.path.isfile(current_file):
                    print(f'{prefix}    {current_file}')
                    continue
                try:
                    file_update_time = os.path.getmtime(current_file)
                except FileNotFoundError:
                    file_update_time = None
                if (file_update_time is not None and
                        file_update_time < self.start_time):
                    print(f'    {current_file} (not modified)')
                    continue
                print(f'{prefix}    {current_file}')
            plant_config.output_files = []

        if plant_config.flag_show_elapsed_time:
            current_time = time.time()
            message = (' elapsed time: %s'
                       % (plant.hms_string(current_time -
                                           self.start_time)))
            print('SYSTEM' + message + plant.bcolors.ColorOff)
        if self.filename is not None:
            message = ' log: ' + self.filename
            print('SYSTEM' + message + plant.bcolors.ColorOff)
        ret = None

        flag_user_interrupted = \
            (error_type == KeyboardInterrupt or
             error_type == plant.PlantExceptionKeyboardInterrupt)
        flag_system_exec_ok = (error_type != SystemExit or
                               (error_value is not None and
                                len(error_value.args) != 0 and
                                error_value.args[0] != 0))

        if error_type == plant.PlantReturn:

            ret = error_value.args

        elif (error_value is not None and
              not flag_user_interrupted and
              flag_system_exec_ok and
              not self.flag_message_error and
              not self.flag_message_exit):

            if (traceback.__class__.__name__ == 'traceback'):
                self.traceback = traceback
            if (plant_config.flag_debug or
                    'ERROR' not in str(error_value)):
                self.print_traceback()
            if 'ERROR' not in str(error_value):
                print(f'ERROR {str(error_type)}: {str(error_value)}')
            else:
                print(str(error_value))

        if (self.flag_message_error or self.flag_message_exit or
                self.flag_message_debug):
            self.current_line = ''
            self.current_line_log = ''
            self.current_line_non_decorated = ''
        self.close()

        return ret

    def flush_temporary_files(self):

        if (plant_config.temporary_files is None or
                len(plant_config.temporary_files) == 0):
            return
        if plant_config.flag_keep_temporary:
            print('SYSTEM keeping temporary file(s)...')
        else:
            print('SYSTEM removing temporary file(s)...')

        for current_file in plant_config.temporary_files:
            if not isinstance(current_file, str):
                current_file = str(current_file)
                print(f'WARNING not removing: {current_file}')
                return
            if os.path.isdir(current_file):
                try:
                    os.rmdir(current_file)
                except OSError:
                    pass
                continue
            if (not os.path.isfile(current_file) and
                    not os.path.islink(current_file)):
                continue
            file_list = [current_file,
                         current_file + '.xml',
                         current_file + '.vrt',
                         current_file + '.aux.xml',
                         current_file + '.aux.hdr',
                         current_file + '.hdr']
            for current_file_with_header in file_list:
                if (os.path.isfile(current_file_with_header) or
                        os.path.islink(current_file_with_header)):
                    print('SYSTEM     %s' % current_file_with_header)
                    if not plant_config.flag_keep_temporary:
                        os.remove(current_file_with_header)
            envi_header = plant.get_envi_header(current_file)
            if (os.path.isfile(envi_header) or
                    os.path.islink(envi_header)):
                print('SYSTEM     %s' % envi_header)
                if not plant_config.flag_keep_temporary:
                    os.remove(envi_header)

        plant_config.output_files = list(
            set(plant_config.output_files) -
            set(plant_config.temporary_files))
        plant_config.temporary_files = []

    def close(self):

        if self.flag_active:
            self.__exit__()
            return
        if plant_config.logger_obj is not self:
            return

        if sys is not None and self.original_stdout is not None:
            sys.stdout = self.original_stdout
        if sys is not None and self.original_stderr is not None:
            sys.stderr = self.original_stderr
        if sys is not None and self.original_stdin is not None:
            sys.stdin = self.original_stdin
        if plant_config.main_script is not None:
            plant_config.reset()
        if (plant_config.logger_obj is self and
                self.filename is not None and
                self.log_file is not None):
            self.log_file.close()

    def print_traceback(self, calframe=None):
        if not plant_config.flag_debug:
            return
        if self.traceback is not None:
            import traceback as tb
            tb.print_tb(self.traceback)
            self.traceback = None

        if calframe is None:
            calframe = self.calframe
            self.calframe = None
        if calframe is None:
            return
        if len(calframe) != 0:
            print('SYSTEM Traceback (most recent call last):')
        for n in reversed(range(len(calframe))):
            method_file = os.path.basename(calframe[n][1])
            method_absfile = calframe[n][1]
            method_name = calframe[n][3]
            if ((method_file == 'PlantScript.py' and
                 method_name == 'print') or
                (method_file == 'PlantScript.py' and
                 method_name == '__init__') or
                (method_file == 'PlantConfig.py' and
                 method_name == 'write') or
                (method_file == 'PlantConfig.py' and
                 method_name == 'write_message') or
                (method_file == 'PlantConfig.py' and
                 method_name == '__exit__') or
                (method_file == 'PlantConfig.py' and
                 method_name == 'print_traceback') or
                (method_file == 'plant_lib.py' and
                 method_name == 'debug') or
                (method_file == 'plant_lib.py' and
                 method_name == 'execute') or
                (method_file == 'plant_lib.py' and
                 method_name == 'handle_exception')):
                continue

            method_line = calframe[n][2]
            print('SYSTEM     File "%s", line %s, in %s'
                  % (method_absfile, method_line, method_name))
            print('SYSTEM       %s' % (calframe[n][4][-1].strip()))
        calframe = None

    def init_file(self, filename):
        if filename is None or self.log_error:
            return
        self.filename = filename
        try:
            self.log_file = open(filename, "w+")
            os.chmod(filename, 0o777)
            self.log_file.write(self.comment_char +
                                '=========================='
                                '=======================\n')
        except BaseException:
            self.terminal.write('ERROR writing file: ' + filename + '\n')
            self.terminal.flush()
            self.log_error = True
            return
        main_script_name = plant_config.main_script.__class__.__name__
        current_script_name = \
            plant_config.current_script.__class__.__name__
        library_path = \
            os.path.dirname(__file__)
        if main_script_name == current_script_name:
            script_str = 'script'
        else:
            script_str = 'current script'
            self.log_file.write(self.comment_char + ' main script: ' +
                                main_script_name + '\n')
        self.log_file.write(self.comment_char + script_str + ': ' +
                            main_script_name + '\n')

        self.log_file.write(self.comment_char +
                            '=========================='
                            '=======================\n')
        self.log_file.write(self.comment_char +
                            'start time: ' + str(time.ctime()) + '\n')
        self.log_file.write(self.comment_char + 'library path: ' +
                            library_path + '\n')
        self.log_file.write(self.comment_char + 'execution path: ' +
                            os.getcwd() + '\n')
        self.log_file.write(self.comment_char + 'platform: ' +
                            str(sys.platform) + '\n')
        self.log_file.write(
            self.comment_char +
            'user: ' +
            getpass.getuser() +
            '\n')
        self.log_file.write(self.comment_char +
                            '=========================='
                            '=======================\n')
        self.log_file.write(self.comment_char + 'bash commands:\n')
        self.log_file.write(self.comment_char +
                            '----------------------------'
                            '---------------------\n')
        command = 'cd ' + os.getcwd() + '\n'
        self.log_file.write("echo " + command)
        self.log_file.write(command)
        self.log_file.write(self.comment_char + 'command: \n')
        self.log_file.write("echo " + plant_config.command_line + "\n")
        self.log_file.write(plant_config.command_line + '\n')
        self.log_file.write('cd -; exit \n')
        self.log_file.write(self.comment_char +
                            '----------------------------'
                            '---------------------\n')

    def write_message(self, message, flag_allow_self_call=False):

        if not flag_allow_self_call:
            try:
                calframe = inspect.stack()

            except KeyboardInterrupt:
                raise plant.PlantExceptionKeyboardInterrupt
            except BaseException:
                calframe = None
            if calframe is not None:

                method_file = os.path.basename(calframe[1][1])
                method_name = calframe[1][3]
                if ((method_file == 'PlantConfig.py' and
                     method_name == 'write_message')):
                    self.log(message)

                    if (not self.flag_mute or
                            (self.flag_mute is None and
                             plant_config.flag_ipython and
                             isinstance(plant_config.main_script,
                                        plant.PlantDisplayLib))):
                        self.terminal.write(message)
                        self.flush()

                    self.current_line = ''
                    self.current_line_log = ''
                    self.current_line_non_decorated = ''
                    return

        flag_startswith_carriage_return = message.startswith('\r')
        if flag_startswith_carriage_return:
            self.current_line = ''
            self.current_line_log = ''
            self.current_line_non_decorated = ''
            message = message.replace('\r', '')

        if self.flag_cancel_new_line and '\n' in message:
            message_splitted = message.split('\n')
            message = (message_splitted[0] +
                       '\n'.join(message_splitted[1:]))
            self.flag_cancel_new_line = False

        message_list = message.split('\n')
        if len(message_list) > 2:
            for sub_message in message_list:

                if not sub_message or self.flag_mute:
                    continue
                self.write_message(sub_message + '\n',
                                   flag_allow_self_call=True)
            return

        if not message:
            return

        self.flag_message_error = (self.flag_message_error or
                                   message.startswith('ERROR'))
        flag_system = message.startswith('SYSTEM')

        if self.flag_message_error and self.calframe is None:

            self.calframe = list(inspect.stack())

        self.flag_message_exit = (self.flag_message_exit or
                                  message.startswith('EXIT'))
        self.flag_message_debug = (self.flag_message_debug or
                                   message.startswith('DEBUG') or
                                   message.startswith('***'))

        if (message.startswith('BOLD file saved: ') or
                message.startswith('file saved: ')):
            file_suffix = message.split('file saved:')
            if len(file_suffix) > 1:
                file_suffix = file_suffix[1].strip()
                filename = file_suffix.split(' ')[0]
                if filename in plant_config.temporary_files:
                    message = f'file saved (temp): {filename}'

        non_decorated_message = message
        dict_message = {}
        dict_message['log_message'] = message
        dict_message['flag_color_applied'] = False

        if ('PLAnT' in message and
                ' - ' in message):
            message_splitted = message.split(' - ')
            message_splitted[0] = \
                (message_splitted[0].replace(
                    'PLAnT', (plant.bcolors.BGreen + 'PLAnT')) +
                 plant.bcolors.ColorOff)
            message_splitted[1] = (plant.bcolors.Cyan +
                                   message_splitted[1] +
                                   plant.bcolors.ColorOff)
            message = ' - '.join(message_splitted)

            dict_message['flag_color_applied'] = True
        dict_message['message'] = message

        add_color(dict_message,
                  'SYSTEM ',
                  color=plant.bcolors.BBlue,
                  flag_remove_sub_string=True,
                  flag_line=True)
        add_color(dict_message,
                  'DEBUG',
                  color=plant.bcolors.Purple,
                  start_only=True)
        add_color(dict_message,
                  '*** ',
                  color=plant.bcolors.Purple,
                  start_only=True)
        add_color(dict_message,
                  'INFO',
                  start_only=True,
                  color=plant.bcolors.BOLD)
        add_color(dict_message,
                  'WARNING',
                  color=plant.bcolors.BYellow)

        add_color(dict_message,
                  'ERROR',
                  start_only=True,
                  color=plant.bcolors.BRed)
        add_color(dict_message,
                  'EXIT ',
                  color=plant.bcolors.BOLD,
                  flag_remove_sub_string=True,
                  flag_line=True)
        add_color(dict_message,
                  'OPTION ',
                  color=plant.bcolors.BBlue,
                  flag_remove_sub_string=True,
                  flag_line=True)

        add_color(dict_message,
                  'BOLD ',
                  color=plant.bcolors.BOLD,
                  flag_remove_sub_string=True,
                  flag_line=True)
        add_color(dict_message,
                  '## ',
                  color=plant.bcolors.BOLD,
                  flag_remove_sub_string=True,
                  flag_line=True)
        add_color(dict_message,
                  'GREEN ',
                  color=plant.bcolors.BGreen,
                  flag_remove_sub_string=True,
                  flag_line=True)
        add_color(dict_message,
                  'OK',
                  color=plant.bcolors.BGreen,
                  flag_inside_brackets=True)
        add_color(dict_message,
                  'NOT FOUND',
                  color=plant.bcolors.BRed,
                  flag_inside_brackets=True)
        add_color(dict_message,
                  'FAIL',
                  color=plant.bcolors.BRed,
                  flag_inside_brackets=True)
        add_color(dict_message,
                  'StdErr',
                  color=plant.bcolors.BRed,
                  flag_inside_brackets=True)
        if message.startswith('PARAMETER'):
            sub_string = message.split(':')
            if len(sub_string) > 1:
                add_color(dict_message,
                          sub_string[0].replace('PARAMETER', ''),
                          match_string=sub_string[0],
                          color=plant.bcolors.BOLD,
                          start_only=True,
                          flag_remove_sub_string=False,
                          flag_line=False)
        options_list = ['positional arguments:',
                        'optional arguments:',
                        'description:',
                        'usage examples:']
        for current_option in options_list:
            add_color(dict_message,
                      current_option,
                      color=plant.bcolors.BBlue,
                      start_only=True,
                      flag_remove_sub_string=False,
                      flag_line=True)
        message = dict_message['message']
        log_message = dict_message['log_message']

        if not dict_message['flag_color_applied']:
            message = plant.bcolors.ColorOff + message

        if not plant_config.flag_debug and self.flag_message_debug:
            if self.flag_active and '\n' in message:
                message_splitted = message.split('\n')
                log_message_splitted = log_message.split('\n')
                non_decorated_message_splitted = \
                    non_decorated_message.split('\n')
                if len(message_splitted) >= 2:
                    self.current_line += '\n'.join(message_splitted[:-2])
                    self.current_line_log += \
                        '\n'.join(log_message_splitted[:-2])

                    self.flag_message_debug = False
            return

        if self.current_line == '' and not self.flag_message_error:
            message = self.prefix + message
            log_message = self.prefix + log_message

        if ((plant_config.flag_debug or
             plant_config.debug_level > 0 or
             plant_config.flag_show_elapsed_time) and

            self.current_line == '' and
                not flag_system):

            current_debug_level = 0

            try:
                calframe = inspect.stack()
            except IndexError:
                calframe = []

            for n in range(len(calframe)):
                if current_debug_level >= plant_config.debug_level:
                    break
                method_file = os.path.basename(calframe[n][1])
                method_name = calframe[n][3]
                if ((method_file == 'PlantScript.py' and
                     method_name == 'print') or
                    (method_file == 'PlantScript.py' and
                     method_name == '__init__') or
                    (method_file == 'PlantConfig.py' and
                     method_name == 'write') or
                    (method_file == 'PlantConfig.py' and
                     method_name == 'write_message') or
                    (method_file == 'PlantConfig.py' and
                     method_name == '__exit__') or
                    (method_file == 'plant_lib.py' and
                     method_name == 'debug') or
                    (method_file == 'plant_lib.py' and
                     method_name == 'execute') or
                    (method_file == 'plant_lib.py' and
                     method_name == 'handle_exception')):
                    continue

                method_line = calframe[n][2]
                cal_info = ('(%s.%s:%d)'
                            % (method_file, method_name, method_line))
                log_message = cal_info + ' ' + log_message
                cal_info += plant.bcolors.ColorOff
                level_color = self.get_level_color(current_debug_level)
                cal_info = level_color + cal_info
                message = cal_info + ' ' + message
                current_debug_level += 1

            if plant_config.flag_show_elapsed_time:
                current_time = time.time()
                diff_time = current_time - self.last_message_time
                self.last_message_time = current_time
                time_str = ('(%s +%s)'
                            % (plant.hms_string(self.last_message_time -
                                                self.start_time,
                                                separator=''),
                               plant.hms_string(diff_time,
                                                separator='')))
                level_color = self.get_level_color(len(calframe))
                log_message = ('%s %s' % (time_str, log_message))
                time_str = level_color + time_str + plant.bcolors.ColorOff
                time_str = time_str.ljust(32)
                message = ('%s %s' % (time_str, message))
                current_debug_level += 1

        if flag_startswith_carriage_return:
            message = '\r' + message
            log_message = '\r' + log_message

        if (((not self.flag_message_error and
              not self.flag_message_exit)) or
            ((self.flag_message_error or self.flag_message_exit) and
             not self.flag_active)):
            if (not flag_startswith_carriage_return or
                    (flag_startswith_carriage_return and
                     '\n' in message)):
                self.log(log_message)

            if (not self.flag_mute and
                (not self.flag_color_text or
                 sys.platform == 'win32' or
                 sys.platform == 'cygwin')):
                self.terminal.write(log_message)
            elif not self.flag_mute:

                try:
                    self.terminal.write(message)
                except UnicodeEncodeError:
                    self.terminal.write(
                        message.encode('ascii',
                                       'ignore').decode('ascii'))

            self.flush()

        if ((self.flag_message_error or self.flag_message_exit) and
                self.flag_active and '\n' in message):

            message_splitted = message.split('\n')
            log_message_splitted = log_message.split('\n')
            non_decorated_message_splitted = non_decorated_message.split('\n')
            if len(message_splitted) >= 2:
                self.current_line += '\n'.join(message_splitted[:-2])
                self.current_line_log += '\n'.join(log_message_splitted[:-2])
                self.current_line_non_decorated += '\n'.join(
                    non_decorated_message_splitted[:-2])

            if self.flag_message_error:
                self.flag_message_error = False
            if self.flag_message_exit:
                self.flag_message_exit = False
            current_line = self.current_line_non_decorated

            self.current_line = ''

            raise plant.PlantExceptionError(current_line)

        if '\n' in message:
            message_splitted = message.split('\n')
            self.current_line = '\n'.join(message_splitted[1:])
        else:
            self.current_line += message

        if '\n' in log_message:
            log_message_splitted = log_message.split('\n')
            self.current_line_log = '\n'.join(
                log_message_splitted[1:])
        else:
            self.current_line_log += log_message

        if '\n' in non_decorated_message:
            non_decorated_message_splitted = \
                non_decorated_message.split('\n')
            self.current_line_non_decorated = '\n'.join(
                non_decorated_message_splitted[1:])
        else:
            self.current_line_non_decorated += non_decorated_message

    def log(self, log_message):
        if self.log_error:
            return

        if self.current_line == '':
            log_message = time.strftime('# %x %X ') + log_message
        if self.log_file is not None:
            self.log_file.write(log_message)

    def flush(self):
        self.terminal.flush()
        if self.log_error or self.log_file is None:
            return
        self.log_file.flush()

    def get_level_color(self, current_debug_level):
        if current_debug_level == 0:
            level_color = plant.bcolors.Blue
        elif current_debug_level == 1:
            level_color = plant.bcolors.BBlue
        elif current_debug_level == 2:
            level_color = plant.bcolors.Purple
        else:
            level_color = plant.bcolors.BPurple
        return level_color


def add_color(dict_message,
              sub_string,
              match_string=None,
              color=None,

              start_only=False,
              flag_remove_sub_string=False,
              flag_inside_brackets=False,
              flag_line=False):
    if color is None:
        color = plant.bcolors.Purple
    if match_string is None:
        match_string = sub_string
    if dict_message['flag_color_applied']:
        return
    if flag_remove_sub_string:
        out_sub_string = ''
        out_log_sub_string = ''
    elif flag_line:
        out_sub_string = sub_string
        out_log_sub_string = sub_string
    else:
        out_sub_string = sub_string + plant.bcolors.ColorOff
        out_log_sub_string = sub_string
    out_sub_string = color + out_sub_string
    if flag_inside_brackets:
        sub_string = '[' + sub_string + ']'
        out_sub_string = '[' + out_sub_string + ']'
        out_log_sub_string = '[' + out_log_sub_string + ']'
        match_string = '[' + match_string + ']'
    message = dict_message['message']
    log_message = dict_message['log_message']
    if (start_only and
            log_message.lstrip().startswith(match_string)):
        message_index = (message.find(match_string) +
                         len(match_string))
        message = out_sub_string + message[message_index:]
        log_message_index = (log_message.find(match_string) +
                             len(match_string))
        log_message = out_log_sub_string + log_message[
            log_message_index:]
        if flag_line:
            message += plant.bcolors.ColorOff
        dict_message['flag_color_applied'] = False
    elif not start_only and match_string in log_message:

        message = message.replace(match_string, out_sub_string)
        log_message = log_message.replace(match_string, out_log_sub_string)
        if flag_line:
            message += plant.bcolors.ColorOff
        dict_message['flag_color_applied'] = False
    dict_message['message'] = message
    dict_message['log_message'] = log_message


class PlantStdin(object):
    def __init__(self, value=None):
        self.stdin = value

    def readline(self):

        value = self.stdin.readline()
        plant_config.logger_obj.log(value)
        plant_config.logger_obj.current_line = ''
        return value

    def fileno(self):
        return 0


class PlantStderr(object):
    def __init__(self, logger_obj):
        self.logger_obj = logger_obj

    def fileno(self):
        return 2

    def write(self, message):
        self.logger_obj.write_message(message)

    def flush(self):
        self.logger_obj.flush()


class PlantStdout(object):

    def __init__(self, logger_obj):
        self.logger_obj = logger_obj

    def fileno(self):
        return 1

    def write(self, message):
        self.logger_obj.write_message(message)

    def flush(self):
        self.logger_obj.flush()


def get_keys(*args, **kwargs):
    end = kwargs.pop('end', None)
    if end is None:
        end = ''
        print(*args, **kwargs, end=end, flush=True)

    flag_jupyter = True
    if flag_jupyter:
        line = input()
    else:
        line = sys.stdin.readline()

    return line


def _write_default_config(user_config_file,
                          download_dir=None):
    flag_new_file = not os.path.isfile(user_config_file)
    writting_mode = 'w'
    if not flag_new_file:
        writting_mode += '+'
    try:
        with open(user_config_file, writting_mode) as f:
            if flag_new_file:
                f.write('# LOG_DIR = ~/dat/plant/log\n')
                f.write('# FLAG_SHOW_ELAPSED_TIME = True\n')
                f.write('# DEBUG_LEVEL = 0\n')
                f.write('# DEBUG = True\n')
            if download_dir is not None:
                f.write('DOWNLOAD_DIR = %s'
                        % (download_dir) + '\n')
    except BaseException:
        pass
