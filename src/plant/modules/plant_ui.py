#!/usr/bin/env python3
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Gustavo H. X. Shiroma, Marco Lavalle
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import os
import time
import copy
import difflib
import itertools
import numpy as np
from collections.abc import Sequence
import matplotlib as mpl
from matplotlib import colors, cm, axes, style
from matplotlib.axes import Axes
from matplotlib.font_manager import FontProperties
from matplotlib.colors import ListedColormap, LinearSegmentedColormap
from matplotlib import colors as mcolors
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.ticker import FormatStrFormatter
import plant

FLAG_FIX_AXIS_DECIMAL_PLACES = False


class MetaProgress(type):
    _meta_instances = {}

    def __call__(cls, *args, **kwargs):

        if cls in cls._meta_instances:
            total = args[0]
            sub_total = kwargs.get('sub_total', None)

            current_cls = cls._meta_instances[cls]
            flag_same_total = total == current_cls.total
            flag_same_subtotal = ((sub_total is None and
                                   current_cls.sub_total is None) or
                                  (sub_total is not None and
                                   current_cls.sub_total is not None and
                                   sub_total == current_cls.sub_total))
            flag_new_class = not flag_same_total or not flag_same_subtotal
            if flag_new_class:
                current_cls.__exit__()
                current_cls.__init__(*args, **kwargs)

        else:
            cls._meta_instances[cls] = \
                super(MetaProgress, cls).__call__(*args, **kwargs)
        return cls._meta_instances[cls]


class PrintProgress(metaclass=MetaProgress):

    def __init__(self, total, sub_total=None, prefix=None,
                 max_value=100, step=1, verbose=True):

        if hasattr(total, '__getitem__'):
            self.total = len(total)
            self.iterator = total

            self.item_counter = 0
        else:
            self.total = total
            self.iterator = None
            self.item_counter = None
        self.sub_total = sub_total
        self._sub_progress_history = []

        self.prefix = prefix
        self.max_value = max_value
        self.step = step
        self.verbose = True
        self.reset()

    def __iter__(self):
        return self

    def __getitem__(self, value):
        ret = self.iterator.__getitem__(value)
        self.print_progress(value)
        return ret

    def __next__(self):
        try:
            ret = self.iterator.__getitem__(self.item_counter)
        except IndexError:
            self.__exit__()
            raise StopIteration
        self.print_progress(self.item_counter)
        self.item_counter += 1
        return ret

    def __enter__(self):
        self.reset()
        return self

    def reset(self):
        self._sub_progress_history = []

        self._last_message_length = None
        self._sub_progress_last = -1
        self._sub_progress_last_value = -1
        self._progress_last = -1
        self._progress_last_value = -1
        self._progress_start_time = time.time()
        self._progress_last_time = time.time()
        return self

    def print_progress(self, value, sub_value=None, prefix=None):

        if prefix is None:
            prefix = self.prefix

        min_start_time = 0
        step_time = 0.1

        progress = self._get_progress(value, self.total)

        sub_progress = self._get_progress(sub_value, self.sub_total)

        self._progress_last_value = value
        flag_first_execution = self._progress_last == -1
        flag_last_execution = progress >= self.max_value
        flag_update = (progress != self._progress_last or
                       (sub_progress != self._sub_progress_last_value and
                        not flag_last_execution))

        if flag_first_execution:
            flag_time = ((time.time() - self._progress_start_time) >
                         min_start_time)
        else:
            flag_time = ((time.time() - self._progress_last_time) >
                         step_time)

        if ((flag_first_execution and flag_last_execution) or
                not flag_update or
                (not flag_time and not flag_last_execution)):
            return

        median_deriv = None
        if sub_progress is not None:

            self._sub_progress_history.append(sub_progress)
            if len(self._sub_progress_history) > 10:
                self._sub_progress_history = self._sub_progress_history[0:10]

            if len(self._sub_progress_history) > 4:
                median_deriv = np.median(
                    np.asarray(self._sub_progress_history[1:]) -
                    np.asarray(self._sub_progress_history[:-1]))
                if median_deriv <= 0:
                    sub_progress = None

        length = 40
        time_diff = time.time() - self._progress_start_time
        if prefix is None:
            prefix = 'progress'
        if sub_progress is None:
            progress_bar = ('=' * int(length * progress / 100) +
                            ' ' * int(length - int(length * progress / 100)))
        else:
            both_progress = min(progress, sub_progress)
            progress_diff = np.clip(progress - both_progress, 0, None)
            sub_progress_diff = np.clip(sub_progress - progress, 0, 100)
            progress_bar = ("=" * int(length * both_progress / 100) +
                            ':' * int(length * progress_diff / 100) +
                            '-' * int(length * sub_progress_diff / 100) +
                            ' ' * int(length - int(length * progress / 100) -
                                      int(length * sub_progress_diff / 100)))

        elapsed_time_str = 'Elapsed: %s | ' % get_time_diff_str(time_diff,
                                                                flag_min=True)
        if sub_progress is None:
            overall_progress = progress
        else:
            overall_progress = (progress + (float(sub_progress) / self.total))

        if sub_progress is None:
            overall_progress = progress
        else:
            overall_progress = self.max_value * (value / self.total +
                                                 sub_value / (self.sub_total *
                                                              self.total))

        if overall_progress != 0 and overall_progress < self.max_value:
            eta = (time_diff * float(self.max_value - overall_progress) /
                   float(overall_progress))
            eta_str = 'ETA: %s | ' % (get_time_diff_str(eta,
                                                        flag_min=True))
        else:
            eta_str = ''

        if not flag_first_execution and self._last_message_length is not None:
            print('\r' + ' ' * (self._last_message_length + 1), end='')

        start_char = '' if flag_first_execution else '\r'
        end_char = '\n' if flag_last_execution else ''

        message = f'{start_char}{prefix} [{progress_bar}] {progress}%'
        if sub_progress is not None:
            message += f' ({sub_progress}%)|'
        message += f' | '
        message += f' {elapsed_time_str}{eta_str}{value:.0f}/{self.total}'
        if sub_progress is not None:
            message += f' ({sub_value:.0f}/{self.sub_total})'

        print(message, end=end_char)
        if progress > self.max_value:
            self.reset()
            return
        self._last_message_length = len(message)
        self._progress_last = progress
        self._progress_last_time = time.time()

    def __exit__(self, type=None, value=None, traceback=None):
        if traceback is None:

            self.print_progress(self.total,
                                sub_value=self.sub_total)

            self.reset()
        else:

            return

    def _get_progress(self, value, total):
        if value is None or self.total is None:
            return
        progress = ((self.max_value * value / total) // self.step) * self.step
        progress = np.clip(progress, 0, self.max_value)
        progress = type(self.step)(progress)
        return progress


def print_progress(value, total, sub_value=None, sub_total=None,
                   prefix=None, max_value=100, step=1, verbose=True):
    pp = plant.PrintProgress(total, sub_total=sub_total, prefix=prefix,
                             max_value=max_value, step=step, verbose=verbose)

    pp.print_progress(value, sub_value=sub_value)


def get_time_diff_str(timediff, flag_min=False):
    timediff = int(timediff)
    hours, remainder = divmod(timediff, 3600)
    minutes, seconds = divmod(remainder, 60)
    if flag_min and hours == 0 and minutes == 0:
        timediff_str = '%ds' % seconds
    elif flag_min and hours == 0:
        timediff_str = '%dm%ds' % (minutes, seconds)
    else:
        timediff_str = '%dh%dm%ds' % (hours, minutes, seconds)
    return timediff_str


def get_image_names(input_list,
                    max_name_size=None,
                    update_names_larger_than=None):
    if len(input_list) == 0:
        return input_list
    master_name = input_list[0]
    master_name_splitted = master_name.split('.')

    new_list = None
    if len(master_name_splitted) >= 2 and len(master_name_splitted[-1]) <= 4:
        ext = '.' + master_name_splitted[-1]
        if all([x.endswith(ext) for x in input_list]):
            new_list = []
            for current_input in input_list:
                new_list.append(current_input.replace(ext, ''))
    if new_list is None:
        new_list = input_list

    basename_list = [os.path.basename(x) for x in new_list]
    basename_list_without_duplicates = list(set(basename_list))
    if len(basename_list) == len(basename_list_without_duplicates):
        new_list = basename_list

    if (update_names_larger_than is None or
            any([len(x) > update_names_larger_than
                 for x in new_list])):
        new_list = get_name_differences(new_list)

    if max_name_size is not None:
        short_name_list = []
        for current_name in new_list:
            if len(current_name) > max_name_size - 3:
                current_name = '...' + current_name[len(current_name) -
                                                    (max_name_size - 3):]
            short_name_list.append(current_name)

        if len(input_list) <= 1:
            return short_name_list
        new_list = short_name_list

    counter = 2
    for im, current_name in enumerate(new_list):
        for im2, fixed_name in enumerate(new_list):
            if im2 >= im:
                continue
            if (current_name == fixed_name.replace(' (1)', '')):
                if (counter == 2 and not fixed_name.endswith(' (1)')):
                    fixed_name += ' (1)'
                current_name = current_name + ' (' + str(counter) + ')'
                counter += 1

    return new_list


def get_blocks(input_1, input_2, block_list=[]):
    args = [None] + [input_1] + [input_2]
    s = difflib.SequenceMatcher(*args)
    keep_chars = np.zeros((len(input_1)))
    for key, value in plant.RADAR_DECOMP_DICT:
        for substring in value:
            if substring in input_1:
                start_index = input_1.index(substring)
                end_index = start_index + len(substring)
                keep_chars[start_index:end_index] = 1
    MIN_NCHAR = 2
    for tag, i1, i2, j1, j2 in s.get_opcodes():
        if tag != 'equal':
            continue
        while keep_chars[i1]:
            i1 += 1
            if i2 - 1 - i1 <= MIN_NCHAR:
                continue
        while keep_chars[i2 - 1]:
            i2 -= 1
            if i2 - 1 - i1 <= MIN_NCHAR:
                continue
        if i2 - 1 - i1 <= MIN_NCHAR:
            continue
        block = input_1[i1:i2]
        if block in block_list:
            continue
        block_list.append(block)
    return block_list


def get_blocks_from_separator(input_1, input_2,
                              separator='_', block_list=[]):
    input_1_list = input_1.split(separator)
    input_2_list = input_2.split(separator)
    if len(input_1_list) <= 1 or len(input_2_list) <= 1:
        return
    common_blocks = []
    for e1 in input_1_list:
        possible_blocks = [separator + e1 + separator,
                           separator + e1, e1 + separator]
        for block in possible_blocks:
            if (block in input_1 and block in input_2):
                common_blocks.append(block)
                break
    if len(common_blocks) == 0:
        return
    for block in common_blocks:
        if block not in block_list:
            block_list.append(block)
    return True


def get_name_differences(input_list):
    if len(input_list) <= 1:
        return input_list

    new_block_list = None
    for separator in plant.SEPARATOR_LIST:
        flag_error = False
        block_list = []
        for input_1, input_2 in itertools.combinations(input_list, 2):
            ret = get_blocks_from_separator(input_1, input_2,
                                            separator=separator,
                                            block_list=block_list)
            if ret is None:
                flag_error = True
                break
        if flag_error:
            continue
        new_block_list = []
        for block in block_list:
            if all([block in x for x in input_list]):
                new_block_list.append(block)
        if len(new_block_list) != 0:
            new_block_list.sort(key=len, reverse=True)
            break
        else:
            new_block_list = None

    if new_block_list is None:
        separator = ' '
        block_list = []
        for input_1, input_2 in itertools.combinations(input_list, 2):
            get_blocks(input_1, input_2, block_list=block_list)
        new_block_list = []
        for block in block_list:
            if all([block in x for x in input_list]):
                new_block_list.append(block)
        new_block_list.sort(key=len, reverse=True)

    last_list = input_list[:]
    new_list = None
    for block in new_block_list:
        new_list = []
        for current_input in last_list:
            new_name = current_input.replace(block, separator)
            new_name = new_name.strip(separator)
            new_list.append(new_name)
        if any([len(x) == 0 for x in new_list]):
            break
        last_list = new_list

    if new_list is None:
        return input_list
    final_list = []
    for block in new_list:
        new_block = block.strip(separator)
        while True:
            block_without_duplicates = new_block.replace(separator + separator,
                                                         separator)
            if new_block == block_without_duplicates:
                break
            new_block = block_without_duplicates
        new_block = block_without_duplicates.replace(separator, ' ')
        final_list.append(new_block)

    return final_list


def imshow(*args, **kwargs):

    if len(args) == 1 and (isinstance(args[0], tuple) or
                           isinstance(args[0], list)):
        args = args[0]
    if len(args) > 1:

        if len(args) == 2:
            args = list(args) + [args[0]]
            if 'vmin' in kwargs.keys():
                vmin = kwargs['vmin']
                if (vmin is not None and
                    not plant.isnumeric(vmin) and
                        len(vmin) == 2):
                    vmin.append(vmin[0])
                    kwargs['vmin'] = vmin
            if 'vmax' in kwargs.keys():
                vmax = kwargs['vmax']
                if (vmax is not None and
                    not plant.isnumeric(vmax) and
                        len(vmax) == 2):
                    vmax.append(vmax[0])
                    kwargs['vmax'] = vmax
        if len(args) <= 3:
            ret = imshow_rgb(*args, **kwargs)
        else:
            ret = imshow_rgba(*args, **kwargs)
        return ret
    if isinstance(args[0], plant.PlantImage):

        image_obj = args[0]
        args = []
        if kwargs.get('geotransform', None) is None:
            kwargs['geotransform'] = image_obj.geotransform
        if kwargs.get('ctable', None) is None:
            kwargs['ctable'] = image_obj.ctable
        for b in range(image_obj.nbands):
            args.append(image_obj.get_image(band=b))
        ret = imshow(*args, **kwargs)
        return ret

    ret = imshow_single(*args, **kwargs)
    return ret


def imshow_single(data_orig, **kwargs):

    db10 = kwargs.pop('db10', False)
    db20 = kwargs.pop('db20', False)
    vmax = kwargs.pop('vmax', None)
    vmin = kwargs.pop('vmin', None)
    fs = kwargs.pop('fs', None)

    percentile = kwargs.pop('percentile', None)
    flag_scale_data = kwargs.pop('flag_scale_data', False)
    geotransform = kwargs.pop('geotransform', None)

    ctable = kwargs.get('ctable', None)
    hillshade = kwargs.get('hillshade', None)
    ret_dict = prepare_data(data_orig,

                            vmin=vmin,
                            vmax=vmax,
                            fs=fs,
                            geotransform=geotransform,
                            db10=db10,
                            db20=db20,
                            percentile=percentile,
                            flag_scale_data=(flag_scale_data and
                                             not hillshade and
                                             ctable is None))

    if ret_dict is None:
        print('ERROR input could not be read.')
        return

    data = ret_dict['data']
    vmin = ret_dict['vmin']
    vmax = ret_dict['vmax']

    geotransform_new = ret_dict['geotransform']
    kwargs['geotransform'] = geotransform_new

    ret = _imshow([data], vmin, vmax, **kwargs)
    return ret


def image_ctable_expand(image_obj):
    ret_list = []
    for b in range(image_obj.nbands):
        band_obj = image_obj.get_band(band=b)
        image = band_obj.get_image
        ctable = band_obj.get_ctable
        image_color_list = image_to_rgb_ctable(image, ctable)
        ret_list += image_color_list

    return ret_list


def ctable_to_array(ctable):
    n_entries = ctable.GetCount()
    all_data = np.arange(n_entries)
    colors = image_to_rgb_ctable(all_data, ctable=ctable,

                                 flag_colors=True)

    return colors, all_data


def image_to_rgb_ctable(image,
                        ctable=None,
                        flag_colors=False,
                        background_color=None,
                        flag_transparent_background=None):
    if ctable is None:
        return [image]

    if background_color is not None:
        if 'transparent' in background_color:
            flag_transparent_background = True
            background_color = background_color.replace('transparent', '')
            background_color = background_color.replace('_', '').strip()
        try:
            background_list = mcolors.to_rgba(background_color)
        except AttributeError:
            background_list = list(
                mcolors.hex2color(mcolors.cnames[background_color])) + [1]
        background_list = 255 * np.asarray(background_list)
        if flag_transparent_background is True:
            background_list[3] = 0

    image_color_list = []

    n_colors = None
    if flag_colors:
        colors = []
    image_unique = np.unique(image).tolist()
    null = None

    if isinstance(image, np.ma.core.MaskedArray):

        null = image.get_fill_value()

        image_unique = np.ma.getdata(image_unique)

    for i in image_unique:
        if i is None:
            continue
        try:
            entry = ctable.GetColorEntry(int(i))
        except BaseException:
            entry = None
        if not entry:
            continue

        if (null is not None and i == null and
                isinstance(image, np.ma.core.MaskedArray)):
            ind = np.where((image == i) | (image.mask))
        else:
            ind = np.where(image == i)
        if plant.get_indexes_len(ind) == 0:
            continue

        if n_colors is None:
            n_colors = len(entry)

        if flag_colors:
            colors.append(list(entry))

            continue

        if len(image_color_list) == 0:
            for color in range(n_colors):
                image_color_list.append(
                    np.zeros((image.shape),
                             dtype=image.dtype))

        for color in range(n_colors):
            if entry[color] == 0:
                continue
            if background_color is not None and color != 3:
                image_color_list[color][ind] = \
                    (entry[color] * entry[3] / 255. +
                     background_list[color] * (1. - entry[3] / 255.))
                continue
            if background_color is not None and color == 3:
                image_color_list[color][ind] = 255
                continue
            image_color_list[color][ind] = entry[color]

    if flag_colors:
        colors = [np.asarray(c) for c in colors]
        if background_color is not None:
            for i, c in enumerate(colors):
                if c[3] == 255:
                    continue

                colors[i][0:3] = (c[0:3] * c[3] / 255. +
                                  background_list[0:3] * (1. - c[3] / 255))
                colors[i][3] = 255

        return colors

    if len(image_color_list) == 0:
        image_color_list = [image, image, image, image]

    return image_color_list


def ctable_to_cmap(ctable, data=None,
                   background_color=None):
    if data is None:
        colors, data = ctable_to_array(
            ctable, background_color=background_color)
    else:
        data = np.unique(data)
        if (isinstance(data, np.ma.core.MaskedArray)):
            data = np.ma.getdata(data)
        colors = image_to_rgb_ctable(
            data, ctable, flag_colors=True,
            background_color=background_color)
    colors = [c / 255. for c in colors]

    cmap = LinearSegmentedColormap.from_list('custom', colors,
                                             len(colors))
    return cmap, data


def get_edgecolor(color):
    if color is None:
        return
    if plant.isnumeric(color):
        color_mean = color
    elif ((isinstance(color, tuple) or
           isinstance(color, list)) and
          all([plant.isnumeric(x) for x in color])):
        color_mean = np.nanmax(color[0:3])
    else:
        try:
            color_vector = colors.to_rgba(color)
        except AttributeError:
            color_vector = list(
                colors.hex2color(colors.cnames[color])) + [1]
        color_mean = np.nanmax(color_vector[0:3])
    edgecolor = 'black' if color_mean >= 0.1 else '0.4'
    return edgecolor


def _imshow(data,
            vmin,
            vmax,
            grid=False,
            title=None,

            geotransform=None,
            flag_folium=None,
            folium_map=None,
            flag_cartopy=None,
            hillshade=None,
            cmap=None,
            ctable=None,
            verbose=True,
            output_file=None,
            crop_output=None,
            no_show=False,
            interpolation=None,
            draw_ocean=None,
            draw_land=None,
            draw_lakes=None,
            draw_borders=None,
            draw_coastline=None,
            draw_rivers=None,
            flag_colorbar_in_line=True,

            background_color=None,
            dark_theme=False,
            plot_style=None,
            zorder=None,

            xlabel_vertical=None,
            xlabel_rotation=None,

            tick_params_top=None,
            tick_params_labeltop=None,
            tick_params_bottom=None,
            tick_params_labelbottom=None,
            tick_params_left=None,
            tick_params_labelleft=None,
            tick_params_right=None,
            tick_params_labelright=None,

            fontsize=None,
            label_x=None,
            label_y=None,
            plot_size_x=None,
            plot_size_y=None,
            dpi=None,
            facecolor=None,

            name=None,
            colorbar_label=None,

            decimal_places=None,
            n_significant_figures=None,

            colorbar_orientation=None,
            flag_colorbar=None,
            colorbar_db=False,
            aspect=None,
            extent=None,
            xlim=None,
            ylim=None,
            invert_y_axis=False,
            invert_x_axis=False,
            origin=None,
            force=False,
            data_orig=None,
            animated=False,
            ax=None,
            plot=None):

    if ((isinstance(data, list) or isinstance(data, tuple)) and
            len(data[0].shape) == 3):
        kwargs = locals()
        kwargs.pop('data')
        depth, length, width = plant.get_image_dimensions(data[0])
        for d in range(depth):
            args = [[plant.shape_image(image[d, :, :]) for image in data]]
            ret = _imshow(*args, **kwargs)
        return ret

    if ((isinstance(data, list) or isinstance(data, tuple)) and
            len(data) > 1):
        data = np.dstack(data)
    else:
        data = data[0]

    import matplotlib.pyplot as plt
    if dark_theme or plot_style == 'dark_background':
        if plot_style is None:
            plot_style = 'dark_background'
        if background_color is None:
            background_color = plant.PLOT_BACKGROUND_COLOR_DARK
        if facecolor is None:
            facecolor = plant.PLOT_BACKGROUND_COLOR_DARK

    if plot_style is not None:
        style.use(plot_style)

    if cmap is None:
        try:
            cmap = copy.copy(mpl.colormaps.get_cmap("jet"))
        except AttributeError:
            cmap = copy.copy(cm.get_cmap("jet"))

        alpha = 0.0
        if background_color is None and not dark_theme:
            background_color = 'white'

        cmap.set_bad(background_color, alpha=alpha)
        cmap.set_over(background_color, alpha=alpha)
        cmap.set_under(background_color, alpha=alpha)

    imshow_min = None
    imshow_max = None
    data_orig = None
    if ctable is not None:

        ret = image_to_rgb_ctable(data, ctable)

        data_orig = data
        cmap = None
        data = np.dstack(tuple(ret))
        vmin = None
        vmax = None

    if hillshade:
        data = np.asarray(data, np.float32)
        if data_orig is None:
            data_orig = data
        image_obj = plant.PlantImage(data,
                                     verbose=False,
                                     only_header=True,
                                     force=force)
        ret_dict = plant.image_obj_expand_rgb(
            image_obj,
            flag_hillshade=True,
            cmap=cmap,
            background_color=background_color,
            cmap_min=vmin,
            cmap_max=vmax,

            verbose=verbose)
        image_obj = ret_dict['image_obj']
        cmap = ret_dict['cmap']
        imshow_min = ret_dict['cmap_min']
        imshow_max = ret_dict['cmap_max']

        data = np.asarray(image_obj.to_array(), dtype=np.float32)

    if imshow_min is None or imshow_max is None:

        if vmin is None or vmax is None:
            ind = np.where(plant.isvalid(data))
            if vmin is None and np.any(data[ind] < 0):
                imshow_min = np.nanmin(data)
            elif vmin is None:
                imshow_min = 0
            if vmax is None and np.any(data[ind] > 255):
                imshow_max = np.nanmax(data)
            elif np.any(data > 1) and vmax is None:
                imshow_max = 255
            elif vmax is None:
                imshow_max = 1

        if (vmin is not None and not isinstance(vmin, list) and
                vmax is not None and not isinstance(vmax, list)):
            imshow_min = vmin
            imshow_max = vmax

        if vmin is None:
            vmin = np.nanmin(data)
        if vmax is None:
            vmax = np.nanmax(data)

    dtype = None
    if (imshow_max is not None and np.max(imshow_max) > 1 and
            'int' in plant.get_dtype_name(data)):
        dtype = np.uint8

    else:
        dtype = np.float32
    if dtype is not None and data.dtype != dtype:
        data = np.asarray(data, dtype=dtype)

    if interpolation is None:

        interpolation = 'None'

    image_length = data.shape[0]
    image_width = data.shape[1]

    if ((flag_cartopy or flag_cartopy is None) and
            geotransform is not None):
        try:
            import cartopy.crs as ccrs
            flag_cartopy = True
        except ModuleNotFoundError:
            if flag_cartopy:
                print('WARNING error importing cartopy. '
                      'Using matplotlib instead..')
            flag_cartopy = False

    if flag_cartopy or flag_folium:

        plant_geogrid_obj = plant.get_coordinates(
            geotransform=geotransform,
            length=image_length,
            width=image_width)

        lat_min = plant_geogrid_obj.yf
        lat_max = plant_geogrid_obj.y0
        lon_max = plant_geogrid_obj.xf
        lon_min = plant_geogrid_obj.x0
        step_x = plant_geogrid_obj.step_x
        step_y = plant_geogrid_obj.step_y

    if image_width > image_length:
        PLOT_N_LAT_LABELS = 10
        PLOT_N_LON_LABELS = 7
    elif image_width * 3 > image_length:
        PLOT_N_LAT_LABELS = 10
        PLOT_N_LON_LABELS = 5
    elif image_width * 2 > image_length:
        PLOT_N_LAT_LABELS = 10
        PLOT_N_LON_LABELS = 4
    else:
        PLOT_N_LAT_LABELS = 10
        PLOT_N_LON_LABELS = 3
    if flag_folium:

        import folium
        from folium import raster_layers
        if folium_map is None:

            folium_map = folium.Map(location=[(lat_min + lat_max) / 2,
                                              (lon_min + lon_max) / 2],

                                    tiles=None)
            folium_map.fit_bounds([[lat_min, lon_min],
                                   [lat_max, lon_max]])

        image = image_to_figure(data,
                                cmap=cmap,
                                cmap_min=imshow_min,
                                cmap_max=imshow_max,

                                verbose=False)

        raster_layers.ImageOverlay(

            image=image,
            name=name,
            mercator_project=True,

            bounds=[[lat_min, lon_min], [lat_max, lon_max]]
        ).add_to(folium_map)

        if (output_file is not None and
            (output_file.endswith('.html') or
             output_file.endswith('.htm'))):
            save_folium(folium_map,
                        output_file,
                        verbose=verbose,
                        force=force)
        return folium_map

    if facecolor is None:
        facecolor = 'w'
    if plot is None:
        plot = plt
        if plot_size_x is not None and plot_size_y is not None:
            mngr = plot.get_current_fig_manager()
            fig = plot.figure(figsize=(plot_size_x, plot_size_y),
                              dpi=dpi,
                              facecolor=facecolor,
                              edgecolor='k')
            mngr.window.setGeometry(0,
                                    0,
                                    plot_size_x,
                                    plot_size_y)
        else:
            fig = plot.figure(facecolor=facecolor,
                              edgecolor='k')

        size_shape = fig.get_size_inches()

        if title is not None:
            fig.canvas.set_window_title(title)

    if ax is None and isinstance(plot, axes._base._AxesBase):
        ax = plot
    elif ax is None:
        ax = plot.gca()

    if flag_cartopy:
        print('INFO using cartopy...')

        mean_step = (step_y + step_x) / 2

        from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, \
            LATITUDE_FORMATTER
        import cartopy.feature as cfeature
        gl = ax.gridlines(crs=ccrs.PlateCarree(),
                          draw_labels=True,

                          linestyle='--')
        if not grid:
            gl.ylines = False
            gl.xlines = False

        try:
            gl.top_labels = False
            gl.right_labels = False
        except BaseException:
            gl.xlabels_top = False
            gl.ylabels_right = False

        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER

        linewidth = 1
        if extent is None and geotransform is not None:
            extent = [lon_min, lon_max, lat_min, lat_max]

    args = [data]
    kwargs = {}
    kwargs['cmap'] = cmap
    kwargs['vmin'] = imshow_min
    kwargs['vmax'] = imshow_max
    kwargs['aspect'] = aspect
    kwargs['extent'] = extent
    kwargs['origin'] = origin
    kwargs['animated'] = animated
    kwargs['interpolation'] = interpolation
    kwargs['resample'] = False

    if zorder is not None:
        kwargs['zorder'] = zorder

    ret = plot.imshow(*args, **kwargs)

    if xlabel_vertical:

        ax.tick_params(axis='x', rotation=90)

    elif xlabel_rotation is not None:

        try:
            ax.tick_params(axis='x', rotation=xlabel_rotation)

        except ValueError:
            print('WARNING there was an error setting the xlabel_rotation'
                  f' to {xlabel_rotation}')

        if (xlabel_rotation == 'horizontal' or
            (plant.isnumeric(xlabel_rotation) and
                float(xlabel_rotation) < 20)):
            ha = 'center'
        else:
            ha = 'right'

        for label in ax.xaxis.get_ticklabels():
            label.set_ha(ha)

    if label_y is not None:
        ax.set_ylabel(label_y, fontsize=fontsize)
    if label_x is not None:
        ax.set_xlabel(label_x, fontsize=fontsize)
    if invert_x_axis:
        ax.invert_xaxis()
    if invert_y_axis:
        ax.invert_yaxis()

    if tick_params_top is not None:
        ax.tick_params(top=tick_params_top)
    if tick_params_labeltop is not None:
        ax.tick_params(labeltop=tick_params_labeltop)
    if tick_params_bottom is not None:
        ax.tick_params(bottom=tick_params_bottom)
    if tick_params_labelbottom is not None:
        ax.tick_params(labelbottom=tick_params_labelbottom)
    if tick_params_left is not None:
        ax.tick_params(left=tick_params_left)
    if tick_params_labelleft is not None:
        ax.tick_params(labelleft=tick_params_labelleft)
    if tick_params_right is not None:
        ax.tick_params(right=tick_params_right)
    if tick_params_labelright is not None:
        ax.tick_params(labelright=tick_params_labelright)

    if xlim:
        xmin, xmax = ax.get_xlim()
        if xmax < xmin:
            xlim = xlim[::-1]
        if plant.isvalid(xlim[0]):
            xmin = xlim[0]
        if plant.isvalid(xlim[1]):
            xmax = xlim[1]
        ax.set_xlim([xmin, xmax])

    if ylim:
        ymin, ymax = ax.get_ylim()
        if ymax < ymin:
            ylim = ylim[::-1]
        if plant.isvalid(ylim[0]):
            ymin = ylim[0]
        if plant.isvalid(ylim[1]):
            ymax = ylim[1]
        ax.set_ylim([ymin, ymax])

    if FLAG_FIX_AXIS_DECIMAL_PLACES and decimal_places is not None:
        ax.xaxis.set_major_formatter(
            FormatStrFormatter(f'%.{decimal_places}f'))
        ax.yaxis.set_major_formatter(
            FormatStrFormatter(f'%.{decimal_places}f'))

    if flag_cartopy:

        if draw_ocean:

            ax.add_feature(cfeature.OCEAN)
        if draw_land:
            ax.add_feature(cfeature.LAND)
        if draw_lakes:
            ax.add_feature(cfeature.LAKES)
        if draw_borders:
            ax.add_feature(cfeature.BORDERS)
        if draw_coastline:
            ax.add_feature(cfeature.COASTLINE)
        if draw_rivers:
            ax.add_feature(cfeature.RIVERS)

        if not grid:
            linewidth = 0
    elif grid:
        plot.grid(True)

    if title:
        try:
            plot.title(title, fontsize=fontsize)
        except TypeError:
            pass

    flag_rgb = len(data.shape) >= 3
    if flag_colorbar is None:
        flag_colorbar = flag_colorbar or colorbar_db or colorbar_label
    if flag_colorbar or (flag_colorbar is None and not flag_rgb):

        ref_fontsize = 12 if fontsize is None else fontsize
        add_vertical_space = 2.0 * bool(label_x) * ref_fontsize / 100.0
        if tick_params_labelbottom is not False:
            add_vertical_space += 2.0 * ref_fontsize / 100

        add_vertical_space += 0.05

        insert_colorbar(cmap=cmap,
                        cmin=vmin,
                        cmax=vmax,
                        name=colorbar_label,
                        flag_db=colorbar_db,
                        ax=ax,
                        orientation=colorbar_orientation,
                        add_vertical_space=add_vertical_space,

                        background_color=background_color,
                        decimal_places=decimal_places,
                        n_significant_figures=n_significant_figures,
                        flag_in_line=flag_colorbar_in_line,
                        ctable=ctable, data=data_orig,
                        fontsize=fontsize)

    if output_file:
        try:

            save_fig(output_file, plot=plot,
                     crop_output=crop_output,
                     verbose=verbose,
                     force=force)
            if verbose and plant.isfile(output_file):
                print('## file saved: ' + output_file)
        except AttributeError:
            print('WARNING error saving: ' + output_file)
    if not no_show:
        try:
            plot.show()
        except AttributeError:
            pass
        except KeyboardInterrupt:
            raise plant.PlantExceptionKeyboardInterrupt

    return ret


def image_to_figure(*args, **kwargs):
    import matplotlib.pyplot as plt
    if (len(args) == 1 and
            isinstance(args[0], np.ndarray) and
            len(args[0].shape) >= 3):
        args = [args[0][:, :, i]
                for i in range(args[0].shape[2])]

    else:
        args = list(args)
    n_images = len(args)
    if n_images == 0:
        print('ERROR (image_to_figure) invalid input')
        return

    percentile = kwargs.pop('percentile', 95)
    cmap = kwargs.pop('cmap', plant.DEFAULT_CMAP)
    cmap_min = kwargs.pop('cmap_min', None)
    cmap_max = kwargs.pop('cmap_max', None)
    background_color = kwargs.pop('background_color', None)
    flag_scale_data = kwargs.pop('flag_scale_data', None)
    flag_transparent_background = kwargs.pop('flag_transparent_background',
                                             None)
    verbose = kwargs.pop('verbose', True)

    if verbose and n_images == 1:
        print('color map: %s' % cmap)
    if verbose and background_color is not None:
        print('background color: %s' % background_color)

    if flag_scale_data is None:
        min_all = np.nan
        max_all = np.nan
        for i in range(n_images):
            image = np.asarray(args[i], dtype=np.float32)
            image_max = np.nanmax(image)
            if plant.isnan(max_all) or max_all < image_max:
                max_all = image_max
            image_min = np.nanmin(image)
            if plant.isnan(min_all) or min_all < image_min:
                min_all = image_min

        flag_scale_data = bool(min_all < 0 or max_all > 1)

    if flag_scale_data is not False:

        nbands = n_images
        cmap_min_band = plant.demux_input(cmap_min, nbands)
        cmap_max_band = plant.demux_input(cmap_max, nbands)
        for i in range(nbands):
            if verbose:
                print(f'preparing band: {i} (imsave)')

            image = args[i]
            ret_dict = plant.prepare_data(image,

                                          flag_scale_data=True,
                                          vmin=cmap_min_band[i],
                                          vmax=cmap_max_band[i],
                                          percentile=percentile)
            if ret_dict is not None:
                image = ret_dict['data']
                args[i] = image
                if verbose:
                    with plant.PlantIndent():
                        vmin = ret_dict['vmin']
                        vmax = ret_dict['vmax']

                        print(f'color min: {vmin}')

                        print(f'color max: {vmax}')
                        if percentile is not None:
                            print(f'percentile: {percentile}')

    for i in range(n_images):

        if np.issubdtype(args[i].dtype, np.uint8):

            continue
        if np.issubdtype(args[i].dtype, np.integer):

            image = np.asarray(args[i])
            image = np.clip(image, 0, 255).astype(np.uint8)

        else:

            image = np.asarray(args[i])
            image = np.clip(image, 0, 1)

        args[i] = image

    if background_color is None:
        background_list = [0, 0, 1, 0]
    elif ',' in background_color:
        flag_error = False
        try:
            background_list = [float(v) for v in background_color.split(',')]
        except BaseException:
            flag_error = True

        if flag_error or len(background_list) < 3 or len(background_list) > 4:
            print(f'ERROR invalid background color: {background_color}')
        background_color = background_list

    else:
        if 'transparent' in background_color:
            flag_transparent_background = True
            background_color = background_color.replace('transparent', '')
            background_color = background_color.replace('_', '').strip()
        try:
            background_list = list(mcolors.to_rgba(background_color))
        except AttributeError:
            background_list = list(
                mcolors.hex2color(mcolors.cnames[background_color])) + [1]
        if flag_transparent_background is True:
            background_list[3] = 0

    if n_images == 1:
        ind = np.where(plant.isnan(args[0]))
        try:
            cmap_obj = copy.copy(mpl.colormaps.get_cmap(cmap))
        except AttributeError:
            cmap_obj = copy.copy(plt.get_cmap(cmap))

        if background_color is None:
            cmap_obj.set_bad('b', alpha=0.0)
            cmap_obj.set_over('b', alpha=0.0)
            cmap_obj.set_under('b', alpha=0.0)
        elif 'transparent' in background_color:
            background_color = background_color.replace('transparent', '')
            background_color_alpha_0 = background_color.replace(
                '_', '').strip()
            cmap_obj.set_bad(background_color_alpha_0, alpha=0.0)
            cmap_obj.set_over(background_color_alpha_0, alpha=0.0)
            cmap_obj.set_under(background_color_alpha_0, alpha=0.0)
        else:
            cmap_obj.set_bad(background_color, alpha=1.0)
            cmap_obj.set_over(background_color, alpha=1.0)
            cmap_obj.set_under(background_color, alpha=1.0)
        args = cmap_obj(args[0])
        args = [args[:, :, 0], args[:, :, 1], args[:, :, 2], args[:, :, 3]]

        n_images = 4

    flag_int = np.issubdtype(args[0].dtype, np.uint8)
    if n_images != 4:
        if not flag_int:
            alpha_channel = np.full_like(args[0], 1)
        else:
            alpha_channel = np.full(args[0].shape, 255, dtype=np.uint8)
        invalid_ind = np.where(plant.isnan(args[0]))
        alpha_channel[invalid_ind] = 0
    if n_images == 2:
        alpha_channel[np.where(plant.isnan(args[1]))] = 0
        image = np.dstack((args[0],
                           args[1],
                           args[0],
                           alpha_channel))
    elif n_images == 3:
        alpha_channel[np.where(plant.isnan(args[2]))] = 0
        image = np.dstack((args[0],
                           args[1],
                           args[2],
                           alpha_channel))
    elif n_images == 4:
        image = np.dstack((args[0],
                           args[1],
                           args[2],
                           args[3]))

    if not flag_int:
        for i in range(min([4, n_images + 1])):
            if i < n_images:
                ind = np.where(plant.isnan(args[i]))
            else:
                ind = np.where(plant.isnan(alpha_channel))
            ind_len = plant.get_indexes_len(ind)
            x, y = np.asarray(np.meshgrid(background_list,
                                          np.zeros(ind_len)))
            image[ind] = x + y

    return image


def folium_add_layer(folium_map,
                     flag_google_maps=False,
                     flag_google_street_view=False,

                     flag_nexrad=False,
                     flag_all=False):
    import folium
    if flag_all:
        flag_google_maps = True
        flag_google_street_view = True

        flag_all = True

    if flag_google_maps:
        folium.raster_layers.TileLayer(
            tiles='http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}',
            attr='google',
            name='google maps',
            max_zoom=20,
            subdomains=['mt0', 'mt1', 'mt2', 'mt3'],
            overlay=False,
            control=True,
        ).add_to(folium_map)

    if flag_google_street_view:
        folium.raster_layers.TileLayer(
            tiles='http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',
            attr='google',
            name='google street view',
            max_zoom=20,
            subdomains=['mt0', 'mt1', 'mt2', 'mt3'],
            overlay=False,
            control=True,
        ).add_to(folium_map)

    if flag_nexrad:
        folium.raster_layers.WmsTileLayer(
            url='http://mesonet.agron.iastate.edu/cgi-bin/wms/nexrad/n0r.cgi',
            name='nexrad',
            fmt='image/png',
            layers='nexrad-n0r-900913',
            attr=u'Weather data © 2012 IEM Nexrad',
            transparent=True,
            overlay=True,
            control=True,
        ).add_to(folium_map)
    folium.LayerControl().add_to(folium_map)

    return folium_map


def imshow_rgba(data_1_orig,
                data_2_orig,
                data_3_orig,
                alpha,
                **kwargs):
    ret = imshow_rgb(data_1_orig,
                     data_2_orig,
                     data_3_orig,
                     alpha=alpha,
                     **kwargs)
    return ret


def imshow_rgb(data_1_orig,
               data_2_orig,
               data_3_orig,

               **kwargs):

    db10 = kwargs.pop('db10', False)
    db20 = kwargs.pop('db20', False)
    vmax = kwargs.pop('vmax', None)
    vmin = kwargs.pop('vmin', None)
    fs = kwargs.pop('fs', None)

    alpha = kwargs.pop('alpha', None)
    percentile = kwargs.pop('percentile', None)
    flag_scale_data = kwargs.pop('flag_scale_data', True)
    geotransform = kwargs.pop('geotransform', None)

    if vmin is None:
        vmin_1 = vmin_2 = vmin_3 = None
    elif plant.isnumeric(vmin):
        vmin_1 = vmin_2 = vmin_3 = vmin
    elif len(vmin) >= 3:
        vmin_1, vmin_2, vmin_3 = vmin[0:3]

    if vmax is None:
        vmax_1 = vmax_2 = vmax_3 = None
    elif plant.isnumeric(vmax):
        vmax_1 = vmax_2 = vmax_3 = vmax
    elif len(vmax) >= 3:
        vmax_1, vmax_2, vmax_3 = vmax[0:3]

    ret_dict = prepare_data(data_1_orig,

                            vmin=vmin_1, vmax=vmax_1, fs=fs,
                            flag_scale_data=flag_scale_data,
                            geotransform=geotransform, db10=db10,
                            db20=db20, percentile=percentile)
    if ret_dict is None:
        print('ERROR input could not be read.')

        return

    data_1 = ret_dict['data']
    vmin_1 = ret_dict['vmin']
    vmax_1 = ret_dict['vmax']

    geotransform_new = ret_dict['geotransform']
    ret_dict = prepare_data(
        data_2_orig,
        vmin=vmin_2, vmax=vmax_2, fs=fs, db10=db10,
        flag_scale_data=flag_scale_data,
        db20=db20, percentile=percentile)
    if ret_dict is None:
        print('ERROR input could not be read.')

        return

    data_2 = ret_dict['data']
    vmin_2 = ret_dict['vmin']
    vmax_2 = ret_dict['vmax']

    ret_dict = prepare_data(data_3_orig,
                            vmin=vmin_3, vmax=vmax_3, fs=fs, db10=db10,
                            flag_scale_data=flag_scale_data,
                            db20=db20, percentile=percentile)
    if ret_dict is None:
        print('ERROR input could not be read.')

        return

    data_3 = ret_dict['data']
    vmin_3 = ret_dict['vmin']
    vmax_3 = ret_dict['vmax']

    if alpha is None:
        alpha = np.ones_like(data_1)
        ind = np.where(plant.isnan(data_1))
        alpha[ind] = 0
        ind = np.where(plant.isnan(data_2))
        alpha[ind] = 0
        ind = np.where(plant.isnan(data_3))
        alpha[ind] = 0
        vmin = [vmin_1, vmin_2, vmin_3]
        vmax = [vmax_1, vmax_2, vmax_3]

        image = [data_1, data_2, data_3, alpha]
    else:
        ret_dict = prepare_data(alpha,

                                flag_scale_data=False)

        alpha = ret_dict['data']
        max_data_1 = np.nanmax(data_1)
        max_alpha = np.nanmax(alpha)
        data_0_to_1 = max_data_1 <= 1
        alpha_is_byte = max_alpha > 1 and max_alpha <= 255
        if alpha_is_byte and data_0_to_1:
            alpha = alpha / 255.0

        elif max_alpha > 1 and data_0_to_1:

            alpha = alpha / max_data_1
        elif alpha_is_byte:
            alpha = np.asarray(alpha, dtype=data_1.dtype)

        image = [data_1, data_2, data_3, alpha]

    if 'int' in plant.get_dtype_name(image):
        for im in range(len(image)):
            image[im] = np.asarray(image[im], dtype=np.float32) / 255

    kwargs['geotransform'] = geotransform_new
    ret = _imshow(image, vmin, vmax, **kwargs)
    return ret


def prepare_data(data_orig,

                 vmin=None,
                 vmax=None,
                 fs=None,
                 geotransform=None,
                 db10=False,
                 db20=False,
                 percentile=None,
                 flag_scale_data=False):

    if geotransform is not None:
        geotransform_new = geotransform[:]
    if isinstance(data_orig, plant.PlantImage):
        data = data_orig.image.copy()
        geotransform_new = data_orig.geotransform
    elif isinstance(data_orig, str):
        image_obj = plant.read_image(data_orig)
        data = image_obj.image.copy()
        geotransform_new = image_obj.geotransform
    else:
        data = data_orig.copy()

    data = plant.get_db(data, db10=db10, db20=db20) \
        if db10 or db20 else data
    if 'complex' in data.dtype.name.lower():
        print('WARNING data is complex, extracting complex magnitude..')
        data = np.absolute(data)
    if 'geotransform_new' not in locals():
        geotransform_new = None
    if np.iscomplexobj(data):
        print('WARNING data is complex. Calculating magnitude..')
        data = np.absolute(data)

    __, length_orig, width_orig = plant.get_image_dimensions(data)

    if percentile is None:
        percentile = plant.DEFAULT_MIN_MAX_PERCENTILE
    if flag_scale_data is not False and vmin is None:
        try:
            vmin = np.nanpercentile(data, 100 - percentile)
        except ValueError:
            vmin = np.nanmin(data)
    if flag_scale_data is not False and vmax is None:
        try:
            vmax = np.nanpercentile(data, percentile)
        except ValueError:
            vmax = np.nanmax(data)

    if geotransform_new is not None:

        geotransform_new = np.asarray(geotransform_new)

    if fs is not None and fs > 1 and 'int' in data.dtype.name:
        data = np.asarray(data, dtype=np.float32)
        data = plant.filter_data(data, boxcar=fs, verbose=False)

    if flag_scale_data is not False:
        data = np.asarray(data, dtype=np.float32)
        new_max = 1.

        data = plant.scale_data(data, vmin, vmax, new_max, clip=True)

    ret_dict = {}
    ret_dict['data'] = data
    ret_dict['vmin'] = vmin
    ret_dict['vmax'] = vmax

    ret_dict['geotransform'] = geotransform_new
    ret_dict['length'] = length_orig
    ret_dict['width'] = width_orig
    return ret_dict


def get_color_display(counter,

                      flag_pol_color=None,
                      color_vect=None,
                      cmap=None,

                      flag_increasing=False,
                      flag_decreasing=False,
                      n_colors=9,
                      flag_line_or_point=False):

    if flag_increasing and flag_decreasing:
        print('ERROR the options `flag_increasing` and'
              ' `flag_decreasing` are mutuallya exclusive')
        return

    if cmap is not None:

        if flag_increasing:
            x = np.linspace(0, 1, n_colors)
        elif flag_decreasing:
            x = np.linspace(1, 0, n_colors)
        else:
            x = [0.0, 0.875, 0.375, 0.625, 0.5, 0.75, 0.25, 1.0, 0.125]
        if isinstance(cmap, str):
            try:
                cmap = mpl.colormaps.get_cmap(cmap)
            except AttributeError:
                cmap = cm.get_cmap(cmap)
        current_color = cmap(x[np.mod(counter, n_colors)])

        return current_color

    python_color_list = [x for x in colors.cnames.keys()]
    if flag_line_or_point:
        plant_color_list = plant.LINE_AND_POINTS_COLOR_LIST
        plant_color_list = [x[1] for x in plant_color_list]
    else:
        plant_color_list = plant.BAR_COLOR_LIST
        plant_color_list = ['#%02x%02x%02x' % x[1]
                            for x in plant_color_list]

    if flag_pol_color and counter < len(plant.POL_COLOR_LIST):
        counter = plant.POL_COLOR_LIST[counter]

    if (color_vect is None and
            (counter < len(plant_color_list))):
        current_color = plant_color_list[counter]
    elif (color_vect is None and
          counter < len(python_color_list)):
        current_color = python_color_list[counter]
    elif not isinstance(color_vect, list):
        current_color = color_vect
    elif len(color_vect) > counter:
        current_color = color_vect[counter]
    else:
        current_color = color_vect[0]

    return current_color


def get_cmap_display(counter,
                     dark_theme=False,
                     cmap_vect=None):
    if cmap_vect is None and not dark_theme:
        ind = counter % len(plant.CMAP_LIGHT_LIST)
        current_cmap = plant.CMAP_LIGHT_LIST[ind]
    elif cmap_vect is None:
        ind = counter % len(plant.CMAP_DARK_LIST)
        current_cmap = plant.CMAP_DARK_LIST[ind]
    elif not isinstance(cmap_vect, list):
        current_cmap = cmap_vect
    elif len(cmap_vect) > counter:
        current_cmap = current_cmap[counter]
    else:
        current_cmap = current_cmap[0]
    return current_cmap


def _make_rgb_axes(ax, pad=None, pad_in=None, size=None,
                   axes_class=None, plot=None,
                   add_all=True, orientation='vertical', n_elements=None):

    from mpl_toolkits.axes_grid1.axes_divider \
        import make_axes_locatable, Size

    divider = make_axes_locatable(ax)

    if pad is None:
        pad = 0.2
    if pad_in is None:
        pad_in = 0.4

    if size is None:
        size = 0.25
    if n_elements is None:
        n_elements = 3

    if 'v' in orientation.lower():

        height = Size.Fraction(
            (1.0 -
             float(
                 n_elements -
                 1) *
                pad_in) /
            n_elements,
            Size.AxesY(ax))
        bar_list = [height]
        pad_size = Size.from_any(pad, fraction_ref=Size.AxesY(ax))
        pad_size_in = Size.from_any(pad_in, fraction_ref=Size.AxesY(ax))
        width = Size.from_any(size, fraction_ref=Size.AxesX(ax))

        for i in range(1, n_elements):
            bar_list.append(pad_size_in)
            bar_list.append(height)
        divider.set_horizontal([Size.AxesX(ax), pad_size, width])
        divider.set_vertical(bar_list)
    else:

        width = Size.Fraction(
            (1.0 -
             float(
                 n_elements -
                 1) *
                pad_in) /
            n_elements,
            Size.AxesX(ax))
        bar_list = [width]
        pad_size = Size.from_any(pad, fraction_ref=Size.AxesX(ax))
        pad_size_in = Size.from_any(pad_in, fraction_ref=Size.AxesX(ax))
        height = Size.from_any(size, fraction_ref=Size.AxesX(ax))

        for i in range(1, n_elements):
            bar_list.append(pad_size_in)
            bar_list.append(width)
        divider.set_vertical([height, pad_size, Size.AxesY(ax)])
        divider.set_horizontal(bar_list)

    if axes_class is None and 'cartopy' in str(ax.__class__):
        axes_class = Axes

    if axes_class is None:
        try:
            axes_class = ax._axes_class
        except AttributeError:
            axes_class = type(ax)
    ax_rgb = []
    for ny in [4, 2, 0]:
        ax1 = axes_class(ax.get_figure(),
                         ax.get_position(original=True))
        if 'v' in orientation.lower():
            locator = divider.new_locator(nx=2, ny=ny)
        else:
            locator = divider.new_locator(nx=ny, ny=0)
        ax1.set_axes_locator(locator)
        ax_rgb.append(ax1)
    if 'v' in orientation.lower():
        locator = divider.new_locator(0, 0, ny1=-1)
        ax.set_axes_locator(locator)

    if add_all:
        fig = ax.get_figure()
        for ax1 in ax_rgb:
            fig.add_axes(ax1)
    return ax_rgb


def insert_colorbar(plot=None,
                    cmap=None,
                    cmin=None,
                    cmax=None,
                    name='',
                    norm=None,
                    ax=None,
                    pad=None,
                    size=None,

                    ctable=None,
                    data=None,
                    cax=None,
                    axes_class=None,
                    add_vertical_space=None,
                    orientation=None,
                    fontsize=None,

                    n_significant_figures=None,
                    decimal_places=None,
                    background_color=None,

                    n_ticks=None,
                    n_elements=None,
                    flag_db=False,
                    flag_in_line=True,
                    flag_fix_colorbar_size=True):
    kwargs = locals()
    pad_orig = pad

    if axes_class is None and 'cartopy' in str(ax.__class__):
        axes_class = Axes
    kwargs_axes = {}
    if axes_class is not None:
        kwargs_axes['axes_class'] = axes_class
    reference_fontsize = 12 if fontsize is None else 20
    if orientation is None or 'v' in orientation.lower():
        orientation = 'vertical'
    else:
        orientation = 'horizontal'

    if 'v' in orientation.lower():
        axis_location = "right"
    else:
        axis_location = "bottom"

    flag_in_line = flag_in_line is True

    if isinstance(cmin, list) or isinstance(cmax, list):
        if n_elements is None:
            n_elements = len(cmin)
        if flag_in_line and ax is not None:
            cax_list = _make_rgb_axes(ax,
                                      orientation=orientation,
                                      plot=plot,
                                      axes_class=None,
                                      n_elements=n_elements)
        elif flag_in_line and cax is not None:
            cax_list = _make_rgb_axes(cax,
                                      orientation=orientation,
                                      axes_class=None,
                                      plot=plot,
                                      n_elements=n_elements)
        elif flag_in_line and plot is not None:
            cax_list = _make_rgb_axes(plot,
                                      orientation=orientation,
                                      plot=plot,
                                      axes_class=None,
                                      n_elements=n_elements)

        for i, (vmin, vmax) in enumerate(zip(cmin, cmax)):
            if name is not None and isinstance(name, list):
                current_label = name[i]
            else:
                current_label = name
            N = 100
            cmap_array = np.zeros((N, 4))
            cmap_array[:, -1] = 1
            cmap_array[:, i] = np.arange(100) / 100
            cmap_new = ListedColormap(cmap_array)
            kwargs['cmap'] = cmap_new
            kwargs['cmin'] = vmin
            kwargs['cmax'] = vmax
            kwargs['n_elements'] = n_elements
            kwargs_axes['axes_class'] = axes_class

            if pad_orig is None:
                pad = 0.1
            if current_label:
                kwargs['name'] = current_label
                if 'v' not in orientation:
                    pad += 3 * reference_fontsize / 100
                else:
                    pad += 4 * reference_fontsize / 100
            else:
                kwargs['name'] = ' '

            kwargs['add_vertical_space'] = None
            kwargs['flag_fix_colorbar_size'] = False

            flag_create_cax = (ax is not None and
                               not ('cartopy' in str(ax.__class__) and
                                    'v' not in orientation))
            if not flag_in_line and i == 0:
                if ('v' not in orientation and
                        add_vertical_space is not None):
                    pad = add_vertical_space
                else:
                    pad = 0.2
                if flag_create_cax:
                    divider = make_axes_locatable(ax)

            pad = pad_orig if pad_orig is not None else pad
            if flag_in_line:
                kwargs['cax'] = cax_list[i]
            elif flag_create_cax:
                if size is None:
                    size = 0.3
                cax = divider.append_axes(axis_location, size,
                                          pad=pad, **kwargs_axes)
                kwargs['cax'] = cax

            if pad_orig is None:
                kwargs['pad'] = pad

            try:
                insert_colorbar(**kwargs)
            except AttributeError:
                pass
        return

    ticks = None
    if n_elements is None:
        n_elements = 1
    if n_ticks is None:
        if cax is not None:
            ref = cax
        elif ax is not None:
            ref = ax
        else:
            ref = plot.gca()
        if 'v' in orientation.lower():
            size = ref.get_window_extent().height / n_elements
        else:
            size = ref.get_window_extent().width / n_elements
        if size < 70:
            n_ticks = 2
        else:
            n_ticks = 2 * (size // 200) + 3

    if ctable is not None:
        cmap, ticklabels = ctable_to_cmap(
            ctable, data=data, background_color=background_color)
        cmin = 0
        cmax = ticklabels.size
        ticks = np.arange(len(ticklabels)) + 0.5

    elif n_ticks is not None or flag_db:
        if flag_in_line:
            if (n_significant_figures is None and
                    decimal_places is None):
                n_significant_figures = 3

            ticks = np.linspace(cmin, cmax, n_ticks)
            if flag_db:
                ticklabels = [plant.get_db(v) for v in ticks]
            else:
                ticklabels = ticks
            ticklabels = plant.format_number(
                ticklabels,
                decimal_places=decimal_places,
                sigfigs=n_significant_figures)

    if norm is None:
        norm = colors.Normalize(vmin=cmin, vmax=cmax)
    if plot is None:
        import matplotlib.pyplot as plot

    sm = plot.cm.ScalarMappable(cmap=cmap, norm=norm)
    sm.set_array([])

    if (ax is not None and cax is None and
            flag_fix_colorbar_size):
        divider = make_axes_locatable(ax)

        if 'v' not in orientation and add_vertical_space is not None:
            pad = add_vertical_space
        else:
            pad = 0.2

        cax = divider.append_axes(axis_location, size=0.25, pad=pad,
                                  **kwargs_axes)
    cbar = None
    try:
        cbar = plot.colorbar(sm, ax=ax, cax=cax, orientation=orientation)
    except ValueError:
        return
    if name:
        cbar.set_label(name, fontsize=fontsize)

    if ticks is not None:
        cbar.set_ticks(ticks)

        cbar.set_ticklabels(ticklabels)

        cbar.ax.set_yticklabels(ticklabels)

        cbar.ax.tick_params(axis=u'both', which=u'both', length=0)

    return cbar


def plot(*args, **kwargs):

    title = kwargs.pop('title', None)
    plot = kwargs.pop('plot', None)

    xlabel = kwargs.pop('xlabel', None)
    ylabel = kwargs.pop('ylabel', None)
    zlabel = kwargs.pop('zlabel', None)

    no_show = kwargs.pop('no_show', None)
    if plot is None:
        import matplotlib.pyplot as plot
        plot.figure()
    plot.plot(*args, **kwargs)
    plot.grid(True)
    if title is not None:
        plot.title(title)

    ax = plot.gca()
    if xlabel is not None:
        ax.set_xlabel(xlabel)
    if ylabel is not None:
        ax.set_ylabel(ylabel)
    if zlabel is not None:
        ax.set_zlabel(zlabel)

    if not no_show:
        plot.show()
    return plot


def plot_complex_coherence(*args,
                           plot_line=False,
                           verbose=True):
    import matplotlib.pyplot as plt
    ax = plt.subplot(111, projection='polar')
    ax.set_rmax(1)
    if plot_line:
        last_abs = None
        last_ang = None
    for i, gamma in enumerate(args):
        abs_value = np.absolute(gamma)
        ang = np.angle(gamma)
        color = get_color_display(i)
        if verbose:
            print('point %d' % i)
            print('    absolute: ', np.absolute(ang))
            print('    angle [deg]: ', np.degrees(ang))
            print('    angle [rad]: ', ang)
        plt.plot(ang, abs_value, marker='o', color=color)
        if not plot_line:
            continue
        if last_abs is not None and last_ang is not None:
            plt.plot([last_ang, ang],
                     [last_abs, abs_value], color=color)
        last_abs = abs_value
        last_ang = ang
    plt.show()
    return plt


def save_fig(output_file, **kwargs):
    plot = kwargs.pop('plot', None)
    if plot is None:
        import matplotlib.pyplot as plot
    verbose = kwargs.pop('verbose', True)
    force = kwargs.pop('force', False)
    crop_output = kwargs.pop('crop_output', False)

    if output_file.upper().startswith('MEM:'):
        output_key = 'OUTPUT:' + output_file[4:]
        plant.plant_config.variables[output_key] = plot
        return

    flag_update_file = plant.overwrite_file_check(output_file,
                                                  force=force)
    if not flag_update_file:
        return
    if not os.path.isdir(plant.dirname(output_file)):
        os.makedirs(os.path.dirname(output_file))
    plot.savefig(output_file, **kwargs)
    if plant.isfile(output_file) and verbose and not crop_output:
        print('## file saved: %s (save_fig)' % output_file)
        return
    plant.crop_fig(output_file)
    if plant.isfile(output_file) and verbose:
        print('## file saved: %s (cropped save_fig)'
              % output_file)


def save_folium(folium_map, output_file, **kwargs):

    verbose = kwargs.pop('verbose', True)
    force = kwargs.pop('force', False)
    if (isinstance(folium_map, Sequence) and
            len(folium_map) > 1):
        raise NotImplementedError(
            'Not implemented option to save multiple folium'
            ' maps')
    if isinstance(folium_map, Sequence):
        folium_map = folium_map[0]

    flag_update_file = plant.overwrite_file_check(output_file,
                                                  force=force)
    if not flag_update_file:
        return
    if not os.path.isdir(plant.dirname(output_file)):
        os.makedirs(os.path.dirname(output_file))

    folium_map.save(output_file,
                    verbose=verbose,
                    force=force)
    if plant.isfile(output_file) and verbose:
        print('## file saved: %s (cropped save_fig)'
              % output_file)


def save_html_slide_compare(self, input_1, input_2, input_3=None,
                            output_file=None,
                            resize=False, resize_factor=None,
                            force=None, title=None):
    import jinja2

    if force is None and 'force' in self.__dict__.keys():
        force = self.force

    if title is None:
        title = ''

    output_dir = os.path.dirname(output_file)
    if isinstance(input_1, plant.PlantImage):
        image_obj = input_1
    else:
        image_obj = self.read_image(input_1)

    if resize and resize_factor is None:

        resize_factor_x = float(plant.DEFAULT_PLOT_SIZE_X) / image_obj.width

        if resize_factor is None and (resize_factor_x > 1.5 or
                                      1.0 / resize_factor_x > 1.5):
            resize_factor = resize_factor_x

    filename_1 = os.path.abspath(image_obj.filename)
    if (resize_factor is not None or
            image_obj.file_format not in plant.FIG_DRIVERS):
        filename_1 = plant.get_temporary_file(
            suffix='_1', ext='.png')
        if resize_factor is not None:
            image_obj = plant.filter_data(image_obj,
                                          oversampling=resize_factor)

        self.save_image(image_obj,
                        os.path.join(output_dir,
                                     filename_1))

    if isinstance(input_2, plant.PlantImage):
        image_2_obj = input_2
    else:
        image_2_obj = self.read_image(input_2)

    filename_2 = os.path.abspath(image_2_obj.filename)
    if (resize_factor is not None or
            image_2_obj.file_format not in plant.FIG_DRIVERS):
        filename_2 = plant.get_temporary_file(
            suffix='_2', ext='.png')
        if resize_factor is not None:
            image_2_obj = plant.filter_data(image_2_obj,
                                            oversampling=resize_factor)
        self.save_image(image_2_obj,
                        os.path.join(output_dir,
                                     filename_2))

    width_str = str(image_obj.width) + 'px'
    height_str = str(image_obj.length) + 'px'
    context_dict = {'image_1': filename_1,
                    'image_2': filename_2,
                    'width': width_str,
                    'height': height_str,
                    'title': title}
    context_dict['background'] = '#000000'

    image_obj.info(stats=True)
    image_2_obj.info(stats=True)

    if input_3 is not None:
        if isinstance(input_3, plant.PlantImage):
            image_3_obj = input_3
        else:
            image_3_obj = self.read_image(input_3)

        filename_3 = os.path.abspath(image_3_obj.filename)
        if (resize_factor is not None or
                image_3_obj.file_format not in plant.FIG_DRIVERS):
            filename_3 = plant.get_temporary_file(
                suffix='_3', ext='.png')
            if resize_factor is not None:
                image_3_obj = plant.filter_data(image_3_obj,
                                                oversampling=resize_factor)
            self.save_image(image_3_obj,
                            os.path.join(output_dir,
                                         filename_3))

            image_3_obj.info(stats=True)
        context_dict['image_3'] = filename_3

    if input_3 is None:
        template_filename = '../db/template_slide_compare.html'
    else:
        template_filename = '../db/template_slide_compare_2.html'
    script_path = os.path.dirname(os.path.abspath(__file__))
    template_file_path = os.path.normpath(os.path.join(script_path,
                                                       template_filename))
    environment = jinja2.Environment(
        loader=jinja2.FileSystemLoader(
            os.path.dirname(template_file_path)))
    output_text = environment.get_template(
        os.path.basename(template_filename)).render(context_dict)
    update = plant.overwrite_file_check(output_file, force=force)
    if not update:
        return
    with open(output_file, "w") as result_file:
        result_file.write(output_text)

    if plant.isfile(output_file):
        print(f'## file saved: {output_file} (HTML)')
        plant.plant_config.output_files.append(output_file)


def save_ctable(ctable,
                output_file,
                data=None,
                background_color=None,
                force=True,
                verbose=True,
                flag_show=False):
    ret = plot_ctable(ctable,
                      output_file=output_file,
                      data=data,
                      background_color=background_color,
                      force=force,
                      verbose=verbose,
                      flag_show=flag_show)
    return ret


def save_cmap(cmap,
              output_file,
              cmap_min,
              cmap_max,
              vmin=None,
              vmax=None,
              verbose=True,
              force=True,
              flag_show=False):
    ret = plot_cmap(cmap,
                    cmap_min,
                    cmap_max,
                    vmin=vmin,
                    vmax=vmax,
                    output_file=output_file,
                    force=force,
                    verbose=verbose,
                    flag_show=flag_show)
    return ret


def plot_ctable(ctable,
                force=True,
                output_file=None,
                name='',
                data=None,
                background_color=None,
                verbose=True,

                flag_show=True):

    import matplotlib.pyplot as plt
    plt.figure(facecolor='white',
               figsize=(6, 1))
    cmap, xticklabels = ctable_to_cmap(
        ctable, data=data, background_color=background_color)
    xticklabels = np.asarray(xticklabels)

    xticks = np.arange(xticklabels.size) + 0.5

    cmap_image = xticks.reshape(1, len(xticks))
    plt.figure(facecolor='white', figsize=(6, 1))
    plt.imshow(cmap_image, cmap=cmap, extent=(0, len(xticklabels), 0.7, 1))

    ret = plot_cbar(plt,
                    output_file=output_file,
                    force=force,
                    verbose=True,
                    flag_show=flag_show,
                    xticks=xticks,
                    xticklabels=xticklabels)
    return ret


def save_cbar_offset_background(output_file,
                                color=None,
                                length=None,
                                width=None,
                                alpha=None,
                                verbose=True,
                                force=True):
    if alpha is None and color is None:
        alpha = 0.
    else:
        alpha = 1
    if color is None:
        color = 'darkgray'
    if length is None:
        length = 1
    if width is None:
        width = 6
    import matplotlib.pyplot as plt
    plt.figure(

        facecolor=color,
        alpha=alpha,
        figsize=(width, length))
    update_image = plant.overwrite_file_check(output_file,
                                              force=force)
    if os.path.isfile(output_file) and update_image:
        os.remove(output_file)
    fig = plt.gcf()
    fig.patch.set_facecolor(color)
    fig.patch.set_alpha(alpha)

    if not os.path.isfile(output_file) or update_image:
        save_fig(output_file,
                 bbox_inches='tight',

                 verbose=verbose,

                 facecolor=color,
                 plt=plt)
    return plt


def plot_cmap(cmap,
              cmap_min,
              cmap_max,
              vmin=None,
              vmax=None,
              force=True,
              output_file=None,

              verbose=True,
              flag_show=True):
    import matplotlib.pyplot as plt
    n_points = 100

    if vmin is None:
        vmin = cmap_min
    if vmax is None:
        vmax = cmap_max
    vals = np.linspace(vmin, vmax, n_points)
    vals = vals.reshape(1, vals.shape[0])

    cmap_image = np.repeat(vals, n_points / 15, axis=0)
    plt.figure(facecolor='white',

               figsize=(6, 1))

    plt.imshow(cmap_image, cmap=cmap, vmin=cmap_min, vmax=cmap_max)

    ret = plot_cbar(plt,
                    output_file=output_file,
                    force=force,
                    flag_show=flag_show,
                    verbose=verbose,
                    xticks=None,
                    xticklabels=None,
                    xticks_min=vmin,
                    xticks_max=vmax)
    return ret


def plot_cbar(plot,
              output_file=None,
              flag_show=True,
              force=True,
              verbose=True,
              xticks=None,
              xticklabels=None,
              xticks_min=None,

              xticks_max=None):
    import matplotlib.pyplot as plt

    if plot is not None:
        plot.subplots_adjust(hspace=-.75)
    ax = plot.gca()

    if xticks is None:
        n_points = 100
        xticks = [0, n_points / 2 - 1, n_points - 1]
    ax.set_xticks(xticks)
    if xticklabels is None:
        xticks_avg = (xticks_max + xticks_min) / 2
        xticks_avg = plant.round_to_sig_figs(xticks_avg)
        xticklabels = [str(xticks_min), str(xticks_avg), str(xticks_max)]
    ax.set_xticklabels(xticklabels,

                       fontsize=18,
                       color='k')

    ax.set_yticks([])

    plot.ylabel('')
    if output_file is not None:

        update_image = plant.overwrite_file_check(output_file,
                                                  force=force)
        if os.path.isfile(output_file) and update_image:
            os.remove(output_file)
        fig = plot.gcf()
        fig.patch.set_facecolor('white')
        fig.patch.set_alpha(0.75)

        if not os.path.isfile(output_file) or update_image:
            save_fig(output_file,
                     bbox_inches='tight',

                     verbose=verbose,
                     facecolor=fig.get_facecolor(),
                     plot=plt)

    if flag_show:
        plot.show()

    return plot


def image_obj_expand_rgb(image_obj,
                         flag_to_rgb=False,
                         flag_hillshade=False,
                         cmap=None,
                         background_color=None,
                         cmap_min=None,
                         cmap_max=None,
                         pixel_size_x=None,
                         pixel_size_y=None,
                         percentile=None,
                         ctable=None,
                         verbose=True):

    import matplotlib.pyplot as plt

    image_list = []
    band_count = 0
    factor = None
    min_factor = None
    max_factor = None

    output_cmap = None

    cmap_min_band_list = []
    cmap_max_band_list = []
    last_ctable = ctable
    for current_band in range(image_obj.nbands):
        image = image_obj.getImage(band=current_band).copy()
        if ctable is None:
            last_ctable = image_obj.get_ctable(band=current_band)
        else:
            last_ctable = ctable

        if flag_to_rgb or flag_hillshade:
            if (cmap is None and
                    last_ctable is not None and
                    flag_to_rgb):
                image_list += image_to_rgb_ctable(image,
                                                  ctable=last_ctable)

                continue
            elif cmap is None and flag_hillshade:
                cmap = 'terrain'
            elif cmap is None:
                cmap = plant.DEFAULT_CMAP
            ret = plant.get_min_max_percentile(image,
                                               percentile=percentile,
                                               cmap_min=cmap_min,
                                               cmap_max=cmap_max)
            cmap_min_band, cmap_max_band = ret
            cmap_min_band_list.append(cmap_min_band)
            cmap_max_band_list.append(cmap_max_band)
            output_cmap = plt.get_cmap(cmap)
            if verbose:
                print('band: %d' % current_band)
                print('    color map: %s' % output_cmap.name)
                print('    color map min: %f' % cmap_min_band)
                print('    color map max: %f' % cmap_max_band)
            if background_color is not None and 'transparent' in background_color:
                background_color = background_color.replace('transparent', '')
                background_color_alpha_0 = background_color.replace(
                    '_', '').strip()
                output_cmap.set_bad(background_color_alpha_0, alpha=0.0)
                output_cmap.set_over(background_color_alpha_0, alpha=0.0)
                output_cmap.set_under(background_color_alpha_0, alpha=0.0)
                if verbose:
                    print(
                        f'    background color: {background_color_alpha_0} (alpha: 0)')
            elif background_color is not None:
                output_cmap.set_bad(background_color, alpha=1)
                output_cmap.set_over(background_color, alpha=1)
                output_cmap.set_under(background_color, alpha=1)
                if verbose:
                    print(f'    background color: {background_color}')
            ind = plant.isnan(image)
            image = plant.scale_data(image,
                                     cmap_min_band,
                                     cmap_max_band,
                                     1.0)
            if flag_hillshade:

                step_x = plant.get_image_derivative_x(image,
                                                      pixel_size_x)
                step_y = plant.get_image_derivative_y(image,
                                                      pixel_size_y)

                slope = np.sqrt(step_x ** 2 + step_y ** 2)

                slope_percentile = 99
                slope_factor = 1.0 / np.nanpercentile(slope,
                                                      slope_percentile)

                slope = np.arctan(slope_factor * slope)

                aspect = np.arctan2(step_y, -step_x)

                zenith = np.pi / 4
                azimuth_angle = 3 * np.pi / 4
                factor = ((np.cos(zenith) * np.cos(slope)) +
                          (np.sin(zenith) * np.sin(slope) *
                           np.cos(plant.wrap_phase(azimuth_angle -
                                                   aspect))))

                min_factor, max_factor = plant.get_min_max_percentile(
                    factor,
                    percentile=percentile)

                factor = plant.scale_data(factor,
                                          min_factor,
                                          max_factor,
                                          0.5) + 0.5
                factor[np.where(plant.isnan(factor))] = 1.0
                if verbose:
                    print('    color factor: terrain relief')
            image[ind] = np.nan
            out_image = output_cmap(image)
            for b in range(out_image.shape[-1]):
                if len(out_image.shape) == 4:
                    out_band = out_image[:, :, :, b]
                elif len(out_image.shape) == 3:
                    out_band = out_image[:, :, b]
                elif len(out_image.shape) == 2:
                    out_band = out_image[:, b]
                else:
                    out_band = out_image[b]
                if b != out_image.shape[-1] - 1 and factor is not None:
                    out_band = factor * out_band
                image_obj.set_image(out_band,
                                    band=band_count)
                band_count += 1
            cmap_min = 0.0
            cmap_max = 1.0
    if len(image_list) != 0:
        for b, current_image in enumerate(image_list):
            image_obj.set_image(current_image,
                                band=b)
            image_obj.get_band(b).ctable = None
    ret_dict = {}
    ret_dict['image_obj'] = image_obj
    ret_dict['ctable'] = last_ctable
    ret_dict['cmap'] = output_cmap
    ret_dict['cmap_min'] = 0.0
    ret_dict['cmap_max'] = 1.0
    if len(cmap_min_band_list) == 1:
        ret_dict['cmap_min_orig'] = cmap_min_band_list[0]
    else:
        ret_dict['cmap_min_orig'] = cmap_min_band_list
    if len(cmap_max_band_list) == 1:
        ret_dict['cmap_max_orig'] = cmap_max_band_list[0]
    else:
        ret_dict['cmap_max_orig'] = cmap_max_band_list
    return ret_dict


def image_obj_ctable_expand(image_obj,
                            ctable=None,
                            verbose=True):

    ret_dict = {}
    if ctable is None:
        ret_dict['image_obj'] = image_obj
        ret_dict['ctable'] = None
        return ret_dict

    out_image_obj = plant.new_image_obj_like(image_obj)
    image_list = []
    last_ctable = ctable
    for current_band in range(image_obj.nbands):
        image = image_obj.getImage(band=current_band).copy()
        if ctable is None:
            last_ctable = image_obj.get_ctable(band=current_band)
        else:
            last_ctable = ctable
        if last_ctable is not None:
            image_list += image_to_rgb_ctable(image,
                                              ctable=last_ctable)

    if len(image_list) != 0:
        for b, current_image in enumerate(image_list):
            out_image_obj.set_image(current_image, band=b)
            out_image_obj.get_band(b).ctable = None

    ret_dict['image_obj'] = out_image_obj
    ret_dict['ctable'] = last_ctable
    return ret_dict
