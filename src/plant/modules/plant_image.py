#!/usr/bin/env python3
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Gustavo H. X. Shiroma, Marco Lavalle
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import sys
import time
import shutil
import glob as python_glob
import xml.etree.ElementTree as ET
import numpy as np
from collections.abc import Sequence
import os
from osgeo import gdal, ogr
import importlib
import numbers
import mimetypes
import matplotlib.image as mpimg
from matplotlib import colors as mcolors
from zipfile import ZipFile
import plant
import h5py

FLAG_VECTOR_SAVE_LAT_LON_BANDS = True


def get_info(input_var,
             name=None,
             fig=False,

             verbose=True,
             approx_stats=False,
             n_points=None,
             band=None,
             input_key=None,

             input_format=None,

             plant_transform_obj=None,
             in_null=None,

             topo_dir=None,
             only_geoinformation=False,
             show_geoinformation=True,

             show_all=None,

             data_stats=False,
             data_min=False,
             data_mean=False,

             data_stddev=False,
             data_max=False,

             force=None):

    kwargs = locals()

    flag_stats = (data_stats or
                  data_min or
                  data_mean or

                  data_stddev or
                  data_max or

                  n_points or
                  fig)

    image_obj = None
    image = None
    if show_all is None and flag_stats:
        show_all = True

    if isinstance(input_var, str):

        image_obj = read_image(input_var,
                               verbose=verbose,
                               band=band,

                               input_key=input_key,

                               input_format=input_format,

                               plant_transform_obj=plant_transform_obj,
                               force=force)

    elif isinstance(input_var, plant.PlantImage):
        image_obj = input_var
    else:
        image_obj = plant.PlantImage(input_var)

    if image_obj is None:
        return

    image_str = str(image_obj)
    if verbose:
        print(f'## image: {image_str}')
    file_format = None
    dtype = None
    width = None
    length = None
    depth = None
    if verbose and name:
        print('## %s' % name)
    if only_geoinformation and not isinstance(image,
                                              plant.PlantImage):
        print('WARNING (info) geoinformation not available')
        return

    with plant.PlantIndent():
        name = image_obj.name
        header_file = image_obj.header_file
        file_format = image_obj.file_format
        dtype = image_obj.dtype
        length = image_obj.length
        width = image_obj.width
        depth = image_obj.depth
        nbands = image_obj.nbands
        scheme = image_obj.scheme
        metadata = image_obj.metadata

        gcp_list = image_obj.gcp_list
        gcp_projection = image_obj.gcp_projection
        n_elements = image_obj.n_elements()

        if topo_dir is not None:
            print('geographic information (geolocation arrays):')
            with plant.PlantIndent():
                plant.print_geoinformation(
                    bbox_topo=topo_dir,
                    length=length,
                    width=width,
                    plant_transform_obj=plant_transform_obj,

                    flag_print_step=False,
                    verbose=False)

        if (show_geoinformation and only_geoinformation and
                not verbose):
            return

        if (show_geoinformation and only_geoinformation and
                image_obj.geotransform is None):
            print('WARNING geoinformation not available')
            return

        if (show_geoinformation and only_geoinformation and
                verbose):
            print('geographic information:')
            with plant.PlantIndent():
                plant.print_geoinformation(
                    geotransform=image_obj.geotransform,
                    plant_transform_obj=plant_transform_obj,

                    projection=image_obj.projection,
                    flag_print_all=show_all)
            return

        if verbose:

            if name is not None and name != image_str:
                print(f'name: {name}')
            if header_file is not None:
                print(f'header file: {header_file}')

            print('structure:')
            with plant.PlantIndent():
                print('number of bands: %d' % nbands)
                if file_format is not None:
                    print(f'file format: {file_format}')
                if scheme is not None:
                    print(f'scheme: {scheme}')
                if length is not None:
                    print(f'length: {length}')
                if width is not None:
                    print(f'width: {width}')
                if depth is not None and depth > 1:
                    print(f'depth: {depth}')
                if n_elements is not None:
                    print(f'number of elements: {n_elements}')
            if gcp_list is not None and len(gcp_list) > 0:
                print(f'GCP number of elements: {len(gcp_list)}')
                if gcp_projection is not None:
                    print(f'GCP projection: {gcp_projection}')
            if metadata is not None:

                print('metadata:')
                with plant.PlantIndent():
                    for i, md_element in enumerate(metadata.items()):
                        key, value = md_element
                        print(f'{key}: {value}')

        if show_geoinformation and verbose:
            print('geographic information:')
            with plant.PlantIndent():
                plant_geogrid_obj = plant.get_coordinates(
                    geotransform=image_obj.geotransform,
                    plant_transform_obj=plant_transform_obj,
                    width=width,
                    length=length,

                    projection=image_obj.projection)
                plant_geogrid_obj.print(flag_print_all=show_all)

        flag_gdal = (not data_stddev and
                     not (plant_transform_obj is not None and
                          plant_transform_obj.flag_apply_transformation()) and
                     test_gdal_open(image_obj.filename))

        kwargs['n_elements'] = n_elements
        kwargs['flag_gdal'] = flag_gdal

        kwargs.pop('input_var', None)
        kwargs.pop('show_geoinformation', None)
        kwargs.pop('only_geoinformation', None)

        kwargs.pop('input_format', None)

        kwargs.pop('band', None)
        kwargs.pop('name', None)
        kwargs.pop('topo_dir', None)

        if band is None:
            band_indexes = list(range(nbands))
        else:
            band_indexes = plant.get_int_list(band)
        if len(band_indexes) == 0:
            return
        if not show_all and not flag_stats:
            band_obj = image_obj.get_band(band=0)
            dtype = plant.get_dtype_name(band_obj.dtype)
            if (dtype is not None and
                    all([dtype == plant.get_dtype_name(
                        image_obj.get_band(band=b).dtype)
                        for b in range(image_obj.nbands)])):
                print(f'data type: {dtype}')
            return

        ret_var = [[] for x in range(nbands)]
        for b, current_band in enumerate(band_indexes):
            band_obj = image_obj.get_band(band=b)
            name = band_obj.name
            if verbose and nbands != 1 and name:
                print(f'## band {current_band}: "{name}"')
            elif verbose and nbands != 1:
                print(f'## band {current_band}')
            kwargs.pop('input_key', None)
            ret_var[b] = info_band(band_obj, **kwargs)
        return ret_var


def info_band(input_var,
              fig=False,

              verbose=True,
              approx_stats=False,
              n_points=None,

              in_null=None,
              plant_transform_obj=None,

              data_stats=False,
              data_min=False,
              data_mean=False,

              data_stddev=False,
              data_max=False,
              n_elements=None,
              show_all=False,
              flag_gdal=False,
              force=None):

    flag_stats = (data_stats or
                  data_min or
                  data_mean or

                  data_stddev or
                  data_max or

                  fig)
    kwargs = locals()

    if isinstance(input_var, plant.PlantBand):
        band_obj = input_var
    else:
        band_obj = plant.PlantBand(input_var)

    image = None

    image_loaded = band_obj.image_loaded
    if (image_loaded or n_points is not None or
            (flag_stats and not flag_gdal)):
        image = band_obj.image
        image_loaded = band_obj.image_loaded
    dtype = plant.get_dtype_name(band_obj.dtype)
    type_size = plant.get_dtype_size(band_obj.dtype)
    if n_elements is not None and type_size is not None:
        data_size = n_elements * type_size
    else:
        data_size = None

    with plant.PlantIndent():
        if verbose:

            if show_all and image_loaded:
                n_valid = np.sum(plant.isvalid(image, in_null))
                if n_elements == 0:
                    n_valid_percent = 0
                else:
                    n_valid_percent = 100 * float(n_valid) / float(n_elements)
                print('# valid elements: %d (%d%%)'
                      % (n_valid,
                         n_valid_percent))
            if dtype is not None:
                print('data type: %s' % dtype)
            if type_size is not None:
                print('data type size: %dB' % type_size)
            if in_null is not None:
                print('null: %s' % in_null)
            if data_size is not None:
                print('data size: %s'
                      % plant.get_file_size_string(data_size))
            if band_obj.ctable is not None:
                print('has color table: %s'
                      % str(band_obj.ctable is not None))
            else:
                print('no ctable')
            if show_all and 'int' in dtype:
                unique_elements = np.unique(band_obj.image)
                print('unique elements:', unique_elements)

        if n_points is not None:
            if (verbose and
                    n_points <= n_elements):
                print('elements:', image)
            elif verbose and n_points is not None:
                print('first ' + str(n_points) + ' elements:',
                      (image.ravel())[0: n_points])
                print('last ' + str(n_points) + ' elements:',
                      (image.ravel())[-1 - n_points: -1])
            if not flag_stats:
                return image.ravel()
        elif not flag_stats:
            return

        ret_dict = None
        if flag_gdal:

            try:
                ret_dict = plant.get_stats_gdal(
                    band_obj.parent_orig.filename,
                    verbose=False,
                    band=band_obj.parent_orig_band_orig_index,
                    plant_transform_obj=plant_transform_obj,
                    approx_stats=approx_stats)
            except RuntimeError:
                image = band_obj.image
                image_loaded = band_obj.image_loaded
        if ret_dict is None:

            if image is None:
                print('ERROR reading input data')
                return
            kwargs.pop('flag_gdal')
            kwargs.pop('input_var')
            kwargs.pop('n_elements')
            kwargs.pop('fig')
            kwargs.pop('force')
            kwargs.pop('show_all')
            kwargs.pop('n_points')
            kwargs.pop('plant_transform_obj')
            ret_dict = get_stats(image, **kwargs)
        if ret_dict is None:
            return

        minimum = None
        mean = None

        maximum = None
        stddev = None
        if 'minimum' in ret_dict.keys():
            minimum = ret_dict['minimum']
        if 'maximum' in ret_dict.keys():
            maximum = ret_dict['maximum']
        if 'mean' in ret_dict.keys():
            mean = ret_dict['mean']
        if 'stdDev' in ret_dict.keys():
            stddev = ret_dict['stdDev']

        if fig:
            import matplotlib.pyplot as plt
            plt.figure()
            plt.imshow(image, aspect='auto')
            plt.grid(True)
            plt.colorbar()
            mngr = plt.get_current_fig_manager()
            mngr.window.setGeometry(0,
                                    0,
                                    plant.DEFAULT_PLOT_SIZE_X,
                                    plant.DEFAULT_PLOT_SIZE_Y)
            plt.show()

        if verbose and approx_stats:
            str_star = '*'
        else:
            str_star = ''

        if verbose:
            text = ''
            if ((data_stats or data_min) and minimum is not None):
                text += ('min%s=%.16f  '
                         % (str_star, minimum))

            if ((data_stats or data_max) and maximum is not None):
                text += ('max%s=%.16f  '
                         % (str_star, maximum))

            if ((data_stats or data_mean) and mean is not None):
                text += ('mean%s=%.16f  '
                         % (str_star, mean))

            if ((data_stats or data_stddev) and stddev is not None):
                text += ('stddev%s=%.16f  '
                         % (str_star, stddev))

            print(text)
        if approx_stats and verbose:
            print('    * approx.')

        if data_stats:
            return [mean, stddev, minimum, maximum]
        if data_mean:
            return mean

        if data_stddev:
            return stddev
        if data_min:
            return minimum
        if data_max:
            return maximum


def get_stats(image,
              approx_stats=True,
              flag_stats=True,
              in_null=None,

              data_stats=False,
              data_min=False,
              data_mean=False,

              data_stddev=False,
              data_max=False,

              verbose=True):

    if approx_stats and flag_stats:
        image = sample_image(image,
                             fast=approx_stats,
                             verbose=False)

    minimum = None
    maximum = None
    ret_dict = {}
    data = np.asarray(0, dtype=np.float64)
    ndata = np.asarray(0, dtype=np.uint64)
    for i in range(image.shape[0]):
        if len(image.shape) > 1:
            data_line = image[i, :]
        else:
            data_line = image
        indexes = np.where(plant.isvalid(data_line, in_null))
        data_line = data_line[indexes]
        if len(data_line) < 1:
            continue
        if 'complex' in plant.get_dtype_name(image.dtype).lower():
            data_line = np.absolute(data_line)
        if data_stats or data_min:
            minimum_temp = np.min(data_line)
            if minimum is None:
                minimum = minimum_temp
            elif minimum_temp < minimum:
                minimum = minimum_temp
        if data_stats or data_max:
            maximum_temp = np.max(data_line)
            if maximum is None:
                maximum = maximum_temp
            elif maximum_temp > maximum:
                maximum = maximum_temp
        if data_stats or data_mean:
            data += np.sum(data_line)
            ndata += np.uint64((data_line).size)

    if data_stats or data_stddev:

        output_stddev = np.nanstd(image)

    if data_stats or (ndata != 0 and data_mean):
        output_mean = data / np.float64(ndata)
    elif verbose and (data_stats or (ndata == 0 and data_mean)):
        print('no valid elements found')
        return
    if data_stats or data_min:
        ret_dict['minimum'] = minimum
    if data_stats or data_mean:
        ret_dict['mean'] = output_mean

    if data_stats or data_max:
        ret_dict['maximum'] = maximum
    if data_stats or data_stddev:
        ret_dict['stdDev'] = output_stddev
    return ret_dict


def new_image_obj_like(ref_image_obj):
    image_obj_copy = plant.PlantImage()
    for key in ref_image_obj.__dict__.keys():
        if key == '_band_list':
            continue
        image_obj_copy.__dict__.update({key: ref_image_obj.__dict__[key]})
    image_obj_copy._nbands = 0
    image_obj_copy._band_orig = None
    return image_obj_copy


def render_vrt(input_file,
               output_file=None,
               force_writting=False,

               force=None,
               flag_temporary=False,
               verbose=False,
               context=None):

    if not output_file:
        if not flag_temporary:
            output_file = input_file
        else:
            timestamp = str(int(time.time()))
            output_file = os.path.basename(input_file) + '_temp_' + timestamp
        if not output_file.endswith('.vrt'):
            output_file += '.vrt'
        if flag_temporary:
            plant.append_temporary_file(output_file)
    if plant.isfile(output_file) and not force_writting:
        output_file = os.path.basename(input_file)
        if not output_file.endswith('.vrt'):
            output_file += '.vrt'
    if plant.isfile(output_file) and not force_writting:

        return output_file

    if context is None:
        image_obj = read_image(input_file,

                               only_header=True)
    else:
        image_obj = context.read_image(input_file,

                                       only_header=True)
    save_image(image_obj,
               output_file,

               force=force,
               output_format='VRT',
               verbose=verbose)
    return output_file


def render_vrt_isce(input_file,
                    output_file=None,
                    force_writting=False):

    if not output_file:
        output_file = input_file
        if not output_file.endswith('.vrt'):
            output_file += '.vrt'
    if plant.isfile(output_file) and not force_writting:
        return output_file
    image_obj = read_image(input_file,

                           only_header=True)

    geotransform = image_obj.geotransform
    root = ET.Element('VRTDataset')
    root.attrib['rasterXSize'] = str(image_obj.width)
    root.attrib['rasterYSize'] = str(image_obj.length)
    if geotransform is not None:
        if plant.valid_coordinates(geotransform=geotransform,
                                   length=image_obj.length,
                                   width=image_obj.width):
            ET.SubElement(root, 'SRS').text = "EPSG:4326"

            geotransform = [str(i) for i in geotransform]
            ET.SubElement(root, 'GeoTransform').text = \
                ', '.join(geotransform)
            if image_obj.projection is not None:
                ET.SubElement(root, 'Projection').text = image_obj.projection
    nbytes = plant.get_dtype_size(image_obj.dataType)
    for band in range(image_obj.bands):
        broot = ET.Element('VRTRasterBand')
        broot.attrib['dataType'] = plant.get_vrt_dtype(image_obj.dataType)
        broot.attrib['band'] = str(band + 1)
        broot.attrib['subClass'] = "VRTRawRasterBand"
        elem_parent = ET.SubElement(broot, 'SimpleSource')
        elem = ET.SubElement(elem_parent, 'SourceFilename')
        elem.attrib['relativeToVRT'] = "1"

        elem.text = image_obj.getFilename()
        if image_obj.scheme.upper() == 'BIL':
            ET.SubElement(broot, 'ImageOffset').text = \
                str(band * image_obj.width * nbytes)
            ET.SubElement(broot, 'PixelOffset').text = str(nbytes)
            ET.SubElement(broot, 'LineOffset').text = \
                str(image_obj.bands * image_obj.width * nbytes)
        elif image_obj.scheme.upper() == 'BIP':
            ET.SubElement(broot, 'ImageOffset').text = str(band * nbytes)
            ET.SubElement(broot, 'PixelOffset').text = \
                str(image_obj.bands * nbytes)
            ET.SubElement(broot, 'LineOffset').text = \
                str(image_obj.bands * image_obj.width * nbytes)
        elif image_obj.scheme.upper() == 'BSQ':
            ET.SubElement(broot, 'ImageOffset').text = \
                str(band * image_obj.width * image_obj.length)
            ET.SubElement(broot, 'PixelOffset').text = str(nbytes)
            ET.SubElement(broot, 'LineOffset').text = \
                str(image_obj.width * nbytes)
        root.append(broot)
    plant.indent_XML(root)
    tree = ET.ElementTree(root)
    tree.write(output_file, encoding='unicode')
    return output_file


def generate_vrt_with_geolocation_files(infile,
                                        lat_file,
                                        lon_file,
                                        flag_temporary=False,
                                        verbose=True):

    lat_dict = plant.parse_filename(lat_file)
    lat_filename = lat_dict['filename']
    lat_band = lat_dict['band']
    if lat_band is None:
        lat_band = 0
    lat_band += 1

    lon_dict = plant.parse_filename(lon_file)
    lon_filename = lon_dict['filename']
    lon_band = lon_dict['band']
    if lon_band is None:
        lon_band = 0
    lon_band += 1

    infile_vrt = render_vrt(infile,
                            flag_temporary=flag_temporary)

    tree = ET.parse(infile_vrt)
    root = tree.getroot()

    update_metadata = True

    for child in root:
        if child.tag == 'GeoTransform':
            root.remove(child)

    for metadata in root.findall('metadata'):
        if len(metadata) == 0:
            root.remove(metadata)
            continue
        meta_dict = metadata.attrib
        flag_found = False
        for key, value in meta_dict.items():
            if key == 'domain' and value == 'GEOLOCATION':
                flag_found = True
        x_dataset = None
        y_dataset = None
        x_band = None
        y_band = None
        for mdi in metadata.findall('mdi'):
            if mdi.attrib.get('key', '') == 'X_DATASET':
                x_dataset = mdi.text
            if mdi.attrib.get('key', '') == 'Y_DATASET':
                y_dataset = mdi.text
            if mdi.attrib.get('key', '') == 'X_BAND':
                x_band = int(mdi.text)
            if mdi.attrib.get('key', '') == 'Y_BAND':
                y_band = int(mdi.text)
        same_lat_file = (os.path.abspath(lat_filename) == y_dataset and
                         lat_band == y_band)
        same_lon_file = (os.path.abspath(lon_filename) == x_dataset and
                         lon_band == x_band)
        update_metadata = (not same_lat_file or not same_lon_file)
        if flag_found and update_metadata:
            root.remove(metadata)

    ret_dict = {}
    ret_dict['lat_file'] = lat_filename
    ret_dict['lon_file'] = lon_filename
    ret_dict['data_file'] = infile_vrt
    if not update_metadata:
        return ret_dict

    meta = ET.SubElement(root, 'metadata')
    meta.attrib['domain'] = "GEOLOCATION"
    meta.tail = '\n'
    meta.text = '\n    '
    rdict = {'Y_DATASET': os.path.abspath(lat_filename),
             'X_DATASET': os.path.abspath(lon_filename),
             'X_BAND': f"{lon_band}",
             'Y_BAND': f"{lat_band}",
             'PIXEL_OFFSET': "0",
             'LINE_OFFSET': "0",
             'LINE_STEP': "1",
             'PIXEL_STEP': "1"}
    for key, val in rdict.items():
        data = ET.SubElement(meta, 'mdi')
        data.text = val
        data.attrib['key'] = key
        data.tail = '\n    '
    data.tail = '\n'

    try:
        tree.write(infile_vrt)
        if plant.isfile(infile_vrt) and verbose:
            print(f'## file saved: {infile_vrt}')
        return ret_dict
    except BaseException:
        pass


def save_vector(data_vect,
                output_file,
                lat_vect=None,
                lon_vect=None,

                plant_geogrid_obj=None,
                step_y=None,
                step_x=None,
                footprint=None,
                footprint_array=None,
                str_data='data',
                save_ndata=False,
                save_as_text=False,
                save_as_vector=False,
                save_as_raster=False,
                save_as_raster_gdal=False,
                loreys_height=False,
                sum_samples=False,
                id_vect=None,
                str_id='id',
                lat_text='lat',
                lon_text='lon',
                nbands=None,
                geotransform=None,
                output_scheme='',
                descr='',
                cmap=None,
                cmap_crop_min=None,
                cmap_crop_max=None,
                cmap_min=None,
                cmap_max=None,
                background_color=None,
                in_null=None,
                out_null=None,
                output_dtype=None,
                output_format=None,
                projection=None,

                verbose=True,
                force=None):

    output_data = None

    if geotransform is not None or plant_geogrid_obj is not None:
        plant_geogrid_obj = plant.get_coordinates(
            geotransform=geotransform,
            plant_geogrid_obj=plant_geogrid_obj,
            step_y=step_y,
            step_x=step_x)

        flag_y_from_lat_vect = (
            plant.isnan(plant_geogrid_obj.y0) or
            plant.isnan(plant_geogrid_obj.yf))
        if flag_y_from_lat_vect and lat_vect is not None:
            plant_geogrid_obj.y0 = np.max(lat_vect)
            plant_geogrid_obj.yf = np.min(lat_vect)

        flag_x_from_lat_vect = (
            plant.isnan(plant_geogrid_obj.x0) or
            plant.isnan(plant_geogrid_obj.xf))

        if flag_x_from_lat_vect and lon_vect is not None:
            plant_geogrid_obj.x0 = np.min(lon_vect)
            plant_geogrid_obj.xf = np.max(lon_vect)

        if plant.isnan(plant_geogrid_obj.step_y) and footprint is not None:
            if plant.isvalid(footprint) and footprint != 0:
                plant_geogrid_obj.step_y = -abs(footprint) / 2
        if plant.isnan(plant_geogrid_obj.step_x) and footprint is not None:
            if plant.isvalid(footprint) and footprint != 0:
                plant_geogrid_obj.step_x = footprint / 2

        geotransform = plant_geogrid_obj.geotransform

    image_obj = plant.PlantImage(data_vect,
                                 nbands=nbands)

    if output_file.upper().startswith('MEM:'):
        output_key = 'OUTPUT:' + output_file[4:]

        plant.plant_config.variables[output_key] = image_obj
        return

    if not os.path.isdir(plant.dirname(output_file)):
        try:
            os.makedirs(plant.dirname(output_file))
        except FileExistsError:
            pass

    data_vect = image_obj.image_list
    nbands = len(data_vect)

    if (isinstance(str_data, list) and
            len(str_data) == len(data_vect)):
        str_data_list = str_data
    elif (isinstance(str_data, str) and
            len(data_vect) == 1):
        str_data_list = [str_data]
    else:
        str_data_list = [str_data + '_band_%d' % b
                         for b in range(len(data_vect))]

    if ((save_as_raster or save_as_raster_gdal) and
            (lat_vect is None or lon_vect is None)):
        print('ERROR lat or lon vector were not '
              'provided to generate output image')
        return
    elif save_as_raster:
        if verbose:
            print('save vector mode: raster (PLAnT)')
        print('lon_size:', plant_geogrid_obj.width)
        ret = rasterize_with_footprint(data_vect,
                                       lat_vect,
                                       lon_vect,
                                       plant_geogrid_obj,

                                       nbands=nbands,
                                       footprint=footprint,
                                       footprint_array=footprint_array,
                                       out_null=out_null,
                                       loreys_height=loreys_height,
                                       sum_samples=sum_samples,
                                       verbose=verbose)
        data_list, ndata, geotransform = ret
        if save_ndata:

            save_image(ndata,
                       output_file + '_ndata',
                       geotransform=geotransform,
                       output_scheme=output_scheme,
                       descr=descr,
                       cmap=cmap,
                       cmap_crop_min=cmap_crop_min,
                       cmap_crop_max=cmap_crop_max,
                       cmap_min=cmap_min,
                       cmap_max=cmap_max,
                       background_color=background_color,
                       in_null=in_null,
                       out_null=out_null,
                       output_dtype=output_dtype,
                       output_format=output_format,
                       projection=projection,
                       verbose=verbose,
                       force=force)

        save_image(data_list,
                   output_file,
                   output_format=output_format,
                   force=force,
                   verbose=verbose,

                   output_dtype=output_dtype,
                   geotransform=geotransform)
    elif save_as_raster_gdal:
        if verbose:
            print('save vector mode: raster (gdal_grid)')
        rasterize_with_footprint_gdal(data_vect,
                                      lat_vect,
                                      lon_vect,
                                      output_file,
                                      plant_geogrid_obj,

                                      footprint,
                                      str_data=str_data_list,
                                      lat_text=lat_text,
                                      lon_text=lon_text,
                                      output_format=output_format,
                                      output_dtype=output_dtype,
                                      out_null=out_null,
                                      verbose=verbose,
                                      force=force)
    elif (save_as_text or
          ((output_file.endswith('.csv') or
            output_file.endswith('.txt')) and
           not save_as_raster_gdal and not
           save_as_vector)):

        args_vects = []
        args_labels = []
        if (lat_vect is not None):
            args_vects += [lat_vect]
            args_labels += [lat_text]
        if (lon_vect is not None):
            args_vects += [lon_vect]
            args_labels += [lon_text]
        if (id_vect is not None):
            args_vects += [id_vect]
            args_labels += [str_id]
        args_vects += data_vect
        args_labels += str_data_list
        args = args_vects + args_labels + [output_file]
        save_text(*args,
                  verbose=verbose,
                  force=force)
    else:

        kwargs = {}
        kwargs['output_scheme'] = output_scheme
        kwargs['descr'] = descr
        kwargs['cmap'] = cmap
        kwargs['cmap_crop_min'] = cmap_crop_min
        kwargs['cmap_crop_max'] = cmap_crop_max
        kwargs['cmap_min'] = cmap_min
        kwargs['cmap_max'] = cmap_max
        kwargs['background_color'] = background_color
        kwargs['in_null'] = in_null
        kwargs['out_null'] = out_null
        kwargs['output_dtype'] = output_dtype
        kwargs['output_format'] = output_format
        kwargs['projection'] = projection
        kwargs['verbose'] = verbose
        kwargs['force'] = force

        new_image_obj = plant.PlantImage(data_vect)

        if FLAG_VECTOR_SAVE_LAT_LON_BANDS:
            if lat_vect is not None:
                new_image_obj.add_band(lat_vect,
                                       name=plant.Y_BAND_NAME)
                nbands += 1
            if lon_vect is not None:
                new_image_obj.add_band(lon_vect,
                                       name=plant.X_BAND_NAME)
                nbands += 1
            kwargs['nbands'] = nbands
            save_image(new_image_obj, output_file, **kwargs)
        else:
            if lat_vect is not None:
                lat_vect_obj = plant.PlantImage(lat_vect)
                lat_vect_obj.get_band().name = 'lat/north'
                lat_file = os.path.join(os.path.dirname(output_file),
                                        'lat.rdr')
                save_image(lat_vect_obj, lat_file, **kwargs)
            if lon_vect is not None:
                lon_vect_obj = plant.PlantImage(lon_vect)
                lon_vect_obj.get_band().name = 'lon/east'
                lon_file = os.path.join(os.path.dirname(output_file),
                                        'lon.rdr')
                save_image(lon_vect_obj, lon_file, **kwargs)
            kwargs['nbands'] = nbands
            save_image(new_image_obj, output_file, **kwargs)
        return new_image_obj
    return output_data


def get_output_format(output_file, output_format=None):

    if output_format is not None and 'tif' in output_format.lower():
        output_format = 'GTiff'
        if output_format in plant.OUTPUT_FORMAT_MAP.keys():
            output_format = plant.OUTPUT_FORMAT_MAP[output_format]
        return output_format

    if output_format is not None and 'ANN' not in output_format:
        if output_format in plant.OUTPUT_FORMAT_MAP.keys():
            output_format = plant.OUTPUT_FORMAT_MAP[output_format]
        return output_format
    if not output_file:
        return
    filename, extension = os.path.splitext(output_file)

    extension = extension.lower()
    if extension and extension.startswith('.'):
        extension = extension[1:]
    if (extension == 'tif' or extension == 'tiff'):
        output_format = 'GTiff'
    elif (extension == 'bin'):
        output_format = 'ENVI'
    elif (extension == 'txt' or extension == 'csv'):
        output_format = 'CSV'
    elif extension.upper() in plant.FIG_DRIVERS:
        output_format = extension.upper()
    elif (extension == 'kml'):
        output_format = 'KML'
    elif (extension == 'kmz'):
        output_format = 'KMZ'
    elif (extension == 'npy'):
        output_format = 'NUMPY'
    elif (extension == 'vrt'):
        output_format = 'VRT'
    elif (extension == 'htm' or extension == 'html'):
        output_format = 'HTML'
    else:
        output_format = plant.DEFAULT_OUTPUT_FORMAT
    if output_format in plant.OUTPUT_FORMAT_MAP.keys():
        output_format = plant.OUTPUT_FORMAT_MAP[output_format]
    return output_format


def rasterize_with_footprint_gdal(data_vect,
                                  lat_vect,
                                  lon_vect,
                                  output_file,
                                  plant_geogrid_obj=None,

                                  footprint=None,
                                  max_points=10,
                                  out_null='nan',
                                  output_format=None,
                                  output_dtype=None,
                                  str_data='data',
                                  lat_text='lat',
                                  lon_text='lon',
                                  nbands=None,
                                  csv_file=None,
                                  verbose=True,
                                  force=None):

    update_image = plant.overwrite_file_check(output_file,
                                              force=force)
    if not update_image:
        print('operation cancelled.')
        sys.exit(0)

    timestamp = str(int(time.time()))

    image_obj = plant.PlantImage(data_vect,
                                 nbands=nbands)
    data_vect = image_obj.image_list
    nbands = len(data_vect)

    if (isinstance(str_data, list) and
            len(str_data) == nbands):
        str_data_list = str_data
    elif (isinstance(str_data, str) and
            nbands == 1):
        str_data_list = [str_data]
    else:
        str_data_list = [str_data + '_band_%d' % b
                         for b in range(len(data_vect))]

    if csv_file is not None and not plant.isfile(csv_file):
        print('ERROR file not found: %s'
              % csv_file)
        return
    elif csv_file is None:
        csv_file = output_file + '_temp_' + timestamp + '.csv'
        args_vects = []
        args_labels = []
        if (lat_vect is not None):
            args_vects += [lat_vect]
            args_labels += [lat_text]
        if (lon_vect is not None):
            args_vects += [lon_vect]
            args_labels += [lon_text]

        args_vects += data_vect
        args_labels += str_data_list
        args = args_vects + args_labels + [csv_file]
        save_text(*args,
                  verbose=verbose,
                  force=force)

        if not plant.isfile(csv_file):
            print('ERROR file could not be generated: ' +
                  csv_file)
            return
        plant.append_temporary_file(csv_file)

    vrt_file = output_file + '_temp_' + timestamp + '.vrt'
    root = ET.Element('OGRVRTDataSource')

    for b in range(nbands):
        elem = ET.SubElement(root, 'OGRVRTLayer')
        elem.attrib['name'] = 'l' + str_data_list[b]
        ET.SubElement(elem, 'SrcDataSource').text = 'CSV:' + csv_file
        ET.SubElement(elem, 'GeometryType').text = 'wkbPoint'

        ET.SubElement(elem, 'SrcLayer').text = \
            os.path.basename(csv_file).replace('.csv', '')
        ET.SubElement(elem, 'LayerSRS').text = 'WGS84'
        elem_sub = ET.SubElement(elem, 'GeometryField')
        elem_sub.attrib['separator'] = ','
        elem_sub.attrib['encoding'] = 'PointFromColumns'
        elem_sub.attrib['x'] = lon_text
        elem_sub.attrib['y'] = lat_text
        elem_sub.attrib['z'] = str_data_list[b]

    plant.indent_XML(root)
    tree = ET.ElementTree(root)

    tree.write(vrt_file, encoding='unicode')
    plant.append_temporary_file(vrt_file)

    if verbose:
        print('file saved (temp): %s (VRT)' % (vrt_file))

    if output_format is None:
        output_format = get_output_format(output_file,
                                          output_format=output_format)
    if output_format is None:
        output_format = 'GTiff'

    if output_dtype is not None:
        outputType = plant.get_gdal_dtype_from_np(output_dtype)
    else:
        outputType = plant.get_gdal_dtype_from_np(np.float32)

    if out_null is None:
        out_null = 'nan'
    algorithm = f'invdistnn:nodata={out_null}'
    if footprint is not None:
        if plant.isvalid(footprint) and footprint != 0:
            footprint_radius = footprint / 2
            algorithm += (':radius=%.15f'
                          % (footprint_radius))
    if max_points is not None:
        algorithm += ':max_points=%d' % max_points

    if plant_geogrid_obj is not None:
        length = plant_geogrid_obj.length
        width = plant_geogrid_obj.width
        outputBounds = [plant_geogrid_obj.x0,
                        plant_geogrid_obj.yf,
                        plant_geogrid_obj.xf,
                        plant_geogrid_obj.y0]
    else:
        outputBounds = None
        width = None
        length = None

    for b in range(nbands):
        if nbands == 1:
            output_file_temp = ('%s_temp_out_%s.tif'
                                % (output_file, timestamp))
        else:
            output_file_temp = ('%s_temp_out_band_%d_%s.tif'
                                % (output_file, b, timestamp))
        plant.append_temporary_file(output_file_temp)

        ret_obj = plant.Grid(output_file_temp, vrt_file,
                             flag_return=True,
                             format=output_format,
                             outputType=outputType,
                             width=width,
                             height=length,
                             outputBounds=outputBounds,
                             algorithm=algorithm,
                             layers=f'l{str_data_list[b]}')

        if b == 0:
            image_obj = ret_obj
            image = image_obj.image
        else:
            image = ret_obj.image

        image_obj.set_image(image, band=b)

    save_image(image_obj,
               output_file,

               output_format=output_format,
               output_dtype=output_dtype,
               verbose=verbose,
               force=force)

    plant.plant_config.output_files.append(output_file)
    if verbose:
        print('## file saved: %s (GDAL_GRID)' % output_file)


def get_images_from_list(input_files,
                         parameters_list=None,
                         invalid_list=None,
                         plant_transform_obj=None,
                         flag_exit_if_error=True,
                         flag_no_messages=False,
                         input_format=None,
                         verbose=True,
                         **kwargs):
    input_images = []
    if parameters_list is not None:
        new_parameters_list = []
    for i, current_file in enumerate(input_files):
        current_file_orig = current_file
        if (test_valid_number_sequence(current_file) or
                test_gdal_open(current_file)):
            input_images.append(current_file)
            if parameters_list is not None:
                new_parameters_list.append(parameters_list[i])
            continue
        elif plant.IMAGE_NAME_SEPARATOR in current_file:

            ret_dict = parse_filename(current_file_orig)

            current_file = ret_dict['filename']
            if 'filename_position' in ret_dict.keys():
                filename_position = ret_dict['filename_position']
            else:
                filename_position = 0
            if 'driver' in ret_dict.keys() and ret_dict['driver'] == 'MEM':
                image_obj = plant.plant_config.variables[current_file]
                if image_obj is not None:
                    input_images.append(current_file_orig)
                    if parameters_list is not None:
                        new_parameters_list.append(parameters_list[i])
                elif invalid_list is not None:
                    plant.handle_exception(
                        'opening file %s' % (current_file_orig),
                        flag_exit_if_error=flag_exit_if_error,
                        flag_no_messages=flag_no_messages,
                        verbose=verbose)
                    invalid_list.append(current_file_orig)
                continue

            current_file_splitted = \
                current_file_orig.split(plant.IMAGE_NAME_SEPARATOR)
        else:
            current_file_splitted = None

        filelist_expanded = plant.glob(current_file)
        if len(filelist_expanded) == 0:
            plant.handle_exception('file not found %s'
                                   % (current_file_orig),
                                   flag_exit_if_error=flag_exit_if_error,
                                   flag_no_messages=flag_no_messages,
                                   verbose=verbose)
            if invalid_list is not None:
                invalid_list.append(current_file_orig)
        for f in filelist_expanded:
            if f in input_images:
                continue
            if current_file_splitted is not None:
                current_file_splitted[filename_position] = f
            if plant.IMAGE_NAME_SEPARATOR in current_file_orig:

                f = get_filename_from_dict(ret_dict, filename=f)
            kwargs['only_header'] = True
            kwargs['verbose'] = False
            kwargs['plant_transform_obj'] = plant_transform_obj
            if input_format is not None:
                kwargs['input_format'] = input_format
            kwargs['flag_exit_if_error'] = False
            image_obj = read_image(f, **kwargs)
            if image_obj is not None:
                if current_file_splitted is not None:
                    input_images.append(plant.IMAGE_NAME_SEPARATOR.join(
                        current_file_splitted))
                else:
                    input_images.append(f)
                if parameters_list is not None:
                    new_parameters_list.append(parameters_list[i])
            elif invalid_list is not None:
                plant.handle_exception(
                    'opening file %s' % (current_file_orig),
                    flag_exit_if_error=flag_exit_if_error,
                    flag_no_messages=flag_no_messages,
                    verbose=verbose)
                invalid_list.append(f)
    if parameters_list is None:
        return input_images
    return (input_images, new_parameters_list)


def get_h5_dataset_compression_ratio(h5_obj, verbose=False):
    if not isinstance(h5_obj, h5py.Dataset):
        return

    if h5_obj.chunks is None:
        return

    h5_pl = h5_obj.id.get_create_plist()

    if not h5_pl.get_nfilters():
        return

    stor_size = h5_obj.id.get_storage_size()

    if stor_size == 0:
        return

    ratio = float(h5_obj.nbytes) / float(stor_size)

    if verbose:
        print(f'Compression ratio for "{h5_obj.name}": {ratio}')

    return ratio


def get_files_from_list(self,
                        input_files,
                        parameters_list=None,
                        verbose=False,
                        flag_no_messages=False,
                        invalid_list=None,
                        all_files_expanded=None):
    file_list = []
    if parameters_list is not None:
        new_parameters_list = []
    for i, current_file in enumerate(input_files):
        current_file_orig = current_file
        if (test_valid_number_sequence(current_file) or
                test_gdal_open(current_file)):
            file_list.append(current_file)
            if parameters_list is not None:
                new_parameters_list.append(parameters_list[i])

            if all_files_expanded is not None:
                all_files_expanded.append(current_file)
            continue
        elif plant.IMAGE_NAME_SEPARATOR in current_file:
            ret_dict = parse_filename(current_file_orig)
            current_file = ret_dict['filename']
            if 'driver' in ret_dict.keys() and ret_dict['driver'] == 'MEM':
                image_obj = plant.plant_config.variables[current_file]
                if image_obj is not None:
                    file_list.append(current_file_orig)
                    if parameters_list is not None:
                        new_parameters_list.append(parameters_list[i])
                else:
                    if not flag_no_messages:
                        print('WARNING error reading file.'
                              ' Ignoring: %s' % (current_file_orig))
                    if invalid_list is not None:
                        invalid_list.append(current_file_orig)

                if all_files_expanded is not None:
                    all_files_expanded.append(current_file)
                continue
        filelist_expanded = plant.glob(current_file)
        if len(filelist_expanded) == 0 and not flag_no_messages:
            print('WARNING file not found.'
                  ' Ignoring: %s' % (current_file_orig))
        if len(filelist_expanded) == 0 and invalid_list is not None:
            invalid_list.append(current_file_orig)

        for f in filelist_expanded:

            if plant.IMAGE_NAME_SEPARATOR in current_file_orig:
                f = get_filename_from_dict(ret_dict, filename=f)
            image_obj = self.read_image(f,
                                        only_header=True,
                                        verbose=verbose,
                                        flag_no_messages=flag_no_messages,
                                        flag_exit_if_error=False)
            if (image_obj is not None or
                    (image_obj is None and os.path.isfile)):
                file_list.append(f)
                if parameters_list is not None:
                    new_parameters_list.append(parameters_list[i])
            elif invalid_list is not None and not flag_no_messages:
                print('WARNING error reading file. '
                      'Ignoring: %s' % (current_file_orig))
                invalid_list.append(f)

            if all_files_expanded is not None:
                all_files_expanded.append(f)

    if parameters_list is None:
        return file_list
    else:
        return (file_list, new_parameters_list)


def create_config_txt(txt_file,
                      input_file='',
                      polar_type='full',
                      polar_case='monostatic',
                      width=None,
                      length=None,
                      force=None,
                      verbose=True):

    ret = plant.overwrite_file_check(txt_file,
                                     force=force)
    if not ret and verbose:
        print('WARNING file not updated: ' + txt_file,
              1)
    else:
        with open(txt_file, 'w') as f:
            if width is None or length is None:
                image_obj = read_image(input_file,
                                       only_header=True)
                length = image_obj.length
                width = image_obj.width
            f.write('Nrow\n')
            f.write('%d\n' % (length))
            f.write('---------\n')
            f.write('Ncol\n')
            f.write('%d\n' % (width))
            if polar_case:
                f.write('---------\n')
                f.write('PolarCase\n')
                f.write(polar_case + '\n')
            if polar_type:
                f.write('---------\n')
                f.write('PolarType\n')
                f.write(polar_type + '\n')


def get_info_from_config_txt(filename,
                             verbose=False,
                             flag_no_messages=False,
                             flag_exit_if_error=True):

    directory = os.path.dirname(filename)
    if not directory:
        directory = '.'
    config_file = os.path.join(directory, 'config.txt')
    nbands_orig = 1
    if not plant.isfile(config_file):

        return
    with open(config_file, 'r') as f:
        config_from_file = f.read()
        config_from_file = config_from_file.upper()
        config_from_file = config_from_file.split('\n')
        try:
            ncol_index = config_from_file.index('NCOL')
            width_orig = int(config_from_file[ncol_index + 1])
        except BaseException:
            width_orig = None
        try:
            nrow_index = config_from_file.index('NROW')
            length_orig = int(config_from_file[nrow_index + 1])
        except BaseException:
            length_orig = None
        if width_orig is not None and length_orig is not None:
            try:
                file_size = os.stat(os.path.realpath(filename)).st_size
            except FileNotFoundError:
                plant.handle_exception(f'file not found: {filename}'
                                       f' (config.txt)',
                                       flag_exit_if_error,
                                       flag_no_messages=flag_no_messages,
                                       verbose=verbose)
                return
            type_size = file_size // (width_orig * length_orig)
            if type_size == 1:
                dtype = np.byte
            elif type_size == 2:
                dtype = np.int16
            elif type_size == 8:
                dtype = np.complex64
            elif type_size == 16:
                dtype = np.complex128
            else:
                dtype = np.float32
        else:
            dtype = None
        ext = 'bin'
        image_obj = plant.PlantImage(filename=filename,
                                     width_orig=width_orig,
                                     length_orig=length_orig,
                                     dtype=dtype,
                                     nbands_orig=nbands_orig,
                                     file_format=ext.upper())
        return image_obj


def get_envi_header(filename):
    if not filename:
        return
    if filename.endswith('.hdr'):
        return filename
    basename = os.path.basename(filename)
    if '.' not in basename:
        return filename + '.hdr'
    new_filename = os.path.join(os.path.dirname(filename),
                                '.'.join(basename.split('.')[0:-1]) + '.hdr')
    return new_filename


def get_info_from_envi_header(metadata, filename=None):

    if not metadata and not filename:
        return
    if not metadata:
        metadata = filename
    image_hdr = get_envi_header(metadata)
    ret = plant.read_parameters_text_file(image_hdr)
    if ret is None:
        return
    filename = filename if filename else metadata
    filename = filename.replace('.hdr', '')
    scheme = ret['interleave']

    width = int(float(ret['samples']))
    length = int(float(ret['lines']))
    nbands = int(float(ret['bands']))
    dtype = int(float(ret['data type']))
    switcher = {
        1: '-b1',
        2: '-i2',
        3: '-i4',
        4: '-r4',
        5: '-r8',
        6: '-c8',
        9: '-c16',
        12: '-i2',
        13: '-i4',
        14: '-i8',
        15: '-i8',
    }
    dtype = switcher.get(dtype, '-r4')
    image_obj = plant.PlantImage(filename=filename,
                                 width=width,
                                 length=length,
                                 dtype=dtype,
                                 scheme=scheme,
                                 file_format='ENVI',
                                 nbands=nbands)
    return image_obj


def get_envi_boundaries(hdr):

    with open(hdr) as f:
        envi_header = f.read()
    lines = envi_header.split('\n')
    for current_line in lines:
        try:
            parameter, value = current_line.split('=')
            if parameter.strip() == 'lat_min':
                lat_min = float(value)
            if parameter.strip() == 'lat_max':
                lat_max = float(value)
            if parameter.strip() == 'lon_min':
                lon_min = float(value)
            if parameter.strip() == 'lon_max':
                lon_max = float(value)
        except BaseException:
            pass
    return lat_min, lat_max, lon_min, lon_max


def is_isce_image(filename):

    if (not plant.isfile(filename + '.xml') and
            not filename.endswith('.xml')):
        return False

    image_obj = read_image(filename,

                           only_header=True,
                           input_format='ISCE',
                           verbose=False,
                           flag_exit_if_error=False)

    return (image_obj is not None)


def test_for_gdal_convention_error(filename):

    return False


def read_matrix(*args, **kwargs):

    filename = kwargs.get('filename_orig', None)
    copy = kwargs.pop('copy', None)
    if isinstance(filename, plant.PlantImage):
        image_obj = filename
    else:
        image_obj = read_image(*args, **kwargs)
    if image_obj is None:
        return

    if copy:
        return image_obj.image.copy()
    return image_obj.image


def sample_image(input_name,
                 sampling_step=None,
                 sampling_step_x=None,
                 sampling_step_y=None,
                 sampling_step_z=None,
                 fast=None,
                 only_header=None,
                 verbose=True,
                 band=None):
    if input_name is None:
        return
    kwargs = locals()
    geotransform = None
    if isinstance(input_name, str):
        image_obj = read_image(search_image(input_name)[0],
                               only_header=only_header,
                               band=band)
    elif isinstance(input_name, plant.PlantImage):
        image_obj = input_name
    else:
        image_obj = None
        depth, length, width = plant.get_image_dimensions(input_name)
    if image_obj is not None:

        if image_obj.nbands > 1 and band is None:
            kwargs.pop('band', None)
            kwargs.pop('input_name', None)
            print(f'resampling image {image_obj.filename_orig}... ')
            with plant.PlantIndent():
                image_obj = sample_image(image_obj,
                                         band=0,
                                         **kwargs)
                for b in range(1, image_obj.nbands):
                    image = image_obj.get_image(band=b)
                    current_image = sample_image(image, **kwargs)
                    image_obj.set_image(current_image, band=b)
                return image_obj
        image = image_obj.get_image(band=band)
        width = image_obj.width
        length = image_obj.length
        depth = image_obj.depth

        geotransform = image_obj.geotransform
    else:
        image = input_name
    image_loaded = image is not None

    if (sampling_step is None and
            sampling_step_x is None and
            sampling_step_y is None and
            sampling_step_z is None and
            fast):
        n_elements = 1
        if width is not None:
            n_elements *= width
        if length is not None:
            n_elements *= length
        if depth is not None:
            n_elements *= depth
        dtype = plant.get_dtype_name(image.dtype)
        type_size = plant.get_dtype_size(dtype)
        data_size = n_elements * type_size
        sampling_step = max([1, data_size // 1e7])

    if sampling_step is None:
        sampling_step = 1

    if (sampling_step_x is None and
            isinstance(sampling_step, numbers.Number)):
        sampling_step_x = sampling_step
    elif (sampling_step_x is None and
          hasattr(sampling_step, '__getitem__')
          and len(sampling_step) >= 1):
        sampling_step_x = sampling_step[0]
    elif sampling_step_x is None:
        sampling_step_x = 1

    if (sampling_step_y is None and
            isinstance(sampling_step, numbers.Number)):
        sampling_step_y = sampling_step
    elif (sampling_step_y is None and
          hasattr(sampling_step, '__getitem__')
          and len(sampling_step) >= 2):
        sampling_step_y = sampling_step[1]
    elif sampling_step_y is None:
        sampling_step_y = 1

    if (sampling_step_z is None and
            isinstance(sampling_step, numbers.Number)):
        sampling_step_z = sampling_step
    elif (sampling_step_z is None and
          hasattr(sampling_step, '__getitem__')
          and len(sampling_step) >= 3):
        sampling_step_z = sampling_step[2]
    elif sampling_step_z is None:
        sampling_step_z = 1

    sampling_step_x = int(sampling_step_x)
    sampling_step_y = int(sampling_step_y)
    sampling_step_z = int(sampling_step_z)

    if (sampling_step_x == 1 and
            sampling_step_y == 1 and
            sampling_step_z == 1):
        return input_name

    if image_loaded and depth <= 1:

        new_image = image[::sampling_step_y,
                          ::sampling_step_x]

    elif image_loaded:
        new_image = image[::sampling_step_y,
                          ::sampling_step_x,
                          ::sampling_step_z]

    else:
        new_width = int(np.ceil(float(width) /
                                sampling_step_x))
        new_length = int(np.ceil(float(length) /
                                 sampling_step_y))
        new_depth = int(np.ceil(float(depth) /
                                sampling_step_z))
    if image_loaded:
        new_depth, new_length, new_width = \
            plant.get_image_dimensions(new_image)

    if geotransform is not None:
        geotransform_new = geotransform[:]
        geotransform_new[1] = geotransform_new[1] * sampling_step_x
        geotransform_new[5] = geotransform_new[5] * sampling_step_y
        image_obj.set_geotransform(geotransform_new,
                                   realize_changes=False)

    if verbose and depth <= 1:
        print('new shape: (%d, %d)'
              % (new_length, new_width))
    elif verbose:
        print('new shape: (%d, %d, %d)'
              % (new_length, new_width, new_depth))
    if image_obj is not None:
        if image_loaded:
            image_obj.set_image(new_image, realize_changes=False)
        image_obj._length = new_length
        image_obj._width = new_width
        image_obj._depth = new_depth
        return image_obj

    return new_image


def test_valid_number_sequence(filename):

    if (isinstance(filename, numbers.Number) or
            isinstance(filename, np.ndarray) or
            isinstance(filename, slice)):
        return True
    if isinstance(filename, list) or isinstance(filename, tuple):
        for element in filename:
            if not test_valid_number_sequence(element):
                return False
        return True
    filename_splitted = filename.split(plant.IMAGE_NAME_SEPARATOR)
    if (len(filename_splitted) >= 1 and

            os.path.isfile(filename_splitted[0])):
        return False
    elif (len(filename_splitted) > 2 and

            os.path.isfile(filename_splitted[1])):
        return False

    filename_mult = filename.replace(plant.LINE_MULTIPLIER,
                                     plant.LINE_SEPARATOR)
    filename_splitted = filename_mult.split(plant.LINE_SEPARATOR)
    for line_str in filename_splitted:
        line_splitted = line_str.split(plant.IMAGE_NAME_SEPARATOR)
        for element in line_splitted:
            element_mult = element.replace(plant.ELEMENT_MULTIPLIER,
                                           plant.ELEMENT_SEPARATOR)
            element_splitted = element_mult.split(plant.ELEMENT_SEPARATOR)
            for number in element_splitted:
                if not plant.isnumeric(number):
                    return False
    return True


def test_gdal_open(filename, geo=False, parameters_dict=None):
    gdal.UseExceptions()
    gdal.ErrorReset()

    if parameters_dict is not None:
        parameters_dict['filename'] = filename
    if filename is None:
        return False

    if not test_valid_number_sequence(filename):
        ret_dict = parse_filename(filename)
        if (ret_dict is not None and 'driver' in ret_dict.keys() and
                'HDF5' == ret_dict['driver'].upper()):
            return False
        if (ret_dict is not None and 'filename' in ret_dict.keys() and
            'driver' in ret_dict.keys() and
                'NETCDF' not in ret_dict['driver']):
            try:
                _ = h5py.File(ret_dict['filename'], 'r')

                return False
            except BaseException:

                pass

    dataset = None
    gdal.UseExceptions()
    gdal.ErrorReset()
    try:
        dataset = gdal.Open(filename)
    except BaseException:
        pass
    if (dataset is None and
            plant.IMAGE_NAME_SEPARATOR not in filename):

        dataset = None
        gdal.ErrorReset()
        return False

    if dataset is None:

        filename_splitted = filename.split(plant.IMAGE_NAME_SEPARATOR)
        filename_to_test = plant.IMAGE_NAME_SEPARATOR.join(
            filename_splitted[:-1])
        complement = filename_splitted[-1]
        test_parameters_dict = {}
        ret = test_gdal_open(filename_to_test, geo=geo,
                             parameters_dict=test_parameters_dict)
        if not ret or parameters_dict is None:
            dataset = None
            gdal.ErrorReset()
            return ret
        parameters_dict['filename'] = filename_to_test

        if ('has_sub_datasets' in test_parameters_dict.keys() and
                test_parameters_dict['has_sub_datasets']):
            parameters_dict['key'] = complement
        else:
            parameters_dict['band'] = complement
        return ret

    if parameters_dict is not None:

        sub_datasets = dataset.GetSubDatasets()
        parameters_dict['has_sub_datasets'] = bool(sub_datasets)

    dataset = None
    gdal.ErrorReset()
    if geo:
        if not plant.valid_coordinates(bbox_file=filename):
            return False
    return True


def test_other_drivers(input_file, parameters_dict=None):
    input_file_splitted = input_file.split(':')

    if (len(input_file_splitted) == 1 and
            ' ' in input_file_splitted[0]):
        return test_other_drivers('UTIL:' + input_file,
                                  parameters_dict=parameters_dict)

    if (len(input_file_splitted) == 1 and
            mimetypes.guess_type((input_file_splitted[0]))[0] ==
            'text/plain'):
        return test_other_drivers('TEXT:' + input_file,
                                  parameters_dict=parameters_dict)

    if len(input_file_splitted) <= 1:
        return False
    driver = input_file_splitted[0].upper()
    flag_driver_found = driver in plant.AVAILABLE_DRIVERS
    if (not flag_driver_found and
            len(input_file_splitted) == 2 and
            driver not in plant.TEXT_DRIVERS):
        if (mimetypes.guess_type((input_file_splitted[0]))[0] ==
                'text/plain'):
            return test_other_drivers('CSV:' + input_file,
                                      parameters_dict=parameters_dict)

    if flag_driver_found and parameters_dict is not None:
        filename = input_file_splitted[1]
        filename_position = 1
        filename = filename.replace('"', '')
        filename = filename.replace("'", '')
        width = None
        length = None
        depth = None
        dtype = None
        key = None
        header_file = None
        if (driver.upper() == 'BIN'):
            dtype = 'float32'
            if len(input_file_splitted) >= 3:
                width = int(input_file_splitted[2])
            if (len(input_file_splitted) >= 4 and
                    plant.isnumeric(input_file_splitted[3])):
                length = int(input_file_splitted[3])
            if (len(input_file_splitted) >= 5 and
                    plant.isnumeric(input_file_splitted[4])):
                depth = int(input_file_splitted[4])
            last_element = input_file_splitted[
                len(input_file_splitted) - 1]
            if not plant.isnumeric(last_element):
                try:
                    dtype = plant.get_dtype_name(
                        plant.get_np_dtype(last_element))
                    if dtype == 'NoneType':
                        dtype = None
                except BaseException:
                    dtype = None
        elif ('SENTINEL' in driver.upper()):
            header_file = input_file_splitted[2]
            if len(input_file_splitted) > 3:
                key = input_file_splitted[3]

        elif len(input_file_splitted) > 2:
            key = input_file_splitted[2]
            key = key.replace('//', '')
        parameters_dict['driver'] = driver

        if len(python_glob.glob(filename)) > 0:
            parameters_dict['filename'] = python_glob.glob(filename)[0]
        else:
            parameters_dict['filename'] = filename
        parameters_dict['filename_position'] = filename_position
        parameters_dict['width'] = width
        parameters_dict['length'] = length
        parameters_dict['depth'] = depth
        parameters_dict['dtype'] = dtype
        parameters_dict['key'] = key
        if plant.isnumeric(key):
            parameters_dict['band'] = key
        parameters_dict['header_file'] = header_file

    plant_module = None
    if not flag_driver_found:
        module_name = driver.replace('.py', '').lower()
        if not module_name.startswith('plant_'):
            module_name = 'plant_' + module_name
        try:
            _ = importlib.import_module('plant.app.' +
                                        module_name)
            plant_module = module_name
            flag_driver_found = True
            driver = module_name
        except ImportError:
            pass
        if (plant_module is not None and
                parameters_dict is not None):
            parameters_dict['plant_module'] = plant_module
            filename = plant.IMAGE_NAME_SEPARATOR.join(
                input_file_splitted[1:])
            parameters_dict['filename'] = filename

    return flag_driver_found


def search_image(filename_orig, invalid_list=None):

    if filename_orig is None:
        return False
    if (isinstance(filename_orig, plant.PlantImage) or
            test_valid_number_sequence(filename_orig) or
            test_gdal_open(filename_orig) or
            test_other_drivers(filename_orig)):

        return [filename_orig]

    if plant.IMAGE_NAME_SEPARATOR in filename_orig:
        ret_dict = parse_filename(filename_orig)
        filename = ret_dict['filename']
        filename_list = []
        glob_list = plant.glob(filename)
        if len(glob_list) == 0:
            if invalid_list is not None:
                invalid_list.append(filename_orig)
            return []
        for f in glob_list:
            filename = get_filename_from_dict(ret_dict, filename=f)
            filename_list += [filename]

        return filename_list
    elif plant.isfile(filename_orig):

        return plant.glob(filename_orig)
    elif invalid_list is not None:
        invalid_list.append(filename_orig)
    return []


def read_image_kml(kml_file,
                   band=None,
                   verbose=False,
                   only_header=True,
                   flag_no_messages=False,
                   flag_exit_if_error=True):
    try:
        kml = ET.parse(kml_file)
    except BaseException:
        plant.handle_exception('file could not be opened: %s'
                               ' (kml)'
                               % kml_file,
                               flag_exit_if_error,
                               flag_no_messages=flag_no_messages,
                               verbose=verbose)
        return

    icon_list = []
    for element in kml.getroot().iter():
        for lat_lon_box_element in element.getchildren():
            if 'LatLonBox' in lat_lon_box_element.tag:
                icon = None
                for other_child in element.getchildren():
                    if 'Icon' in other_child.tag:
                        icon = other_child.getchildren()[0].text
                if icon is not None:
                    north = None
                    south = None
                    east = None
                    west = None
                    for coord in lat_lon_box_element.getchildren():
                        if 'north' in coord.tag:
                            north = coord.text
                        if 'south' in coord.tag:
                            south = coord.text
                        if 'east' in coord.tag:
                            east = coord.text
                        if 'west' in coord.tag:
                            west = coord.text
                    try:
                        icon_list.append([icon,
                                          [float(south),
                                           float(north),
                                           float(east),
                                           float(west)]])
                    except BaseException:
                        pass
    image_obj = None

    band_count = 0
    image_obj = None
    for i, icon in enumerate(icon_list):
        current_image_obj = read_image(icon[0],
                                       verbose=verbose,
                                       only_header=(only_header or
                                                    band is not None))
        if current_image_obj is None:
            continue
        if image_obj is not None:
            if (image_obj.width != current_image_obj.width or
                    image_obj.length != current_image_obj.length):
                if not flag_no_messages:
                    print('WARNING ignoring %s because image '
                          'dimensions diffe from %s'
                          % (icon[0], icon_list[0][0]))
                continue
        if (band is not None and
                band < band_count + current_image_obj.nbands):
            image_obj = read_image(icon[0],
                                   verbose=verbose,
                                   band=band - band_count,
                                   only_header=only_header)
            geotransform = get_geotransform_kml(icon[1],
                                                image_obj.length,
                                                image_obj.width)
            image_obj.set_geotransform(geotransform,
                                       realize_changes=False)
            break
        if band is None and i == 0:
            image_obj = current_image_obj
            geotransform = get_geotransform_kml(icon[1],
                                                image_obj.length,
                                                image_obj.width)
            image_obj.set_geotransform(geotransform,
                                       realize_changes=False)
        elif band is None:
            for b in range(current_image_obj.bands):
                image_obj.set_band(current_image_obj.get_band(band=b),
                                   band=band_count + b)
        band_count += current_image_obj.nbands

    return image_obj


def get_geotransform_kml(bbox_outer, length, width):

    geotransform = plant.get_geotransform(bbox=bbox_outer,
                                          length=length,
                                          width=width)
    return geotransform


def parse_filename(filename_orig):
    band = None
    key = None
    ret_dict = {}

    ret_dict['band'] = band
    ret_dict['key'] = key
    ret_dict['filename_position'] = 0

    if filename_orig.startswith('MEM:'):
        ret_dict['filename'] = filename_orig
        return ret_dict
    elif len(python_glob.glob(filename_orig)) > 0:
        ret_dict['filename'] = python_glob.glob(filename_orig)[0]
        return ret_dict
    filename_splitted = filename_orig.split(plant.IMAGE_NAME_SEPARATOR)

    if filename_splitted[0].upper() in plant.AVAILABLE_DRIVERS:
        parameters_dict = {}
        test_other_drivers(filename_orig,
                           parameters_dict=parameters_dict)
        return parameters_dict

    filename_position = 1 if len(filename_splitted) > 2 else 0

    filename = filename_splitted[filename_position]
    if len(filename_splitted) > 1:
        complement = filename_splitted[filename_position + 1]
        if plant.isnumeric(complement) and band is None:
            band = int(complement)
        elif not plant.isnumeric(complement) and key is None:
            key = complement
    ret_dict = {}
    ret_dict['filename_position'] = filename_position

    if len(python_glob.glob(filename)) > 0:
        ret_dict['filename'] = python_glob.glob(filename)[0]
    else:
        ret_dict['filename'] = filename
    ret_dict['band'] = band
    ret_dict['key'] = key
    return ret_dict


def get_filename_from_dict(input_dict, filename=None, band=None, key=None,
                           driver=None):
    if not filename:
        filename = input_dict.get('filename', None)
    if band is None:
        band = input_dict.get('band', None)
    if band is not None:
        filename += f':{band}'
    if key is None:
        key = input_dict.get('key', None)
    if key is not None:
        filename += f':{key}'

    if driver is None:
        driver = input_dict.get('driver', None)
    if driver is not None:
        filename = f'{driver}:{filename}'

    return filename


def _read_image_unlock(image_obj):
    if image_obj is None:
        return
    image_obj.unlock_realize_changes()


def read_image(filename_orig,
               band=None,
               verbose=True,
               input_format=None,
               only_header=True,
               read_only=True,

               plant_transform_obj=None,
               ref_image_obj=None,
               scheme=None,

               geotransform=None,
               projection=None,

               input_width=None,
               input_length=None,
               input_depth=None,
               input_dtype=None,

               input_key=None,
               header_file=None,
               in_null=None,
               out_null=None,

               update=None,
               force=None,
               flag_no_messages=False,
               flag_exit_if_error=True,
               **kwargs_orig):

    if filename_orig is None:
        plant.handle_exception('opening file: None',
                               flag_exit_if_error,
                               flag_no_messages=flag_no_messages,
                               verbose=verbose)
        return

    if input_format is not None:
        input_format = input_format.strip().upper()

    if plant_transform_obj is None:
        method_to_execute = plant.get_plant_transform_obj

        transform_kwargs = {'method_to_execute': method_to_execute}

        transform_kwargs = plant.populate_kwargs(kwargs_orig,
                                                 **transform_kwargs)

        for kwarg_orig in kwargs_orig.keys():
            if kwarg_orig not in transform_kwargs:
                error_message = ('ERROR invalid keyword argument:'
                                 f' "{kwarg_orig}"')
                print(error_message)
                raise RuntimeError(error_message)

        plant_transform_obj = method_to_execute(**transform_kwargs)

    if isinstance(filename_orig, plant.PlantImage):

        image_obj = filename_orig
        prepare_image(image_obj,

                      flag_exit_if_error=flag_exit_if_error,
                      plant_transform_obj=plant_transform_obj,
                      in_null=in_null,
                      out_null=out_null,
                      input_format=input_format,
                      verbose=verbose,
                      only_header=only_header,
                      flag_no_messages=flag_no_messages,
                      force=force)
        _read_image_unlock(image_obj)
        if update:
            image_obj.realize_changes()
        return image_obj

    if (isinstance(filename_orig, np.ndarray) or
            isinstance(filename_orig, numbers.Number) or
            isinstance(filename_orig, list) or
            isinstance(filename_orig, tuple) or
            isinstance(filename_orig, slice)):
        if (isinstance(filename_orig, numbers.Number)):
            filename_orig = np.asarray(filename_orig)
        elif (isinstance(filename_orig[0], str) and
              all([plant.isfile(f) for f in filename_orig])):

            kwargs = locals()
            kwargs.pop('filename_orig')
            image_obj = None
            current_band = None
            for f in filename_orig:

                if image_obj is None:
                    image_obj = plant.read_image(f, **kwargs)
                    current_band = image_obj.nbands
                    continue
                current_image_obj = plant.read_image(f, **kwargs)
                for b in range(current_image_obj.nbands):
                    image_obj.set_image(current_image_obj.get_image(
                        band=b), band=current_band)
                    current_band += 1
            _read_image_unlock(image_obj)
            return image_obj
        image_obj = plant.PlantImage(filename_orig,
                                     file_format='ARRAY',
                                     ref_image_obj=ref_image_obj,
                                     plant_transform_obj=plant_transform_obj)
        _read_image_unlock(image_obj)
        if update:
            image_obj.realize_changes()
        return image_obj

    if test_valid_number_sequence(filename_orig):
        parameters_dict = {}

        flag_test_gdal_open = False
    else:
        parameters_dict = plant.parse_filename(filename_orig)

        flag_test_gdal_open = test_gdal_open(filename_orig,
                                             parameters_dict=parameters_dict)

    kwargs = locals()

    if not flag_no_messages and not input_format:
        gdal.UseExceptions()
        gdal.ErrorReset()

    if input_format is None and 'driver' in parameters_dict.keys():
        input_format = parameters_dict['driver']

    if (input_format is None and isinstance(filename_orig, str) and
            'filename' in parameters_dict.keys()):
        filename = parameters_dict['filename']

        try:
            hdf5_obj = h5py.File(filename, 'r')

            input_format = 'HDF5'
        except OSError:
            pass

    gdal.ErrorReset()
    kwargs['flag_no_messages'] = (flag_no_messages or
                                  input_format is None or
                                  'HDF5' not in input_format)
    kwargs['input_format'] = input_format
    image_obj = _read_image(**kwargs)

    if image_obj is None:
        plant.handle_exception(f'opening file {filename_orig}',
                               flag_exit_if_error,
                               flag_no_messages=flag_no_messages,
                               verbose=verbose)
        return

    if (ref_image_obj is not None and
            id(image_obj) != id(ref_image_obj)):
        image_obj = image_obj.copy(ref_image_obj=ref_image_obj)
    _read_image_unlock(image_obj)

    if update:
        image_obj.realize_changes()
    return image_obj


def _read_image(filename_orig,
                band=None,
                verbose=True,
                input_format=None,
                only_header=True,
                read_only=True,

                plant_transform_obj=None,
                ref_image_obj=None,
                scheme=None,
                input_width=None,
                input_length=None,
                input_depth=None,
                input_dtype=None,
                input_key=None,
                header_file=None,
                in_null=None,

                geotransform=None,
                projection=None,

                force=None,
                flag_no_messages=False,
                flag_exit_if_error=True,

                parameters_dict=None,
                flag_test_gdal_open=None,
                **kwargs_orig):

    image_obj_kwargs = {}
    image_obj_kwargs['realize_changes_locked'] = True
    if ref_image_obj is not None:
        image_obj_kwargs['ref_image_obj'] = ref_image_obj

    out_null = None
    if flag_no_messages:
        verbose = False

    if not filename_orig:
        plant.handle_exception('invalid input',
                               flag_exit_if_error,
                               flag_no_messages=flag_no_messages,
                               verbose=verbose)
        return

    kwargs = locals()
    kwargs['image_obj_kwargs'] = image_obj_kwargs

    args = []
    args.append(kwargs.pop('filename_orig'))

    width_orig = input_width
    length_orig = input_length
    depth_orig = input_depth

    image = None

    dtype = None if input_dtype is None else input_dtype

    image_obj = None

    if test_valid_number_sequence(filename_orig):
        image_obj = read_image_array(filename_orig,
                                     verbose=verbose,
                                     plant_transform_obj=plant_transform_obj,
                                     in_null=None,
                                     out_null=None,
                                     force=None,
                                     only_header=only_header,
                                     flag_no_messages=False,
                                     flag_exit_if_error=False)
        return image_obj

    if flag_test_gdal_open:
        filename = parameters_dict['filename']
        if band is None and 'band' in parameters_dict.keys():
            band = parameters_dict['band']
        if input_key is None and 'key' in parameters_dict.keys():
            input_key = parameters_dict['key']
    elif test_other_drivers(filename_orig,
                            parameters_dict=parameters_dict):

        filename = parameters_dict['filename']
        if header_file is None and 'header_file' in parameters_dict.keys():
            header_file = parameters_dict['header_file']
        if input_key is None and 'key' in parameters_dict.keys():
            input_key = parameters_dict['key']
        if input_dtype is None and 'dtype' in parameters_dict.keys():
            dtype = parameters_dict['dtype']
        else:
            dtype = input_dtype
        if width_orig is None and 'width' in parameters_dict.keys():
            width_orig = parameters_dict['width']
        if length_orig is None and 'length' in parameters_dict.keys():
            length_orig = parameters_dict['length']
        if depth_orig is None and 'depth' in parameters_dict.keys():
            depth_orig = parameters_dict['depth']
        if input_format is None and 'driver' in parameters_dict.keys():
            input_format = parameters_dict['driver']
        if input_format is not None and input_format == 'MEM':
            try:
                image_obj = plant.plant_config.variables[filename]
            except KeyError:

                image_list = [x for x in globals().values()
                              if isinstance(x, plant.PlantImage) and
                              x.get_filename() == filename]

                if len(image_list) == 0:
                    import gc
                    image_list = [x for x in gc.get_objects()
                                  if isinstance(x, plant.PlantImage) and
                                  x.get_filename() == filename]
                if len(image_list) == 0:
                    image_obj = None
                else:
                    image_obj = image_list[0]

            if image_obj is None:
                plant.handle_exception(f'opening memory file {filename_orig}',
                                       flag_exit_if_error,
                                       flag_no_messages=flag_no_messages,
                                       verbose=verbose)
                return

            if not isinstance(image_obj, plant.PlantImage):
                out_image_obj = plant.PlantImage(image_obj)
            elif band is None:
                out_image_obj = image_obj.deep_copy(
                    ref_image_obj=ref_image_obj)
            else:
                out_image_obj = image_obj.soft_copy(
                    ref_image_obj=ref_image_obj)
                out_image_obj.nbands = 0
                band_list = band if hasattr(band, '__getitem__') else [band]
                for i, b in enumerate(band_list):
                    out_image_obj.set_band(image_obj.get_band(band=b).copy(),
                                           band=i)
            if (not out_image_obj.image_loaded and
                    out_image_obj.plant_transform_obj is not None and
                    plant_transform_obj is not None and
                    out_image_obj.plant_transform_obj is not
                    plant_transform_obj):
                out_image_obj.realize_changes()

            if not out_image_obj.filename_orig:
                out_image_obj.filename_orig = filename_orig
            if not out_image_obj.filename:
                out_image_obj.filename = filename_orig
            if not out_image_obj.file_format:
                out_image_obj.file_format = 'MEM'

            input_format = 'MEM'

            prepare_image(out_image_obj,
                          filename,
                          flag_exit_if_error=flag_exit_if_error,
                          plant_transform_obj=plant_transform_obj,
                          in_null=in_null,
                          out_null=out_null,
                          input_format=input_format,
                          verbose=verbose,
                          only_header=only_header,
                          flag_no_messages=flag_no_messages,
                          force=force)

            return out_image_obj

        if ('plant_module' in parameters_dict.keys() and
                parameters_dict['plant_module'] is not None):
            plant_module = parameters_dict['plant_module']
            if filename_orig in plant.plant_config.cache_dict.keys():
                image_obj = plant.plant_config.cache_dict[filename_orig]
            else:
                module_obj = plant.ModuleWrapper(plant_module)

                image_obj = module_obj(filename, verbose=True)
                if not isinstance(image_obj, plant.PlantImage):
                    image_obj = plant.read_image(image_obj)

                image_obj.filename_orig = ''
                filename = ''
                image_obj.filename = filename
                input_format = 'MEM'
                plant.plant_config.cache_dict[filename_orig] = image_obj
                image_obj.file_format = input_format

            prepare_image(image_obj,
                          filename,
                          flag_exit_if_error=flag_exit_if_error,
                          plant_transform_obj=plant_transform_obj,
                          in_null=in_null,
                          out_null=out_null,
                          input_format=input_format,
                          verbose=verbose,
                          only_header=only_header,
                          flag_no_messages=flag_no_messages,
                          force=force)

            return image_obj
    elif plant.IMAGE_NAME_SEPARATOR in filename_orig:
        ret_dict = parse_filename(filename_orig)
        filename = ret_dict['filename']
        if band is None and 'band' in ret_dict.keys():
            band = ret_dict['band']
        if input_key is None and 'key' in ret_dict.keys():
            input_key = ret_dict['key']
    elif not plant.isfile(filename_orig):
        filename_temp = plant.glob(filename_orig)
        if not filename_temp or len(filename_temp) == 0:
            plant.handle_exception('file not found: ' + filename_orig,
                                   flag_exit_if_error,
                                   flag_no_messages=flag_no_messages,
                                   verbose=verbose)
            return
        filename = filename_temp[0]
        if os.path.isdir(filename):
            plant.handle_exception('input is a directory: ' + filename,
                                   flag_exit_if_error,
                                   flag_no_messages=flag_no_messages,
                                   verbose=verbose)
            return
    else:
        filename = os.path.expanduser(filename_orig)
        filename_list = plant.glob(filename)
        if len(filename_list) == 0:
            plant.handle_exception(f'file not found: {filename}',
                                   flag_exit_if_error,
                                   flag_no_messages=flag_no_messages,
                                   verbose=verbose)
            return
        filename = filename_list[0]
    args.append(filename)

    kwargs['band'] = band
    kwargs['input_key'] = input_key
    kwargs['input_width'] = input_width

    if not input_format:
        input_format = ''

    if ((not (input_format) and filename.endswith('.npy')) or
            input_format == 'NUMPY'):

        image = np.fromfile(filename)
        image_obj = plant.PlantImage(filename=filename,
                                     image=image,

                                     file_format='NUMPY',

                                     plant_transform_obj=plant_transform_obj,
                                     **image_obj_kwargs)

        prepare_image(image_obj,
                      filename,
                      flag_exit_if_error=flag_exit_if_error,
                      plant_transform_obj=plant_transform_obj,
                      in_null=in_null,
                      out_null=out_null,
                      input_format=input_format,
                      verbose=verbose,
                      only_header=only_header,
                      flag_no_messages=flag_no_messages,
                      force=force)
        return image_obj

    header_dict = None
    if header_file is None:
        header_dict = plant.parse_uavsar_filename(filename)
        header_file = plant.get_annotation_file(
            header_dict, dirname=os.path.dirname(filename))
        if isinstance(header_file, list) and len(header_file) > 0:
            header_file = header_file[0]

    if (not input_format or
            'ANN' in input_format.upper() or
            (header_file and header_file.endswith('.ann'))):
        if (len(plant.glob(os.path.join(plant.dirname(filename),
                                        '*ann'))) != 0):
            kwargs['header_file'] = header_file
            kwargs['header_dict'] = header_dict
            image_obj = _read_image_ann(*args, **kwargs)
            kwargs.pop('header_dict')
            if image_obj is not None:
                return image_obj
            elif input_format == 'ANN':
                plant.handle_exception(f'reading file {filename_orig} (ANN)',
                                       flag_exit_if_error,
                                       flag_no_messages=flag_no_messages,
                                       verbose=verbose)
                return
        elif input_format == 'ANN':
            plant.handle_exception('annotation file not found',
                                   flag_exit_if_error,
                                   flag_no_messages=flag_no_messages,
                                   verbose=verbose)
            return

    if input_format in ['HDF5', 'NISAR']:
        kwargs['input_format'] = input_format
        image_obj = _read_hdf5(*args, **kwargs)
        if image_obj is not None:
            return image_obj

    if ((not input_format and not scheme) or
            (input_format and
             (input_format not in plant.AVAILABLE_DRIVERS or
              input_format in ['HDF5', 'NETCDF', 'SENTINEL']))):
        kwargs.pop('flag_exit_if_error')
        if input_format:
            kwargs['input_format'] = input_format
        image_obj = _read_image_gdal(*args, **kwargs)
        kwargs['flag_exit_if_error'] = flag_exit_if_error

        if 'SENTINEL' in input_format and input_key:
            image_obj = _read_image_sentinel(image_obj, filename,
                                             header_file, input_key,
                                             verbose)

        if image_obj is not None:
            return image_obj

        if input_format == 'GDAL' or 'HDF5' in input_format:
            plant.handle_exception('file could not be opened: ' +
                                   filename + ' (GDAL)',
                                   flag_exit_if_error,
                                   flag_no_messages=flag_no_messages,
                                   verbose=verbose)
            return

    if not scheme and filename.endswith('.xml'):
        try:
            image_obj = get_info_from_xml(filename)
        except KeyError:
            pass
    elif not scheme and plant.isfile(filename + '.xml'):
        try:
            image_obj = get_info_from_xml(filename + '.xml')
        except KeyError:
            pass
    if image_obj is not None:
        scheme = image_obj.scheme
    if not scheme:
        scheme = 'BSQ'
    if (input_format == 'ISCE' or
            not input_format or
            input_format == 'ENVI'):
        flag_exit_if_error_isce = (kwargs.pop('flag_exit_if_error', True) and
                                   input_format == 'ISCE')
        kwargs['flag_exit_if_error'] = flag_exit_if_error_isce
        kwargs['image_obj'] = image_obj
        image_obj = _read_image_bin(*args, **kwargs)
        if image_obj is None and (input_format == 'ISCE' or
                                  input_format == 'ENVI'):
            plant.handle_exception(
                f'file could not be opened: {filename}'
                f' using format: {input_format}',
                flag_exit_if_error,
                flag_no_messages=flag_no_messages,
                verbose=verbose)
            return
        elif image_obj is not None:
            return image_obj

    flag_open_srf = (not input_format or
                     input_format == 'SRF')
    if flag_open_srf:
        try:
            header = np.fromfile(filename, dtype=np.uint32, count=8).tolist()
            if len(header) == 0:
                header = [0]
            if header[0] != plant.SRF_MAGIC_NUMBER:
                flag_open_srf = False
        except BaseException:
            flag_open_srf = False

    if input_format == 'SRF' and not flag_open_srf:
        print(f'ERROR opening {filename_orig} using SRF format')
        return

    if flag_open_srf:
        if verbose and not only_header:
            print('opening: ' + filename + ' (SRF)')
        width_orig = header[1]
        length_orig = header[2]
        depth_orig = 1
        dtype = plant.get_np_dtype_from_srf(header[5])
        dtype_size = header[3] / 8
        n_elements = header[4]
        nbands_orig = max([1, int(n_elements /
                                  (width_orig * length_orig * 8))])
        if dtype is None:
            print('SRF header: ', header)
            print(f'    width: {width_orig}')
            print(f'    length: {length_orig}')
            print(f'    data type: {dtype}')
            print(f'    data type size: {dtype_size}')
            print(f'    n_elements: {n_elements}')
            print(f'    nbands: {nbands_orig}')
            plant.handle_exception('file could not be opened %s (SRF)'
                                   % filename,
                                   flag_exit_if_error,
                                   flag_no_messages=flag_no_messages,
                                   verbose=verbose)
            return

        if in_null is None:
            in_null = -9999.0

        image_obj = plant.PlantImage(filename=filename,

                                     filename_orig=filename_orig,
                                     dtype=dtype,
                                     width=width_orig,
                                     length=length_orig,
                                     depth=depth_orig,
                                     null=in_null,
                                     nbands=nbands_orig,
                                     file_format='SRF',

                                     plant_transform_obj=plant_transform_obj,
                                     scheme=scheme,
                                     **image_obj_kwargs)

        if not only_header:
            kwargs['image_obj'] = image_obj

            kwargs['offset'] = 32
            image_obj = _read_image_bin(*args, **kwargs)
        image_obj = plant.PlantImage(input_data=image_obj,
                                     filename=filename,
                                     filename_orig=filename_orig,

                                     plant_transform_obj=plant_transform_obj,
                                     file_format='SRF',
                                     scheme=scheme,
                                     **image_obj_kwargs)
        if not only_header:
            return image_obj

    elif (filename.endswith('.csv') or filename.endswith('.txt') or
          input_format in plant.TEXT_DRIVERS):
        image_obj = read_image_csv(filename,
                                   filename_orig,
                                   plant_transform_obj,
                                   band=band,
                                   key=input_key,
                                   in_null=in_null,
                                   out_null=out_null,
                                   only_header=only_header,
                                   input_format=input_format,
                                   flag_exit_if_error=flag_exit_if_error,
                                   force=force,
                                   flag_no_messages=flag_no_messages,
                                   verbose=verbose)
        return image_obj

    elif (filename.endswith('.bin') or
          input_format == 'BIN' or
          (filename.endswith('.dat') and
           plant.isfile(filename.replace('.dat', '.Hdr')))):

        if band is not None:
            band_suffix = f', band: {band}'
        else:
            band_suffix = ''
        if ((width_orig is not None or length_orig is not None) and
                dtype is not None):
            image_obj = None
            dtype_size = plant.get_dtype_size(dtype)
            file_size = int(os.stat(filename).st_size)
            if plant.isvalid(file_size) and plant.isvalid(dtype_size):
                n_elements = file_size / dtype_size
                if length_orig is None and width_orig is not None:
                    length_orig = int(n_elements / width_orig)
                elif width_orig is None and length_orig is not None:
                    width_orig = int(n_elements / length_orig)
                elif width_orig is not None and length_orig is not None:
                    depth_orig = int(n_elements / (width_orig *
                                                   length_orig))
                else:
                    width_orig = int(n_elements)
                    length_orig = 1
            else:
                n_elements = np.nan
                length_orig = np.nan
            if depth_orig is None:
                depth_orig = 1

            filename_orig = (f'BIN:{filename}:{width_orig}:'
                             f'{length_orig}:{depth_orig}:'
                             f'{dtype}')
            band_orig = None if band is None else [band]
            image_obj = plant.PlantImage(
                input_data=image_obj,
                filename=filename,
                filename_orig=filename_orig,
                nbands_orig=1,
                band_orig=band_orig,
                width_orig=width_orig,
                length_orig=length_orig,
                depth=depth_orig,
                dtype=dtype,

                plant_transform_obj=plant_transform_obj,
                file_format='BIN',
                scheme=scheme,
                **image_obj_kwargs)
            if verbose and not only_header:
                print('opening: %s (binary mmap%s)'
                      % (filename, band_suffix))
            if verbose:
                if plant.isvalid(dtype_size):
                    print('    data type size: %d' % (dtype_size))
                if plant.isvalid(file_size):
                    print('    file size: %d' % (file_size))
                if plant.isvalid(n_elements):
                    print('    n_elements: %d' % (n_elements))
                print('    width: %d' % (width_orig))
                if plant.isvalid(length_orig):
                    print('    length: %d' % (length_orig))
        elif filename.endswith('dat'):
            image_obj = read_NOHRSC(filename,
                                    verbose=verbose)
        elif plant.isfile(os.path.join(os.path.dirname(filename),
                                       'config.txt')):
            if verbose and not only_header:
                print('opening: %s (config.txt mmap%s)'
                      % (filename, band_suffix))
            image_obj = get_info_from_config_txt(
                filename,
                flag_exit_if_error=flag_exit_if_error,
                flag_no_messages=flag_no_messages,
                verbose=verbose)
        elif input_format:
            if verbose and not only_header:
                print('opening: %s (binary%s)'
                      % (filename, band_suffix))
            kwargs['only_header'] = True
            kwargs['plant_transform_obj'] = None
            kwargs['input_format'] = None
            image_obj = read_image(filename, **kwargs)
            kwargs['only_header'] = only_header
            kwargs['input_format'] = input_format
            kwargs['plant_transform_obj'] = plant_transform_obj

        if image_obj is None:
            plant.handle_exception('file could not be opened %s'
                                   % filename,
                                   flag_exit_if_error,
                                   flag_no_messages=flag_no_messages,
                                   verbose=verbose)
            return

        kwargs['image_obj'] = image_obj

        image_obj = _read_image_bin(*args, **kwargs)

        image_obj = plant.PlantImage(input_data=image_obj,
                                     filename=filename,
                                     filename_orig=filename_orig,

                                     plant_transform_obj=plant_transform_obj,
                                     file_format='BIN',
                                     scheme=scheme,
                                     **image_obj_kwargs)
        if not only_header:
            return image_obj

    elif (input_format == 'KML' or
          filename.endswith('.kml')):
        image_obj = read_image_kml(filename,
                                   verbose=verbose,
                                   flag_no_messages=flag_no_messages,
                                   flag_exit_if_error=flag_exit_if_error,
                                   only_header=only_header,
                                   band=band)
        if image_obj is None:
            return
        width_orig = image_obj.width
        length_orig = image_obj.length
        depth_orig = image_obj.depth

    elif (filename.endswith('.shp') or
          input_format == 'shapefile'):
        FLAG_OGR_READ_ONLY = 0

        data_source = ogr.Open(filename, FLAG_OGR_READ_ONLY)
        if data_source is None:
            plant.handle_exception('file could not be opened %s'
                                   % filename,
                                   flag_exit_if_error,
                                   flag_no_messages=flag_no_messages,
                                   verbose=verbose)
            return

        kwargs = {}
        kwargs['filename'] = filename
        kwargs['filename_orig'] = filename_orig
        kwargs['plant_transform_obj'] = plant_transform_obj
        kwargs['scheme'] = scheme
        kwargs['band'] = band

        kwargs['verbose'] = verbose
        kwargs['only_header'] = only_header
        kwargs['flag_no_messages'] = flag_no_messages
        kwargs['flag_exit_if_error'] = flag_exit_if_error

        if length_orig is not None and length_orig == 0:
            raise

        if geotransform is None:
            image_obj = _read_shapefile_as_vector(data_source,
                                                  **kwargs)

        if image_obj is None or image_obj.image is None:
            image_obj = _read_shapefile_as_image(data_source,
                                                 geotransform=geotransform,
                                                 projection=projection,
                                                 width=width_orig,
                                                 length=length_orig,
                                                 out_null=out_null,
                                                 **kwargs)

    prepare_image(image_obj,
                  filename,
                  flag_exit_if_error=flag_exit_if_error,
                  plant_transform_obj=plant_transform_obj,
                  in_null=in_null,
                  out_null=out_null,
                  input_format=input_format,
                  verbose=verbose,
                  only_header=only_header,
                  flag_no_messages=flag_no_messages,
                  force=force)
    return image_obj


def _read_shapefile_as_image(data_source,
                             filename,
                             filename_orig,
                             plant_transform_obj,
                             scheme,

                             geotransform=None,
                             projection=None,
                             out_null=None,
                             length=None,
                             width=None,
                             band=None,
                             only_header=True,
                             flag_no_messages=False,
                             flag_exit_if_error=False,
                             verbose=True,
                             image_obj_kwargs={}):

    SHAPEFILE_DEFAULT_WIDTH_LENGTH = 4096
    if geotransform:

        step_x = geotransform[1]
        step_y = geotransform[5]
        if len(geotransform) > 6:

            geotransform = geotransform[:6]
        if width is None or length is None:
            x_min, x_max, y_min, y_max = _shapefile_get_extent(
                data_source)
    else:
        x_min, x_max, y_min, y_max = _shapefile_get_extent(
            data_source)
        if width:
            step_x = ((x_max - x_min) / width)
        else:
            step_x = ((x_max - x_min) /
                      SHAPEFILE_DEFAULT_WIDTH_LENGTH)
        if length:
            step_y = ((y_max - y_min) / length)
        else:
            step_y = ((y_max - y_min) /
                      SHAPEFILE_DEFAULT_WIDTH_LENGTH)
        if length is None and width is None:
            step_x = min([step_x, step_y])
            step_y = step_x
    if not width:
        width = int((x_max - x_min) / step_x)
    else:
        x_min = geotransform[0]
        x_max = x_min + width * step_x
    if not length:
        length = int((y_max - y_min) / step_y)
    else:
        y_max = geotransform[3]
        y_min = y_max - length * step_y

    image_list = []
    target_ds = \
        gdal.GetDriverByName('MEM').Create('', width, length,
                                           gdal.GDT_Byte)
    geotransform = (x_min, step_x, 0, y_max, 0, -step_y)
    if projection is not None:
        target_ds.SetProjection(projection)
    target_ds.SetGeoTransform(geotransform)
    n_layers = data_source.GetLayerCount()

    if band is None:
        band_orig = None

    elif isinstance(band, Sequence) or isinstance(band, np.ndarray):
        band_orig = band

    else:
        band_orig = [band]

    for b in range(n_layers):
        if band_orig is not None and b not in band_orig:
            continue

        layer = data_source.GetLayer(b)
        if projection is None:
            projection = str(layer.GetSpatialRef())
        band = target_ds.GetRasterBand(1)
        if out_null is not None:
            band.SetNoDataValue(out_null)
        else:
            band.SetNoDataValue(0)
        gdal.RasterizeLayer(target_ds, [1], layer, burn_values=[1])
        image = band.ReadAsArray()
        image_list.append(image)
        continue

    image_obj = plant.PlantImage(
        input_data=image_list,
        band_orig=band_orig,
        geotransform=geotransform,
        filename=filename,
        filename_orig=filename_orig,

        plant_transform_obj=plant_transform_obj,
        projection=projection,
        file_format='SHP',
        scheme=scheme,
        **image_obj_kwargs)
    return image_obj


def _shapefile_get_extent(data_source):
    n_layers = data_source.GetLayerCount()
    for b in range(n_layers):
        layer = data_source.GetLayer(b)
        if b == 0:
            x_min, x_max, y_min, y_max = layer.GetExtent()
        else:
            x_min_temp, x_max_temp, y_min_temp, y_max_temp = \
                layer.GetExtent()
            x_min = min([x_min, x_min_temp])
            y_min = min([y_min, y_min_temp])
            x_max = max([x_max, x_max_temp])
            y_max = max([y_max, y_max_temp])
    return x_min, x_max, y_min, y_max


def _read_shapefile_as_vector(data_source,
                              filename,
                              filename_orig,
                              scheme,
                              plant_transform_obj=None,
                              band=None,
                              only_header=True,
                              flag_no_messages=False,
                              flag_exit_if_error=False,
                              verbose=True,
                              image_obj_kwargs={}):
    image_x_list = []
    image_y_list = []
    image_z_dict = {}
    max_len = None
    image = None
    image_length_list = []
    for layer in data_source:
        field_list = []
        layer_definition = layer.GetLayerDefn()
        for i in range(layer_definition.GetFieldCount()):
            fieldName = \
                layer_definition.GetFieldDefn(i).GetName()
            field_list.append(fieldName)
        for i, feature in enumerate(layer):

            geom = feature.GetGeometryRef()
            points = geom.GetPoints()
            if points is None:
                continue

            image = np.asarray(points)
            if image.size == 0:
                continue
            if not only_header:
                image_x = image[:, 0]
                image_y = image[:, 1]
                image_x_list.append(image_x)
                image_y_list.append(image_y)
            else:
                image_length_list.append(image.shape[0])
            if max_len is None or image.shape[0] > max_len:
                max_len = image.shape[0]
            remove_list = []
            for j, field in enumerate(field_list):
                image_z = feature.GetField(field)
                if isinstance(image_z, str):
                    remove_list.append(field)
                    continue
                if only_header:
                    continue
                if field not in image_z_dict.keys():
                    image_z_dict[field] = []
                if hasattr(image_z, '__getitem__'):
                    image_z_dict[field].append(image_z)
                else:
                    image_z_dict[field].append([image_z])
            for field in remove_list:
                field_list.remove(field)
    zbands = len(field_list)
    if band is None:
        band_orig = None
        nbands = 2 + zbands
    elif isinstance(band, Sequence) or isinstance(band, np.ndarray):
        band_orig = band
        nbands = len(band)
    else:
        band_orig = [band]
        nbands = 1

    depth_orig = 1
    width = max_len

    if only_header:
        length_orig = len(image_length_list)
        if length_orig == 0:
            return
        image = None
    else:
        length_orig = len(image_x_list)
        if length_orig == 0:
            return
        image_x = np.full((length_orig, width), np.nan)
        image_y = np.full((length_orig, width), np.nan)
        for i in range(length_orig):
            line_len = len(image_x_list[i])
            image_x[i, :line_len] = image_x_list[i]
            image_y[i, :line_len] = image_y_list[i]

    if band is None:
        if not only_header:
            image = [image_x, image_y]
        image_name_list = [plant.X_BAND_NAME,
                           plant.Y_BAND_NAME]
    else:
        band_indexes = plant.get_int_list(band)
        if not only_header:
            image = []
        image_name_list = []
        for b in band_indexes:
            if b == 0:
                if not only_header:
                    image.append(image_x)
                image_name_list.append(plant.X_BAND_NAME)
            elif b == 1:
                if not only_header:
                    image.append(image_y)
                image_name_list.append(plant.Y_BAND_NAME)

    for j, field in enumerate(field_list):
        if band_orig is not None and j + 2 not in band_orig:
            continue
        image_name_list.append(field)
        if only_header:
            continue
        image_z_list = image_z_dict[field]

        image_z = np.full((length_orig, width), np.nan)

        for i in range(length_orig):
            line_len = len(image_z_list[i])
            image_z[i, :line_len] = image_z_list[i]
        image.append(image_z)

    if not only_header and (image is None or len(image) == 0):
        return

    image_obj = plant.PlantImage(
        input_data=image,

        band_orig=band_orig,
        filename=filename,
        filename_orig=filename_orig,

        width=width,
        length=length_orig,
        depth=depth_orig,
        nbands=nbands,
        plant_transform_obj=plant_transform_obj,
        file_format='SHP',
        scheme=scheme,
        **image_obj_kwargs)
    for b, name in enumerate(image_name_list):
        image_obj.get_band(band=b).name = name
    return image_obj


def _read_image_sentinel(image_obj, filename, header_file, input_key,
                         verbose):
    if verbose:
        message = f'opening: {image_obj} (SENTINEL)'
        if header_file:
            message += f' header: {header_file}'
        if input_key:
            message += f' radiometry: {input_key}'
        print(message)
    if header_file is None:
        header_basename = plant.replace_extension(
            os.path.basename(filename), '.xml')
        header_basename = 'calibration-' + header_basename
        header_file_test = os.path.join(
            plant.dirname(filename),
            '../annotation/calibration/',
            header_basename)
        if plant.isfile(header_file_test):
            header_file = header_file_test
            if verbose:
                print('INFO SENTINEL calibration header'
                      f' found at {header_file}')

    if not header_file:
        return image_obj

    flag_beta = input_key and 'BETA' in input_key.upper()
    flag_sigma = input_key and 'SIGMA' in input_key.upper()
    flag_gamma = (input_key and ('GAMMA' in input_key.upper() or
                                 'GAMA' in input_key.upper()))
    if flag_beta:
        calibration_name = 'beta-naught'
    elif flag_sigma:
        calibration_name = 'sigma-naught'
    elif flag_gamma:
        calibration_name = 'gamma-naught'

    if image_obj is None:
        return
    print(f'calibration header file: {header_file}')
    rcs_lut = plant.sentinel_get_calibration_vectors(
        header_file,
        flag_beta=flag_beta,
        flag_sigma=flag_sigma,
        flag_gamma=flag_gamma)

    if not rcs_lut:
        return
    print('applying radiometric correction:'
          f' {calibration_name}..')
    rcs_lut_array = None
    for b in range(image_obj.nbands):

        image = np.asarray(image_obj.get_image(band=b),
                           np.float32)

        if rcs_lut_array is None:
            y_ind = np.arange(image.shape[0])
            x_ind = np.arange(image.shape[1])
            rcs_lut_array = rcs_lut(y_ind, x_ind)

        image = np.asarray(image, dtype=float) / rcs_lut_array
        image *= image
        ind = np.where(image > 10)
        image[ind] = np.nan
        image_obj.set_image(image, band=b,
                            realize_changes=False)

    return image_obj


def read_slice(filename_orig, default_stop=None):
    try:
        input_slice = plant.str_to_slice(filename_orig)
    except AttributeError:
        input_slice = None

    if (input_slice is not None and
            input_slice.start is None and
            input_slice.stop is not None and
            input_slice.step is None):
        input_slice = None
    if input_slice is not None:
        ret = plant.expand_slice(input_slice,
                                 default_stop=default_stop)
        if ret is not None:
            return [list(ret)]

        return

    rows = plant.read_image(
        filename_orig,
        plant_transform_obj=None,
        verbose=False).image.tolist()
    return rows


def read_image_array(filename_orig,
                     verbose=True,
                     plant_transform_obj=None,

                     in_null=None,
                     out_null=None,
                     force=None,
                     only_header=True,
                     flag_no_messages=False,
                     flag_exit_if_error=False,
                     image_obj_kwargs={}):
    if verbose:
        print('input array: %s' % filename_orig)
    if (isinstance(filename_orig, numbers.Number) or
            isinstance(filename_orig, np.ndarray) or
            isinstance(filename_orig, list) or
            isinstance(filename_orig, tuple)):
        image = np.asarray(filename_orig)
        filename_orig = str(filename_orig)
    else:

        line_splitted = filename_orig.split(plant.LINE_SEPARATOR)
        line_list = []
        for line_str in line_splitted:
            line_multiplier, line_str = _get_multiplier_array(
                line_str, plant.LINE_MULTIPLIER,
                plant.LINE_SEPARATOR)
            element_list = []
            line_str_splitted = line_str.split(plant.ELEMENT_SEPARATOR)
            for element_str in line_str_splitted:
                element_multiplier, element_str = _get_multiplier_array(
                    element_str, plant.ELEMENT_MULTIPLIER,
                    plant.ELEMENT_SEPARATOR)
                if plant.IMAGE_NAME_SEPARATOR in element_str:
                    element_str_splitted = element_str.split(
                        plant.IMAGE_NAME_SEPARATOR)
                    parameters = [plant.str_to_number(x)
                                  for x in element_str_splitted]
                    if parameters[0] == parameters[1]:
                        parameters_int = [int(x)
                                          for x in element_str_splitted[2:]]
                        element = np.zeros((tuple(parameters_int)),
                                           dtype=np.float32)
                        element += parameters[0]
                    else:
                        element = np.arange(*parameters)
                else:
                    element = np.asarray(np.fromstring(
                        element_str,
                        sep=plant.ELEMENT_SEPARATOR))
                    element = element.reshape(element.size)
                this_element = element.tolist() * element_multiplier
                if not isinstance(this_element, Sequence):
                    this_element = [this_element]
                element_list.extend(this_element)

            line_list.extend([element_list] * line_multiplier)

        image = plant.shape_image(line_list)

    depth_orig, length_orig, width_orig = plant.get_image_dimensions(image)
    dtype = image.dtype
    image_obj = plant.PlantImage(input_data=image,
                                 width=width_orig,
                                 length=length_orig,
                                 depth=depth_orig,
                                 filename=filename_orig,
                                 file_format='ARRAY',

                                 plant_transform_obj=plant_transform_obj,
                                 dtype=dtype,
                                 **image_obj_kwargs)
    if plant_transform_obj is not None:

        plant.apply_crop(image_obj,
                         plant_transform_obj=plant_transform_obj,
                         verbose=False)

    plant.apply_null(image_obj,
                     in_null=in_null,
                     out_null=out_null)

    plant.apply_mask(image_obj,
                     plant_transform_obj=plant_transform_obj,
                     verbose=verbose,
                     force=force)
    return image_obj


def _get_multiplier_array(input_str, multiplier_symbol,
                          multiplier_separator):
    if multiplier_symbol not in input_str:
        return 1, input_str
    line_mult_splitted = input_str.split(multiplier_symbol)
    if len(line_mult_splitted) > 2:
        print(f'ERROR multiplier {multiplier_symbol}'
              ' can only be used once within between each'
              f' separator {multiplier_separator}')
        return
    if plant.isnumeric(line_mult_splitted[0]):
        multiplier = int(line_mult_splitted[0])
        input_str = line_mult_splitted[1]
    elif plant.isnumeric(line_mult_splitted[1]):
        input_str = line_mult_splitted[0]
        multiplier = int(line_mult_splitted[1])
    else:
        print('ERROR line multiplier can only be used with'
              ' constants')
        return
    return multiplier, input_str


def prepare_image(image_obj,
                  filename=None,
                  flag_exit_if_error=True,
                  plant_transform_obj=None,
                  in_null=None,
                  out_null=None,
                  input_format=None,
                  verbose=True,
                  only_header=True,
                  flag_no_crop=False,
                  flag_no_messages=False,
                  force=None):
    if image_obj is not None and filename is None:
        filename = image_obj.filename
    if image_obj is not None and plant_transform_obj is None:
        plant_transform_obj = image_obj.plant_transform_obj
    if image_obj is None and input_format:
        plant.handle_exception('file could not be opened: %s with '
                               'format %s'
                               % (filename, input_format),
                               flag_exit_if_error,
                               flag_no_messages=flag_no_messages,
                               verbose=verbose)
        return
    elif image_obj is None:
        plant.handle_exception('file could not be opened: %s'
                               % (filename),
                               flag_exit_if_error,
                               flag_no_messages=flag_no_messages,
                               verbose=verbose)
        return

    if not flag_no_crop:
        plant.apply_crop(image_obj,
                         plant_transform_obj=plant_transform_obj,
                         verbose=verbose)

    if image_obj is None:
        plant.handle_exception('no valid data after cropping '
                               'file: %s' % filename,
                               flag_exit_if_error,
                               flag_no_messages=flag_no_messages,
                               verbose=verbose)

    plant.apply_null(image_obj, in_null=in_null, out_null=out_null)

    plant.apply_mask(image_obj,
                     plant_transform_obj=plant_transform_obj,
                     verbose=verbose,
                     force=force)
    return image_obj


def utf_8_encoder(unicode_csv_data):
    for line in unicode_csv_data:
        yield line.decode('utf-8')


def read_image_csv(filename,
                   filename_orig,
                   plant_transform_obj,

                   band=None,
                   key=None,
                   delimiter=None,
                   in_null=None,
                   out_null=None,
                   input_format='CSV',
                   only_header=False,
                   force=None,
                   flag_exit_if_error=True,
                   flag_no_messages=False,
                   verbose=True,
                   image_obj_kwargs={}):
    import pandas as pd

    kwargs = locals()
    if verbose and not only_header and verbose:
        print('opening: ' + filename + ' (CSV pandas)')

    offset_x = None
    offset_y = None
    width = None
    length = None
    if plant_transform_obj is not None:
        offset_x, offset_y, width, length = plant_transform_obj.crop_window
        if offset_x is None and offset_y is not None:
            offset_x = offset_y
        if width is None and length is not None:
            width = length
    if key is None:
        key_list = None
    else:
        key_list = key.split(',')
        if all([plant.isnumeric(k) for k in key_list]):
            try:
                key_list = [int(k) for k in key_list]
            except ValueError:
                pass
    if delimiter is None:
        delimiter = ','
    skiprows = list(range(offset_x)) if offset_x else None
    header = 'infer' if key_list is None else 0

    try:
        df = pd.read_csv(filename,
                         sep=delimiter,
                         usecols=key_list,
                         skiprows=skiprows,
                         header=header,

                         nrows=width)
    except BaseException:
        plant.handle_exception('file could not be opened: %s'
                               ' (pandas csv)'
                               % filename,
                               flag_exit_if_error,
                               flag_no_messages=flag_no_messages,
                               verbose=verbose)
        return
    width, nbands = df.shape
    length_orig = 1
    depth_orig = 1

    bands_count = 0
    for b, column in enumerate(df.columns):
        if band is not None and b != band:
            continue
        if only_header:
            image = None
        elif isinstance(df[column][0], str):
            from dateutil import parser
            flag_date = True
            try:
                date_array = [parser.parse(d) for d in df[column]]
            except ValueError:
                flag_date = False
            if flag_date:
                image = np.asarray(date_array).reshape(length_orig, width)
            else:
                image = np.asarray(df[column],
                                   dtype=str).reshape(length_orig, width)
        else:
            image = np.asarray(df[column]).reshape(length_orig, width)

        if bands_count == 0:
            image_obj = plant.PlantImage(
                filename=filename_orig,
                image=image,
                width=width,
                length=length_orig,
                depth=depth_orig,
                input_key=key,

                nbands=nbands,

                plant_transform_obj=plant_transform_obj,
                file_format=input_format,
                geotransform=None,
                **image_obj_kwargs)
        else:
            image_obj.set_image(image, band=bands_count,
                                realize_changes=False)

        image_obj.get_band(band=bands_count).set_name(column)
        bands_count += 1

    prepare_image(image_obj,
                  filename,
                  flag_exit_if_error=flag_exit_if_error,
                  plant_transform_obj=plant_transform_obj,
                  in_null=in_null,
                  out_null=out_null,
                  input_format=input_format,
                  flag_no_crop=True,
                  verbose=verbose,
                  only_header=only_header,
                  flag_no_messages=flag_no_messages,
                  force=force)

    return image_obj


def read_image_csv_python(filename,
                          filename_orig,
                          plant_transform_obj,

                          band=None,
                          key=None,
                          delimiter=None,
                          input_format='CSV',
                          in_null=None,
                          out_null=None,
                          only_header=False,
                          force=None,
                          flag_exit_if_error=True,
                          flag_no_messages=False,
                          verbose=True,
                          image_obj_kwargs={}):
    kwargs = locals()

    if delimiter is None:
        delimiter_list = [',', '\t', ' ']
        for delimiter in delimiter_list:
            kwargs['delimiter'] = delimiter
            ret = read_image_csv(**kwargs)
            if ret is not None:
                return ret
        plant.handle_exception('file could not be opened: %s (csv)'
                               % filename,
                               flag_exit_if_error,
                               flag_no_messages=flag_no_messages,
                               verbose=verbose)
        return

    import csv
    if verbose and not only_header:
        print('opening: ' + filename + ' (CSV)')

    offset_x = None
    offset_y = None
    width = None
    length = None
    if plant_transform_obj is not None:
        offset_x, offset_y, width, length = plant_transform_obj.crop_window
        if offset_y is None and offset_x is not None:
            offset_y = offset_x
        if width is None and length is not None:
            width = length

    if key is None:
        key_list = []
    else:
        key_list = key.split(',')

    with open(filename, 'rb') as csvfile:

        dtype = np.float32
        image_list = []
        csvfile.seek(0)
        if offset_x is None:
            offset_x = 0
        csv_lines = (row for row in utf_8_encoder(csvfile)
                     if not row.startswith('#'))
        csv_reader = csv.DictReader(csv_lines,
                                    delimiter=delimiter,
                                    quotechar='|')
        if csv_reader is None:
            plant.handle_exception('file could not be opened: %s (csv)'
                                   % filename,
                                   flag_exit_if_error,
                                   flag_no_messages=flag_no_messages,
                                   verbose=verbose)
            return
        flag_first_value_as_key = False
        for i, line in enumerate(csv_reader):

            pos = i + int(flag_first_value_as_key)
            if i != 0 and pos < offset_x:
                continue
            if (width is not None and pos > width + offset_x):
                break
            if i == 0:
                key_list_file = list(line.keys())
                flag_user_key_list_is_numeric = all([plant.isnumeric(k)
                                                     for k in key_list])
                new_key_list = []

                if len(key_list) == 0:
                    flag_first_value_as_key = all([plant.isnumeric(x)
                                                   for x in key_list_file])
                    new_key_list = key_list_file
                else:
                    new_key_list = [x for x in key_list_file
                                    for k in key_list
                                    if k.strip() == x.strip()]

                    if len(new_key_list) == 0:
                        if flag_user_key_list_is_numeric:
                            try:
                                new_key_list = [key_list_file[int(k)]
                                                for k in key_list]
                                flag_first_value_as_key = True
                            except IndexError:
                                pass
                if len(new_key_list) == 0:
                    print('ERROR key(s) %s not found in %s'
                          % (str(key_list), filename))
                    return
                key_list = new_key_list
                if (flag_first_value_as_key and
                        (offset_x is None or offset_x == 0)):
                    image_list = [[] for j in key_list]
                    for j, current_key in enumerate(key_list):
                        image_list[j].append(np.float64(current_key))
            pos = i + int(flag_first_value_as_key)

            if pos < offset_x:
                continue
            if len(image_list) == 0:
                image_list = [[] for j in key_list]
            for j, current_key in enumerate(key_list):
                value = line[current_key]
                if plant.isnumeric(value):
                    value = np.float64(value)
                elif not value:
                    value = np.float64(np.nan)
                else:
                    value = str(value)

                    dtype = None

                image_list[j].append(value)

        width = len(image_list[0])

        length_orig = 1
        depth_orig = 1
        if only_header:
            image_obj = plant.PlantImage(
                filename=filename_orig,
                image=None,

                width=width,
                length=length_orig,
                depth=depth_orig,
                nbands=len(image_list),
                input_key=key,

                plant_transform_obj=plant_transform_obj,
                file_format=input_format,
                geotransform=None,
                **image_obj_kwargs)
            return image_obj
        for b in range(len(image_list)):
            image = np.asarray(image_list[b],
                               dtype=dtype).reshape(length_orig, width)
            if b == 0:
                image_obj = plant.PlantImage(
                    filename=filename_orig,
                    image=image,

                    width=width,
                    length=length_orig,
                    depth=depth_orig,
                    input_key=key,

                    plant_transform_obj=plant_transform_obj,
                    file_format=input_format,
                    geotransform=None,
                    **image_obj_kwargs)
            else:
                image_obj.set_image(image, band=b)

        prepare_image(image_obj,
                      filename,
                      flag_exit_if_error=flag_exit_if_error,
                      plant_transform_obj=plant_transform_obj,
                      in_null=in_null,
                      out_null=out_null,
                      flag_no_crop=True,
                      input_format=input_format,
                      verbose=verbose,
                      only_header=only_header,
                      flag_no_messages=flag_no_messages,
                      force=force)

        return image_obj
    return


def read_csv_data_lat_lon(filename,
                          verbose=True,
                          lat_column=None,
                          lon_column=None,
                          data_column_list=None,
                          lat_text='lat',
                          lon_text='lon',
                          data_text_list=None,
                          separator=',',
                          n_lines=None,

                          y0=None,
                          yf=None,
                          x0=None,
                          xf=None,
                          first_line=None,
                          first_header_column=0,
                          flag_no_messages=False,
                          flag_exit_if_error=True,
                          trim_whitespaces=False):

    if flag_no_messages:
        verbose = False

    if not plant.isfile(filename):
        plant.handle_exception(f'file {filename} not found or invalid',
                               flag_exit_if_error,
                               flag_no_messages=flag_no_messages,
                               verbose=verbose)
        return

    if ((data_text_list is None or len(data_text_list) == 0) and
            (data_column_list is None or len(data_column_list) == 0)):
        plant.handle_exception('please specify CSV data text or'
                               ' CSV data column number',
                               flag_exit_if_error,
                               flag_no_messages=flag_no_messages,
                               verbose=verbose)

        return

    lat_vect = []
    lon_vect = []

    if data_column_list is None and data_text_list is not None:
        data_column_list = [None for i in range(len(data_text_list))]

    data_vect = [[] for i in range(len(data_column_list))]

    if verbose:
        print('opening: %s (CSV)' % (filename))
        print('separator: "%s"' % (separator))

    with open(filename, 'r', encoding="ISO-8859-1") as f:
        lines = f.readlines()
        if n_lines is not None:
            lines = lines[0:n_lines + 1]
        else:
            lines = lines[0:]

        if first_line is not None:
            lines = lines[first_line:]

        line = lines[0].replace('\n', '')
        if trim_whitespaces:
            line = ' '.join(line.split())

        line_splitted = line.split(separator)
        line_splitted = line_splitted[first_header_column:]

        if (lat_column is None or
                lon_column is None or
                any([plant.isnan(x) for x in data_column_list])):
            for j, element in enumerate(line_splitted):
                element = element.strip()
                if (lat_text == element):
                    lat_column = j
                    if verbose:
                        print('latitude in column: ' + str(j) +
                              '   label: ' + element)
                    continue
                elif (lon_text == element):
                    if verbose:
                        print('longitude in column: ' + str(j) +
                              '   label: ' + element)
                    lon_column = j
                    continue
                elif (element in data_text_list):
                    index = data_text_list.index(element)
                    if verbose:
                        print('data %s in column: %d'
                              % (element, j))
                        print('   label: ' + element)
                    data_column_list[index] = j
                    continue
        for i, data_column in enumerate(data_column_list):
            if data_column is None:
                if data_text_list[i]:
                    print('WARNING data column not found: %s in %s'
                          % (data_text_list[i],
                             str(line_splitted)))
                else:
                    print('WARNING data column not found')
                print('CSV separator: "%s"' % (separator))
                return

        lines = lines[1:]
        for i, line in enumerate(lines):
            line = line.replace('\n', '')
            if trim_whitespaces:
                line = ' '.join(line.split())
            line_splitted = line.split(separator)
            data = []
            for j in range(2 + len(data_column_list)):
                current_text = None
                if j == 0 and lat_column is not None:
                    current_column = lat_column
                    if lat_text is not None:
                        current_text = lat_text
                elif j == 1 and lon_column is not None:
                    current_column = lon_column
                    if lon_text is not None:
                        current_text = lon_text
                elif j < 2:
                    continue
                else:
                    current_column = data_column_list[j - 2]
                    if data_text_list is not None:
                        current_text = data_text_list[j - 2]

                output_value = None
                try:
                    output_value = float(line_splitted[current_column])
                except KeyboardInterrupt:
                    raise plant.PlantExceptionKeyboardInterrupt
                except BaseException:
                    if current_text is not None:
                        print(f'WARNING error reading {current_text} '
                              f' (column: {current_column}) from CSV '
                              f'file splitted line {i}: {line_splitted}')
                    else:
                        print(f'WARNING error reading'
                              f' column: {current_column} from CSV '
                              f'file splitted line {i}: {line_splitted}')
                    continue

                if j == 0:
                    lat = output_value
                    if plant.isvalid(y0) and lat > y0:
                        break
                    if plant.isvalid(yf) and lat < yf:
                        break
                elif j == 1:
                    lon = output_value
                    if lon > 180:
                        lon -= 360

                    if plant.isvalid(x0) and lon < x0:
                        break
                    if plant.isvalid(xf) and lon > xf:
                        break
                else:
                    data.append(output_value)

            if plant.isvalid(y0) and lat > y0:
                continue
            if plant.isvalid(yf) and lat < yf:
                continue
            if plant.isvalid(x0) and lon < x0:
                continue
            if plant.isvalid(xf) and lon > xf:
                continue

            if lat_column is not None:
                lat_vect.append(lat)
            if lon_column is not None:
                lon_vect.append(lon)

            for j in range(len(data_column_list)):
                (data_vect[j]).append(data[j])

    return data_vect, lat_vect, lon_vect


def save_image_figure(*args, **kwargs):
    force = kwargs.get('force', None)
    output_format = kwargs.pop('output_format', 'PNG')
    output_file = args[-1]
    args = args[0:-1]
    n_images = len(args)
    update_image = plant.overwrite_file_check(output_file,
                                              force=force)
    verbose = kwargs.get('verbose', True)
    if not update_image:
        print(f'WARNING file not updated: {output_file}')
        return
    if n_images > 4:
        print('WARNING output has more than four '
              'channels. Only the first four channels '
              'will be saved')

    image = plant.image_to_figure(*args, **kwargs)

    mpimg.imsave(output_file,
                 image,
                 format=output_format.lower())
    plant.plant_config.output_files.append(output_file)
    if verbose and plant.isfile(output_file):
        print('## file saved: %s (%s)'
              % (output_file,
                 output_format))


def save_image_kmz(*args, **kwargs):

    import matplotlib as mpl
    mpl.use('Agg')

    ret = None
    verbose = kwargs.get('verbose', True)
    output_format = kwargs.get('output_format', 'KML')
    if verbose and output_format:
        print(f'## saving image with format: {output_format}')

    with plant.PlantIndent():
        ret = _save_image_kmz(*args, **kwargs)
    return ret


def _save_image_kmz(*args, **kwargs):

    image_obj = kwargs.get('image_obj', None)
    image = kwargs.get('image', None)
    output_file = kwargs.get('output_file', None)
    nbands = kwargs.get('nbands', None)
    cmap = kwargs.get('cmap', plant.DEFAULT_CMAP)
    cmap_crop_min = kwargs.get('cmap_crop_min', None)
    cmap_crop_max = kwargs.get('cmap_crop_max', None)

    cmap_min = kwargs.pop('cmap_min', None)
    cmap_max = kwargs.pop('cmap_max', None)
    out_null = kwargs.pop('out_null', None)

    background_color = kwargs.pop('background_color', None)
    flag_cmap_already_expanded = kwargs.get('flag_cmap_already_expanded',
                                            False)

    flag_add_kmz_cbar_offset = kwargs.get('flag_add_kmz_cbar_offset', None)
    kmz_cbar_offset_color = kwargs.get('kmz_cbar_offset_color', None)
    kmz_cbar_offset_length = kwargs.get('kmz_cbar_offset_length', None)
    kmz_cbar_offset_width = kwargs.get('kmz_cbar_offset_width', None)
    kmz_cbar_offset_alpha = kwargs.get('kmz_cbar_offset_alpha', None)

    ctable = kwargs.get('ctable', None)
    flag_scale_data = kwargs.get('flag_scale_data', None)

    output_format = kwargs.get('output_format', 'KML')
    extension = '.' + output_format.lower()
    percentile = kwargs.get('percentile', None)

    verbose = kwargs.get('verbose', True)
    force = kwargs.get('force', None)
    data_output_file = kwargs.get('data_output_file',
                                  output_file.replace(extension, ''))
    geotransform = kwargs.get('geotransform')

    if geotransform is None:
        print('ERROR geo-reference not found')
        return
    data_output_format = 'PNG'
    file_list = []

    if output_format.upper() == 'KMZ' and output_file.endswith('.kmz'):
        kml_file = output_file.replace('.kmz', '_' + str(time.time()) + '.kml')
    elif output_format.upper() == 'KMZ':
        kml_file = output_file + '_' + str(time.time()) + '.kml'
    elif output_file.endswith('.kml'):
        kml_file = output_file
    else:
        kml_file = output_file + '.kml'

    with open(kml_file, 'w') as fp:

        if image_obj is not None:
            cbar_file = 'temp_cmap_' + str(time.time()) + '.png'

            ret = plant.image_obj_ctable_expand(image_obj,
                                                ctable=ctable)
            image_obj = ret['image_obj']

            ctable = ret['ctable']
            if ctable is not None:
                cmap = None
            length = image_obj.length
            width = image_obj.width
            projection = image_obj.projection
            nbands = image_obj.nbands
        else:

            depth, length, width = plant.get_image_dimensions(image)
            projection = None
            nbands = 1

        if nbands == 1 and cmap.upper() == 'RGB':

            cmap = 'gray'

        output_projection = plant.PROJECTION_REF

        flag_fix_projection = plant.compare_projections(
            projection, output_projection) is False

        if flag_fix_projection:
            plant_geogrid_obj = plant.PlantGeogrid(
                projection=projection, geotransform=geotransform,
                length=length, width=width)

            plant_geogrid_obj.projection = plant.PROJECTION_REF

            geotransform_latlon = plant_geogrid_obj.geotransform
            x0 = geotransform_latlon[0]
            step_x = geotransform_latlon[1]

            xf = x0 + width * step_x
            if xf > 180:

                output_projection = plant.PROJECTION_ANTIMERIDIAN_REF

                flag_fix_projection = plant.compare_projections(
                    projection, output_projection) is False

        outputSRS = plant.get_srs_from_projection(output_projection)

        if flag_fix_projection:
            temp_file_in = plant.get_temporary_file(
                suffix='_in', ext=plant.DEFAULT_GDAL_EXTENSION, append=True)
            temp_file_out = plant.get_temporary_file(
                suffix='_out', ext=plant.DEFAULT_GDAL_EXTENSION)

            plant.save_image(image_obj, temp_file_in,
                             flag_cmap_already_expanded=False,
                             ctable=None,

                             in_null=out_null,
                             out_null=out_null,

                             output_format=plant.DEFAULT_GDAL_FORMAT,
                             force=True)

            if ctable is not None or 'int' in image_obj.dtype.lower():
                resample_algorithm = 'near'
            else:

                resample_algorithm = 'average'
            plant.Warp(temp_file_out,
                       temp_file_in,

                       resampleAlg=resample_algorithm,
                       dstSRS=outputSRS,
                       errorThreshold=0,
                       flag_return=False,
                       srcNodata=out_null,
                       dstNodata=out_null,
                       format=plant.DEFAULT_GDAL_FORMAT)
            plant.append_temporary_file(temp_file_out)

            image_obj = plant.read_image(temp_file_out)
            projection = image_obj.projection
            geotransform = image_obj.geotransform
            depth, length, width = plant.get_image_dimensions(image_obj)

        plant_geogrid_obj = plant.get_coordinates(
            geotransform=geotransform,

            projection=projection,
            length=length,
            width=width)
        x0 = plant_geogrid_obj.x0
        xf = plant_geogrid_obj.xf
        y0 = plant_geogrid_obj.y0
        yf = plant_geogrid_obj.yf

        if output_format.upper() == 'KMZ':
            tile_size = plant.KMZ_TILE_SIZE
        else:
            tile_size = plant.KML_TILE_SIZE
        n_tiles_x = np.ceil(float(width) / tile_size)
        n_tiles_y = np.ceil(float(length) / tile_size)
        n_tiles = int(n_tiles_x * n_tiles_y)
        if n_tiles > 1 and verbose:
            print(f'total number of tiles: {n_tiles}')
        lat_tile_range = (tile_size * (y0 - yf) / length)
        lon_tile_range = (tile_size * (xf - x0) / width)

        mean_lon = (x0 + xf) / 2
        mean_lat = (yf + y0) / 2
        name = os.path.basename(output_file).replace(extension, '')
        fp.write('<?xml version="1.0" encoding="UTF-8"?>\n')
        fp.write('<kml xmlns="http://www.opengis.net/kml/2.2">\n')
        fp.write('  <Document>\n')

        fp.write('\t <LookAt>\n')
        fp.write(f'\t\t <longitude>{mean_lon}</longitude>\n')
        fp.write(f'\t\t <latitude>{mean_lat}</latitude>\n')
        fp.write('\t\t <altitude>100000</altitude>\n')

        fp.write('\t\t <tilt>0</tilt>\n')
        fp.write('\t\t <heading>0</heading>\n')
        fp.write('\t\t <altitudeMode>relativeToGround</altitudeMode>\n')

        fp.write('\t </LookAt>\n')

        fp.write('\t<Region>\n')
        fp.write('\t\t<LatLonAltBox>\n')
        fp.write('\t\t<north>%f</north>\n' % y0)
        fp.write('\t\t<south>%f</south>\n' % yf)
        fp.write('\t\t<east>%f</east>\n' % xf)
        fp.write('\t\t<west>%f</west>\n' % x0)
        fp.write('\t\t</LatLonAltBox>\n')

        fp.write('\t</Region>\n')

        cmap_min_band_list = []
        cmap_max_band_list = []
        cbar_file_list = []
        flag_one_cmap = (flag_cmap_already_expanded or
                         (cmap is not None and
                          flag_scale_data is False and
                          not hasattr(cmap_min, '__getitem__') and
                          not hasattr(cmap_max, '__getitem__')))

        if flag_add_kmz_cbar_offset:
            cbar_offset_background_prefix = \
                data_output_file + '_cbar_offset_background'
            if output_format.upper() != 'KMZ':
                cbar_offset_background_file = \
                    cbar_offset_background_prefix + '.png'
            else:
                cbar_offset_background_file = \
                    (cbar_offset_background_prefix + '_' + str(time.time()) +
                     '.png')
            plant.save_cbar_offset_background(
                cbar_offset_background_file,
                color=kmz_cbar_offset_color,
                length=kmz_cbar_offset_length,
                width=kmz_cbar_offset_width,
                alpha=kmz_cbar_offset_alpha,
                verbose=verbose, force=force)
            file_list.append(cbar_offset_background_file)

        if ctable is None and not flag_one_cmap:

            for k in range(min([nbands, 3])):

                cbar_prefix = data_output_file + f'_colorbar_{k}'
                if output_format.upper() != 'KMZ':
                    cbar_file = cbar_prefix + '.png'
                else:
                    cbar_file = (cbar_prefix + '_' +
                                 str(time.time()) + '.png')

                if image_obj is not None:
                    current_image = image_obj.get_image(band=k)
                if current_image is not None:
                    image = current_image
                if 'complex' in plant.get_dtype_name(
                        image.dtype).lower():
                    image = np.absolute(image)

                cmap_min_band = None
                cmap_max_band = None
                percentile_temp = percentile
                if percentile_temp is None:
                    percentile_temp = plant.DEFAULT_MIN_MAX_PERCENTILE

                while percentile_temp <= 100:
                    if (cmap_min is None or
                            cmap_max is None or
                            not hasattr(cmap_min, '__getitem__') or
                            not hasattr(cmap_max, '__getitem__')):
                        ret = plant.get_min_max_percentile(image,
                                                           percentile_temp,
                                                           cmap_min,
                                                           cmap_max,
                                                           band=k)
                        cmap_min_band, cmap_max_band = ret
                    if hasattr(cmap_min, '__getitem__'):
                        cmap_min_band = plant.get_element_from_arg(cmap_min,
                                                                   index=k)
                    if hasattr(cmap_max, '__getitem__'):
                        cmap_max_band = plant.get_element_from_arg(cmap_max,
                                                                   index=k)

                    if (cmap_min_band != cmap_max_band or
                        (cmap_min_band == cmap_max_band and
                            percentile is not None)):
                        break
                    percentile_temp += 1

                if (cmap_min_band is None or
                        cmap_max_band is None):
                    print('ERROR determining min and max for percentile:'
                          f' {percentile}')
                cmap_min_band_rounded = plant.round_to_sig_figs(cmap_min_band)
                cmap_max_band_rounded = plant.round_to_sig_figs(cmap_max_band)
                if (cmap_min_band != cmap_max_band and
                        cmap_min_band_rounded == cmap_max_band_rounded):
                    cmap_min_band_rounded = cmap_min_band
                    cmap_max_band_rounded = cmap_max_band

                if verbose:
                    print('band: %d' % k)
                    print('    color map: ', cmap)
                    if percentile is not None:
                        print('    percentile: ', percentile)
                    print('    color map min: ', cmap_min_band_rounded)
                    print('    color map max: ', cmap_max_band_rounded)
                    print('    background color: ', background_color)

                if (cmap is not None and
                    (isinstance(cmap, mcolors.LinearSegmentedColormap) or
                     cmap.upper() != 'RGB')):

                    if output_format.upper() == 'KMZ':
                        plant.append_temporary_file(cbar_file)
                    plant.save_cmap(cmap,
                                    cbar_file,
                                    cmap_min_band_rounded,
                                    cmap_max_band_rounded,
                                    verbose=verbose)

                    file_list.append(cbar_file)
                    cbar_file_list.append(cbar_file)

                if flag_scale_data is not False:

                    image = plant.scale_data(image, cmap_min_band_rounded,
                                             cmap_max_band_rounded, 1.0)

                cmap_min_band_list.append(cmap_min_band_rounded)
                cmap_max_band_list.append(cmap_max_band_rounded)
                image_obj.set_image(image, band=k)
        else:
            if output_format.upper() == 'KMZ':
                plant.append_temporary_file(cbar_file)
            if flag_one_cmap:

                if (cmap_min is None or
                        cmap_max is None):
                    if image is None and image_obj is not None:
                        image = image_obj.image
                    if percentile is None:
                        percentile = plant.DEFAULT_MIN_MAX_PERCENTILE
                    ret = plant.get_min_max_percentile(image,
                                                       percentile,
                                                       cmap_min,
                                                       cmap_max)
                    cmap_min, cmap_max = ret

                cmap_min_rounded = plant.round_to_sig_figs(cmap_min)
                cmap_max_rounded = plant.round_to_sig_figs(cmap_max)
                if (cmap_min != cmap_max and
                        cmap_min_rounded == cmap_max_rounded):
                    cmap_min_rounded = cmap_min
                    cmap_max_rounded = cmap_max

                plant.save_cmap(cmap,
                                cbar_file,
                                cmap_min_rounded,
                                cmap_max_rounded,
                                vmin=cmap_crop_min,
                                vmax=cmap_crop_max,
                                verbose=verbose)
            else:

                image_orig_obj = kwargs.get('image_obj', None)
                if image_orig_obj is not None and image_orig_obj.nbands == 1:
                    image_orig = image_orig_obj.image
                else:
                    image_orig = None

                plant.save_ctable(ctable,
                                  cbar_file,
                                  data=image_orig,
                                  background_color=background_color,
                                  verbose=verbose)
                image_orig = None

            file_list.append(cbar_file)
            cbar_file_list.append(cbar_file)

            add_legend_to_kml(fp, cbar_file)

        if (ctable is None and not flag_one_cmap and
            (not flag_cmap_already_expanded and
             cmap is not None and
             (isinstance(cmap, mcolors.LinearSegmentedColormap) or
              cmap.upper() != 'RGB')) or nbands == 1):
            valid_tiles_list = [False for t in range(n_tiles)]
            for band_count in range(min([nbands, 3])):
                name = os.path.basename(output_file).replace(
                    extension, ' (band: %d)' % (band_count + 1))
                fp.write('\t<Folder>\n')
                fp.write('\t\t<name>%s</name>\n' % name)
                if band_count == 0 and nbands == 1:
                    visibility = 1
                else:
                    visibility = 0
                fp.write('\t\t<visibility>%d</visibility>\n'
                         % (visibility))
                fp.write('\t\t<open>0</open>\n')
                if nbands > 1:
                    image = image_obj.getImage(band=band_count)
                    band_suffix = '_band_' + str(band_count)
                    current_data_output_file = (data_output_file +
                                                band_suffix)
                else:
                    current_data_output_file = data_output_file
                valid_tile_count = 0

                if nbands > 1:
                    name = f'Legend (band: {band_count+1})'

                if not flag_one_cmap and flag_add_kmz_cbar_offset:
                    band_cbar_file_list = [cbar_file_list[band_count],
                                           cbar_offset_background_file]
                    cbar_image = plant.concatenate_y_filelist(
                        band_cbar_file_list)

                    cbar_prefix = data_output_file + \
                        f'_colorbar_{band_count}_2'
                    if output_format.upper() != 'KMZ':
                        band_cbar_file = cbar_prefix + '.png'
                    else:
                        band_cbar_file = (cbar_prefix + '_' +
                                          str(time.time()) + '.png')

                    if output_format.upper() == 'KMZ':
                        plant.append_temporary_file(band_cbar_file)
                    file_list.append(band_cbar_file)

                    plant.save_image(cbar_image,
                                     band_cbar_file,
                                     verbose=verbose)
                    add_legend_to_kml(fp, band_cbar_file,
                                      name=name, visibility=visibility)
                else:
                    add_legend_to_kml(fp, cbar_file_list[band_count],
                                      name=name, visibility=visibility)

                for tile_count in range(n_tiles):
                    if n_tiles != 1:
                        if verbose:
                            print(f'processing tile {tile_count+1}/'
                                  f'{n_tiles}...')
                        i = int(tile_count / n_tiles_x)
                        j = np.mod(tile_count, n_tiles_x)
                        ret = _get_tile(i,
                                        j,
                                        plant_geogrid_obj,
                                        lat_tile_range,
                                        lon_tile_range,
                                        length,
                                        width,
                                        tile_size)

                        tile_suffix = '_tile_' + str(valid_tile_count)
                        bbox, start_y, end_y, start_x, end_x = ret
                        image_tile = image[start_y:end_y, start_x:end_x]
                        current_data_output_file_tile = \
                            (current_data_output_file +
                             tile_suffix)

                        name = os.path.basename(output_file)
                        if nbands == 1:
                            name = name.replace(extension,
                                                '(tile: %d)' % (tile_count))
                        else:
                            name = name.replace(extension,
                                                ' (band: %d, tile: %d)'
                                                % (band_count,
                                                   tile_count))
                        name = os.path.basename(name)
                    else:
                        current_data_output_file_tile = \
                            current_data_output_file
                        bbox = plant_geogrid_obj.bbox
                        image_tile = image
                        name = os.path.basename(output_file)
                        if nbands == 1:
                            name = name.replace(extension, '')
                        else:
                            name = name.replace(extension,
                                                ' (band: %d)' % (band_count))
                    if output_format.upper() == 'KMZ':
                        current_data_output_file_tile += '_' + str(time.time())
                    current_data_output_file_tile += '.png'
                    if np.all(plant.isnan(image_tile)):
                        continue
                    valid_tiles_list[tile_count] = True
                    valid_tile_count += 1
                    if output_format.upper() == 'KMZ':
                        plant.append_temporary_file(
                            current_data_output_file_tile)
                    save_image_figure(image_tile,
                                      current_data_output_file_tile,
                                      output_format=data_output_format,

                                      cmap_min=0,
                                      cmap_max=1,
                                      cmap=cmap,
                                      flag_scale_data=False,
                                      background_color=background_color,

                                      verbose=False,

                                      force=force)
                    if not plant.isfile(current_data_output_file_tile):
                        print('ERROR saving file'
                              f' {current_data_output_file_tile}')
                    elif verbose:
                        print('## file saved:'
                              f' {current_data_output_file_tile}')
                    file_list.append(current_data_output_file_tile)
                    plant.append_temporary_file(current_data_output_file_tile)
                    add_layer_to_kml(fp,
                                     name,
                                     os.path.basename(
                                         current_data_output_file_tile),
                                     bbox,

                                     visibility=visibility)

                fp.write('\t</Folder>\n')
        else:
            valid_tiles_list = [True for t in range(n_tiles)]

        if nbands > 1:
            name = os.path.basename(output_file.replace(extension, ' (RGB)'))
            fp.write('\t<Folder>\n')
            fp.write('\t\t<name>%s</name>\n' % name)
            visibility = 1
            fp.write('\t\t<visibility>%d</visibility>\n'
                     % (visibility))
            fp.write('\t\t<open>0</open>\n')
            valid_tile_count = 0

            if ctable is None and not flag_one_cmap:

                cbar_file_list = []

                for i, cmap in enumerate(['Reds', 'Greens', 'Blues']):
                    cbar_prefix = data_output_file + f'_colorbar_{cmap}'
                    if output_format.upper() != 'KMZ':
                        cbar_file = cbar_prefix + '.png'
                    else:
                        cbar_file = (cbar_prefix + '_' +
                                     str(time.time()) + '.png')

                    if (i > len(cmap_min_band_list) - 1 or
                            i > len(cmap_max_band_list) - 1):
                        break
                    plant.append_temporary_file(cbar_file)

                    plant.save_cmap(cmap,
                                    cbar_file,
                                    cmap_min_band_list[i],
                                    cmap_max_band_list[i],
                                    verbose=verbose)

                    cbar_file_list.append(cbar_file)
                if flag_add_kmz_cbar_offset:
                    cbar_file_list.append(cbar_offset_background_file)
                cbar_prefix = data_output_file + '_colorbar_rgb'
                if output_format.upper() != 'KMZ':
                    cbar_file = cbar_prefix + '.png'
                else:
                    cbar_file = (cbar_prefix + '_' +
                                 str(time.time()) + '.png')

                cbar_image = plant.concatenate_y_filelist(cbar_file_list)

                if output_format.upper() == 'KMZ':
                    plant.append_temporary_file(cbar_file)
                plant.save_image(cbar_image,
                                 cbar_file,
                                 verbose=verbose)

                file_list.append(cbar_file)
                plant.append_temporary_file(cbar_file)

            name = 'Legend (RGB)'
            add_legend_to_kml(fp, cbar_file, name=name,
                              visibility=True)

            for tile_count in range(n_tiles):
                if not valid_tiles_list[tile_count]:
                    continue
                if n_tiles != 1:
                    i = int(tile_count / n_tiles_x)
                    j = np.mod(tile_count, n_tiles_x)
                    ret = _get_tile(i,
                                    j,
                                    plant_geogrid_obj,
                                    lat_tile_range,
                                    lon_tile_range,
                                    length,
                                    width,
                                    tile_size)

                    bbox, start_y, end_y, start_x, end_x = ret
                    tile_suffix = '_tile_' + str(valid_tile_count)
                    data_output_file_tile = (data_output_file +
                                             tile_suffix)
                    name = output_file.replace(extension,
                                               ' (RGB tile %d)'
                                               % (tile_count))
                else:
                    data_output_file_tile = data_output_file
                    bbox = plant_geogrid_obj.bbox
                    name = output_file.replace(extension,
                                               ' (RGB)')
                args = []
                for k in range(min([nbands, 4])):
                    image = image_obj.get_image(band=k)

                    if n_tiles != 1:
                        image = image[start_y:end_y, start_x:end_x]

                    args.append(image)

                valid_tile_count += 1
                if output_format.upper() == 'KMZ':
                    data_output_file_tile += '_' + str(time.time())
                data_output_file_tile += '.png'
                args.append(data_output_file_tile)
                if output_format.upper() == 'KMZ':
                    plant.append_temporary_file(
                        data_output_file_tile)

                save_image_figure(*args,
                                  output_format=data_output_format,

                                  cmap_min=0,
                                  cmap_max=1,
                                  flag_scale_data=False,
                                  background_color=background_color,
                                  verbose=False,

                                  force=force)

                if not plant.isfile(data_output_file_tile):
                    print('ERROR saving file'
                          f' {current_data_output_file_tile}')
                elif verbose:
                    print('## file saved:'
                          f' {data_output_file_tile}')
                file_list.append(data_output_file_tile)
                plant.append_temporary_file(data_output_file_tile)
                add_layer_to_kml(fp,
                                 name,
                                 data_output_file_tile,
                                 bbox,

                                 visibility=True)
            fp.write('\t</Folder>\n')

        fp.write('  </Document>\n')
        fp.write('</kml>\n')
    if not plant.isfile(kml_file):
        return
    if output_format.upper() == 'KMZ':
        plant.append_temporary_file(kml_file)
    file_list.append(kml_file)
    if verbose:
        print('## file saved: %s (KML)' % kml_file)
    if output_format.upper() != 'KMZ':
        plant.plant_config.output_files.append(kml_file)
    else:
        if verbose:
            print('compressing files into %s...'
                  % (output_file))

        with ZipFile(output_file, 'w') as myzip:
            for filename in file_list:
                myzip.write(filename,
                            os.path.basename(filename))

        plant.plant_config.output_files.append(output_file)
        if plant.isfile(output_file) and verbose:
            print('## file saved: %s (KMZ)' % output_file)


def _get_tile(i,
              j,
              plant_geogrid_obj,
              lat_tile_range,
              lon_tile_range,
              length,
              width,
              tile_size):

    x0 = plant_geogrid_obj.x0
    xf = plant_geogrid_obj.xf
    y0 = plant_geogrid_obj.y0
    yf = plant_geogrid_obj.yf

    start_lat = y0 - i * lat_tile_range
    end_lat = y0 - (i + 1) * lat_tile_range
    end_lat = max([end_lat, yf])
    start_lon = x0 + j * lon_tile_range
    end_lon = x0 + (j + 1) * lon_tile_range
    end_lon = min([end_lon, xf])

    bbox = plant.get_bbox(start_lon, end_lon, start_lat, end_lat)
    start_y = int(i * tile_size)
    end_y = int((i + 1) * tile_size) + 1
    end_y = min([end_y, length])
    start_x = int(j * tile_size)
    end_x = int((j + 1) * tile_size) + 1
    end_x = min([end_x, width])
    return (bbox, start_y, end_y, start_x, end_x)


def add_legend_to_kml(fp, icon, name='Legend', visibility=1):
    fp.write('\t\t<ScreenOverlay>')
    fp.write('\t\t\t<name>%s</name>' % name)
    fp.write('\t\t\t<visibility>%d</visibility>' % visibility)
    fp.write('\t\t\t<Icon>')
    fp.write('\t\t\t<href>%s</href>' % os.path.basename(icon))
    fp.write('\t\t\t</Icon>')
    fp.write('\t\t\t<overlayXY x="0" xunits="fraction" '
             'y="0.0" yunits="fraction" />')
    fp.write('\t\t\t<screenXY x="0" xunits="fraction" '
             'y="0.02" yunits="fraction" />')
    fp.write('\t\t\t<rotationXY x="0" xunits="fraction" '
             'y="0" yunits="fraction" />')
    image_obj = plant.read_image(icon, verbose=False)

    if image_obj is None:
        print(f'ERROR creating {icon}')
        return
    width = image_obj.width / 2
    length = image_obj.length / 2
    fp.write(f'\t\t\t<size x="{width}" xunits="pixels"'
             f' y="{length}" yunits="pixels" />')
    fp.write('\t\t</ScreenOverlay>')


def add_layer_to_kml(fp, name, icon, bbox, visibility=1):
    fp.write('\t\t<GroundOverlay>\n')
    fp.write('\t\t\t<name>%s</name>\n' % name)
    fp.write('\t\t\t<visibility>%d</visibility>\n' % visibility)
    fp.write('\t\t\t<color>ffffffff</color>\n')
    fp.write('\t\t\t<drawOrder>1</drawOrder>\n')
    fp.write('\t\t\t<Icon>\n')
    fp.write('\t\t\t\t<href>%s</href>\n' % os.path.basename(icon))
    fp.write('\t\t\t</Icon>\n')
    fp.write('\t\t\t<LatLonBox>\n')
    fp.write('\t\t\t\t<north>%f</north>\n' % bbox[1])
    fp.write('\t\t\t\t<south>%f</south>\n' % bbox[0])
    fp.write('\t\t\t\t<east>%f</east>\n' % bbox[3])
    fp.write('\t\t\t\t<west>%f</west>\n' % bbox[2])
    fp.write('\t\t\t</LatLonBox>\n')
    fp.write('\t\t</GroundOverlay>\n')


def save_image(input_data,
               output_file,

               verbose=True,
               save_header_only=False,
               force=None,
               geotransform=None,
               projection=None,
               nbands=None,
               lat_size=None,
               lon_size=None,

               flag_scale_data=None,
               flag_add_kmz_cbar_offset=None,
               kmz_cbar_offset_length=None,
               kmz_cbar_offset_width=None,
               kmz_cbar_offset_alpha=None,
               output_scheme='',
               descr='',

               folium_map=None,
               percentile=None,
               cmap=None,
               cmap_crop_min=None,
               cmap_crop_max=None,
               cmap_min=None,
               cmap_max=None,
               flag_cmap_already_expanded=False,
               ctable=None,
               background_color=None,
               in_null=None,
               out_null=None,
               output_dtype=None,
               output_format=None,
               output_projection=None,

               flag_temporary=False):

    if not output_file:
        print('ERROR output file is empty or invalid')
        return

    if isinstance(input_data, plant.PlantImage):
        filename = input_data.filename
        file_format = input_data.file_format
    else:
        filename = ''
        file_format = None
    if (output_format is None and
            filename and output_file and
            filename.split('.')[-1] == output_file.split('.')[-1]):
        output_format = get_output_format(output_file, file_format)

    else:
        output_format = get_output_format(output_file,
                                          output_format=output_format)

    image_obj = plant.PlantImage(
        input_data,

        update=output_format.upper() != 'VRT',

        nbands=nbands,

        scheme=output_scheme)

    if ctable is None and image_obj.ctable is not None:
        ctable = image_obj.ctable

    if ((lat_size is not None and
         lat_size != image_obj.length) or
        (lon_size is not None and
         lon_size != image_obj.width)):
        geotransform = None
        projection = None
    else:
        if (plant.geotransform_all_invalid(image_obj.geotransform) and
                not plant.geotransform_all_invalid(geotransform)):
            image_obj.geotransform = geotransform
        if (image_obj.projection is None and
                projection is not None):
            image_obj.projection = projection

    if image_obj is None:
        print('ERROR saving %s. Invalid data' % output_file)
        return

    if (image_obj.depth is not None and
            image_obj.depth > 1 and
            (not output_format or
             (output_format != 'ISCE' and
              output_format not in plant.FIG_DRIVERS))):
        print('WARNING output image is 3D. '
              'Forcing format to ISCE.')
        output_format = 'ISCE_NP'

    if (in_null is not None or
            out_null is not None):

        plant.apply_null(image_obj,
                         in_null=in_null,
                         out_null=out_null,
                         force_update=True)

    if output_file.upper().startswith('MEM:'):

        output_key = output_file[4:]

        image_obj.realize_changes()
        image_obj.filename = output_file
        image_obj.filename_orig = output_file

        plant.plant_config.variables[output_key] = image_obj
        return

    image = image_obj.image if output_format != 'VRT' else None

    nbands = image_obj.nbands
    geotransform = image_obj.geotransform

    metadata = image_obj.metadata

    gcp_list = image_obj.gcp_list
    gcp_projection = image_obj.gcp_projection
    projection = image_obj.projection

    if (cmap_min is None and cmap_max is None and
            image_obj.file_format in plant.FIG_DRIVERS):
        cmap_min = 0
        cmap_max = 255

    scheme = image_obj.scheme
    if scheme is None:
        scheme = 'BSQ'
    projection = image_obj.projection
    flag_figure = output_format.upper() in plant.FIG_DRIVERS
    flag_html = output_format.upper() == 'HTML'
    flag_kml_kmz = (output_format.upper() == 'KML' or
                    output_format.upper() == 'KMZ')
    flag_csv = output_format.upper() == 'CSV'
    flag_isce = (output_format.upper() == 'ISCE_NP' or
                 (output_format.upper() == 'ISCE' and
                  (save_header_only or
                   (scheme and scheme.upper() != 'BSQ'))))
    flag_srf = output_format.upper() == 'SRF'
    flag_numpy = output_format.upper() == 'NUMPY'

    if image is not None:
        flag_isce = flag_isce or len(image.shape) == 1
        for b in range(image_obj.nbands):
            band = image_obj.get_band(band=b)
            dtype = band.dtype
            if output_dtype is not None:
                band_dtype = output_dtype
            else:
                band_dtype = dtype
            flag_force_output_dtype = False
            if (output_dtype is not None and
                    (dtype is None or output_dtype != dtype)):
                flag_force_output_dtype = True
            elif (dtype is not None and
                  dtype == 'bool'):
                flag_force_output_dtype = True
                band_dtype = 'byte'
            if (ctable is not None and
                    'int' not in plant.get_dtype_name(dtype) and
                    'short' not in plant.get_dtype_name(dtype) and
                    'long' not in plant.get_dtype_name(dtype) and
                    'byte' not in plant.get_dtype_name(dtype) and
                    not flag_figure and
                    not flag_html and
                    not flag_kml_kmz):
                dtype_size = plant.get_dtype_size(dtype) * 8
                print(f'WARNING forcing output to uint{dtype_size}'
                      ' because image has color table (GDAL)..')

                band_dtype = np.__dict__[f'uint{dtype_size}']
                flag_force_output_dtype = True
            if flag_force_output_dtype:
                band.dtype = band_dtype

    if not os.path.isdir(plant.dirname(output_file)):
        try:
            os.makedirs(plant.dirname(output_file))
        except FileExistsError:
            pass

    ret = plant.overwrite_file_check(output_file,
                                     force=force)
    if not ret:
        return

    update_image = not save_header_only

    if os.path.isfile(output_file) and update_image:
        os.remove(output_file)

    if flag_numpy:
        _save_numpy(image, output_file, verbose,
                    flag_temporary)
        return

    if flag_srf:
        _save_srf(image, image_obj, output_file, verbose,
                  flag_temporary)
        return

    flag_save_isce_header = output_format.upper() == 'ISCE'

    if flag_isce:
        ret = _save_isce(image, image_obj, output_file,
                         nbands, output_dtype, scheme, geotransform,
                         update_image, verbose, force,
                         flag_temporary)
        if ret is None:
            return
        flag_save_isce_header = False
        if scheme and scheme.upper() != 'BSQ':
            print(f'WARNING changing scheme from {scheme.upper()}'
                  ' to BSQ and file format'
                  f' from {output_format.upper()} to ENVI')
            scheme = 'BSQ'
            output_format = 'ENVI'
        if output_format.upper() == 'ISCE_NP':
            output_format = 'ISCE'

    cmap = plant.DEFAULT_CMAP if cmap is None else cmap

    if flag_figure or flag_html:
        nbands = image_obj.nbands
        if ctable is not None:
            ret_dict = plant.image_obj_ctable_expand(image_obj,
                                                     ctable=ctable)
            image_obj = ret_dict['image_obj']
            cmap_min = None
            cmap_max = None
            flag_scale_data = False
            nbands = image_obj.nbands

        elif (nbands > 3 and image_obj.file_format is not None and
              image_obj.file_format.upper() not in plant.FIG_DRIVERS):
            print('WARNING input is 4 bands or more. Only the'
                  ' first three bands will be saved to'
                  ' the output.')
            nbands = 3

        if nbands == 1:
            args = [image]
        else:
            args = []
            for i in range(nbands):
                args.append(image_obj.get_image(band=i))
        if flag_figure:
            args.append(output_file)

            save_image_figure(*args,
                              output_format=output_format,
                              percentile=percentile,
                              flag_scale_data=flag_scale_data,
                              background_color=background_color,
                              cmap=cmap,
                              cmap_min=cmap_min,
                              cmap_max=cmap_max,
                              verbose=verbose,
                              force=force)
        elif flag_html:
            import folium
            from folium import raster_layers

            plant_geogrid_obj = plant.get_coordinates(
                geotransform=geotransform,
                length=image_obj.length,
                width=image_obj.width)

            lat_min = plant_geogrid_obj.yf
            lat_max = plant_geogrid_obj.y0
            lon_max = plant_geogrid_obj.xf
            lon_min = plant_geogrid_obj.x0

            if folium_map is None:
                folium_map = folium.Map(location=[(lat_min + lat_max) / 2,
                                                  (lon_min + lon_max) / 2],

                                        tiles=None)

            folium_basemap = folium.FeatureGroup(name='Basemap', overlay=True,
                                                 control=False)

            folium_map.fit_bounds([[lat_min, lon_min],
                                   [lat_max, lon_max]])

            image = plant.image_to_figure(*args,
                                          cmap=cmap,
                                          cmap_min=cmap_min,
                                          cmap_max=cmap_max,

                                          background_color=background_color,

                                          verbose=False)

            folium_layer_1 = folium.FeatureGroup(name='layer1', overlay=True,
                                                 control=True)
            raster_layers.ImageOverlay(

                image=image,
                mercator_project=True,

                bounds=[[lat_min, lon_min], [lat_max, lon_max]]
            ).add_to(folium_layer_1)
            folium_layer_1.add_to(folium_map)

            folium_basemap = folium.FeatureGroup(name='Basemap', overlay=False,
                                                 control=False)
            plant.folium_add_layer(folium_basemap,
                                   flag_google_maps=True)
            folium_basemap.add_to(folium_map)

            plant.save_folium(folium_map,
                              output_file,
                              verbose=verbose,
                              force=force)
        return

    if flag_kml_kmz:

        save_image_kmz(image_obj=image_obj,
                       nbands=nbands,
                       flag_scale_data=flag_scale_data,
                       flag_add_kmz_cbar_offset=flag_add_kmz_cbar_offset,
                       kmz_cbar_offset_length=kmz_cbar_offset_length,
                       kmz_cbar_offset_width=kmz_cbar_offset_width,
                       kmz_cbar_offset_alpha=kmz_cbar_offset_alpha,
                       percentile=percentile,
                       cmap=cmap,
                       cmap_crop_min=cmap_crop_min,
                       cmap_crop_max=cmap_crop_max,
                       cmap_min=cmap_min,
                       cmap_max=cmap_max,
                       flag_cmap_already_expanded=flag_cmap_already_expanded,
                       out_null=out_null,
                       ctable=ctable,
                       background_color=background_color,
                       verbose=verbose,
                       geotransform=geotransform,
                       output_file=output_file,
                       output_format=output_format,
                       force=force)
        return

    if flag_csv:
        image = image.ravel()
        ind = np.where(np.zeros(image.shape) + 1)
        if len(image.shape) == 1:

            save_text(image.ravel(),
                      'data',
                      output_file,
                      force=force)
            return
        else:
            data_x = ind[1]
            data_y = ind[0]
            save_text(data_x, data_y, image.ravel(),
                      'x', 'y', 'data',
                      output_file,
                      force=force)
            return

    if save_header_only:
        output_file_orig = output_file
        output_file = get_temporary_file()
        verbose_orig = verbose
        verbose = False

    ret = _save_image_gdal(image, image_obj, output_file,

                           output_format, nbands, output_dtype, ctable,
                           geotransform, metadata,

                           projection, gcp_list, gcp_projection,
                           out_null,
                           save_header_only, flag_save_isce_header,
                           verbose, flag_temporary)
    if ret is None and save_header_only:
        verbose = verbose_orig
        file_list = plant.glob(output_file + '*')
        for current_file in file_list:

            plant.append_temporary_file(current_file)
            new_file = current_file.replace(output_file,
                                            output_file_orig)
            if current_file == new_file:
                continue
            update = plant.overwrite_file_check(new_file,
                                                force=force)
            if not update:
                continue
            os.rename(current_file, new_file)
            if plant.isfile(new_file):
                print(f'## file saved: {new_file} (GDAL)')
        current_file = get_envi_header(output_file)

        plant.append_temporary_file(current_file)
        if plant.isfile(current_file):
            update = plant.overwrite_file_check(new_file,
                                                force=force)
            if update:
                new_file = get_envi_header(output_file_orig)
                os.rename(current_file, new_file)
                if plant.isfile(new_file):
                    print(f'## file saved: {new_file} (GDAL)')
    if ret is None:
        return

    ret = _save_isce(image, image_obj, output_file,
                     nbands, output_dtype, scheme, geotransform,
                     update_image, verbose, force,
                     flag_temporary)


def get_image_dimensions(image):
    shape = image.shape
    width = shape[-1] if len(shape) >= 1 else 1
    length = shape[-2] if len(shape) >= 2 else 1
    depth = shape[-3] if len(shape) >= 3 else 1
    return depth, length, width


def _save_srf(image, image_obj, output_file, verbose,
              flag_temporary):
    image = plant.apply_srf_dtype(image)
    nbands = image_obj.nbands
    depth, length, width = get_image_dimensions(image)
    dtype = plant.get_dtype_name(image)
    dtype_srf = plant.get_srf_dtype(dtype)
    dtype_size = plant.get_dtype_size(dtype)
    header = np.zeros((8), dtype=np.uint32)
    header[0] = plant.SRF_MAGIC_NUMBER
    header[1] = width
    header[2] = length
    header[3] = dtype_size * 8
    header[4] = width * length * nbands * 8
    header[5] = dtype_srf
    header[6] = 0
    header[7] = 0
    with open(output_file, "wb") as of:

        header.tofile(of)
        for b in range(image_obj.nbands):
            if b != 0:
                image = image_obj.get_image(band=b)
                image = plant.apply_srf_dtype(image)

            image.tofile(of)
    if not plant.isfile(output_file):
        print(f'ERROR saving image to {output_file} (SRF)')
        return
    if flag_temporary:
        plant.append_temporary_file(output_file)
        return
    plant.plant_config.output_files.append(output_file)
    if verbose:
        print(f'## file saved: {output_file} (SRF)')


def _save_numpy(image, output_file, verbose,
                flag_temporary):

    image.tofile(output_file)
    if not plant.isfile(output_file):
        print(f'ERROR saving image to {output_file} (NUMPY)')
        return
    if flag_temporary:
        plant.append_temporary_file(output_file)
        return
    plant.plant_config.output_files.append(output_file)
    if verbose:
        print(f'## file saved: {output_file} (NUMPY)')


def _save_isce(image, image_obj, output_file,
               nbands, dtype, scheme,
               geotransform, update_image, verbose, force,
               flag_temporary):

    if dtype is None:
        dtype = plant.get_dtype_name(image)
    depth, length, width = get_image_dimensions(image)

    if update_image and nbands == 1:
        image.tofile(output_file)
    elif update_image:
        data = np.zeros((length * width * depth * nbands),
                        dtype=dtype)
        if scheme.upper() == 'BIP':
            data[0::nbands] = image.ravel()
            for i in range(1, nbands):
                current_image = image_obj.getImage(band=i)
                data[i::nbands] = current_image.ravel()
        elif scheme.upper() == 'BIL':
            for i in range(0, length):
                offset = i * width * nbands
                data[offset:offset + width] = image[i, :]
            for b in range(1, nbands):
                current_image = image_obj.getImage(band=b)
                for i in range(0, length):
                    offset = i * width * nbands + b * width
                    data[offset:offset + width] = current_image[i, :]
        elif scheme.upper() == 'BSQ':
            data[0:length * width * depth] = image.ravel()
            for i in range(1, nbands):
                current_image = image_obj.getImage(band=i)
                data[i * length * width * depth:
                     (i + 1) * length * width * depth] = \
                    current_image.ravel()
        else:
            print('ERROR cannot save ISCE image'
                  ' with scheme %s'
                  % (scheme))
            return
        data.tofile(output_file)
    if flag_temporary:
        plant.append_temporary_file(output_file)
    else:
        plant.plant_config.output_files.append(output_file)
        if (verbose and update_image and
                plant.isfile(output_file) and nbands == 1):
            print('## file saved: %s (ISCE)' % output_file)
        elif verbose and update_image and plant.isfile(output_file):
            print('## file saved: %s (ISCE: %s)' % (output_file, scheme))
        elif verbose and update_image:
            print('ERROR saving file: ' + output_file)
            return

    header_files = [output_file + ext for ext in ['.xml', '.vrt']]
    if not update_image:
        for header_file in header_files:
            if not plant.isfile(header_file):
                continue
            update_header = plant.overwrite_file_check(header_file,
                                                       force=force)
            if not update_header:
                print('WARNING operation cancelled')
                return
    if create_isce_header(output_file,
                          data=image,
                          scheme=scheme,
                          nbands=nbands,
                          geotransform=geotransform):
        if not update_image:
            for header_file in header_files:
                if flag_temporary:
                    plant.append_temporary_file(header_file)
                    continue
                plant.plant_config.output_files.append(
                    header_file)
                if plant.isfile(header_file) and verbose:
                    print('## file saved: %s' % header_file)

        return
    print('WARNING there was an error saving the ISCE header')

    return True


def get_temporary_file(basefile=None, dirname=None, append=False,
                       suffix=None, ext=None):
    timestamp = str(float(time.time())) + str(np.random.randint(1000))
    temp_file = 'temp_' + timestamp
    if basefile is not None:
        ret_dict = plant.parse_filename(basefile)
        filename = ret_dict['filename']
        temp_file = os.path.basename(filename) + '_' + temp_file
    if suffix is not None:
        temp_file += suffix
    if dirname is not None:
        temp_file = os.path.join(dirname,
                                 os.path.basename(temp_file))
    if ext is not None:
        if not ext.startswith('.'):
            ext = '.' + ext
        temp_file += ext
    if append:
        append_temporary_file(temp_file)
    return temp_file


def append_output_file(output_file):
    plant.plant_config.output_files.append(output_file)


def append_temporary_file(temp_file):
    plant.plant_config.temporary_files.append(str(temp_file))


def save_as_cog(filename,
                ovr_resamp_algorithm=None,
                compression='DEFLATE',
                nbits=None,
                overviews_list=None):

    try:
        gdal_ds = gdal.Open(filename, gdal.GA_Update)
    except RuntimeError:

        gdal_ds = gdal.OpenEx(filename, gdal.GA_Update,
                              open_options=["IGNORE_COG_LAYOUT_BREAK=YES"])

    gdal_dtype = gdal_ds.GetRasterBand(1).DataType
    dtype_name = gdal.GetDataTypeName(gdal_dtype).lower()

    is_integer = 'byte' in dtype_name or 'int' in dtype_name

    if overviews_list:
        if ovr_resamp_algorithm is None and is_integer:
            ovr_resamp_algorithm = 'NEAREST'
        elif ovr_resamp_algorithm is None:
            ovr_resamp_algorithm = 'CUBICSPLINE'

        gdal_ds.BuildOverviews(ovr_resamp_algorithm, overviews_list,
                               gdal.TermProgress_nocb)

    del gdal_ds
    external_overview_file = filename + '.ovr'
    if os.path.isfile(external_overview_file):
        os.remove(external_overview_file)

    temp_file = plant.get_temporary_file(
        suffix='_cog_temp', ext='.tif', append=True)

    tile_size = 512
    gdal_translate_options = ['BIGTIFF=IF_SAFER',
                              'MAX_Z_ERROR=0',
                              'TILED=YES',
                              f'BLOCKXSIZE={tile_size}',
                              f'BLOCKYSIZE={tile_size}',
                              'COPY_SRC_OVERVIEWS=YES']

    if compression:
        gdal_translate_options += [f'COMPRESS={compression}']

    if is_integer:
        gdal_translate_options += ['PREDICTOR=2']
    elif dtype_name in ['Float32', 'Float64']:
        gdal_translate_options += ['PREDICTOR=3']

    if nbits is not None:
        gdal_translate_options += [f'NBITS={nbits}']

        gdal.SetConfigOption('CPL_LOG', '/dev/null')

    gdal.Translate(temp_file, filename,
                   creationOptions=gdal_translate_options)

    shutil.move(temp_file, filename)


def _save_image_gdal(image, image_obj, output_file,
                     output_format, nbands, dtype, ctable, geotransform,

                     metadata,

                     projection, gcp_list, gcp_projection,
                     out_null,
                     save_header_only,
                     flag_save_isce_header, verbose, flag_temporary):

    kwargs = locals()
    if dtype is None:
        dtype = plant.get_dtype_name(image)
    gdal_dtype = plant.get_gdal_dtype_from_np(dtype)
    if (gdal_dtype == gdal.GDT_Unknown and
            'int' in plant.get_dtype_name(dtype).lower()):
        print('WARNING invalid data type %s (GDAL).'
              ' Converting output to Int32..' % dtype)
        dtype = np.int32
        gdal_dtype = plant.get_gdal_dtype_from_np(dtype)
    elif gdal_dtype == gdal.GDT_Unknown:
        print('WARNING invalid data type %s (GDAL).'
              ' Converting output to Float32..' % dtype)
        dtype = np.float32
        gdal_dtype = plant.get_gdal_dtype_from_np(dtype)
    gdal.ErrorReset()

    dataset = None
    if metadata is None:
        metadata = {}

    if (output_format is not None and output_format.upper() == 'VRT'):

        src_win = None
        file_list = []
        band_list = []
        for b in range(image_obj.nbands):
            band = image_obj.get_band(band=b)

            file_list.append(band.parent_orig.filename_orig)
            band_list.append(band.parent_orig_band_orig_index + 1)
            if (src_win is None and
                    band.parent_orig.width != band.parent_orig.width_orig or
                    band.parent_orig.length != band.parent_orig.length_orig):

                src_win = band.parent_orig.crop_window

        n_files = len(list(set(file_list)))
        separate = True
        bandlist_kwargs = {}

        gdal_version_major_minor = float(
            '.'.join(gdal.__version__.split('.')[0:-1]))

        if (gdal_version_major_minor < 3.8 or
                not all(b == 1 for b in band_list)):
            bandlist_kwargs['bandList'] = band_list

        if n_files == 1:
            ref_file = list(set(file_list))[0]
            plant.Translate(output_file, ref_file, verbose=False,
                            srcWin=src_win,

                            metadataOptions=metadata,
                            format='VRT',
                            **bandlist_kwargs)
        elif src_win is None:
            args = [output_file]
            args.append(file_list)
            plant.BuildVRT(*args,

                           verbose=False,
                           separate=separate,
                           **bandlist_kwargs)
        else:
            print('WARNING the current implementation of GDAL'
                  ' Translate and BuildVRT (v.3.0.0) do not allow'
                  ' for cropping and multi-input files'
                  ' simultaneously.'
                  ' Creating a temporary file to perform both'
                  ' operations...')
            temp_file = get_temporary_file(append=False) + '.vrt'
            args = [temp_file]
            args.append(file_list)
            plant.BuildVRT(*args,

                           verbose=False,
                           separate=separate)
            ref_file = temp_file
            plant.Translate(output_file, ref_file, verbose=False,
                            metadataOptions=metadata,
                            srcWin=src_win, format='VRT')

        if flag_temporary:
            plant.append_temporary_file(output_file)
            return

        plant.plant_config.output_files.append(
            output_file)
        if plant.isfile(output_file) and verbose:
            print('## file saved: %s (GDAL:VRT)' % output_file)

        return

    else:
        if output_format.upper() != 'ISCE':
            driver = gdal.GetDriverByName(output_format)
        else:
            driver = gdal.GetDriverByName('ENVI')
        if driver is None:
            del dataset
            gdal.ErrorReset()
            print('ERROR saving file %s. '
                  'Output format not recognized: %s'
                  % (output_file, output_format))
            return
        flag_error = False
        try:
            dataset = driver.Create(output_file,
                                    image_obj.width,
                                    image_obj.length,
                                    nbands,
                                    gdal_dtype)
        except RuntimeError:
            error_message = plant.get_error_message()
            flag_error = True

            gdal.ErrorReset()

        if flag_error and (gdal_dtype == gdal.GDT_UInt64 or
                           gdal_dtype == gdal.GDT_Int64):
            if gdal_dtype == gdal.GDT_UInt64:
                gdal_dtype = gdal.GDT_UInt32
            elif gdal_dtype == gdal_dtype == gdal.GDT_Int64:
                gdal_dtype = gdal.GDT_Int32
            else:
                raise NotImplementedError
            try:
                dataset = driver.Create(output_file,
                                        image_obj.width,
                                        image_obj.length,
                                        nbands,
                                        gdal_dtype)
                flag_error = False
            except RuntimeError:
                error_message = plant.get_error_message()
                flag_error = True

                gdal.ErrorReset()

        if flag_error:

            message = ('saving file %s with format %s. %s'
                       % (output_file, output_format, error_message))
            if (output_format.upper() == 'ENVI' or
                    output_format.upper() == 'ISCE'):
                print(f'WARNING error {message}')
                return True
            print(f'ERROR {message}')
            return

    if dataset is None:
        print('ERROR saving file: %s' % output_file)
        return

    if geotransform is not None:
        dataset.SetGeoTransform(geotransform)

    dataset.SetMetadata(metadata)

    if (not projection and
            plant.valid_coordinates(geotransform=geotransform,
                                    width=image_obj.width,
                                    length=image_obj.length,
                                    projection='WGS84')):
        projection = plant.PROJECTION_REF_WKT
        print('WARNING (save_image) projection not found, '
              'considering: %s' % (projection))
    if projection:

        try:
            dataset.SetProjection(projection)

        except RuntimeError:
            print(f'WARNING invalid projection: {projection}')

    if gcp_list is not None and len(gcp_list) > 0:
        dataset.SetGCPs(gcp_list, gcp_projection)

    for i in range(nbands):
        current_plant_band_obj = image_obj.getBand(band=i)
        current_image = current_plant_band_obj.image
        current_band = dataset.GetRasterBand(i + 1)

        if ctable is not None:

            current_band.SetColorTable(ctable)

        name = current_plant_band_obj.name

        if verbose and name:
            print(f'saving band {i}: {name}')

        elif nbands > 1 and verbose:
            print(f'saving band: {i}')
        if name:

            current_band.SetDescription(name)

        if out_null is None:
            band_null = current_plant_band_obj.null
        else:
            band_null = out_null
        if band_null is not None:
            current_band.SetNoDataValue(band_null)

        if not save_header_only:
            try:
                current_band.WriteArray(current_image)
            except ValueError:
                error_message = plant.get_error_message()
                print('ERROR saving band with dimensions ',
                      f'{current_image.shape}: {error_message}')
            current_band.FlushCache()
        current_band = None

    if not save_header_only:
        dataset.FlushCache()
    del dataset
    gdal.ErrorReset()

    if output_format == 'GTiff':
        save_as_cog(output_file, compression='DEFLATE')

    if plant.isfile(output_file + '.aux.xml'):
        os.remove(output_file + '.aux.xml')

    plant.plant_config.output_files.append(output_file)
    if verbose and plant.isfile(output_file):
        print('## file saved: %s (GDAL:%s)'
              % (output_file, output_format))
    if flag_save_isce_header:
        create_isce_header(output_file,
                           data=image,
                           nbands=nbands,
                           dtype=dtype,
                           image_obj=image_obj,
                           geotransform=geotransform)
    if plant.isfile(output_file + '.aux.xml'):
        plant.plant_config.temporary_files.append(output_file + '.aux.xml')


def rename_image(input_file,
                 output_file,
                 verbose=True,
                 input_format=None,
                 force=None,
                 in_null=np.nan,
                 out_null=np.nan,
                 output_format=None,
                 output_dtype=None):

    image_obj = read_image(input_file,

                           input_format=input_format)
    output_dirname = os.path.dirname(output_file)
    if output_dirname and not os.path.isdir(output_dirname):
        try:
            os.makedirs(output_dirname)
        except FileExistsError:
            pass
    if verbose:
        print('renaming ' + input_file + ' to ' + output_file + ' ..')
    if os.path.isfile(output_file):
        if plant.overwrite_file_check(output_file,
                                      force=force):
            if not os.path.samefile(input_file, output_file):
                os.remove(output_file)
            for ext in ['.vrt', '.xml', '.aux.xml', None]:
                if ext is not None:
                    input_file_ext = input_file + ext
                    output_file_ext = output_file + ext
                else:
                    input_file_ext = get_envi_header(input_file)
                    output_file_ext = get_envi_header(output_file)
                if (os.path.isfile(input_file_ext) and
                        os.path.isfile(output_file_ext) and
                    not os.path.samefile(input_file_ext,
                                         output_file_ext)):
                    os.remove(output_file_ext)
    plant.apply_null(image_obj,
                     in_null=in_null,
                     out_null=out_null)
    kwargs = {}
    if output_format is not None:
        kwargs['output_format'] = output_format
    if output_dtype is not None:
        kwargs['output_dtype'] = output_dtype

    save_image(image_obj,
               output_file,
               verbose=verbose,
               force=True,
               **kwargs)
    if not os.path.isfile(output_file):
        print('ERROR there was an error saving file:'
              f' {output_file}')
        return
    if not os.path.samefile(input_file, output_file):
        os.remove(input_file)
    for ext in ['.vrt', '.xml', '.aux.xml', None]:
        if ext is not None:
            input_file_ext = input_file + ext
            output_file_ext = output_file + ext
        else:
            input_file_ext = get_envi_header(input_file)
            output_file_ext = get_envi_header(output_file)
        if (os.path.isfile(input_file_ext) and
                os.path.isfile(output_file_ext) and
            not os.path.samefile(input_file_ext,
                                 output_file_ext)):
            os.remove(input_file_ext)
        elif os.path.isfile(input_file_ext):
            os.rename(input_file_ext, output_file_ext)


def copy_image(input_file,
               output_file,
               verbose=True,
               create_link=False,
               input_format=None,
               force=None,
               in_null=np.nan,
               out_null=np.nan,
               output_dtype=None,
               output_format=None):
    image_obj = read_image(input_file,

                           input_format=input_format)
    if create_link:
        if verbose:
            print('linking ' + input_file + ' to ' + output_file + ' ..')
    else:
        if verbose:
            print('copying ' + input_file + ' to ' + output_file + ' ..')
    if plant.isfile(output_file):
        if plant.overwrite_file_check(output_file,
                                      force=force):
            os.remove(output_file)
            if plant.isfile(output_file + '.vrt'):
                os.remove(output_file + '.vrt')
            if plant.isfile(output_file + '.xml'):
                os.remove(output_file + '.xml')
            envi_hdr = get_envi_header(output_file)
            if plant.isfile(envi_hdr):
                os.remove(envi_hdr)

    if create_link:
        os.symlink(os.path.abspath(input_file),
                   os.path.abspath(output_file))

        create_isce_header(output_file,
                           nbands=1,
                           length=image_obj.length,
                           width=image_obj.width,
                           dtype=image_obj.dtype,
                           scheme=image_obj.scheme,
                           descr='',
                           geotransform=image_obj.geotransform)

    else:

        plant.apply_null(image_obj,
                         in_null=in_null,
                         out_null=out_null)
        kwargs = {}
        if output_format is not None:
            kwargs['output_format'] = output_format
        if output_dtype is not None:
            kwargs['output_dtype'] = output_dtype
        save_image(image_obj,
                   output_file,
                   verbose=verbose,
                   force=force,
                   **kwargs)


def fix_vrt(file_list,
            force=None):

    for current_file in file_list:
        save_image(read_image(current_file,
                              verbose=False),
                   current_file,
                   only_header=True,
                   verbose=False,
                   force=force)


def read_large(input_name, name=None,
               function=''):

    test_var = np.memmap(search_image(input_name)[0], mode="r")
    total_size = test_var.shape[0]
    max_chunk_size = 2**24
    print('total size: ' + str(total_size))
    print('max chunk size: ' + str(max_chunk_size))
    nchunks = 1
    while (float(total_size) / nchunks > max_chunk_size or
           total_size % nchunks != 0):
        nchunks += 1
    chunk_size = total_size / nchunks
    print('new shape: ' + str(nchunks) + ' x ' + str(chunk_size))
    test_var = np.reshape(test_var, (nchunks, chunk_size))

    if not function:
        function = get_info
    for i in range(test_var.shape[0]):
        function(test_var)


def save_text(*args, **kwargs):

    force = kwargs.pop('force', None)
    verbose = kwargs.pop('verbose', True)
    delimiter = kwargs.pop('delimiter', ',')
    str_list = []
    vector_list = []
    for count, arg in enumerate(args):
        if isinstance(arg, str):
            str_list.append(arg)
        elif isinstance(arg, dict):
            for key in arg:
                str_list.append(key)
                vector_list.append(arg[key])
        elif isinstance(arg, np.ndarray):
            vector_list.append(arg.ravel())
        else:
            vector_list.append(arg)
    output_file = str_list[-1]
    str_list = str_list[:-1]

    if (len(str_list) != len(vector_list)
            and len(str_list) != 0):
        print('vector list size: ', len(vector_list))
        print('string list size: ', len(str_list))
        print('ERROR vector and string list sizes do not '
              'match')
        return

    if plant.isfile(output_file):
        ret = plant.overwrite_file_check(output_file,
                                         force=force)
        if not ret and verbose:
            print('WARNING file not updated: ' + output_file,
                  1)
            return

    if not os.path.isdir(plant.dirname(output_file)):
        try:
            os.makedirs(plant.dirname(output_file))
        except FileExistsError:
            pass
    with open(output_file, 'w') as f:

        if len(str_list) > 1:
            line_str = ('%s' % (str_list[0]))
            for n in range(1, len(str_list)):
                line_str += ('%s%s' % (delimiter,
                                       str_list[n]))
            line_str += '\n'
            f.write(line_str)

        if isinstance(vector_list[0], np.ndarray):
            n_lines = vector_list[0].size
        else:
            n_lines = len(vector_list[0])
        for p in range(n_lines):
            if isinstance((vector_list[0])[p], int):
                line_str = ('%d' % float((vector_list[0])[p]))
            elif plant.isnumeric((vector_list[0])[p]):
                line_str = ('%.16f' % float((vector_list[0])[p]))
            elif isinstance((vector_list[0])[p], str):
                line_str = ('"%s"' % (vector_list[0])[p])
            else:
                line_str = ('%s' % (vector_list[0])[p])
            for n in range(1, len(vector_list)):
                try:
                    value = (vector_list[n])[p]
                except BaseException:
                    continue
                if isinstance(value, int):
                    line_str += ('%s%.16f' % (delimiter, value))
                elif plant.isnumeric(value):
                    line_str += ('%s%.16f' % (delimiter, value))
                elif isinstance(value, str):
                    line_str += ('%s"%s"' % (delimiter, value))
                else:
                    line_str += ('%s%s' % (delimiter, value))
            line_str += '\n'
            f.write(line_str)
        plant.plant_config.output_files.append(
            output_file)
        if verbose:
            print('## file saved: %s (TEXT)' % output_file)


def get_footprint_grid(footprint, step_y, step_x):
    footprint_lat = footprint // step_y
    footprint_lon = footprint // step_x
    if (footprint_lat == 1
            and footprint_lon == 1):
        fp_x = np.asarray([0], dtype=np.int32)
        fp_y = np.asarray([0], dtype=np.int32)
    elif (footprint_lat == 1
            and footprint_lon == 2):
        fp_x = np.asarray([0, 1], dtype=np.int32)
        fp_y = np.asarray([0, 0],
                          dtype=np.int32)
    elif (footprint_lat == 2
            and footprint_lon == 1):
        fp_x = np.asarray([0, 0], dtype=np.int32)
        fp_y = np.asarray([0, 1],
                          dtype=np.int32)
    elif (footprint_lat == 2
            and footprint_lon == 2):
        fp_x = np.asarray([0, 0, 0, 1, -1], dtype=np.int32)
        fp_y = np.asarray([0, 1, -1, 0, 0], dtype=np.int32)
    else:
        from skimage.draw import ellipse
        fp_y, fp_x = ellipse(2 * footprint_lat,
                             2 * footprint_lon,
                             footprint_lat / 2,
                             footprint_lon / 2)
        fp_x = np.asarray(fp_x - np.mean(fp_x),
                          dtype=np.int32)
        fp_y = np.asarray(fp_y - np.mean(fp_y),
                          dtype=np.int32)
    return fp_x, fp_y


def rasterize_with_footprint(data_vect,
                             lat_vect,
                             lon_vect,
                             plant_geogrid_obj,

                             footprint=None,
                             footprint_array=None,
                             nbands=None,
                             out_null=0,
                             verbose=True,
                             loreys_height=False,
                             sum_samples=False,
                             E=None):

    lat_vect = np.asarray(lat_vect)
    lon_vect = np.asarray(lon_vect)
    image_obj = plant.PlantImage(data_vect,
                                 nbands=nbands)
    data_vect = image_obj.image_list
    data_vect = [x.ravel() for x in data_vect]
    width = data_vect[0].size

    if width != lat_vect.size:
        print('ERROR dimensions do not match '
              '(data size=%d, lat vect width=%d)'
              % (width, lat_vect.size))
        return

    if width != lon_vect.size:
        print('ERROR dimensions do not match '
              '(data size=%d, lon vect width=%d)'
              % (width, lon_vect.size))
        return

    nbands = len(data_vect)

    lat_size = plant_geogrid_obj.length
    lon_size = plant_geogrid_obj.width

    data = [np.zeros((lat_size, lon_size)) for b in range(nbands)]
    ndata = np.zeros((lat_size, lon_size))

    if loreys_height:
        weight_sum = np.zeros((lat_size, lon_size))

    min_step = min([abs(plant_geogrid_obj.step_y), plant_geogrid_obj.step_x])

    footprint_type = 0
    if footprint is not None:
        if min_step < footprint / 2:
            fp_x, fp_y = get_footprint_grid(footprint / 2,
                                            plant_geogrid_obj.step_y,
                                            plant_geogrid_obj.step_x)
            footprint_type = 1
    if footprint_array is not None:
        footprint_type = 2

    weight = 1.0
    for i in range(0, width):
        plant.print_progress(i, width, verbose=verbose)
        lat_ind = int(round((lat_vect[i] - plant_geogrid_obj.y0) /
                            plant_geogrid_obj.step_y))
        lon_ind = int(round((lon_vect[i] - plant_geogrid_obj.x0) /
                            plant_geogrid_obj.step_x))

        if lat_ind >= lat_size or lon_ind >= lon_size:

            continue
        if lat_ind < 0 or lon_ind < 0:

            continue

        if footprint_type == 2:
            small_footprint = min_step >= footprint_array[i] / 2
        else:
            small_footprint = footprint_type == 0

        if not small_footprint:
            if footprint_type == 2:
                fp_x, fp_y = get_footprint_grid(footprint_array[i] / 2,
                                                plant_geogrid_obj.step_y,
                                                plant_geogrid_obj.step_x)
            lat_footprint_ind = lat_ind + fp_y
            lon_footprint_ind = lon_ind + fp_x
            valid = np.where(np.logical_and(
                np.logical_and((lat_footprint_ind >= 0),
                               (lon_footprint_ind >= 0)),
                np.logical_and(
                    (lat_footprint_ind <= (lat_size - 1)),
                    (lon_footprint_ind <= (lon_size - 1)))))
            lat_footprint_ind = np.asarray(lat_footprint_ind[valid],
                                           dtype=np.int32)
            lon_footprint_ind = np.asarray(lon_footprint_ind[valid],
                                           dtype=np.int32)
            ndata[lat_footprint_ind, lon_footprint_ind] += 1

        else:
            ndata[lat_ind, lon_ind] += 1
        for b in range(nbands):
            if not small_footprint:
                if loreys_height:

                    weight = plant.tree_diameter_from_height(data_vect[b][i],
                                                             E=E)**2
                    weight_sum[lat_footprint_ind, lon_footprint_ind] += weight
                data[b][lat_footprint_ind, lon_footprint_ind] += \
                    weight * data_vect[b][i]
            else:
                if loreys_height:

                    weight = plant.tree_diameter_from_height(data_vect[b][i],
                                                             E=E)**2
                    weight_sum[lat_ind, lon_ind] += weight
                data[b][lat_ind, lon_ind] += weight * data_vect[b][i]
    plant.print_progress(width, width, verbose=verbose)

    if verbose:
        print('100')
    if loreys_height:
        ind = np.where(weight_sum != 0)
        for b in range(nbands):
            data[b][ind] = data[b][ind] / weight_sum[ind]

    if not sum_samples:
        ind = np.where(ndata != 0)
        for b in range(nbands):
            data[b][ind] = data[b][ind] / ndata[ind]

    ind = np.where(ndata == 0)
    for b in range(nbands):
        data[b][ind] = out_null

    return data, ndata, plant_geogrid_obj.geotransform


def get_info_from_xml(xmlFile, filename=None):

    if not plant.isfile(xmlFile):
        return

    try:
        from iscesys.Parsers.FileParserFactory import createFileParser
        from isceobj.Util import key_of_same_content
    except ModuleNotFoundError:

        return

    PA = createFileParser('xml')
    try:
        dictNow, dictFact, dictMisc = PA.parse(xmlFile)
    except BaseException:
        return
    width = key_of_same_content('width', dictNow)[1]
    length = key_of_same_content('length', dictNow)[1]

    try:
        dtype = key_of_same_content('data_type', dictNow)[1]
    except BaseException:

        return
    nbands = key_of_same_content('number_bands', dictNow)[1]
    scheme = key_of_same_content('scheme', dictNow)[1]
    numpyType = plant.get_np_dtype(dtype)

    if filename is None:
        filename = xmlFile.replace('.xml', '')

    image_obj = plant.PlantImage(filename=filename,
                                 width=width,
                                 length=length,
                                 dtype=numpyType,
                                 nbands=nbands,
                                 scheme=scheme)
    return image_obj


def get_hdf5_dataset_metadata(hdf5_obj):
    metadata = {}
    for attr_key, attr_value in hdf5_obj.attrs.items():
        key = f'{hdf5_obj.name}#{attr_key}'
        if isinstance(attr_value, np.bytes_):
            metadata[key] = attr_value.decode()

        else:
            metadata[key] = attr_value

    return metadata


def _read_image_hdf5(hdf5_obj,
                     hdf5_file,
                     image_obj,

                     only_header=False,
                     plant_transform_obj=None,
                     flag_no_messages=False,
                     flag_exit_if_error=True,
                     out_null=None,
                     verbose=True,
                     force=False):

    try:
        dtype = hdf5_obj.dtype
    except TypeError:
        dtype = None

    if plant_transform_obj is not None:
        image_obj._applied_transform_obj_crop_list.append(
            plant_transform_obj.get_crop_dict())

    for b in range(image_obj.nbands):

        if not only_header and len(hdf5_obj.shape) == 0:
            image = np.asarray(hdf5_obj[()])
        elif not only_header and len(hdf5_obj.shape) == 1:
            image = hdf5_obj[image_obj.offset_x:
                             image_obj.offset_x + image_obj.width]
        elif not only_header and len(hdf5_obj.shape) == 2:
            image = hdf5_obj[image_obj.offset_y:
                             image_obj.offset_y + image_obj.length,
                             image_obj.offset_x:
                             image_obj.offset_x + image_obj.width]
        elif not only_header and len(hdf5_obj.shape) == 3:

            image = hdf5_obj[image_obj.band_orig[b],
                             image_obj.offset_y:
                             image_obj.offset_y + image_obj.length,
                             image_obj.offset_x:
                             image_obj.offset_x + image_obj.width]
        elif only_header:
            image = None
            if dtype is None:
                dtype = np.complex64
        else:
            error_message = ('ERROR not supported HDF5 objects with'
                             ' 4 dimensions or more')
            plant.handle_exception(error_message,
                                   flag_exit_if_error,
                                   flag_no_messages=flag_no_messages,
                                   verbose=verbose)
            return

        if dtype is None or 'void' in dtype.name.lower():
            if verbose and not flag_no_messages:
                print('WARNING there was an error reading data'
                      f' type of {image_obj.filename_orig}.'
                      ' Casting sub-dataset to complex64')
            dtype = np.complex64
            if image is not None:
                image = plant.read_c4_dataset_as_c8(image)

        if (image is not None and dtype is not None and
                (dtype == 'object' or '|S' in str(dtype))):
            image = np.array(image, dtype=str)

    band_obj = plant.PlantBand(image, dtype=dtype)
    if plant_transform_obj is not None and only_header:
        band_obj._applied_transform_obj_crop_header_list = \
            [plant_transform_obj.get_crop_dict()]
    elif plant_transform_obj is not None:
        band_obj._applied_transform_obj_crop_image_list = \
            [plant_transform_obj.get_crop_dict()]
    image_obj.set_band(band_obj, band=b, realize_changes=False)
    image_obj.set_null(out_null, realize_changes=False)

    if isinstance(hdf5_obj, h5py.Dataset):

        with plant.PlantIndent():
            metadata = get_hdf5_dataset_metadata(hdf5_obj)

            image_obj.metadata = metadata
            image_obj.set_metadata(metadata)

            metadata = get_hdf5_dataset_metadata(hdf5_obj)
            for metadata_key, metadata_value in metadata.items():
                cf_attribute = metadata_key.split('#')[-1]
                if 'long_name' not in cf_attribute:
                    continue
                name = metadata_value
                image_obj.name = name
                break

    plant.apply_mask(image_obj,
                     plant_transform_obj=plant_transform_obj,
                     verbose=verbose,
                     force=force)

    image_obj = prepare_image(image_obj,

                              flag_exit_if_error=flag_exit_if_error,
                              plant_transform_obj=plant_transform_obj,

                              out_null=out_null,

                              verbose=verbose,
                              only_header=only_header,
                              flag_no_messages=flag_no_messages,
                              force=force)
    return image_obj


def read_parameters_annotation_file(filename,
                                    eq_symbol='='):

    if not plant.isfile(filename):
        return
    parameters = None
    with open(filename, 'r', encoding='ISO-8859-1') as f:
        lines = f.readlines()
        parameters = {}
        for i, current_line in enumerate(lines):
            try:
                parameter, value = current_line.split(eq_symbol)
                value = value.replace('"', '')
                if ';' in value:
                    value = value[:value.index(';')]
                parameter = parameter.strip()
                if parameter[-1] == ')':
                    for i in range(len(parameter)):
                        if parameter[len(parameter) - 1 - i] == '(':
                            parameter = parameter[:len(parameter) - 1 - i]
                            parameter = parameter.strip()
                            break
                value = value.strip()
                if plant.isnumeric(value):
                    value = float(value)
                parameters[parameter] = value
            except BaseException:
                pass
    return parameters


def read_NOHRSC(filename,
                verbose=False):
    header_file = filename.replace('.dat', '.Hdr')
    if not plant.isfile(header_file):
        return
    if verbose:
        print('opening: %s (NOHRSC)'
              % filename)
    para_dict = read_parameters_annotation_file(header_file,
                                                eq_symbol=':')

    dtype = para_dict['Data type'].lower()

    type_size = int(para_dict['Data bytes per pixel'])

    if 'int' in dtype:
        dtype = 'uint' + str(type_size * 8)
    elif 'float' in dtype:
        dtype = 'float' + str(type_size * 8)
    elif 'complex' in dtype:
        dtype = 'complex' + str(type_size * 8)
    elif 'byte' in dtype:
        dtype = 'byte' + str(type_size * 8)

    description = para_dict['Description']
    if verbose:
        print('    description: ' + description)

    data_units = para_dict['Data units']
    if verbose:
        print('    data unit: ' + data_units)
    width = int(para_dict['Number of columns'])
    length = int(para_dict['Number of rows'])

    dummy_value = para_dict['No data value']

    lon_beg = para_dict['Minimum x-axis coordinate']
    lon_end = para_dict['Maximum x-axis coordinate']

    lat_beg = para_dict['Minimum y-axis coordinate']
    lat_end = para_dict['Maximum y-axis coordinate']

    step_x = para_dict['X-axis resolution']
    step_y = para_dict['Y-axis resolution']
    geotransform = plant.get_geotransform(

        y0=lat_end,
        yf=lat_beg,
        x0=lon_beg,
        xf=lon_end,
        step_y=step_y,
        step_x=step_x)

    image_obj = plant.PlantImage(
        filename=filename,
        width=width,
        length=length,
        dtype=dtype,
        description=description,

        null=dummy_value,
        geotransform=geotransform)

    return image_obj


def _read_image_ann(filename_orig,
                    filename,
                    band=None,
                    verbose=True,
                    input_format=None,
                    only_header=True,
                    read_only=True,

                    header_file=None,
                    header_dict=None,

                    plant_transform_obj=None,
                    metadata=None,
                    in_null=None,
                    out_null=None,
                    input_key=None,
                    force=None,
                    flag_no_messages=False,
                    flag_exit_if_error=True,
                    image_obj_kwargs={},
                    **kwargs_orig):
    kwargs = locals()
    args = []
    args.append(kwargs.pop('filename_orig'))
    args.append(kwargs.pop('filename'))
    ext = filename.split('.')[-1]
    error_message = ('data dimensions of %s '
                     'could not be determined'
                     % filename)
    if not ((input_format is not None and
             'ANN' in input_format) or
            ext in plant.EXT_READ_FROM_ANN_FILE):
        return
    if header_dict is None:
        header_dict = plant.parse_uavsar_filename(filename)
    if header_file is None:
        header_file = plant.get_annotation_file(
            header_dict, dirname=os.path.dirname(filename))

    if header_file is None:
        header_file_search_string = \
            os.path.join(os.path.dirname(filename), '*ann')
        header_file_list = plant.glob(header_file_search_string)
        if len(header_file_list) == 0:
            if filename.endswith('.dat'):
                return
            error_message = f'annotation file not found for {filename}'
            plant.handle_exception(error_message,
                                   flag_exit_if_error,
                                   flag_no_messages=flag_no_messages,
                                   verbose=verbose)

            return

    if header_file is None:
        header_file = header_file_list[0]
    elif isinstance(header_file, list):
        header_file = header_file[0]

    if header_dict is None:
        header_dict = plant.parse_uavsar_filename(header_file)

    if metadata is None:
        try:
            metadata = read_parameters_annotation_file(header_file)
        except BaseException:
            pass

    if not metadata:
        plant.handle_exception(error_message,
                               flag_exit_if_error,
                               flag_no_messages=flag_no_messages,
                               verbose=verbose)
        return

    if ext and ext.lower() == 'llh' or ext.lower() == 'lkv':
        nbands_orig = 3
        scheme = 'BIP'
    else:
        nbands_orig = 1
        scheme = 'BSQ'

    length_orig = None
    width_orig = None
    flag_geo = ext in plant.EXT_READ_FROM_ANN_FILE_GEO
    file_size = os.stat(filename).st_size
    complement_list = ['_pwr', '_mag', '', '_phase']
    suffix = ''
    if 'segment_number' in header_dict.keys():
        suffix += header_dict['segment_number'][1:]
    if 'downsample_factor' in header_dict.keys():
        if suffix:
            suffix += '_'
        suffix += header_dict['downsample_factor']
    if suffix:
        suffix = '_' + suffix
        for complement in reversed(complement_list[:]):
            complement_list.insert(0, suffix + complement)

    if ext.lower() == 'sch':
        try:
            length_orig = int(metadata['SCH Number of Along Track Lines'])
            width_orig = int(metadata['SCH Number of Cross Track Samples'])
        except KeyError:
            pass
    elif flag_geo:
        try:
            length_orig = int(metadata['GRD Latitude Lines'])
            width_orig = int(metadata['GRD Longitude Samples'])
        except KeyError:
            pass

    if width_orig is None:
        for complement in complement_list:
            try:
                length_orig = int(metadata[ext + complement +
                                           '.set_rows'])
                width_orig = int(metadata[ext + complement +
                                          '.set_cols'])
                break
            except KeyError:
                pass

    if width_orig is None and not flag_geo:
        for complement in complement_list:
            ext_effective = 'slt'
            try:
                length_orig = int(metadata[ext_effective + complement +
                                           '.set_rows'])
                width_orig = int(metadata[ext_effective + complement +
                                          '.set_cols'])
                break
            except KeyError:
                pass

    if width_orig is None:
        file_list = plant.glob(os.path.join(plant.dirname(filename), '*'))
        similar_file = None
        if ext == 'inc':
            similar_file_ext_list = ['hgt']
        elif ext.endswith('grd'):
            similar_file_ext_list = ['grd']
        else:
            similar_file_ext_list = ['slc']
        for similar_file_ext in similar_file_ext_list:
            for complement in complement_list:
                try:
                    length_orig = int(metadata[similar_file_ext +
                                               complement +
                                               '.set_rows'])
                    width_orig = int(metadata[similar_file_ext +
                                              complement +
                                              '.set_cols'])
                    break
                except KeyError:
                    pass
    if width_orig is None:
        plant.handle_exception(error_message,
                               flag_exit_if_error,
                               flag_no_messages=flag_no_messages,
                               verbose=verbose)
        return

    type_size = file_size / (length_orig * width_orig * nbands_orig)

    if type_size == 1:
        dtype_str = 'byte'
    elif type_size == 2:
        dtype_str = 'int16'
    elif type_size == 4:
        dtype_str = 'float32'
    elif type_size == 8:
        dtype_str = 'complex64'
    elif type_size == 16:
        dtype_str = 'complex128'
    else:
        plant.handle_exception(error_message,
                               flag_exit_if_error,
                               flag_no_messages=flag_no_messages,
                               verbose=verbose)

        return

    geotransform = None

    y0 = None
    yf = None
    x0 = None
    xf = None

    if flag_geo:
        try:
            lat = float(metadata['GRD Starting Latitude'])
            lon = float(metadata['GRD Starting Longitude'])
            step_y = float(metadata['GRD Latitude Spacing'])
            step_x = float(metadata['GRD Longitude Spacing'])
            y0 = lat - step_y / 2.0
            yf = lat - step_y / 2.0 + step_y * length_orig
            x0 = lon - step_x / 2.0
            xf = lon - step_x / 2.0 + step_x * width_orig

        except BaseException:
            pass

    if any([plant.isnan(v) for v in [y0, yf, x0, xf]]) and flag_geo:
        for complement in complement_list:
            try:
                lat = float(metadata['grd' + complement +
                                     '.row_addr'])
                lon = float(metadata['grd' + complement +
                                     '.col_addr'])
                step_y = float(metadata['grd' + complement +
                                        '.row_mult'])
                step_x = float(metadata['grd' + complement +
                                        '.col_mult'])

                y0 = lat - step_y / 2.0
                yf = lat - step_y / 2.0 + step_y * length_orig
                x0 = lon - step_x / 2.0
                xf = lon - step_x / 2.0 + step_x * width_orig

            except BaseException:
                pass

    if (any([plant.isnan(v) for v in [y0, yf, x0, xf]]) and
            ext in plant.EXT_READ_FROM_ANN_FILE_GEO):
        for complement in complement_list:
            try:
                lat = float(metadata[ext + complement +
                                     '.row_addr'])
                lon = float(metadata[ext + complement +
                                     '.col_addr'])
                step_y = float(metadata[ext + complement +
                                        '.row_mult'])
                step_x = float(metadata[ext + complement +
                                        '.col_mult'])

                y0 = lat - step_y / 2.0
                yf = lat - step_y / 2.0 + step_y * length_orig
                x0 = lon - step_x / 2.0
                xf = lon - step_x / 2.0 + step_x * width_orig

            except BaseException:
                pass

    if all([plant.isvalid(v) for v in [y0, yf, x0, xf]]):
        geotransform = plant.get_geotransform(

            y0=y0,
            yf=yf,
            x0=x0,
            xf=xf,
            step_y=step_y,
            step_x=step_x)
        if step_y < 0:
            geotransform = plant.get_geotransform(
                geotransform=geotransform,
                width=width_orig,
                length=length_orig)
        geotransform, length, width = plant.crop_geotransform(
            geotransform,
            plant_transform_obj=plant_transform_obj,

            width=width_orig,
            length=length_orig)
        if plant.isnan(length):
            length = length_orig
        if plant.isnan(width):
            width = width_orig

    else:
        length = length_orig
        width = width_orig

    if band is None:
        band_orig = None
        nbands = nbands_orig
    elif isinstance(band, Sequence) or isinstance(band, np.ndarray):
        band_orig = band
        nbands = len(band)
    else:
        band_orig = [band]
        nbands = 1
    image_obj = plant.PlantImage(
        filename=filename,
        filename_orig=filename_orig,
        header_file=header_file,

        nbands=nbands,
        nbands_orig=nbands_orig,
        width=width,
        length=length,
        input_key=input_key,
        band_orig=band_orig,

        dtype=dtype_str,
        plant_transform_obj=plant_transform_obj,
        file_format='BIN',

        scheme=scheme)
    kwargs['image_obj'] = image_obj
    kwargs['band'] = band
    kwargs['verbose'] = False

    kwargs.pop('header_dict')
    kwargs.pop('header_file')
    if verbose and not only_header and band is None:
        print(f'opening: {filename} (annotation file)')
    elif verbose and not only_header:
        print(f'opening: {filename} (annotation file, band: {band})')

    image_obj = _read_image_bin(*args, **kwargs)

    if (image_obj is not None and plant_transform_obj is not None and
            only_header):

        image_obj.get_band(band=0)._applied_transform_obj_crop_header_list = \
            [plant_transform_obj.get_crop_dict()]

    elif image_obj is not None and plant_transform_obj is not None:

        image_obj.get_band(band=0)._applied_transform_obj_crop_image_list = \
            [plant_transform_obj.get_crop_dict()]

    if image_obj is not None and geotransform is not None:
        image_obj.set_geotransform(geotransform,
                                   realize_changes=False)

    if (plant.isnan(in_null) and
            ((filename.endswith('.hgt') or
              filename.endswith('.hgt.' + ext)) or
             (filename.endswith('.inc') or
              filename.endswith('.inc.' + ext)))):
        in_null = -10000
    elif (plant.isnan(in_null) and
            ((filename.endswith('.slope') or
              filename.endswith('.slope.' + ext)))):
        in_null = -10000 - 1j * 10000
    elif (plant.isnan(in_null) and
          (filename.endswith('.grd') or
           filename.endswith('.grd.' + ext) or
           filename.endswith('.cor') or
           filename.endswith('.cor.' + ext) or
           filename.endswith('.rtc') or
           filename.endswith('.rtc.' + ext) or
           filename.endswith('.rtcgrd') or
           filename.endswith('.rtcgrd.' + ext) or
           filename.endswith('.mlc') or
           filename.endswith('.mlc.' + ext) or
           filename.endswith('.slc') or
           filename.endswith('.slc.' + ext) or
           filename.endswith('.pwr') or
           filename.endswith('.pwr.' + ext))):
        in_null = 0
    image_obj.file_format = 'ANN'

    plant.apply_null(image_obj,
                     in_null=in_null,
                     out_null=out_null)
    return image_obj


def get_annotation_file(input_dict, ext=None, dirname=None):
    if input_dict is None:
        return
    ext = '.ann' if ext is None else f'*{ext}'.replace('..', '.')
    ext = ext.lower()
    site_name = input_dict.get('site_name', '*')
    line_id = input_dict.get('line_id', '*')
    flight_id = input_dict.get('flight_id', '*')
    data_take_number = input_dict.get('data_take_number', '*')
    acquisition_date = input_dict.get('acquisition_date', '*')
    stack_number = input_dict.get('stack_number', '*')

    baseline_corrected = input_dict.get('baseline_corrected', False)
    baseline_uncorrected = input_dict.get('baseline_uncorrected', False)
    baseline_corrected_str = ''
    if baseline_corrected:
        baseline_corrected_str = '_BC'
    elif baseline_uncorrected:
        baseline_corrected_str = '_BU'

    band = input_dict.get('band', '*')
    steering = input_dict.get('steering', '*')
    polarization = input_dict.get('polarization', '*')
    downsample_factor = input_dict.get('downsample_factor', None)
    segment_number = input_dict.get('segment_number', None)
    list_ext = []
    if 'dop' not in ext and segment_number:
        list_ext += [segment_number]
    if 'dop' not in ext and downsample_factor:
        list_ext += [downsample_factor]
    if len(list_ext) == 0:
        extended_ext = ext
    else:
        extended_ext = '_' + '_'.join(list_ext) + ext
    if 'slc' in ext:
        annotation_file = (f'{site_name}_{line_id}_{flight_id}'
                           f'_{data_take_number}_{acquisition_date}'
                           f'_{band}{steering}{polarization}'
                           f'_{stack_number}'

                           f'{baseline_corrected_str}{extended_ext}')
    elif 'llh' in ext or 'lkv' in ext:
        annotation_file = (f'{site_name}_{line_id}_{stack_number}'
                           f'{baseline_corrected_str}{extended_ext}')
    elif 'dop' in ext:
        annotation_file = (f'{site_name}_{line_id}_{stack_number}'
                           f'{baseline_corrected_str}{extended_ext}')
    else:
        annotation_file = (f'{site_name}_{line_id}_{flight_id}'
                           f'_{data_take_number}_{acquisition_date}'
                           f'_{band}{steering}*_{stack_number}'
                           f'{baseline_corrected_str}{extended_ext}')

    annotation_file = '*'.join([x for x in annotation_file.split('*') if x])
    if dirname is not None:
        annotation_file = os.path.join(dirname, annotation_file)
    annotation_file_list = plant.glob(annotation_file)

    if len(annotation_file_list) == 0:
        return
    if len(annotation_file_list) == 1:
        return annotation_file_list[0]
    return annotation_file_list


def _read_image_gdal(filename_orig,
                     filename,
                     **kwargs):
    kwargs['filename'] = filename
    kwargs['filename_orig'] = filename_orig

    current_dataset = None
    gdal.UseExceptions()
    gdal.ErrorReset()
    try:
        current_dataset = gdal.Open(filename)
    except KeyboardInterrupt:
        current_dataset = None
        gdal.ErrorReset()
        raise plant.PlantExceptionKeyboardInterrupt
    except BaseException:
        current_dataset = None
        gdal.ErrorReset()
        return

    image_obj = read_image_from_gdal_dataset(
        current_dataset,
        **kwargs)
    return image_obj


def _get_hdf5_sub_dataset_reference(filename, sub_dataset_path):
    netcdf_reference = f'NETCDF:{filename}:{sub_dataset_path}'
    try:
        gdal.PushErrorHandler('CPLQuietErrorHandler')
        gdal_ds = gdal.Open(netcdf_reference)

        if gdal_ds is not None:
            if plant.valid_coordinates(
                    geotransform=gdal_ds.GetGeoTransform(),
                    width=gdal_ds.RasterXSize,
                    length=gdal_ds.RasterYSize):
                gdal.PopErrorHandler()
                return netcdf_reference
    except BaseException:
        pass
    gdal.PopErrorHandler()
    return f'HDF5:{filename}:{sub_dataset_path}'


def _hdf5_get_sub_datasets(h5py_obj, sub_datasets_keys, sub_datasets,
                           filename):

    for s in sub_datasets_keys:

        try:
            h5py_obj_sub_dataset = h5py_obj[s]
        except KeyError:
            print(f'WARNING there was an error retrieving sub-dataset: {s}')
            continue

        if ('keys' not in h5py_obj_sub_dataset.__dir__() and
                isinstance(h5py_obj_sub_dataset, h5py._hl.datatype.Datatype)):
            name = ''
            try:
                name += '(' + str(h5py_obj_sub_dataset.dtype) + ')'
            except TypeError:
                pass
            name += f' datatype: {h5py_obj_sub_dataset.name}'
            sub_dataset_path = h5py_obj.name + '/' + s

            sub_dataset_reference = f'HDF5:{filename}:{sub_dataset_path}'
            sub_datasets.append((sub_dataset_reference, name))
            continue
        elif 'keys' not in h5py_obj_sub_dataset.__dir__():
            shape = [str(x) for x in h5py_obj_sub_dataset.shape]
            name = ''

            try:
                name += '(' + str(h5py_obj_sub_dataset.dtype) + ')'
            except TypeError:
                pass
            test_for_netcdf_driver = False
            if len(shape) > 0:
                if name:
                    name += ' '
                name += f'{len(shape)}-D array [' + 'x'.join(shape) + ']'
                test_for_netcdf_driver = len(shape) >= 2
            if len(shape) == 0:
                try:
                    name += f' {h5py_obj_sub_dataset[()]}'
                except TypeError:
                    name += f' {h5py_obj_sub_dataset}'

            sub_dataset_path = h5py_obj.name + '/' + s
            if test_for_netcdf_driver:
                sub_dataset_reference = _get_hdf5_sub_dataset_reference(
                    filename, sub_dataset_path)
            else:
                sub_dataset_reference = f'HDF5:{filename}:{sub_dataset_path}'
            sub_datasets.append((sub_dataset_reference, name))

            continue
        sub_datasets_keys = list(h5py_obj_sub_dataset.keys())

        sub_datasets_keys = [s + '/' + k for k in sub_datasets_keys]

        _hdf5_get_sub_datasets(h5py_obj, sub_datasets_keys, sub_datasets,
                               filename)


def _get_nisar_band_and_product_type_from_hdf5_obj(hdf5_obj, input_key):
    for freq_band in plant.NISAR_BAND_LIST:
        for product in plant.NISAR_PRODUCT_LIST:
            current_key = (os.path.join('//', 'science', freq_band, product,
                                        f'frequency{input_key}'))
            if current_key in hdf5_obj:
                product_key_group = hdf5_obj[current_key]
                return freq_band, product, product_key_group


def get_nisar_subdatasets(hdf5_file, input_key, band=None):

    if isinstance(hdf5_file, str):
        hdf5_obj = h5py.File(hdf5_file, 'r')
    else:
        hdf5_obj = hdf5_file
    ret = _get_nisar_band_and_product_type_from_hdf5_obj(hdf5_obj, input_key)
    if ret is None:
        return

    freq_band, product, product_key_group = ret

    if 'listOfPolarizations' not in product_key_group:
        print(
            f'ERROR not found listOfPolarizations group in file: {hdf5_file}')

    list_of_polarizations = product_key_group['listOfPolarizations']

    if band is None:
        nbands = len(list_of_polarizations)
        band_indexes = list(range(nbands))
    else:
        band_indexes = plant.get_int_list(band)
        nbands = len(band_indexes)
    list_of_polarizations = list_of_polarizations[:][band_indexes]
    subdatasets_dict = {}
    for b, pol_encoded in enumerate(list_of_polarizations):
        pol = pol_encoded.decode().upper()
        current_key = (f'//science/{freq_band}/{product}/'
                       f'frequency{input_key}/{pol}')
        if current_key not in hdf5_obj:
            current_key = (f'//science/{freq_band}/{product}/'
                           f'frequency{input_key}/{pol}{pol}')
        subdatasets_dict[pol] = current_key

    return subdatasets_dict


def _read_hdf5(filename_orig,
               filename,
               input_format=None,
               only_header=False,

               input_key=None,
               band=None,

               plant_transform_obj=None,

               out_null=None,
               force=False,
               verbose=True,
               flag_no_messages=False,
               flag_exit_if_error=False,
               image_obj_kwargs={},
               **kwargs_orig):

    kwargs = locals()
    length_orig = 1
    width_orig = 1

    ret_dict = plant.parse_filename(filename_orig)
    filename = ret_dict['filename']

    if input_key is None:
        input_key = ret_dict['key']

    flag_single_subdataset = input_key is not None

    hdf5_obj = None
    if flag_single_subdataset:
        if verbose:
            print(f'opening: {filename} (HDF5: {input_key})')
        hdf5_file = h5py.File(filename, 'r')
        if input_format == 'NISAR' and input_key.upper() in ['A', 'B']:
            with plant.PlantIndent():
                subdatasets_dict = get_nisar_subdatasets(hdf5_file,
                                                         input_key,
                                                         band)
                del hdf5_file
                if subdatasets_dict is None:
                    print('ERROR NISAR product not found:'
                          f' {input_key} in {filename}')
                    return

                for b, pol in enumerate(subdatasets_dict.keys()):
                    current_key = subdatasets_dict[pol]

                    if ('GCOV' in current_key or
                            'GSLC' in current_key or
                            'GUNW' in current_key):
                        driver = 'NETCDF'
                    else:
                        driver = 'HDF5'
                    kwargs['input_format'] = driver
                    kwargs['input_key'] = current_key

                    kwargs.pop('band', None)
                    kwargs['filename_orig'] = (driver + ':' +
                                               filename + ':' +
                                               current_key)

                    if driver == 'NETCDF':
                        image_obj = read_image(**kwargs)
                    else:
                        image_obj = _read_hdf5(**kwargs)

                    if image_obj is None:
                        return

                    if b == 0:
                        out_image_obj = image_obj.soft_copy()

                    out_image_obj.set_band(image_obj.band, band=b,
                                           realize_changes=False)
                    out_image_obj.set_name(pol, band=b)

                out_image_obj.nbands_orig = len(subdatasets_dict)
                out_image_obj._band_orig = np.arange(
                    out_image_obj._nbands_orig)
                out_image_obj.file_format = 'NISAR'
                out_image_obj.input_key = input_key
                return out_image_obj

        nbands_orig = 1
        try:
            hdf5_obj = hdf5_file[input_key]
        except KeyError:
            print(f'ERROR sub-dataset {input_key} not found'
                  f' in {filename}')
            return
        flag_single_subdataset = not isinstance(hdf5_obj, h5py.Group)

    if not flag_single_subdataset:
        nbands_orig = 0
        sub_datasets = []
        if hdf5_obj is None:
            hdf5_obj = h5py.File(filename, 'r')
        sub_datasets_keys = list(hdf5_obj.keys())

        _hdf5_get_sub_datasets(hdf5_obj, sub_datasets_keys, sub_datasets,
                               filename=filename)

        error_message = ('the input has multiple sub-datasets.'
                         ' Please, select one sub-dataset'
                         ' from the list above.')
        error_description = ''
        if not flag_no_messages:

            for i, sub_dataset in enumerate(sub_datasets):
                for j, element in enumerate(sub_dataset):

                    prefix = '' if j == 0 else ' ' * 4
                    error_description += f'{prefix}{element}\n'

        plant.handle_exception(error_message,
                               flag_exit_if_error,
                               error_description=error_description,
                               flag_no_messages=flag_no_messages,
                               verbose=verbose)
        return

    if len(hdf5_obj.shape) == 1:
        width_orig = hdf5_obj.shape[0]
    elif len(hdf5_obj.shape) == 2:
        length_orig = hdf5_obj.shape[0]
        width_orig = hdf5_obj.shape[1]
    elif len(hdf5_obj.shape) == 3:

        length_orig = hdf5_obj.shape[1]
        width_orig = hdf5_obj.shape[2]
        nbands_orig = hdf5_obj.shape[0]
    elif len(hdf5_obj.shape) != 0:
        raise NotImplementedError

    geotransform = None
    projection = None
    flag_rotate_and_flip_x = False
    flag_flip_y = False

    try:
        kwargs_netcdf_driver = kwargs.copy()
        kwargs_netcdf_driver['only_header'] = True
        kwargs_netcdf_driver['input_format'] = 'netCDF'
        kwargs_netcdf_driver['verbose'] = False
        image_netcdf_obj = plant.read_image(**kwargs_netcdf_driver)
        if image_netcdf_obj.geotransform is not None:
            geotransform = image_netcdf_obj.geotransform
            projection = image_netcdf_obj.projection
            metadata = image_netcdf_obj.metadata

            grid_header_metadata_key = None
            for k in metadata.keys():
                if 'GridHeader' not in k:
                    continue
                grid_header_metadata_key = k

            if grid_header_metadata_key:

                grid_header = metadata[grid_header_metadata_key]
                ret_dict = _read_geotransform_from_grid_header(
                    grid_header,
                    length_orig,
                    width_orig,
                    flag_rotate_and_flip_x,
                    flag_flip_y,
                    only_header)
                if ret_dict is not None:

                    if 'geotransform' in ret_dict.keys():
                        geotransform = ret_dict['geotransform']
                    if 'flag_flip_y' in ret_dict.keys():
                        flag_flip_y = ret_dict['flag_flip_y']
                    if 'flag_rotate_and_flip_x' in ret_dict.keys():
                        flag_rotate_and_flip_x = ret_dict['flag_rotate_and_flip_x']

            elif verbose:
                print('INFO geoinformation found from NetCDF driver.')
    except BaseException:
        pass

    if band is None:
        nbands = nbands_orig
        band_indexes = list(range(nbands))

    else:
        band_indexes = plant.get_int_list(band)
        nbands = len(band_indexes)

    if flag_rotate_and_flip_x or flag_flip_y:

        image_stack = []
        input_key_with_double_slash = input_key
        while not input_key_with_double_slash.startswith('//'):
            input_key_with_double_slash = '/' + input_key_with_double_slash
        temp_filename = f'HDF5:{filename}:{input_key_with_double_slash}'
        gdal_dataset = gdal.Open(temp_filename)

        if flag_rotate_and_flip_x:
            length_orig, width_orig = width_orig, length_orig

        if (not only_header and (len(hdf5_obj.shape) == 2 or
                                 len(hdf5_obj.shape) == 3)):
            if len(hdf5_obj.shape) == 2:
                range_nbands = range(nbands)
            else:
                original_shape = hdf5_obj.shape
                range_nbands = range(hdf5_obj.shape[0])
            for b in range_nbands:
                current_band = gdal_dataset.GetRasterBand(b + 1)
                no_data = current_band.GetNoDataValue()
                if len(hdf5_obj.shape) == 2:
                    image = hdf5_obj[:, :]
                else:
                    image = hdf5_obj[b, :, :]
                if flag_rotate_and_flip_x:
                    image = np.rot90(image)
                    image = np.flip(image, axis=1)
                if flag_flip_y:
                    image = np.flip(image, axis=0)
                plant.apply_null(image,
                                 in_null=no_data)
                if len(hdf5_obj.shape) > 2:
                    image_stack.append(image)
            if len(hdf5_obj.shape) == 2:
                hdf5_obj = image
            else:
                hdf5_obj = np.hstack(image_stack).reshape(original_shape)

        current_band = None
        gdal_dataset = None
        gdal.ErrorReset()

    if plant_transform_obj is not None:
        plant_transform_obj.update_crop_window(length_orig=length_orig,
                                               width_orig=width_orig,
                                               geotransform=geotransform,
                                               projection=projection)
        crop_window = plant_transform_obj.crop_window
        offset_x, offset_y, width, length = crop_window
    else:
        offset_x = 0
        offset_y = 0
        width = width_orig
        length = length_orig

    image_obj = plant.PlantImage(
        filename=filename,
        filename_orig=filename_orig,
        width=width,
        length=length,
        width_orig=width_orig,
        length_orig=length_orig,
        offset_x=offset_x,
        offset_y=offset_y,

        nbands=nbands,
        nbands_orig=nbands_orig,
        band_orig=band_indexes,
        input_key=input_key,

        file_format=input_format,
        plant_transform_obj=plant_transform_obj,
        geotransform=geotransform,
        projection=projection,

        **image_obj_kwargs)

    image_obj = _read_image_hdf5(hdf5_obj,
                                 hdf5_file,
                                 image_obj,
                                 only_header=only_header,
                                 plant_transform_obj=plant_transform_obj,
                                 out_null=out_null,
                                 verbose=verbose,
                                 force=force)
    return image_obj


def _read_geotransform_from_grid_header(grid_header,
                                        length_orig,
                                        width_orig,
                                        flag_rotate_and_flip_x,
                                        flag_flip_y,
                                        only_header):
    ret_dict = None
    try:
        grid_header = grid_header.replace('\n', '')
        grid_header_splitted = grid_header.split(';')
        grid_header_dict = {}
        for element in grid_header_splitted:
            element_splitted = element.split('=')
            if len(element_splitted) != 2:
                continue
            if plant.isnumeric(element_splitted[1]):
                value = float(element_splitted[1])
            else:
                value = element_splitted[1]
            grid_header_dict[element_splitted[0]] = value
        step_y = grid_header_dict['LatitudeResolution']
        step_x = grid_header_dict['LongitudeResolution']
        lat_arr = [grid_header_dict['SouthBoundingCoordinate'],
                   grid_header_dict['NorthBoundingCoordinate']]
        lon_arr = [grid_header_dict['WestBoundingCoordinate'],
                   grid_header_dict['EastBoundingCoordinate']]
        length_temp = (lat_arr[1] - lat_arr[0]) / step_y
        width_temp = (lon_arr[1] - lon_arr[0]) / step_x
        if length_temp == width_orig and width_temp == length_orig:
            flag_rotate_and_flip_x = True
            length_orig = width_temp
            width_orig = length_temp
            if not only_header:
                print('WARNING dimensions seem to be rotated.'
                      ' Rotating image...')
        geotransform = plant.get_geotransform(lat_arr=lat_arr,
                                              lon_arr=lon_arr,
                                              step_y=step_y,
                                              step_x=step_x)
        if 'CENTER' in grid_header_dict['Registration'].upper():
            geotransform = plant.geotransform_centers_to_edges(
                geotransform,
                length=length_orig,
                width=width_orig)

        if 'NORTH' in grid_header_dict['Origin'].upper():
            flag_flip_y = not flag_flip_y
        ret_dict = {}
        ret_dict['geotransform'] = geotransform
        ret_dict['flag_rotate_and_flip_x'] = flag_rotate_and_flip_x
        ret_dict['flag_flip_y'] = flag_flip_y
    except BaseException:
        print('WARNING there was an error parsing the metadata'
              ' grid header: "GridHeader"')

    return ret_dict


def read_image_from_gdal_dataset(
        dataset,
        input_format=None,
        only_header=False,

        input_key=None,
        band=None,
        filename=None,
        filename_orig=None,

        plant_transform_obj=None,
        in_null=None,
        out_null=None,
        force=False,
        verbose=True,
        flag_no_messages=False,
        flag_exit_if_error=False,
        image_obj_kwargs={},
        **kwargs_orig):

    if input_format is None:
        input_format = 'GDAL'

    file_format = str(dataset.GetDriver().ShortName)
    sub_datasets = dataset.GetSubDatasets()
    if 'HDF5' in file_format.upper():

        length_orig = 1
        width_orig = 1

        ret_dict = plant.parse_filename(filename_orig)

        filename = ret_dict['filename']

        if input_key is None:
            input_key = ret_dict['key']
        if input_key is not None:
            if verbose:
                print(f'opening: {filename} (HDF5: {input_key})')
            nbands_orig = 1
            hdf5_obj = h5py.File(filename, 'r')[input_key]

            if len(hdf5_obj.shape) == 1:
                width_orig = hdf5_obj.shape[0]
            elif len(hdf5_obj.shape) == 2:
                length_orig = hdf5_obj.shape[0]
                width_orig = hdf5_obj.shape[1]
            elif len(hdf5_obj.shape) == 3:

                length_orig = hdf5_obj.shape[1]
                width_orig = hdf5_obj.shape[2]
                nbands_orig = hdf5_obj.shape[0]
            elif len(hdf5_obj.shape) != 0:
                raise NotImplementedError
        else:
            nbands_orig = 0
            sub_datasets = []
            h5py_obj = h5py.File(filename, 'r')
            sub_datasets_keys = list(h5py_obj.keys())

            _hdf5_get_sub_datasets(h5py_obj, sub_datasets_keys, sub_datasets,
                                   filename=filename)

    else:
        try:
            width_orig = dataset.RasterXSize
        except BaseException:
            width_orig = None
        if (width_orig is None and
                input_format == 'GDAL'):
            dataset = None
            gdal.ErrorReset()
            plant.handle_exception('opening file %s (GDAL)'
                                   % filename,
                                   flag_exit_if_error,
                                   flag_no_messages=flag_no_messages,
                                   verbose=verbose)
            return
        elif width_orig is None:
            return
        length_orig = dataset.RasterYSize
        nbands_orig = dataset.RasterCount

        if plant.isnumeric(input_key):

            width_orig_no_overview = width_orig
            length_orig_no_overview = length_orig
            band_obj = dataset.GetRasterBand(1)
            overview_obj = band_obj.GetOverview(int(input_key))
            if overview_obj is None:
                dataset = None
                gdal.ErrorReset()
                plant.handle_exception(
                    f'opening overview {input_key} of file {filename} (GDAL)',
                    flag_exit_if_error,
                    flag_no_messages=flag_no_messages,
                    verbose=verbose)
                return

            width_orig = overview_obj.XSize
            length_orig = overview_obj.YSize
            nbands_orig = dataset.RasterCount

    if band is None:
        nbands = nbands_orig
        band_indexes = list(range(nbands))

    else:
        band_indexes = plant.get_int_list(band)
        nbands = len(band_indexes)

    metadata = dataset.GetMetadata()

    projection = dataset.GetProjectionRef()

    if 'LOCAL_CS["Arbitrary"' in projection:
        projection = None

    geotransform = list(dataset.GetGeoTransform())

    if plant.isnumeric(input_key):

        geotransform[1] *= width_orig_no_overview / width_orig
        geotransform[5] *= length_orig_no_overview / length_orig

    gcp_list = dataset.GetGCPs()
    try:
        gcp_projection = dataset.GetGCPProjection()
    except RuntimeError:

        gcp_projection = None

    valid_geotransform = \
        plant.valid_coordinates(geotransform=geotransform,
                                length=length_orig,
                                width=width_orig,
                                projection=projection)

    if (not valid_geotransform and (plant.isfile(filename + '.xml') or
                                    filename.endswith('.xml'))):

        geotransform_temp = plant.get_geotransform_from_header(filename)
        if not plant.valid_coordinates(geotransform=geotransform_temp,
                                       length=length_orig,
                                       width=width_orig,
                                       projection=projection):
            geotransform = None
        else:
            geotransform = geotransform_temp
    elif not valid_geotransform:

        geotransform = None

    if plant_transform_obj is None:
        plant_transform_obj = plant.PlantTransform(verbose=verbose)

    if geotransform is not None and plant_transform_obj is not None:
        plant_transform_obj.update_crop_window(geotransform=geotransform,
                                               projection=projection,
                                               length_orig=length_orig,
                                               width_orig=width_orig)

    crop_window = plant_transform_obj.crop_window

    offset_x, offset_y, width, length = crop_window
    read_as_array_kwargs = {}
    if offset_x is not None:
        read_as_array_kwargs['xoff'] = int(offset_x)
    if offset_y is not None:
        read_as_array_kwargs['yoff'] = int(offset_y)
    if width is not None:
        read_as_array_kwargs['win_xsize'] = int(width)

    if length is not None:
        read_as_array_kwargs['win_ysize'] = int(length)

    flag_rotate_and_flip_x = False
    flag_flip_y = False
    grid_header_metadata_key = None

    if ('NETCDF' in file_format.upper() or 'HDF5' in file_format.upper()):
        for k in metadata.keys():
            if 'GridHeader' not in k:
                continue
            grid_header_metadata_key = k

        if grid_header_metadata_key:
            if verbose:
                print('INFO found GridHeader in metadata.')
            grid_header = metadata[grid_header_metadata_key]
            ret_dict = _read_geotransform_from_grid_header(
                grid_header,
                length_orig,
                width_orig,
                flag_rotate_and_flip_x,
                flag_flip_y,
                only_header)
            if ret_dict is not None:

                if 'geotransform' in ret_dict.keys():
                    geotransform = ret_dict['geotransform']
                if 'flag_flip_y' in ret_dict.keys():
                    flag_flip_y = ret_dict['flag_flip_y']
                if 'flag_rotate_and_flip_x' in ret_dict.keys():
                    flag_rotate_and_flip_x = ret_dict['flag_rotate_and_flip_x']

    if ('HDF5' not in file_format.upper() and
            band_indexes and
            np.max(band_indexes) + 1 > dataset.RasterCount):

        gdal.ErrorReset()
        plant.handle_exception('invalid band: %d '
                               '(number of available bands: %d)'
                               % (np.max(band_indexes),
                                  dataset.RasterCount),
                               flag_exit_if_error,
                               flag_no_messages=flag_no_messages,
                               verbose=verbose)
        dataset = None
        gdal.ErrorReset()

        return

    if band_indexes:

        current_band = dataset.GetRasterBand(band_indexes[0] + 1)
    else:
        current_band = dataset.GetRasterBand(1)

    if plant.isnumeric(input_key):

        current_band = band_obj.GetOverview(int(input_key))

    if current_band is None and sub_datasets and not input_key:

        error_message = ('indeterminate HDF5 subset. Please,'
                         ' select a sub-dataset: \n')
        if not flag_no_messages:

            for i, sub_dataset in enumerate(sub_datasets):
                for j, element in enumerate(sub_dataset):

                    prefix = '' if j == 0 else ' ' * 4
                    error_message += f'{prefix}{element}\n'

        plant.handle_exception(error_message,
                               flag_exit_if_error,
                               flag_no_messages=flag_no_messages,
                               verbose=verbose)
        return

    if current_band is None and not input_key:
        dataset = None
        gdal.ErrorReset()
        if band_indexes:
            band_str = ' (band: %d)' % band_indexes[0]
        else:
            band_str = ''
        plant.handle_exception(f'file could not be opened: {filename}'
                               f' {band_str}',
                               flag_exit_if_error,
                               flag_no_messages=flag_no_messages,
                               verbose=verbose)

        return

    length = length if length is not None else length_orig
    width = width if width is not None else width_orig

    null = in_null if only_header else out_null

    image_obj = plant.PlantImage(
        filename=filename,
        filename_orig=filename_orig,
        width=width,
        length=length,
        width_orig=width_orig,
        length_orig=length_orig,
        offset_x=offset_x,
        offset_y=offset_y,
        metadata=metadata,

        nbands=nbands,
        nbands_orig=nbands_orig,
        band_orig=band_indexes,
        input_key=input_key,
        null=null,

        projection=projection,
        file_format=file_format,

        plant_transform_obj=plant_transform_obj,
        geotransform=geotransform,
        gcp_list=gcp_list,
        gcp_projection=gcp_projection,
        **image_obj_kwargs)

    if geotransform is not None:
        if (offset_x or
                offset_y or
                width is not None or
                length is not None):

            geotransform, length, width = plant.crop_geotransform(
                geotransform,
                offset_x=offset_x,
                offset_y=offset_y,
                new_width=width,
                new_length=length,
                width=width_orig,
                length=length_orig)

        image_obj.set_geotransform(geotransform,
                                   realize_changes=False)
        if plant.isvalid(length):
            image_obj.set_length(length)
        if plant.isvalid(width):
            image_obj.set_width(width)

    if 'HDF5' in file_format:
        image_obj = _read_image_hdf5(hdf5_obj,
                                     hdf5_obj,
                                     image_obj,
                                     only_header=only_header,
                                     plant_transform_obj=plant_transform_obj,
                                     out_null=out_null,
                                     verbose=verbose,
                                     force=force)
        return image_obj

    if verbose and not only_header:
        if band is None or nbands_orig == 1:
            print('opening: %s (GDAL: %s) '
                  % (filename, file_format))
        else:
            print('opening: %s (GDAL: %s, band(s): %s) '
                  % (filename, file_format, band))

    if plant_transform_obj is not None:
        image_obj._applied_transform_obj_crop_list.append(
            plant_transform_obj.get_crop_dict())

    for b in range(nbands):

        if b != 0:
            current_band = dataset.GetRasterBand(band_indexes[b] + 1)
            if plant.isnumeric(input_key):
                current_band = current_band.GetOverview(int(input_key))
            if current_band is None:
                current_band = None
                dataset = None
                gdal.ErrorReset()
                plant.handle_exception('band ' + str(b) +
                                       ' not found in ' + filename,
                                       flag_exit_if_error,
                                       flag_no_messages=flag_no_messages,
                                       verbose=verbose)

                return

        dtype = plant.get_np_dtype_from_gdal(current_band.DataType)

        if not only_header:

            image = current_band.ReadAsArray(**read_as_array_kwargs)

            if image is None and len(read_as_array_kwargs) > 0:
                current_band = None
                dataset = None
                gdal.ErrorReset()
                plant.handle_exception('file %s could not be opened'
                                       ' using GDAL driver and parameters %s'
                                       % (filename,
                                          str(read_as_array_kwargs)),
                                       flag_exit_if_error,
                                       flag_no_messages=flag_no_messages,
                                       verbose=verbose)

                return

        no_data = current_band.GetNoDataValue()

        if file_format == 'netCDF' and no_data == 0:

            ret_dict = parse_filename(filename)

            current_file = ret_dict['filename']
            dataset = h5py.File(current_file)[input_key]
            if '_FillValue' in dataset.attrs:
                no_data = dataset.attrs['_FillValue']

        band_null = no_data if plant.isnan(in_null) else in_null
        if not only_header:

            plant.apply_null(image,
                             in_null=band_null,
                             out_null=out_null)
            if flag_rotate_and_flip_x:
                image = np.rot90(image)
                image = np.flip(image, axis=1)

            if flag_flip_y:
                image = np.flip(image, axis=0)
        else:
            image = None

        ctable = current_band.GetColorTable()
        null = in_null if only_header else out_null
        if null is None:
            null = no_data
        description = current_band.GetDescription()
        if nbands == 1:
            flag_default_description = (
                isinstance(description, str) and
                description.startswith('Band ') and
                plant.isnumeric(description.replace('Band ', '')))
            if flag_default_description:
                description = None

        if (image is not None and
                'int' in plant.get_dtype_name(image).lower() and
                no_data is not None):
            image = np.ma.array(image, mask=image == no_data,
                                fill_value=no_data)

        band_obj = plant.PlantBand(image,
                                   dtype=dtype,
                                   null=null,
                                   name=description,

                                   ctable=ctable)

        if plant_transform_obj is not None and only_header:
            band_obj._applied_transform_obj_crop_header_list = \
                [plant_transform_obj.get_crop_dict()]
        elif plant_transform_obj is not None:
            band_obj._applied_transform_obj_crop_image_list = \
                [plant_transform_obj.get_crop_dict()]

        image_obj.set_band(band_obj, band=b, realize_changes=False)

    current_band = None
    dataset = None
    gdal.ErrorReset()

    if not only_header:
        image_obj.set_null(out_null, realize_changes=False)

    plant.apply_mask(image_obj,
                     plant_transform_obj=plant_transform_obj,
                     verbose=verbose,
                     force=force)

    return image_obj


def _read_image_bin(filename_orig,
                    filename,
                    band=None,
                    verbose=True,
                    only_header=True,
                    read_only=True,

                    image_obj=None,
                    header_file=None,
                    input_format=None,
                    scheme=None,

                    plant_transform_obj=None,
                    offset=None,
                    in_null=None,
                    out_null=None,
                    force=None,
                    flag_no_messages=False,
                    flag_exit_if_error=True,
                    image_obj_kwargs={},
                    **kwargs_orig):

    kwargs = locals()
    args = []
    args.append(kwargs.pop('filename_orig'))
    args.append(kwargs.pop('filename'))

    geotransform = None
    dtype = None

    if header_file is None and filename.endswith('.xml'):
        header_file = filename
    elif header_file is None and plant.isfile(filename + '.xml'):
        header_file = filename + '.xml'
    if image_obj is None and header_file is not None:
        try:
            image_obj = get_info_from_xml(header_file)
        except BaseException:
            header_file = None

    if image_obj is None:
        kwargs['only_header'] = True
        kwargs['flag_no_messages'] = True
        kwargs.pop('image_obj')
        image_obj = _read_image_gdal(*args, **kwargs)

    if image_obj is None and header_file is None:
        if filename.endswith('.xml'):
            header_file = filename
        else:
            header_file = filename + '.xml'
        plant.handle_exception('invalid ISCE header: ' +
                               header_file,
                               flag_exit_if_error,
                               flag_no_messages=flag_no_messages,
                               verbose=verbose)

    if image_obj is None:
        plant.handle_exception('reading file: ' + filename + ' (ISCE)',
                               flag_exit_if_error,
                               flag_no_messages=flag_no_messages,
                               verbose=verbose)

        return
    filelist = plant.glob(filename)
    if len(filelist) == 0:
        return
    filename = filelist[0]

    width_orig = image_obj.width_orig
    length_orig = image_obj.length_orig
    depth_orig = image_obj.depth_orig

    dtype = image_obj.dtype
    if not scheme:
        scheme = image_obj.scheme
    if scheme is None:
        scheme = 'BSQ'
    nbands_orig = image_obj.nbands_orig

    nbands = nbands_orig
    band_indexes = plant.get_int_list(band)
    if offset is None:
        offset = 0
    if band_indexes:
        if np.max(band_indexes) > nbands_orig - 1:
            plant.handle_exception('band %d not found in %s'
                                   ' with %d bands'
                                   % (np.max(band_indexes), filename,
                                      nbands_orig),
                                   flag_exit_if_error,
                                   flag_no_messages=flag_no_messages,
                                   verbose=verbose)

            return
        nbands = len(band_indexes)

    geotransform = plant.get_geotransform_from_header(filename)

    if not plant.valid_coordinates(geotransform=geotransform,
                                   length=length_orig,
                                   width=width_orig):
        geotransform = None

    if geotransform is not None and plant_transform_obj is not None:
        plant_transform_obj.update_crop_window(geotransform=geotransform,
                                               length_orig=length_orig,
                                               width_orig=width_orig)

    image_obj = plant.PlantImage(
        image_obj,
        filename=filename,
        filename_orig=filename_orig,
        header_file=header_file,

        width_orig=width_orig,
        length_orig=length_orig,
        depth_orig=depth_orig,
        dtype=dtype,

        file_format='ISCE',

        plant_transform_obj=plant_transform_obj,
        geotransform=geotransform,
        nbands_orig=nbands_orig,
        band_orig=band_indexes,

        nbands=nbands,
        scheme=scheme,
        **image_obj_kwargs)

    if only_header:

        plant.apply_crop(image_obj,
                         plant_transform_obj=plant_transform_obj,
                         verbose=verbose)
        return image_obj

    if band_indexes is None:
        band_indexes = np.arange(nbands)

    flag_error = False
    if header_file is not None and depth_orig <= 1:
        try:
            for i, current_band in enumerate(band_indexes):
                from isceobj.Util.ImageUtil import ImageLib as IML

                current_image = IML.mmapFromISCE(filename,
                                                 IML.createLogger(
                                                     0)).bands[current_band]
                if verbose:
                    str_message = ('opening: ' + filename +
                                   ' (ISCE:mmap)')
                    if nbands_orig > 1:
                        str_message += (' (band=' +
                                        str(int(current_band)) + ')')
                    print(str_message)
                if not read_only:
                    current_image = current_image.copy()
                image_obj.set_image(current_image, band=band_indexes[i],
                                    realize_changes=False)
        except BaseException:
            flag_error = True

    if flag_error or header_file is None or depth_orig >= 1:
        dtype_np = plant.get_np_dtype(dtype)
        image = np.memmap(filename,
                          dtype=dtype_np,
                          mode='r',
                          offset=offset,
                          shape=(length_orig * depth_orig *
                                 width_orig * nbands_orig))

        str_message = f'opening: {filename} (ISCE numpy)'

        shape = (depth_orig, length_orig, width_orig)
        if nbands_orig == 1:
            if verbose:
                print(str_message)
            image = np.reshape(image, shape)
            image_obj.set_image(image, realize_changes=False)
        else:
            if verbose and len(band_indexes) == 1:
                str_message += (' (band=' +
                                str(int(band_indexes[0])) + ')')
            nbands_ref = depth_orig * nbands_orig
            for band_index, current_band in enumerate(band_indexes):
                current_image = np.zeros(shape, dtype=dtype)
                for depth in range(depth_orig):
                    band_location = current_band * depth_orig + depth
                    if scheme.upper() == 'BIP':
                        image_temp = \
                            image[band_location::
                                  nbands_ref].reshape(shape[1:])
                        current_image[depth, :, :] = image_temp
                    elif scheme.upper() == 'BIL':
                        for i in range(length_orig):
                            band_offset = (i * width_orig * nbands_ref +
                                           band_location * width_orig)
                            current_image[depth, i, :] = \
                                image[band_offset:band_offset + width_orig]
                    elif scheme.upper() == 'BSQ':
                        current_image[depth, :, :] = \
                            image[(width_orig * length_orig) * band_location:
                                  (width_orig * length_orig) *
                                  (band_location + 1)].reshape(length_orig,
                                                               width_orig)
                    else:
                        plant.handle_exception(
                            'read_image(): not implemented '
                            'for scheme: ' + scheme,
                            flag_exit_if_error,
                            flag_no_messages=flag_no_messages,
                            verbose=verbose)

                        return
                if depth == 1:
                    current_image = np.reshape(current_image,
                                               (length_orig, width_orig))
                else:
                    current_image = np.reshape(current_image,
                                               (depth_orig, length_orig,
                                                width_orig))

                image_obj.set_image(current_image, band=band_index,
                                    realize_changes=False)

    prepare_image(image_obj,

                  flag_exit_if_error=flag_exit_if_error,
                  plant_transform_obj=plant_transform_obj,
                  in_null=in_null,
                  out_null=out_null,
                  input_format=input_format,
                  verbose=verbose,
                  only_header=only_header,
                  flag_no_messages=flag_no_messages,
                  force=force)
    return image_obj


def create_isce_header(input_file,
                       nbands=1,
                       length=None,
                       width=None,
                       dtype=None,
                       scheme=None,

                       step=None,
                       descr=None,
                       data=None,
                       verbose=False,
                       geotransform=None,
                       projection=None,
                       image_obj=None):

    try:
        from isceobj import createImage
    except ImportError:
        error_message = plant.get_error_message()

        return
    try:
        img = createImage()
    except ImportError:

        return
    except BaseException:
        error_message = plant.get_error_message()
        print('WARNING %s' % error_message)
        return

    if image_obj is None:
        image_obj = data
    image_obj = plant.PlantImage(
        image_obj,

        nbands=nbands,
        length=length,
        width=width,
        geotransform=geotransform,

        scheme=scheme,
        dtype=dtype,
        file_format='ISCE',

        projection=projection)

    if input_file and not input_file.endswith('xml'):
        img.filename = input_file
    else:
        img.filename = image_obj.filename

    img.width = image_obj.width
    img.length = image_obj.length

    plant_geogrid_obj = plant.get_coordinates(
        step_x=step,
        step_y=step,
        geotransform=image_obj.geotransform,
        width=image_obj.width,
        length=image_obj.length)

    img.setDeltaLatitude(plant_geogrid_obj.step_y)
    img.setDeltaLongitude(plant_geogrid_obj.step_x)

    if plant_geogrid_obj.has_valid_coordinates():
        img.coord2.coordStart = plant_geogrid_obj.y0 + plant_geogrid_obj.step_y / 2
        img.coord2.coordEnd = plant_geogrid_obj.yf - plant_geogrid_obj.step_y / 2
        img.setXmin(plant_geogrid_obj.y0 + plant_geogrid_obj.df / 2)
        img.setXmax(plant_geogrid_obj.yf - plant_geogrid_obj.df / 2)

    else:
        img.coord2.coordEnd = np.nan
        img.setXmax(np.nan)

    if descr is not None:
        img.addDescription(descr)
    n_elements = (image_obj.width *
                  image_obj.length *
                  image_obj.depth)

    filename = os.path.realpath(img.filename)
    if isheader(filename):
        return

    if image_obj.dtype is None:
        print('WARNING could not determine data type of: ' +
              input_file +
              ' considering it as: ' + str(dtype))
    else:
        img.dataType = plant.get_isce_dtype(image_obj.dtype)

    if image_obj.scheme is not None:
        img.scheme = image_obj.scheme
    else:
        img.scheme = 'BSQ'
    img.bands = image_obj.nbands

    if plant.isfile(filename):
        file_size = os.stat(filename).st_size
        observed_type_size = round(file_size /
                                   (n_elements * image_obj.nbands))
        type_size = plant.get_dtype_size(img.dataType)
        if (file_size % n_elements) != 0:
            print('WARNING dimensions seems to be wrong. ')
            verbose = True
    else:
        type_size = None

    if (type_size is not None and
            type_size != observed_type_size):
        print('WARNING %s data type seems to be wrong'
              % (img.filename))
        print('file size: %d' % (file_size))
        print('number of pixels: %d' % (n_elements))
        print('expected type size: %d' % (type_size))
        print('observed type size: %d' % (observed_type_size))
        print('bands: %d' % (image_obj.nbands))
        verbose = True

    if verbose:
        print('filename: %s' % (img.filename))
        print('width: %d' % (img.width))
        print('length: %d' % (img.length))
        print('depth: %d' % (image_obj.depth))
        print('number of elements: %d' % (n_elements))
        if plant.isfile(img.filename):
            print('file size: %d' % (file_size))
            print('file size/n_elements: %f' % (file_size / n_elements))
        print('dtype: %s' % (img.dataType))

        print('dtype size: %s' % (observed_type_size))

        print('scheme: %s' % (img.scheme))
        print('bands: %s' % (img.bands))
        if geotransform or bbox:
            print('lat beg (upper left): ', img.coord2.coordStart)
            print('lat end (lower left): ', img.coord2.coordEnd)
            print('lat step: ', img.getDeltaLatitude())
            print('lon beg: ', img.getXmin())
            print('lon end: ', img.getXmax())
            print('lon step: ', img.getDeltaLongitude())
    try:
        img.renderHdr()
    except BaseException:
        error_message = plant.get_error_message()
        if dtype == 'LONG':
            print('WARNING ISCE header could '
                  'not be created. Please, try another '
                  'output data type. %s'
                  % error_message)
        else:
            print('WARNING ISCE header could '
                  'not be created. %s'
                  % error_message)
    return True


def isheader(filename):
    ext_list = ['.txt', '.vrt', '.xml']
    for ext in ext_list:
        if filename.endswith(ext):
            return True
    return False


def crop_valid_box_image_obj(input_name, null=None,
                             verbose=True, flag_all=False):
    flag_return_array = False
    if isinstance(input_name, str):
        image_obj = plant.read_image(input_name)
    elif isinstance(input_name, plant.PlantImage):
        image_obj = input_name
    else:
        flag_return_array = True
        image_obj = plant.PlantImage(input_name)

    min_y = image_obj.length
    max_y = 0
    min_x = image_obj.width
    max_x = 0
    for current_band in range(image_obj.nbands):
        image_1 = image_obj.getImage(band=current_band)
        for i in range(min_y):
            if np.sum(plant.isvalid(image_1[i, :],
                      null=null)) != 0:
                min_y = i
                break
        for j in range(min_x):
            if np.sum(plant.isvalid(image_1[:, j],
                      null=null)) != 0:
                min_x = j
                break
        for i in range(image_obj.length - max_y):
            if np.sum(plant.isvalid(image_1[image_obj.length - i - 1,
                                            :],
                      null=null)) != 0:
                max_y = image_obj.length - i - 1
                break
        for j in range(image_obj.width - max_x):
            if np.sum(plant.isvalid(image_1[:, image_obj.width -
                                            j - 1],
                      null=null)) != 0:
                max_x = image_obj.width - j - 1
                break
    if min_y > max_y:
        min_y = np.nan
        max_y = np.nan
    if min_x > max_x:
        min_x = np.nan
        max_x = np.nan

    if verbose and plant.isvalid(min_y) and plant.isvalid(min_x):
        print('Start point (x, y): (%d, %d)' % (min_x, min_y))
    if verbose and plant.isvalid(max_y) and plant.isvalid(max_x):
        print('End point (x, y): (%d, %d)' % (max_x, max_y))

    if (plant.isnan(min_y) and plant.isnan(min_x) and
            plant.isnan(max_y) and plant.isnan(max_x)):
        return

    out_image_obj = image_obj.copy()
    for current_band in range(image_obj.nbands):
        image_1 = image_obj.getImage(band=current_band)
        image_1 = image_1[min_y:max_y + 1, min_x:max_x + 1]
        out_image_obj.set_image(image_1, band=current_band)
    if flag_return_array:
        image = out_image_obj.image
    else:
        image = out_image_obj
    if not flag_all:
        return image
    ret_dict = {}
    ret_dict['image'] = image
    ret_dict['min_x'] = min_x
    ret_dict['min_y'] = min_y
    ret_dict['max_x'] = max_x
    ret_dict['max_y'] = max_y
    return ret_dict


def crop_fig(filename, force=True, verbose=True):
    image_obj = plant.read_image(filename)
    image_obj = crop_valid_box_image_obj(image_obj,
                                         null=255,
                                         verbose=verbose)
    plant.save_image(image_obj, filename, force=force,
                     verbose=verbose)
