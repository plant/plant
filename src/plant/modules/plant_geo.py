#!/usr/bin/env python3
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Gustavo H. X. Shiroma, Marco Lavalle
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import time
import copy
import numpy as np
from os import path, remove
from osgeo import gdal, osr, ogr
import plant


class PlantGeogrid():

    def __init__(self, **kwargs):

        self._y0 = kwargs.pop('y0', None)
        self._step_y = kwargs.pop('step_y', None)
        length = kwargs.pop('length', None)
        if length is not None:
            self._length = int(np.round(length))
        else:
            self._length = None

        self._yf = kwargs.pop('yf', None)

        self._x0 = kwargs.pop('x0', None)
        self._step_x = kwargs.pop('step_x', None)
        width = kwargs.pop('width', None)
        if width is not None:
            self._width = int(np.round(width))
        else:
            self._width = None

        self._xf = kwargs.pop('xf', None)

        geotransform = kwargs.pop('geotransform', None)
        if geotransform is not None:

            if (plant.isvalid(self._x0) and
                plant.isvalid(self._step_x) and
                    plant.isvalid(self._width)):
                xf = self._x0 + self._step_x * self._width
            else:
                xf = self._xf
            if (plant.isvalid(self._y0) and
                plant.isvalid(self._step_y) and
                    plant.isvalid(self._length)):
                yf = self._y0 + self._step_y * self._length
            else:
                yf = self._yf

            if not plant.isvalid(self._x0):
                self._x0 = geotransform[0]
            if not plant.isvalid(self._step_x):
                self._step_x = geotransform[1]
            if not plant.isvalid(self._y0):
                self._y0 = geotransform[3]
            if not plant.isvalid(self._step_y):
                self._step_y = geotransform[5]

            if (plant.isvalid(xf) and
                plant.isvalid(self._x0) and
                    plant.isvalid(self._step_x)):
                self._width = int(np.round((xf - self._x0) / self._step_x))

            if (plant.isvalid(yf) and
                plant.isvalid(self._y0) and
                    plant.isvalid(self._step_y)):
                self._length = int(np.round((yf - self._y0) / self._step_y))

        if (plant.isvalid(self._x0) and plant.isvalid(self._step_x) and
                not plant.isvalid(self._width) and plant.isvalid(self._xf)):
            self._width = int(np.round((self._xf - self.x0) / self._step_x))
        elif (plant.isvalid(self._x0) and not plant.isvalid(self._step_x) and
                plant.isvalid(self._width) and plant.isvalid(self._xf)):
            self._step_x = (self._xf - self.x0) / self._width

        if (plant.isvalid(self._y0) and plant.isvalid(self._step_y) and
                not plant.isvalid(self._length) and plant.isvalid(self._yf)):
            self._length = int(np.round((self._yf - self.y0) / self._step_y))
        elif (plant.isvalid(self._y0) and not plant.isvalid(self._step_y) and
                plant.isvalid(self._length) and plant.isvalid(self._yf)):
            self._step_y = (self._yf - self.y0) / self._length

        self._projection = kwargs.pop('projection', None)

    @property
    def geotransform(self):
        geotransform = [self._x0, self._step_x, 0, self._y0, 0, self._step_y]
        return geotransform

    @geotransform.setter
    def geotransform(self, val):
        if plant.isnan(val):
            return
        self._x0, self._step_x, _, self._y0, _, self._step_y = val

    @property
    def gdal_output_bounds(self):
        output_bounds = np.asarray([self.x0, self.yf, self.xf, self.y0])
        return output_bounds

    @property
    def bbox(self):
        bbox = np.asarray([self.yf, self.y0, self.x0, self.xf])
        return bbox

    @bbox.setter
    def bbox(self, val):
        if plant.isnan(val):
            return
        if not plant.isvalid(self._step_x):
            print('ERROR cannot set bounding box without a valid X-spacing:'
                  f' "{self._step_x}"')
        if not plant.isvalid(self._step_y):
            print('ERROR cannot set bounding box without a valid Y-spacing:'
                  f' "{self._step_y}"')

        self._x0, xf, self._y0, yf = val

        self.xf = xf
        self.yf = yf

    @property
    def x0(self):
        return self._x0

    @x0.setter
    def x0(self, val):
        self._x0 = val

    @property
    def xf(self):
        if (plant.isvalid(self._x0) and
            plant.isvalid(self._step_x) and
                plant.isvalid(self._width)):
            return self._x0 + self._step_x * self._width
        return self._xf

    @xf.setter
    def xf(self, val):
        if (plant.isvalid(val) and plant.isvalid(self._x0) and
                plant.isvalid(self._step_x)):
            self._width = int(np.round((val - self._x0) / self._step_x))
        self._xf = val

    @property
    def step_x(self):
        return self._step_x

    @step_x.setter
    def step_x(self, val):

        xf = self.xf
        self._step_x = val
        self.xf = xf

    @property
    def y0(self):
        return self._y0

    @y0.setter
    def y0(self, val):
        self._y0 = val

    @property
    def yf(self):
        if (plant.isvalid(self._y0) and
            plant.isvalid(self._step_y) and
                plant.isvalid(self._length)):
            return self._y0 + self._step_y * self._length
        return self._yf

    @yf.setter
    def yf(self, val):
        if (plant.isvalid(val) and plant.isvalid(self._y0) and
                plant.isvalid(self._step_y)):
            self._length = int(np.round((val - self._y0) / self._step_y))
        self._yf = val

    @property
    def step_y(self):
        return self._step_y

    @step_y.setter
    def step_y(self, val):

        yf = self.yf
        self._step_y = val
        self.yf = yf

    @property
    def length(self):
        if (not plant.isvalid(self._length) and plant.isvalid(self._y0) and
                plant.isvalid(self._yf) and plant.isvalid(self._step_y)):
            self._length = int(np.round((self._yf - self._y0) / self._step_y))
        return self._length

    @length.setter
    def length(self, val):
        if plant.isnan(val):
            return
        self._length = int(np.round(val))
        if (plant.isvalid(self._y0) and not plant.isvalid(self._step_y) and
                plant.isvalid(self._length) and plant.isvalid(self._yf)):
            self._step_y = (self._yf - self.y0) / self._length

    @property
    def width(self):
        if (not plant.isvalid(self._width) and plant.isvalid(self._x0) and
                plant.isvalid(self._xf) and plant.isvalid(self._step_x)):
            self._width = int(np.round((self._xf - self._x0) / self._step_x))
        return self._width

    @width.setter
    def width(self, val):
        if plant.isnan(val):
            return
        self._width = int(np.round(val))
        if (plant.isvalid(self._x0) and not plant.isvalid(self._step_x) and
                plant.isvalid(self._width) and plant.isvalid(self._xf)):
            self._step_x = (self._xf - self.x0) / self._width

    @property
    def projection(self):
        return self._projection

    @projection.setter
    def projection(self, new_projection):
        if (not self.projection or
                not new_projection or
                compare_projections(self.projection,
                                    new_projection) is not False):
            self._projection = new_projection
        else:
            temp_grid_obj = update_geotransform_projection(
                plant_grid_obj=self,
                output_projection=new_projection)

            self.set_geogrid(temp_grid_obj)

    def get_polygon(self):
        tile_ring = ogr.Geometry(ogr.wkbLinearRing)
        tile_ring.AddPoint(self.x0, self.y0)
        tile_ring.AddPoint(self.xf, self.y0)
        tile_ring.AddPoint(self.xf, self.yf)
        tile_ring.AddPoint(self.x0, self.yf)
        tile_ring.AddPoint(self.x0, self.y0)
        tile_polygon = ogr.Geometry(ogr.wkbPolygon)
        tile_polygon.AddGeometry(tile_ring)
        tile_polygon.AssignSpatialReference(self.srs)
        return tile_polygon

    def intersects(self, plant_geogrid_obj):
        if not self.has_valid_coordinates():
            return
        if not plant_geogrid_obj.has_valid_coordinates():
            return
        polygon_1 = self.get_polygon()
        polygon_2 = plant_geogrid_obj.get_polygon()
        intersection = polygon_1.Intersection(polygon_2)
        return not intersection.IsEmpty()

    def set_geogrid(self, temp_grid_obj):
        if temp_grid_obj is None:
            return
        self._y0 = temp_grid_obj.y0
        self._step_y = temp_grid_obj.step_y
        self._length = temp_grid_obj.length
        self._yf = temp_grid_obj.yf

        self._x0 = temp_grid_obj.x0
        self._step_x = temp_grid_obj.step_x
        self._width = temp_grid_obj.width
        self._xf = temp_grid_obj.xf

        self._projection = temp_grid_obj.projection

    def populate_missing_attributes(self, temp_grid_obj):
        xf = self.xf
        yf = self.yf

        if not plant.isvalid(self._y0) and plant.isvalid(temp_grid_obj.y0):
            self._y0 = temp_grid_obj.y0
        if not plant.isvalid(self._x0) and plant.isvalid(temp_grid_obj.x0):
            self._x0 = temp_grid_obj.x0

        if (not plant.isvalid(self._step_y) and
                plant.isvalid(temp_grid_obj.step_y)):
            self._step_y = temp_grid_obj.step_y
        if (not plant.isvalid(self._step_x) and
                plant.isvalid(temp_grid_obj.step_x)):
            self._step_x = temp_grid_obj.step_x

        if (not plant.isvalid(self._length) and
                plant.isvalid(temp_grid_obj.length)):
            self._length = temp_grid_obj.length
        if (not plant.isvalid(self._width) and
                plant.isvalid(temp_grid_obj.width)):
            self._width = temp_grid_obj.width

        if not plant.isvalid(xf) and plant.isvalid(temp_grid_obj.xf):
            self.xf = temp_grid_obj.xf
        if not plant.isvalid(yf) and plant.isvalid(temp_grid_obj.yf):
            self.yf = temp_grid_obj.yf

        if not self._projection:
            self._projection = temp_grid_obj.projection

    def has_valid_coordinates(self):

        if not self.has_valid_geotransform():
            return False

        element_list = [self.length, self.width]

        if not all([plant.isvalid(x) for x in element_list]):
            return False

        if self.is_geographic() is not False:
            return True

        plant_geogrid_obj = self.deepcopy()
        plant_geogrid_obj.projection = 'WGS84'
        return plant_geogrid_obj.has_valid_coordinates()

    @property
    def srs(self):
        return plant.get_srs_from_projection(self._projection)

    def is_geographic(self):
        return is_geographic(self._projection)

    def is_projected(self):
        return is_projected(self._projection)

    def is_polar_stereographic(self):
        return is_polar_stereographic(self._projection)

    def has_dummy_geotransform(self):

        return np.array_equal(self.geotransform,
                              [0.0, 1.0, 0.0, 0.0, 0.0, 1.0])

    def has_valid_geotransform(self):
        element_list = [self.y0,
                        self.step_y,
                        self.x0,
                        self.step_x]
        if not all([plant.isvalid(x) for x in element_list]):
            return False

        return not self.has_dummy_geotransform()

    def has_valid_bounding_box(self):
        element_list = [self.y0,
                        self.yf,
                        self.x0,
                        self.xf]
        return all([plant.isvalid(x) for x in element_list])

    def has_valid_steps(self):
        element_list = [self.step_x,
                        self.step_y]
        return all([plant.isvalid(x) for x in element_list])

    def is_complete(self):
        return self.has_valid_coordinates() and self.projection

    def merge(self, temp_grid_obj):

        if temp_grid_obj.has_dummy_geotransform():
            return

        if self.has_dummy_geotransform():
            self.set_geogrid(temp_grid_obj)

        flag_same_projection = \
            compare_projections(self._projection,
                                temp_grid_obj.projection) is not False

        if flag_same_projection:
            self.populate_missing_attributes(temp_grid_obj)

        flag_current_projection_valid = (
            bool(self._projection) and
            get_srs_from_projection(self._projection) is not None)

        flag_projection_temp_valid = (
            bool(temp_grid_obj.projection) and
            get_srs_from_projection(temp_grid_obj.projection) is not None)

        if (flag_same_projection is None and
                (not flag_current_projection_valid and
                 not self.has_valid_coordinates()) and
                (flag_projection_temp_valid and
                 temp_grid_obj.has_valid_coordinates())):
            self.set_geogrid(temp_grid_obj)
            return

        if flag_same_projection is False:

            temp_grid_obj = temp_grid_obj.deepcopy()
            temp_grid_obj.projection = self.projection

        self.populate_missing_attributes(temp_grid_obj)

    def print(self, **kwargs):
        print_geoinformation(plant_geogrid_obj=self, **kwargs)

    def deepcopy(self):
        return copy.deepcopy(self)


def get_coordinate_string(lat=None, lon=None):

    if lat is not None:
        str_lat = 'S' if lat < 0 else 'N'
        str_lat += "%02d" % abs(lat,)
        if lon is None:
            return str_lat
    if lon is not None:
        str_lon = 'W' if lon < 0 else 'E'
        str_lon += "%03d" % abs(lon,)
        if lat is None:
            return str_lon
    return str_lat, str_lon


def get_coordinates_string(min_lat=None,
                           max_lat=None,
                           min_lon=None,
                           max_lon=None):

    flag_global = np.ceil(max_lon - min_lon) >= 360
    if flag_global:
        min_lon = -180
        max_lon = 180
    else:
        if min_lon > 180:
            min_lon -= 360
        if max_lon > 180:
            max_lon -= 360

    str_lat_min = get_coordinate_string(lat=min_lat)
    str_lon_min = get_coordinate_string(lon=min_lon)

    str_lat_max = get_coordinate_string(lat=max_lat)
    str_lon_max = get_coordinate_string(lon=max_lon)

    return 'Lat_%s_%s_Lon_%s_%s' % (str_lat_min, str_lat_max,
                                    str_lon_min, str_lon_max)


def compare_geotransforms(geotransform_1, geotransform_2, error=0.001):
    diff = np.absolute(np.array(geotransform_1) -
                       np.array(geotransform_2))
    return all([d < error for d in diff])


def geotransform_iscomplete(geotransform):
    if geotransform is None:
        return False
    return all([x is not None and np.isfinite(x) for x in geotransform[0:6]])


def get_point_topo_dir(input_data=None,
                       image_obj=None,
                       image=None,
                       width=None,
                       length=None,
                       geotransform=None,
                       projection=None,
                       lat=None,
                       lon=None,
                       topo_dir=None,
                       y_pos=None,
                       x_pos=None,
                       footprint_m=None,
                       verbose=False):
    if footprint_m is not None:
        raise NotImplementedError

    if lat is not None:
        lat = plant.read_image(lat).image
    if lon is not None:
        lon = plant.read_image(lon).image
    if x_pos is not None:
        x_pos = plant.read_image(x_pos).image
    if y_pos is not None:
        y_pos = plant.read_image(y_pos).image
    if topo_dir is None:
        kwargs = locals()
        return get_point(**kwargs)

    if image_obj is None:
        image_obj = plant.PlantImage(input_data,
                                     verbose=verbose)

    if lat is not None and lon is not None:
        ret_dict = get_lat_lon_pos_topo_dir(
            lat, lon, topo_dir, image_obj, verbose=verbose)
        if ret_dict is None:
            ret_dict = {}
            return ret_dict
    else:
        ret_dict = get_lat_lon_topo_dir(
            y_pos, x_pos, topo_dir, verbose=verbose)
        if ret_dict is None:
            ret_dict = {}
            return ret_dict
    x_pos_image = ret_dict['x_pos_image']
    y_pos_image = ret_dict['y_pos_image']
    if image_obj is not None:
        data_value = image_obj.image[y_pos_image, x_pos_image]
        ret_dict['data'] = data_value
    if verbose:
        print_get_point_results(ret_dict)
    return ret_dict


def print_get_point_results(ret_dict):
    x_pos = ret_dict.get('x_pos', None)
    y_pos = ret_dict.get('y_pos', None)
    lon = ret_dict.get('lon', None)
    lat = ret_dict.get('lat', None)
    x_pos_image = ret_dict.get('x_pos_image', None)
    y_pos_image = ret_dict.get('y_pos_image', None)
    lon_image = ret_dict.get('lon_image', None)
    lat_image = ret_dict.get('lat_image', None)
    data_value = ret_dict.get('data', None)
    flag_outside_image = ret_dict.get('flag_outside_image', None)

    flag_outside_image_array = np.asarray(flag_outside_image).ravel()
    lat_array = np.asarray(lat).ravel()
    lon_array = np.asarray(lon).ravel()
    x_pos_array = np.asarray(x_pos).ravel()
    y_pos_array = np.asarray(y_pos).ravel()
    lat_image_array = np.asarray(lat_image).ravel()
    lon_image_array = np.asarray(lon_image).ravel()
    x_pos_image_array = np.asarray(x_pos_image).ravel()
    y_pos_image_array = np.asarray(y_pos_image).ravel()
    data_value_array = np.asarray(data_value).ravel()
    for i in range(lat_image.size):
        if flag_outside_image_array[i]:
            msg_list = []
            if i < len(x_pos_array) and i < len(y_pos_array):
                msg_list.extend(['x=%.0f' % x_pos_array[i],
                                 'y=%.0f' % y_pos_array[i]])
            if i < len(lat_array) and i < len(lon_array):
                msg_list.extend(['lat=%.16f' % lat_array[i],
                                 'lon=%.16f' % lon_array[i]])
            if len(msg_list) > 0:
                print('WARNING coordinates (%s)'
                      ' outside image boundaries'
                      % ', '.join(msg_list))
        msg_list = []
        if i < len(x_pos_image_array) and i < len(y_pos_image_array):
            msg_list.extend(['x=%.0f' % x_pos_image_array[i],
                             'y=%.0f' % y_pos_image_array[i]])
        if (i < len(lat_array) and i < len(lon_array) and
                plant.isvalid(lat_array[i]) and
                plant.isvalid(lon_array[i]) and
                not flag_outside_image_array[i]):
            msg_list.extend(['lat=%.16f' % lat_array[i],
                             'lon=%.16f' % lon_array[i]])
        elif (i < len(lat_image_array) and i < len(lon_image_array) and
                flag_outside_image_array[i]):
            if plant.isvalid(lat_image_array[i]):
                msg_list.extend(['lat=%.16f' % lat_image_array[i]])
            if plant.isvalid(lon_image_array[i]):
                msg_list.extend(['lon=%.16f' % lon_image_array[i]])
        if i < len(data_value_array):
            print('value at (%s): %s'
                  % (', '.join(msg_list), str(data_value_array[i])))
        else:
            print('position: %s'
                  % (', '.join(msg_list)))


def get_lat_lon_topo_dir(y_pos, x_pos, topo_dir=None, image_obj=None,
                         verbose=False):
    ret = get_lat_lon_files_from_topo_dir(topo_dir)
    if ret is None:
        return
    file_lat, file_lon = ret
    lat_image = plant.read_matrix(file_lat)
    lon_image = plant.read_matrix(file_lon)
    flag_outside_image_list = []
    x_pos_image_list = []
    y_pos_image_list = []
    out_lat_list = []
    out_lon_list = []
    y_pos_vect = np.asarray(y_pos).ravel()
    x_pos_vect = np.asarray(x_pos).ravel()
    for y, x in zip(y_pos_vect, x_pos_vect):
        current_y = int(y)
        current_x = int(x)
        flag_outside_image = (current_y < 0 or
                              current_x < 0 or
                              (image_obj is not None and
                               (current_y > image_obj.image.shape[0] or
                                (current_x > image_obj.image.shape[1]))))
        flag_outside_image_list.append(flag_outside_image)
        x_pos_image_list.append(current_x)
        y_pos_image_list.append(current_y)
        if not flag_outside_image:
            out_lat_list.append(lat_image[current_y, current_x])
            out_lon_list.append(lon_image[current_y, current_x])
        else:
            out_lat_list.append(np.nan)
            out_lon_list.append(np.nan)

    ret_dict = _get_point_prepare_output(
        y_pos_image=y_pos_image_list, x_pos_image=x_pos_image_list,
        lat_image=out_lat_list, lon_image=out_lon_list,
        flag_outside_image=flag_outside_image_list,
        y_pos=y_pos_vect, x_pos=x_pos_vect,
        ref_array=y_pos)
    return ret_dict


def get_lat_lon_pos_topo_dir(lat, lon,
                             topo_dir=None,
                             image_obj=None,
                             verbose=False):
    ret = get_lat_lon_files_from_topo_dir(topo_dir)
    if ret is None:
        return
    file_lat, file_lon = ret
    MAX_DIST = 0.00083
    lat_image = plant.read_matrix(file_lat)
    lon_image = plant.read_matrix(file_lon)

    lat_vect = np.asarray(lat).ravel()
    lon_vect = np.asarray(lon).ravel()
    x_pos_image_list = []
    y_pos_image_list = []
    flag_outside_image_list = []
    out_lat_list = []
    out_lon_list = []
    for current_lat, current_lon in zip(lat_vect, lon_vect):
        dist_image = ((lat_image - current_lat)**2 +
                      (lon_image - current_lon)**2)
        index_ravel = np.argmin(dist_image)
        index = np.unravel_index(index_ravel,
                                 dist_image.shape)
        y_pos, x_pos = index
        out_lat_list.append(lat_image[y_pos, x_pos])
        out_lon_list.append(lon_image[y_pos, x_pos])
        flag_outside_image = dist_image[y_pos, x_pos] > MAX_DIST
        if flag_outside_image and verbose:
            lat_orig = current_lat
            lon_orig = current_lon
            current_lat = lat_image[y_pos, x_pos]
            current_lon = lon_image[y_pos, x_pos]
            print('Requested point: (lat=%f, lon=%f)'
                  % (lat_orig, lon_orig))
            print('Nearest point: (lat=%f, lon=%f)'
                  % (current_lat, current_lon))
            print('Distance to the nearest point [deg]: %f'
                  % (dist_image[y_pos, x_pos]))
        x_pos_image_list.append(x_pos)
        y_pos_image_list.append(y_pos)
        flag_outside_image_list.append(flag_outside_image)
    ret_dict = _get_point_prepare_output(
        lat=lat_vect, lon=lon_vect,
        y_pos_image=y_pos_image_list, x_pos_image=x_pos_image_list,
        lat_image=out_lat_list, lon_image=out_lon_list,
        flag_outside_image=flag_outside_image_list, ref=lat)
    return ret_dict


def _get_point_prepare_output(**kwargs):
    ref = kwargs.pop('ref', None)
    ret_dict = {}
    if ref is not None and np.asarray(ref).size == 1:
        for key, value in kwargs.items():
            ret_dict[key] = value[0]
        return ret_dict
    for key, value in kwargs.items():
        if ref is not None and isinstance(ref, np.ndarray):
            ret_dict[key] = np.asarray(value).reshape(ref.shape)
        else:
            ret_dict[key] = np.asarray(value)
    return ret_dict


def get_point(input_data=None,
              image_obj=None,

              width=None,
              length=None,
              geotransform=None,
              lat=None,
              lon=None,
              topo_dir=None,
              projection=None,
              y_pos=None,
              x_pos=None,
              footprint_m=None,
              verbose=False):
    if lat is not None:
        lat = plant.read_image(lat).image
    if lon is not None:
        lon = plant.read_image(lon).image
    if x_pos is not None:
        x_pos = plant.read_image(x_pos).image
    if y_pos is not None:
        y_pos = plant.read_image(y_pos).image
    if topo_dir is not None:
        kwargs = locals()
        return get_point_topo_dir(**kwargs)

    if image_obj is None:
        image_obj = plant.PlantImage(input_data,
                                     verbose=verbose,
                                     only_header=False)

    if geotransform is None:
        geotransform = image_obj.geotransform
    if not projection:
        projection = image_obj.projection

    if width is None:
        width = image_obj.width
    if length is None:
        length = image_obj.length

    if geotransform is not None:
        plant_geogrid_obj = get_coordinates(geotransform=geotransform,
                                            projection=projection,
                                            flag_output_in_wgs84=True)
        if not plant_geogrid_obj.has_valid_geotransform():
            print('WARNING (plant_geo.get_point) input does not '
                  'have a valid geo-reference')
            return

        lat_beg = plant_geogrid_obj.y0
        lon_beg = plant_geogrid_obj.x0
        step_y = plant_geogrid_obj.step_y
        step_x = plant_geogrid_obj.step_x

    elif y_pos is None or x_pos is None:
        print(f'ERROR georeference of "{image_obj}" not found')
        return
    else:
        lat_beg = np.nan
        lon_beg = np.nan
        step_y = np.nan
        step_x = np.nan

    if footprint_m is not None and geotransform is not None:
        footprint_deg = plant.m_to_deg(footprint_m)
        filter_size = [footprint_deg[0] / step_y,
                       footprint_deg[1] / step_x]

        if (((float(filter_size[0]) - float(filter_size[1])) /
             float(filter_size[0])) > 0.1):

            image_obj = plant.filter_data(image_obj,
                                          mean=filter_size)
        else:
            image_obj = plant.filter_data(image_obj,
                                          trapezoid=filter_size,
                                          trapezoid_slope=1)
    elif footprint_m is not None:
        raise NotImplementedError

    lat_array = np.asarray(lat).ravel()
    lon_array = np.asarray(lon).ravel()
    x_pos_array = np.asarray(x_pos).ravel()
    y_pos_array = np.asarray(y_pos).ravel()

    x_pos_list = []
    y_pos_list = []
    lat_list = []
    lon_list = []
    x_pos_image_list = []
    y_pos_image_list = []
    lon_image_list = []
    lat_image_list = []
    flag_outside_image_list = []
    data_list = []
    for i in range(max([lat_array.size, y_pos_array.size])):
        current_lat = lat_array[i] if lat is not None else None
        current_lon = lon_array[i] if lon is not None else None
        current_y_pos = y_pos_array[i] if y_pos is not None else None
        current_x_pos = x_pos_array[i] if x_pos is not None else None

        ret_dict = _get_single_point(current_lat, current_lon,
                                     current_y_pos, current_x_pos,
                                     length, width, lat_beg, lon_beg,
                                     step_y, step_x, image_obj)
        x_pos_list.append(ret_dict.get('x_pos', None))
        y_pos_list.append(ret_dict.get('y_pos', None))
        lat_list.append(ret_dict.get('lat', None))
        lon_list.append(ret_dict.get('lon', None))
        x_pos_image_list.append(ret_dict.get('x_pos_image', None))
        y_pos_image_list.append(ret_dict.get('y_pos_image', None))
        lon_image_list.append(ret_dict.get('lon_image', None))
        lat_image_list.append(ret_dict.get('lat_image', None))
        flag_outside_image_list.append(ret_dict.get('flag_outside_image',
                                                    None))
        data_list.append(ret_dict.get('data', None))
    ret_dict = _get_point_prepare_output(
        y_pos=y_pos_list, x_pos=x_pos_list, lat=lat_list, lon=lon_list,
        lat_image=lat_image_list, lon_image=lon_image_list,
        y_pos_image=y_pos_image_list, x_pos_image=x_pos_image_list,
        flag_outside_image=flag_outside_image_list, data=data_list,
        ref_array=x_pos)
    if verbose:
        print_get_point_results(ret_dict)
    return ret_dict


def _get_single_point(lat, lon, y_pos, x_pos, length, width,
                      lat_beg, lon_beg, step_y, step_x,
                      image_obj):

    if plant.isvalid(lat) and plant.isnan(y_pos):

        y_pos = int(np.floor((lat - lat_beg) / step_y))

    if plant.isnan(lat) and plant.isvalid(y_pos):

        lat = y_pos * step_y + lat_beg

    if plant.isvalid(lon) and plant.isnan(x_pos):
        x_pos = int(np.floor((lon - lon_beg) / step_x))
    if plant.isnan(lon) and plant.isvalid(x_pos):
        lon = x_pos * step_x + lon_beg

    flag_valid_y = not (plant.isnan(y_pos) or
                        y_pos < 0 or
                        y_pos > length - 1)
    flag_valid_x = not (plant.isnan(x_pos) or
                        x_pos < 0 or
                        x_pos > width - 1)

    x_pos = np.nan if plant.isnan(x_pos) else x_pos
    y_pos = np.nan if plant.isnan(y_pos) else y_pos
    flag_outside_image = not flag_valid_x or not flag_valid_y

    if plant.isvalid(y_pos) and not flag_valid_y:
        y_pos_image = int(min(y_pos, length - 1))
        y_pos_image = int(max(y_pos_image, 0))
    else:
        y_pos_image = y_pos
    lat_image = None
    lon_image = None
    if (flag_valid_y and plant.isvalid(length) and
            plant.isvalid(y_pos_image) and
            plant.isvalid(step_y) and
            plant.isvalid(lat_beg)):

        lat_image = (y_pos_image + 0.5) * step_y + lat_beg

    else:
        lat_image = y_pos * np.nan
    if plant.isvalid(x_pos) and not flag_valid_x:
        x_pos_image = int(min(x_pos, width - 1))
        x_pos_image = int(max(x_pos_image, 0))
    else:
        x_pos_image = x_pos

    if (flag_valid_x and
            plant.isvalid(x_pos_image) and
            plant.isvalid(step_x) and
            plant.isvalid(lon_beg)):
        lon_image = (x_pos_image + 0.5) * step_x + lon_beg
    else:
        lat_image = x_pos * np.nan

    ret_dict = {}
    ret_dict['x_pos'] = x_pos
    ret_dict['y_pos'] = y_pos
    ret_dict['lat'] = lat
    ret_dict['lon'] = lon
    ret_dict['x_pos_image'] = x_pos_image
    ret_dict['y_pos_image'] = y_pos_image
    ret_dict['lon_image'] = lon_image
    ret_dict['lat_image'] = lat_image
    ret_dict['flag_outside_image'] = flag_outside_image

    if (plant.isnan(y_pos_image) or plant.isnan(x_pos_image) or
            image_obj is None):

        return ret_dict

    data_value = []

    image_crop_obj = plant.read_image(image_obj, select_row=int(y_pos_image),
                                      select_col=int(x_pos_image))

    for b in range(image_obj.nbands):
        image = image_crop_obj.get_image(band=b)
        if image is None:
            print('ERROR reading %s band %d'
                  % (image_obj.filename, b))
        data_value.append(image[0, 0])

    ret_dict['data'] = data_value

    return ret_dict


def update_geotransform_with_coord_transformation(
        plant_grid_obj,
        coord_transformation,
        output_srs):

    if coord_transformation is None:
        return plant_grid_obj

    y0 = plant_grid_obj.y0
    yf = plant_grid_obj.yf
    step_y = plant_grid_obj.step_y
    length = plant_grid_obj.length

    x0 = plant_grid_obj.x0
    xf = plant_grid_obj.xf
    step_x = plant_grid_obj.step_x
    width = plant_grid_obj.width

    y_list = [y0, y0, yf, yf]
    x_list = [x0, xf, x0, xf]

    new_x_list = []
    new_y_list = []

    new_step_x = None
    new_step_y = None
    for y in y_list:
        for x in x_list:
            if not plant.isvalid(x) or not plant.isvalid(y):
                continue
            new_x, new_y, _ = coord_transformation.TransformPoint(
                float(x), float(y), float(0))
            if new_step_x is None:
                new_x_temp, _, _ = coord_transformation.TransformPoint(
                    float(x + step_x), float(y), float(0))

                if output_srs.IsGeographic() and new_x_temp - new_x > 180:
                    new_step_x = new_x_temp - 360 - new_x
                else:
                    new_step_x = new_x_temp - new_x

            if new_step_y is None:
                _, new_y_temp, _ = \
                    coord_transformation.TransformPoint(float(x),
                                                        float(y + step_y),
                                                        float(0))
                new_step_y = new_y_temp - new_y

            new_x_list.append(new_x)
            new_y_list.append(new_y)

    min_x = np.nanmin(new_x_list)
    max_x = np.nanmax(new_x_list)

    min_y = np.nanmin(new_y_list)
    max_y = np.nanmax(new_y_list)

    if output_srs.IsGeographic() and max_x - min_x > 180:

        unwrapped_new_x_list = [x + (x < 0) * 360 for x in new_x_list]
        min_x = np.min(unwrapped_new_x_list)
        max_x = np.max(unwrapped_new_x_list)

    max_size_xy = np.ceil(np.sqrt(width ** 2 + length ** 2))
    min_size_xy = min([width, length])

    if new_step_y == 0:
        new_step_y = 1e-10
    if new_step_x == 0:
        new_step_x = 1e-10

    new_width = (max_x - min_x) / new_step_x
    new_length = (min_y - max_y) / new_step_y

    if (new_width < min_size_xy or
            new_width > max_size_xy):
        new_width = max_size_xy

    if (new_length < min_size_xy or
            new_length > max_size_xy):
        new_length = max_size_xy

    new_projection = get_projection_wkt_from_srs(output_srs)

    output_plant_grid_obj = plant.PlantGeogrid(
        y0=max_y,
        length=new_length,
        x0=min_x,
        width=new_width,
        step_x=new_step_x,
        step_y=new_step_y,
        projection=new_projection)

    return output_plant_grid_obj


def utm_to_wgs(north, east, zone):
    wgs84_coordinate_system = osr.SpatialReference()
    wgs84_coordinate_system.SetWellKnownGeogCS("WGS84")
    try:
        wgs84_coordinate_system.SetAxisMappingStrategy(
            osr.OAMS_TRADITIONAL_GIS_ORDER)
    except AttributeError:
        pass

    utm_coordinate_system = osr.SpatialReference()
    utm_coordinate_system.SetWellKnownGeogCS("WGS84")
    utm_coordinate_system.SetUTM(zone, True)
    try:
        utm_coordinate_system.SetAxisMappingStrategy(
            osr.OAMS_TRADITIONAL_GIS_ORDER)
    except AttributeError:
        pass

    transformation = osr.CoordinateTransformation(utm_coordinate_system,
                                                  wgs84_coordinate_system)
    lon, lat, _ = transformation.TransformPoint(
        float(east), float(north), float(0))
    return (lat, lon)


def get_boundaries_from_gcp_list(gcp_list):
    if not gcp_list:
        return

    min_y = None
    max_y = None
    min_x = None
    max_x = None
    for gcp in gcp_list:

        lon = gcp.GCPX
        lat = gcp.GCPY
        if plant.isnan(min_y) or min_y > lat:
            min_y = lat
        if plant.isnan(max_y) or max_y < lat:
            max_y = lat
        if plant.isnan(min_x) or min_x > lon:
            min_x = lon
        if plant.isnan(max_x) or max_x < lon:
            max_x = lon
    ret_dict = {
        'x0': min_x,
        'xf': max_x,
        'y0': max_y,
        'yf': min_y
    }

    return ret_dict


def get_coordinates(plant_geogrid_obj=None,
                    geo_center=None,
                    geo_search_str=None,
                    bbox=None,

                    gcp_list=None,
                    gcp_projection=None,

                    step=None,
                    step_y=None,
                    step_x=None,
                    step_m=None,
                    step_m_y=None,
                    step_m_x=None,
                    bbox_topo=None,
                    bbox_file=None,
                    verbose=None,

                    projection=None,
                    default_step=None,
                    geotransform=None,
                    width=None,
                    length=None,

                    image_obj=None,
                    plant_transform_obj=None,
                    flag_output_in_wgs84=False,
                    flag_debug=False):

    if geotransform == [0.0, 1.0, 0.0, 0.0, 0.0, 1.0]:
        geotransform = None

    kwargs = locals()

    if plant_geogrid_obj is not None:
        if flag_debug:
            print('*** 0 - plant_geogrid_obj from argument')
        plant_geogrid_obj = plant_geogrid_obj.deepcopy()
        if flag_output_in_wgs84:
            plant_geogrid_obj.projection = 'WGS84'
    else:
        if flag_debug:
            print('*** 0 - empty plant_geogrid_obj')
        plant_geogrid_obj = plant.PlantGeogrid()

    if flag_debug:
        print(f'*** 1 - plant_geogrid_obj: {plant_geogrid_obj.print()}')

    if not plant_geogrid_obj.is_complete() and image_obj is not None:
        temp_grid_obj = plant.PlantGeogrid(
            geotransform=image_obj.geotransform,
            width=image_obj.width,
            length=image_obj.length,
            projection=image_obj.projection)

        plant_geogrid_obj.merge(temp_grid_obj)

        if flag_output_in_wgs84:
            plant_geogrid_obj.projection = 'WGS84'
        if flag_debug:
            print(f'*** 2 - geotransform: {plant_geogrid_obj.print()}')

    if flag_debug:
        print(f'*** 3 - image_obj: {plant_geogrid_obj.print()}')

    if not plant_geogrid_obj.is_complete() and gcp_list:
        if not width or not length:
            print('ERROR cannot retrieve GCP geogrid without grid sizes')

        ret_dict = get_boundaries_from_gcp_list(gcp_list)
        temp_geogrid_obj = plant.PlantGeogrid(
            x0=ret_dict['x0'],
            step_x=(ret_dict['xf'] - ret_dict['x0']) / width,
            y0=ret_dict['y0'],
            step_y=(ret_dict['yf'] - ret_dict['y0']) / length,
            width=width,
            length=length,
            projection=gcp_projection)

        plant_geogrid_obj.merge(temp_geogrid_obj)
        if flag_output_in_wgs84:
            plant_geogrid_obj.projection = 'WGS84'
    if flag_debug:
        print(f'*** 4 - GCPs: {plant_geogrid_obj.print()}')

    if not plant_geogrid_obj.is_complete() and bbox_file is not None:

        geotransform_temp = None
        geo_list = [bbox_file] if isinstance(bbox_file, str) else bbox_file

        y_list_temp = []
        x_list_temp = []
        step_y_list_temp = []
        step_x_list_temp = []
        flag_print = True
        flag_geo_list_with_diff_projections = False
        projection_temp = None

        if len(geo_list) > 1:
            for current_geo in geo_list:
                if not plant.isfile(current_geo):
                    continue
                image_obj = plant.read_image(
                    current_geo,
                    only_header=True,
                    plant_transform_obj=plant_transform_obj)
                if image_obj is None:
                    continue
                if projection_temp is None:
                    projection_temp = image_obj.projection
                    continue
                if (compare_projections(projection_temp, image_obj.projection)
                        is False):
                    flag_geo_list_with_diff_projections = True
                    break

        for current_geo in geo_list:
            if not plant.isfile(current_geo):
                continue
            image_obj = plant.read_image(
                current_geo,
                only_header=True,
                plant_transform_obj=plant_transform_obj)
            if image_obj is None:
                continue
            geotransform_temp = image_obj.geotransform

            gcp_list_temp = image_obj.gcp_list

            if geotransform_temp is None and gcp_list_temp:
                ret_dict = get_boundaries_from_gcp_list(gcp_list_temp)

                temp_grid_obj = plant.PlantGeogrid(
                    x0=ret_dict['x0'],
                    step_x=(ret_dict['xf'] - ret_dict['x0']) / width,
                    y0=ret_dict['y0'],
                    step_y=(ret_dict['yf'] - ret_dict['y0']) / length,
                    width=image_obj.width,
                    length=image_obj.length,
                    projection=image_obj.gcp_projection)

            elif geotransform_temp is None:
                continue

            else:
                temp_grid_obj = plant.PlantGeogrid(
                    geotransform=image_obj.geotransform,
                    width=image_obj.width,
                    length=image_obj.length,
                    projection=image_obj.projection)

            if verbose and flag_print:
                print('coordinates from reference GEO')
                flag_print = False

            if (flag_output_in_wgs84 or flag_geo_list_with_diff_projections):
                temp_grid_obj.projection = 'WGS84'

            x_list_temp.append(temp_grid_obj.x0)
            x_list_temp.append(temp_grid_obj.xf)
            y_list_temp.append(temp_grid_obj.y0)
            y_list_temp.append(temp_grid_obj.yf)

            step_y_list_temp.append(temp_grid_obj.step_y)

            step_x_list_temp.append(temp_grid_obj.step_x)

        if len(x_list_temp) > 1:
            min_x = np.nanmin(x_list_temp)
            max_x = np.nanmax(x_list_temp)
            min_step_x = np.nanmin(step_x_list_temp)

            min_y = np.nanmin(y_list_temp)
            max_y = np.nanmax(y_list_temp)
            min_step_y = np.nanmin(step_y_list_temp)

            output_plant_grid_obj = plant.PlantGeogrid(
                x0=min_x,
                y0=max_y,
                width=(max_x - min_x) / min_step_x,
                step_x=min_step_x,
                step_y=min_step_y,

                length=(min_y - max_y) / min_step_y,
                projection=temp_grid_obj.projection)
            plant_geogrid_obj.merge(output_plant_grid_obj)

    if flag_debug:
        print(f'*** 5 - bbox_file ({bbox_file}):'
              f' {plant_geogrid_obj.print()}')

    if not plant_geogrid_obj.is_complete() and bbox_topo is not None:
        geotransform_temp = None

        if isinstance(bbox_topo, list):
            print(f'ERROR list of topo directories not supported: {bbox_topo}')

        ret_dict = get_lat_lon_boundaries_from_topo_dir(
            bbox_topo,
            plant_transform_obj=plant_transform_obj)

        if verbose and ret_dict is not None:
            print('coordinates from georeference files')
        if ret_dict is not None:

            temp_grid_obj = plant.PlantGeogrid(
                x0=ret_dict['x0'],
                step_x=ret_dict['step_x'],
                width=(ret_dict['xf'] - ret_dict['x0']) / ret_dict['step_x'],

                y0=ret_dict['y0'],
                step_y=ret_dict['step_y'],
                length=(ret_dict['yf'] - ret_dict['y0']) / ret_dict['step_y']

            )
            temp_grid_obj.print()

            plant_geogrid_obj.merge(temp_grid_obj)

    if flag_debug:
        print(f'*** 6 - topo_dir: {plant_geogrid_obj.print()}')

    if kwargs['projection'] is None:
        kwargs['projection'] = plant_geogrid_obj.projection
    temp_geogrid_obj = plant_geogrid_obj

    plant_geogrid_obj = get_geogrid_precedence(**kwargs)

    if geo_search_str:
        geogrid_geo_search_obj = \
            get_geogrid_from_geo_search(geo_search_str)

        plant_geogrid_obj.merge(geogrid_geo_search_obj)

    if (not plant_geogrid_obj.has_valid_bounding_box() and
            not plant_geogrid_obj.has_valid_steps()):
        plant_geogrid_obj = temp_geogrid_obj

    elif (compare_projections(plant_geogrid_obj.projection,
                              temp_geogrid_obj.projection) is not False):
        plant_geogrid_obj.populate_missing_attributes(temp_geogrid_obj)
    else:

        plant_geogrid_obj.merge(temp_geogrid_obj)

    if flag_output_in_wgs84:

        plant_geogrid_obj.projection = 'WGS84'

    if flag_debug:
        print(f'*** 7 - precedence parameters: {plant_geogrid_obj.print()}')

    if (not plant.isvalid(plant_geogrid_obj.step_x) and
            default_step is not None):
        plant_geogrid_obj.step_x = abs(default_step)
    if (not plant.isvalid(plant_geogrid_obj.step_y) and
            default_step is not None):
        plant_geogrid_obj.step_y = -abs(default_step)

    if verbose:
        plant_geogrid_obj.print()

    if flag_debug:
        print(f'*** 8 - returned variable: {plant_geogrid_obj.print()}')

    return plant_geogrid_obj


def get_geogrid_from_geo_search(geo_search_str):
    if geo_search_str is None:
        return
    ret_dict = get_lat_lon_search_str(geo_search_str[0],
                                      verbose=True)
    print('coordinates from geo_search: ', ret_dict)
    lat = ret_dict['lat']
    lon = ret_dict['lon']
    lat_diff = float(geo_search_str[1]) / 2
    lon_diff = float(geo_search_str[2]) / 2

    yf = lat - lat_diff

    y0 = lat + lat_diff

    x0 = lon - lon_diff

    xf = lon + lon_diff
    projection = (
        'GEOGCS["WGS 84",DATUM["WGS_1984",SPHEROID["WGS 84",'
        '6378137,298.257223563,AUTHORITY["EPSG","7030"]],'
        'AUTHORITY["EPSG","6326"]],PRIMEM["Greenwich",0],'
        'UNIT["degree",0.0174532925199433,AUTHORITY["EPSG","9122"]],'
        'AXIS["Latitude",NORTH],AXIS["Longitude",EAST],'
        'AUTHORITY["EPSG","4326"]]')
    plant_geogrid_obj = plant.PlantGeogrid(
        x0=x0,
        xf=xf,
        yf=yf,
        y0=y0,

        projection=projection)

    return plant_geogrid_obj


def get_geogrid_precedence(

    geotransform=None,
    length=None,
    width=None,
    geo_center=None,

    bbox=None,
    step=None,
    step_y=None,
    step_x=None,
    step_m=None,
    step_m_y=None,
    step_m_x=None,
    verbose=None,

    projection=None,
    default_step=None,

        **kwargs):
    x0 = None
    xf = None
    y0 = None
    yf = None

    if geotransform is not None and plant.isnan(x0):
        x0 = geotransform[0]

    if geotransform is not None and plant.isnan(y0):
        y0 = geotransform[3]

    if geo_center is not None:
        if not plant.isvalid(yf):
            yf = geo_center[0] - geo_center[2] / 2
        if not plant.isvalid(y0):
            y0 = geo_center[0] + geo_center[2] / 2
        if not plant.isvalid(x0):
            x0 = geo_center[1] - geo_center[3] / 2
        if not plant.isvalid(xf):
            xf = geo_center[1] + geo_center[3] / 2

    if bbox is not None:
        if verbose:
            print('coordinates from bbox')
        if not plant.isvalid(yf):
            yf = float(bbox[0])
        if not plant.isvalid(y0):
            y0 = float(bbox[1])
        if y0 < yf:
            print(f'ERROR yf ({y0}) is greater than y0 ({yf})')
            return
        if not plant.isvalid(x0):
            x0 = float(bbox[2])
        if not plant.isvalid(xf):
            xf = float(bbox[3])
        if xf < x0:
            print(f'ERROR x0 ({x0}) is greater than xf ({xf})')
            return

    flag_projected = projection is not None and is_projected(projection)

    if step_m_y is None and step_m is not None:
        step_m_y = -abs(step_m)

    if step_y is None and step is not None:
        step_y = -abs(step)

    if step_y is None and step_m_y is not None and flag_projected:
        step_y = step_m_y

    elif step_y is None and step_m_y is not None:
        step_y = m_to_deg_lat(step_m_y)

    if step_m_x is None and step_m is not None:
        step_m_x = step_m

    if step_x is None and step is not None:
        step_x = step

    if step_x is None and step_m_x is not None and flag_projected:
        step_x = step_m_x

    elif step_x is None and step_m_x is not None:
        if y0 is not None and yf is not None:
            lat = np.nanmean([y0, yf])
        elif y0 is not None:
            lat = y0
        elif yf is not None:
            lat = yf
        else:
            lat = None
        step_x = m_to_deg_lon(step_m_x, lat=lat)

    if geotransform is not None and plant.isnan(step_x):
        step_x = geotransform[1]

    if geotransform is not None and plant.isnan(step_y):
        step_y = geotransform[5]

    if plant.isnan(x0) and plant.isnan(xf):
        length = None
    if plant.isnan(y0) and plant.isnan(yf):
        width = None
    plant_geogrid_obj = plant.PlantGeogrid(
        x0=x0,
        xf=xf,
        yf=yf,
        y0=y0,
        step_x=step_x,
        step_y=step_y,
        width=width,
        length=length,
        projection=projection)

    return plant_geogrid_obj


def get_lat_lon_search_str(search_str, verbose=True):

    import requests

    session = requests.Session()
    PARAMS = {'action': "query",
              'prop': "coordinates",
              'titles': search_str,
              'format': "json"}
    request = session.get(url=plant.WIKI_API, params=PARAMS)
    data = request.json()
    pages = data['query']['pages']
    ret_dict = {}
    for key, value in pages.items():
        if 'coordinates' in value:
            ret_dict['lat'] = value['coordinates'][0]['lat']
            ret_dict['lon'] = value['coordinates'][0]['lon']
            break
    else:
        if verbose:
            print(f'ERROR location not found: {search_str}')
        return
    return ret_dict


def epsg_to_wkt(epsg_code):
    spatial_ref = osr.SpatialReference()
    spatial_ref.ImportFromEPSG(epsg_code)

    wkt = spatial_ref.ExportToWkt()

    return wkt


def get_projection_wkt(projection):
    srs = plant.get_srs_from_projection(projection)
    if srs is None:
        return srs
    projection_wkt = get_projection_wkt_from_srs(srs)
    return projection_wkt


def get_projection_wkt_from_srs(srs):
    projection_wkt = str(srs).replace('\n', '')
    projection_wkt = projection_wkt.replace('    ', ' ')
    projection_wkt = projection_wkt.strip()
    return projection_wkt


def get_projection_proj4(projection):
    srs = plant.get_srs_from_projection(projection)
    if srs is None:
        return
    projection = srs.ExportToProj4()
    projection = projection.strip()
    return projection


def get_srs_from_projection(projection,
                            flag_oams_traditional_gis_order=True):
    if not projection:
        return
    srs = osr.SpatialReference()
    if projection.upper() == 'WGS84':
        srs.SetWellKnownGeogCS(projection)
    else:
        srs.ImportFromProj4(projection)
    if not flag_oams_traditional_gis_order:
        return srs
    try:
        srs.SetAxisMappingStrategy(
            osr.OAMS_TRADITIONAL_GIS_ORDER)
    except AttributeError:
        pass
    return srs


def compare_projections(projection_1, projection_2):
    if ((projection_1 is None and projection_2 is not None) or
            (projection_1 is not None and projection_2 is None)):
        return
    srs_1 = get_srs_from_projection(projection_1)
    if srs_1 is None:
        return
    srs_2 = get_srs_from_projection(projection_2)
    if srs_2 is None:
        return

    return bool(srs_1.IsSame(srs_2))


def update_geotransform_projection(plant_grid_obj,
                                   output_projection='WGS84'):

    if not output_projection:
        return plant_grid_obj

    if not plant_grid_obj.projection:

        plant_grid_obj.projection = output_projection
        return plant_grid_obj

    if compare_projections(
            plant_grid_obj.projection, output_projection) is not False:

        return plant_grid_obj

    if not plant_grid_obj.has_valid_geotransform():
        return plant_grid_obj

    current_srs = get_srs_from_projection(
        plant_grid_obj.projection)
    if not current_srs:
        print('WARNING error transforming projection from %s to %s'
              % (plant_grid_obj.projection, output_projection))
        return plant_grid_obj

    output_srs = get_srs_from_projection(output_projection)
    if not output_srs:
        print('WARNING error transforming projection from %s to %s'
              % (plant_grid_obj.projection, output_projection))
        return plant_grid_obj

    coord_transformation = None
    try:
        coord_transformation = osr.CoordinateTransformation(
            current_srs,
            output_srs)
    except TypeError:
        print('WARNING error transforming projection from %s to %s (TypeError)'
              % (plant_grid_obj.projection, output_projection))
        return plant_grid_obj

    output_plant_grid_obj = update_geotransform_with_coord_transformation(
        plant_grid_obj,
        coord_transformation=coord_transformation,
        output_srs=output_srs)

    return output_plant_grid_obj


def is_geographic(projection):
    if not projection:
        return
    srs = plant.get_srs_from_projection(projection)
    return bool(srs.IsGeographic())


def is_projected(projection):
    if not projection:
        return
    srs = plant.get_srs_from_projection(projection)
    return bool(srs.IsProjected())


def is_polar_stereographic(projection):
    if not projection:
        return
    if not is_projected(projection):
        return False
    projection_upper = projection.upper()
    return 'POLAR' in projection_upper


def print_geoinformation(plant_geogrid_obj=None, **kwargs):

    prefix = kwargs.pop('prefix', '')

    flag_print_all = kwargs.pop('flag_print_all', False)
    flag_print_step = kwargs.pop('flag_print_step', True)

    kwargs['verbose'] = False
    if plant_geogrid_obj is None:
        plant_geogrid_obj = get_coordinates(**kwargs)

    x0 = plant_geogrid_obj.x0
    xf = plant_geogrid_obj.xf
    y0 = plant_geogrid_obj.y0
    yf = plant_geogrid_obj.yf
    step_x = plant_geogrid_obj.step_x
    step_y = plant_geogrid_obj.step_y
    width = plant_geogrid_obj.width
    length = plant_geogrid_obj.length

    print('*** print_geoinformation()')
    print('*** x0:', plant_geogrid_obj.x0)
    print('*** xf:', plant_geogrid_obj.xf)
    print('*** y0:', plant_geogrid_obj.y0)
    print('*** yf:', plant_geogrid_obj.yf)
    print('*** step_x:', plant_geogrid_obj.step_x)
    print('*** step_y:', plant_geogrid_obj.step_y)
    print('*** width:', plant_geogrid_obj.width)
    print('*** length:', plant_geogrid_obj.length)
    if (not plant_geogrid_obj.has_valid_bounding_box() and
            not plant_geogrid_obj.has_valid_steps()):
        return

    flag_projected = plant_geogrid_obj.is_projected()
    flag_polar_stereographic = plant_geogrid_obj.is_polar_stereographic()

    if not flag_projected:
        northing_str = 'lat'
        easting_str = 'lon'
        unit_str = '[deg]'
    elif flag_polar_stereographic:
        northing_str = 'Y'
        easting_str = 'X'
        unit_str = '[m]'
    else:
        northing_str = 'northing'
        easting_str = 'easting'
        unit_str = '[m]'

    if plant_geogrid_obj.projection:
        print(prefix + 'projection:',
              plant_geogrid_obj.projection.replace(
                  '],', '],\n' + prefix + ' ' * len('projection: ')), end='\r')
    else:
        print(prefix + 'projection: (empty)')

    if flag_print_all or flag_print_step:
        if ((plant.isvalid(yf) and plant.isvalid(y0)) or
                (plant.isvalid(x0) and plant.isvalid(xf))):
            print(prefix + 'boundaries:')
        if (plant.isvalid(yf) and plant.isvalid(y0) and yf != y0):
            print(prefix + f'%s {unit_str}: %.16f, %.16f'
                  % (northing_str, yf, y0))
        elif plant.isvalid(yf):
            print(prefix + f'%s {unit_str}: %.16f'
                  % (northing_str, yf))
        if (plant.isvalid(x0) and plant.isvalid(xf) and x0 != xf):
            print(prefix + f'%s {unit_str}: %.16f, %.16f'
                  % (easting_str, x0, xf))
        elif plant.isvalid(x0):
            print(prefix + f'%s {unit_str}: %.16f'
                  % (easting_str, x0))

    if (plant.isvalid(yf) and plant.isvalid(y0) and yf != y0):
        print(prefix + f'center %s {unit_str}: %.16f'
              % (northing_str, (yf + y0) / 2.0))
    if (plant.isvalid(x0) and plant.isvalid(xf) and x0 != xf):
        print(prefix + f'center %s {unit_str}: %.16f'
              % (easting_str, (x0 + xf) / 2.0))

    if flag_print_step and step_y is not None:
        if not flag_projected:
            approx_str = '(approx. %.1fm)' % deg_to_m_lat(step_y)
        else:
            approx_str = ''
        print(prefix + f'%s step {unit_str}: %.16f %s'
              % (northing_str, step_y, approx_str))
    if flag_print_step and step_x is not None:
        if (flag_projected or plant.isnan(yf) or plant.isnan(y0)):
            approx_str = ''
        else:
            mean_lat = (yf + y0) / 2.0
            approx_str = ('(approx. %.1fm at lat=%.0f)'
                          % (deg_to_m_lon(step_x, lat=mean_lat), mean_lat))
        print(prefix + f'%s step {unit_str}: %.16f %s'
              % (easting_str, step_x, approx_str))

    if flag_print_all:
        plant_parameters_str = \
            f'{prefix}PLAnT parameters: -b {yf} {y0} {x0} {xf}'
        gdal_translate_parameters_str = \
            (f'{prefix}GDAL translate parameters:'
             f' -projwin {x0} {y0} {xf} {yf}')
        gdal_warp_parameters_str = \
            f'{prefix}GDAL warp parameters: -te {x0} {yf} {xf} {y0}'

        if (flag_print_step and step_x is not None and
                step_x is not None):
            plant_parameters_str += \
                f' --step-lat {step_y}  --step-lon {step_x}'
            gdal_translate_parameters_str += f' -tr {step_x} {step_y}'
            gdal_warp_parameters_str += f' -tr {step_x} {step_y}'

        print(plant_parameters_str)
        print(gdal_translate_parameters_str)
        print(gdal_warp_parameters_str)

        print(prefix + 'polygon (counterclockwise) (lon, lat): '
              '%.8f,%.8f,%.8f,%.8f,%.8f,%.8f,%.8f,%.8f'
              % (xf, y0, x0, y0, x0, yf, xf, yf))

    if length is not None and step_y is not None:
        if flag_projected:
            approx_str = ''
        else:
            approx_str = ('(approx. %.3fkm)'
                          % (1e-3 * length * deg_to_m_lat(abs(step_y))))
        print(prefix + f'%s size {unit_str}: %.16f %s'
              % (northing_str, length * abs(step_y), approx_str))

    if width is not None and step_x is not None:
        if flag_projected:
            approx_str = ''
        else:
            mean_lat = (yf + y0) / 2.0
            approx_str = ('(approx. %.3fkm at lat=%.0f)'
                          % (1e-3 * width * deg_to_m_lon(step_x, lat=mean_lat),
                             mean_lat))
        print(prefix + f'%s size {unit_str}: %.16f %s'
              % (easting_str, width * step_x, approx_str))


def m_to_deg(m_value, lat=0):

    delta_lat = 180 * m_value / (np.pi * plant.EARTH_RADIUS)
    delta_lon = 180 * m_value / \
        (np.pi * plant.EARTH_RADIUS * np.cos(np.pi * lat / 180))
    return delta_lat, delta_lon


def m_to_deg_lat(m_value):
    delta_lat = (180 * m_value /
                 (np.pi * plant.EARTH_RADIUS))
    return delta_lat


def m_to_deg_lon(m_value, lat=None):
    if lat is None:
        lat = 0.0
    delta_lon = (180 * m_value /
                 (np.pi * plant.EARTH_RADIUS * np.cos(np.pi * lat / 180)))
    return delta_lon


def deg_to_m(deg_value, lat=0):

    delta_lat = deg_value * np.pi * plant.EARTH_RADIUS / 180
    delta_lon = deg_value * \
        (np.pi * plant.EARTH_RADIUS * np.cos(np.pi * lat / 180)) / 180
    return delta_lat, delta_lon


def deg_to_m_lat(deg_value):
    delta_lat = deg_value * np.pi * plant.EARTH_RADIUS / 180
    return delta_lat


def deg_to_m_lon(deg_value, lat=0):
    delta_lon = (deg_value * (np.pi * plant.EARTH_RADIUS *
                              np.cos(np.pi * lat / 180)) / 180)
    return delta_lon


def test_overlap(plant_geogrid_obj=None,
                 image_obj=None,

                 plant_geogrid_2_obj=None,

                 image_2_obj=None):

    plant_geogrid_a_obj = get_coordinates(
        plant_geogrid_obj=plant_geogrid_obj,
        image_obj=image_obj,

        flag_output_in_wgs84=True)

    if not plant_geogrid_a_obj.has_valid_coordinates():

        return

    plant_geogrid_b_obj = get_coordinates(
        plant_geogrid_obj=plant_geogrid_2_obj,
        image_obj=image_2_obj,

        flag_output_in_wgs84=True)

    if not plant_geogrid_b_obj.has_valid_coordinates():

        return

    flag_intersects = plant_geogrid_a_obj.intersects(plant_geogrid_b_obj)

    return flag_intersects


def test_point_inside_bbox(lat, lon, bbox):
    if (lat >= bbox[0] and lat <= bbox[1] and
            lon >= bbox[2] and lon <= bbox[3]):

        return True

    return False


def geotransform_all_invalid(geotransform):

    if not plant.is_sequence(geotransform):
        return True
    for i, value in enumerate(geotransform):
        if plant.isvalid(value) and i != 2 and i != 4:
            return False
    return True


def valid_coordinates(**kwargs):

    plant_geogrid_obj = get_coordinates(**kwargs)
    return plant_geogrid_obj.has_valid_coordinates()


def get_geotransform(bbox=None,

                     y0=None,
                     yf=None,
                     x0=None,
                     xf=None,
                     step_y=None,
                     step_x=None,
                     step=None,
                     length=None,
                     width=None,
                     geotransform=None):

    if geotransform is not None:
        if step_x is None:
            step_x = geotransform[1]
        if step_y is None:
            step_y = geotransform[5]
        if y0 is None:
            y0 = geotransform[3]
        if x0 is None:
            x0 = geotransform[0]
        if (length is not None and
                step_y is not None):
            yf = (geotransform[3] + length * step_y)
        if (width is not None and
                step_x is not None):
            xf = (geotransform[0] + width * step_x)

    if plant.isnan(yf) and bbox is not None:
        yf = bbox[0]
    if plant.isnan(yf):
        yf = np.nan
    else:
        yf = float(yf)

    if plant.isnan(y0) and bbox is not None:
        y0 = bbox[1]
    if plant.isnan(y0):
        y0 = np.nan
    else:
        y0 = float(y0)

    if plant.isnan(x0) and bbox is not None:
        x0 = bbox[2]
    if plant.isnan(x0):
        x0 = np.nan
    else:
        x0 = float(x0)

    if plant.isnan(xf) and bbox is not None:
        xf = bbox[3]

    if plant.isnan(xf):
        xf = np.nan
    else:
        xf = float(xf)

    if (step is not None and
            (step_y is None or
             step_x is None)):
        step_temp = np.asarray(step)
        dim = np.sum(step_temp.shape)
        if dim == 0:
            step_temp = [step, step]
        elif dim == 1:
            step_temp = [step[0], step[0]]
        else:
            step_temp = step
        if step_y is None:
            step_y = step_temp[0]
        if step_x is None:
            step_x = step_temp[1]

    if (plant.isvalid(yf) and
            plant.isvalid(y0) and
            plant.isvalid(step_y) and
            (yf - y0) * step_y < 0):
        step_y *= -1

    if length is None:
        if (plant.isvalid(yf) and
                plant.isvalid(y0) and
                plant.isvalid(step_y)):
            length = (float(yf - y0) / step_y)
        else:
            length = np.nan

    if (plant.isnan(step_y) and
            plant.isvalid(y0) and
            plant.isvalid(yf) and
            plant.isvalid(length)):
        step_y = float(yf - y0) / length

    if width is None:
        if (plant.isvalid(x0) and
                plant.isvalid(xf) and
                plant.isvalid(step_x)):
            width = round(float(xf - x0) / step_x)
        else:
            width = np.nan

    if (plant.isnan(step_x) and
            plant.isvalid(xf) and
            plant.isvalid(x0) and
            plant.isvalid(width)):
        step_x = float(xf - x0) / width

    geotransform = [x0,
                    step_x,
                    0,
                    y0,
                    0,
                    step_y]

    return geotransform


def get_bbox(x0, xf, y0, yf, string=False):

    bbox = np.asarray([yf, y0, x0, xf]).ravel()
    if string:
        return ' '.join([str(x) for x in bbox])
    return bbox


def get_lat_lon_arr(bbox):
    lat_arr = np.asarray(bbox[0:2], dtype=np.float64)
    lon_arr = np.asarray(bbox[2:4], dtype=np.float64)
    return lat_arr, lon_arr


def geotransform_shift(geotransform_orig, y_shift, x_shift,
                       length=None, width=None):
    if geotransform_orig is None:
        return
    plant_geogrid_obj = get_coordinates(geotransform=geotransform_orig,
                                        length=length,
                                        width=width)
    if not plant_geogrid_obj.has_valid_geotransform() is None:
        return

    print('shifting geo-reference by (%.16f, %.16f)' % (y_shift, x_shift))

    geotransform = [plant_geogrid_obj.x0 + x_shift,
                    plant_geogrid_obj.step_x,
                    0,
                    plant_geogrid_obj.y0 - y_shift,
                    0,
                    plant_geogrid_obj.step_y]

    return geotransform


def geotransform_edges_to_centers(geotransform_orig,
                                  length=None,
                                  width=None):
    if geotransform_orig is None:
        return

    plant_geogrid_obj = get_coordinates(geotransform=geotransform_orig,
                                        length=length,
                                        width=width)

    if not plant_geogrid_obj.has_valid_geotransform():
        return

    geotransform = [plant_geogrid_obj.x0 + plant_geogrid_obj.step_x / 2,
                    plant_geogrid_obj.step_x,
                    0,
                    plant_geogrid_obj.y0 + plant_geogrid_obj.step_y / 2,
                    0,
                    plant_geogrid_obj.step_y]

    return geotransform


def geotransform_centers_to_edges(geotransform_orig,
                                  length=None,
                                  width=None):
    if geotransform_orig is None:
        return

    plant_geogrid_obj = get_coordinates(geotransform=geotransform_orig,
                                        length=length,
                                        width=width)

    if not plant_geogrid_obj.has_valid_geotransform():
        return

    geotransform = [plant_geogrid_obj.x0 - plant_geogrid_obj.step_x / 2,
                    plant_geogrid_obj.step_x,
                    0,
                    plant_geogrid_obj.y0 - plant_geogrid_obj.step_y / 2,
                    0,
                    plant_geogrid_obj.step_y]

    return geotransform


def get_geotransform_from_header(filename_orig, verbose=False):

    if plant.test_other_drivers(filename_orig):
        return

    try:
        from iscesys.Parsers.FileParserFactory import createFileParser
        from isceobj.Util import key_of_same_content
    except ModuleNotFoundError:
        return

    geotransform = None

    filename = filename_orig
    if ((not path.isfile(filename_orig)) and
            plant.IMAGE_NAME_SEPARATOR in filename_orig):
        filename = filename.split(plant.IMAGE_NAME_SEPARATOR)[0]

    gdal.UseExceptions()
    try:
        current_dataset = gdal.Open(filename_orig, gdal.GA_ReadOnly)
        file_format = str(current_dataset.GetDriver().ShortName)
        if verbose:
            print('reading coordinates from: %s (GDAL:%s) '
                  % (filename_orig, file_format))
    except BaseException:
        current_dataset = None

    if (current_dataset is None and
            plant.isfile(filename + '.vrt')):
        try:
            current_dataset = gdal.Open(filename + '.vrt',
                                        gdal.GA_ReadOnly)
            file_format = str(current_dataset.GetDriver().ShortName)
            if verbose:
                print('reading coordinates from: %s (GDAL:%s) '
                      % (filename, file_format))
        except BaseException:
            current_dataset = None

    if current_dataset is not None:

        geotransform = list(current_dataset.GetGeoTransform())
        width = current_dataset.RasterXSize
        length = current_dataset.RasterYSize

        if not valid_coordinates(geotransform=geotransform,
                                 length=length,
                                 width=width):
            geotransform = None

        current_dataset = None
        gdal.ErrorReset()

    if (geotransform is None and
            plant.isfile(filename + '.xml') or
            filename.endswith('.xml')):
        if verbose:
            print('reading coordinates from: %s (ISCE) ')
        PA = createFileParser('xml')
        if filename.endswith('.xml'):
            dictNow, dictFact, dictMisc = PA.parse(filename)
        else:
            dictNow, dictFact, dictMisc = PA.parse(filename + '.xml')
        flag_outer_edges = False
        for i in range(1, 3):
            try:
                coordinate = key_of_same_content(
                    'Coordinate' + str(i), dictNow)[1]
                starting_value = float(key_of_same_content(
                    'startingValue', coordinate)[1])
                delta = float(key_of_same_content(
                    'delta', coordinate)[1])
                size = float(key_of_same_content(
                    'size', coordinate)[1])
            except BaseException:
                return

            ending_value_center = starting_value + delta * (size - 1)
            ending_value_edges = starting_value + delta * (size)

            try:
                ending_value_header = float(key_of_same_content(
                    'endingValue', coordinate)[1])
            except BaseException:
                ending_value_header = float('nan')

            flag_force_pixels_center = True

            message = ''
            if plant.isvalid(ending_value_header):
                diff_center = abs(ending_value_center -
                                  ending_value_header)
                diff_edges = abs(ending_value_edges -
                                 ending_value_header)
                if (flag_force_pixels_center or
                        diff_center <= diff_edges):
                    ending_value = ending_value_center
                    message = ('WARNING (ISCE header) '
                               'considering pixels center convention..')
                else:
                    ending_value = ending_value_edges
                    flag_outer_edges = True
                    message = ('WARNING (ISCE header) '
                               'considering outer edges convention')
            else:

                ending_value = ending_value_header

            if i == 1:
                step_x = delta
                if flag_outer_edges:
                    lon_arr = [starting_value, ending_value]
                else:
                    lon_arr = [starting_value - step_x / 2.0,
                               ending_value + step_x / 2.0]
                lon_arr = [min(lon_arr), max(lon_arr)]
                width = size
            else:

                step_y = abs(delta)
                lat_arr = [starting_value, ending_value]
                if flag_outer_edges:
                    lat_arr = [min(lat_arr), max(lat_arr)]
                else:
                    lat_arr = [min(lat_arr) - step_y / 2.0,
                               max(lat_arr) + step_y / 2.0]
                length = size
        geotransform = [lon_arr[0], step_x, 0, lat_arr[1], 0,
                        -step_y, lon_arr[1], lat_arr[0], width,
                        length]

        if valid_coordinates(geotransform=geotransform):
            if message and verbose:
                print(message)
        else:
            geotransform = None

    return geotransform


def assign_georeferences(input_var,
                         topo_dir=None,
                         lat_file=None,
                         lon_file=None,
                         output_file=None,
                         plant_transform_obj=None,
                         suffix='temp',

                         flag_temporary=True,
                         force=True,
                         verbose=True):

    if isinstance(input_var, plant.PlantImage):
        image_obj = input_var
    else:
        image_obj = plant.PlantImage(input_var)
    if output_file is None:
        output_file = image_obj.filename
    width_data = image_obj.width
    length_data = image_obj.length

    if lat_file is None or lon_file is None:
        ret_dict = plant.get_topo_files(topo_dir)
        if lat_file is None and 'lat_file' in ret_dict.keys():
            lat_file = ret_dict['lat_file']
        if lon_file is None and 'lon_file' in ret_dict.keys():
            lon_file = ret_dict['lon_file']

    if not plant.isfile(lat_file):
        print('ERROR file not found: '
              f'lat.rdr, lat.vrt or lat.rdr.vrt in {topo_dir}')
        return
    if not plant.isfile(lon_file):
        print('ERROR file not found: '
              f'lon.rdr, lon.vrt or lon.rdr.vrt in {topo_dir}')
        return

    if verbose:
        print(f'Y (e.g., northing, latitude) file: {lat_file}')
        print(f'X (e.g., easting, longitude) file: {lon_file}')

    flag_generate_lat_lon = False
    flag_generate_new_file = False
    new_file = None
    lat_file_obj = None
    lon_file_obj = None

    if not isinstance(lat_file, str):
        lat_file_obj = lat_file
        flag_generate_lat_lon = True

    if not isinstance(lon_file, str):
        lon_file_obj = lon_file
        flag_generate_lat_lon = True

    if (not flag_generate_lat_lon and
        (not plant.test_gdal_open(lat_file) or
            not plant.test_gdal_open(lon_file) or
            (plant_transform_obj is not None and
             plant_transform_obj.flag_apply_transformation()))):

        flag_generate_lat_lon = True
    elif not flag_generate_lat_lon:
        ret_dict = plant.generate_vrt_with_geolocation_files(
            output_file, lat_file, lon_file,
            flag_temporary=flag_temporary)
        if ret_dict is None:
            flag_generate_lat_lon = True
            flag_generate_new_file = True
        else:
            new_file = ret_dict['data_file']
            plant.append_temporary_file(new_file)

    if flag_generate_lat_lon:
        print('updating northing/easting gelocation files...')
        if lat_file_obj is None:
            new_lat = path.basename(lat_file + '_geo_temp_lat_' +
                                    suffix).replace(':', '_')
            lat_file_obj = plant.read_image(
                lat_file,
                plant_transform_obj=plant_transform_obj,
                verbose=False)
        else:
            new_lat = path.basename('geo_temp_lat_' +
                                    suffix).replace(':', '_')

        if (lat_file_obj.length != length_data or
                lat_file_obj.width != width_data):
            print('ERROR input data and northing/lat geolocation array'
                  ' have different dimmensions: (%d, %d) and (%d, %d)'
                  % (length_data, width_data, lat_file_obj.length,
                     lat_file_obj.width))
            return
        if lon_file_obj is None:
            new_lon = path.basename(lon_file + '_geo_temp_lon_' +
                                    suffix).replace(':', '_')
            lon_file_obj = plant.read_image(
                lon_file,
                plant_transform_obj=plant_transform_obj,
                verbose=False)
        else:
            new_lon = path.basename('geo_temp_lon_' +
                                    suffix).replace(':', '_')
        if (lon_file_obj.length != length_data or
                lon_file_obj.width != width_data):
            print('ERROR input data and eating/lon geolocation array'
                  ' have different dimmensions: (%d, %d) and (%d, %d)'
                  % (length_data, width_data, lon_file_obj.length,
                     lon_file_obj.width))
            return
        plant.append_temporary_file(new_lat)
        plant.append_temporary_file(new_lon)
        plant.save_image(lat_file_obj, new_lat, force=force,
                         output_format=plant.DEFAULT_GDAL_FORMAT)
        plant.save_image(lon_file_obj, new_lon, force=force,
                         output_format=plant.DEFAULT_GDAL_FORMAT)
        if new_file is None and not flag_generate_new_file:
            ret_dict = plant.generate_vrt_with_geolocation_files(
                output_file, new_lat, new_lon,
                flag_temporary=flag_temporary)
            if ret_dict is not None:
                new_file = ret_dict['data_file']
        if new_file is None:
            new_file = path.basename(output_file + '_geo_temp_ag_' +
                                     suffix)
            plant.append_temporary_file(new_file)
            plant.copy_image(output_file,
                             new_file,
                             create_link=True,
                             force=force)
            ret_dict = plant.generate_vrt_with_geolocation_files(
                new_file, new_lat, new_lon,
                flag_temporary=flag_temporary)
            if ret_dict is not None:
                new_file = ret_dict['data_file']
    if new_file and not new_file.endswith('.vrt'):
        new_file += '.vrt'
    return new_file


def get_lat_lon_files_from_topo_dir(bbox_topo):
    topo_dict = plant.get_topo_files(bbox_topo)

    if topo_dict is None:
        print('ERROR invalid geolocation directory (topo_dir): %s'
              % bbox_topo)
        return
    file_lat = topo_dict['lat_file']
    if not plant.isfile(file_lat):
        print('ERROR northing/latitude file not found in %s'
              % bbox_topo)
        return
    file_lon = topo_dict['lon_file']
    if not plant.isfile(file_lon):
        print('ERROR easting/longitude file not found in %s'
              % bbox_topo)
        return
    return file_lat, file_lon


def get_lat_lon_boundaries_from_topo_dir(
        bbox_topo,
        plant_transform_obj=None):

    ret = get_lat_lon_files_from_topo_dir(bbox_topo)
    if ret is None:
        return
    file_y, file_x = ret
    ret = plant.get_min_max_gdal(file_y, plant_transform_obj)
    min_y, max_y = ret

    ret = plant.get_min_max_gdal(file_x, plant_transform_obj)
    min_x, max_x = ret

    lat_image_obj = plant.read_image(
        file_y,
        plant_transform_obj=plant_transform_obj)
    lon_image_obj = plant.read_image(
        file_x,
        plant_transform_obj=plant_transform_obj)
    width = lat_image_obj.width
    length = lat_image_obj.length
    y_data = lat_image_obj.image
    x_data = lon_image_obj.image

    angle_1 = np.arctan2(y_data[0, 0] - y_data[-1, 0],
                         x_data[0, 0] - x_data[-1, 0])
    angle_2 = np.arctan2(y_data[0, -1] - y_data[-1, -1],
                         x_data[0, -1] - x_data[-1, -1])
    angle = (angle_1 + angle_2) / 2.0
    length = abs(length * np.sin(angle)) + abs(width * np.cos(angle))
    width = abs(length * np.cos(angle)) + abs(width * np.sin(angle))

    step_y = float(min_y - max_y) / length
    step_x = float(max_x - min_x) / width
    step_x = (abs(step_y) + step_x) / 2.0
    step_y = -step_x

    ret_dict = {
        'x0': min_x,
        'xf': max_x,
        'yf': min_y,
        'y0': max_y,
        'step_x': step_x,
        'step_y': step_y
    }
    return ret_dict


def coregister_geo(input_file,
                   reference_file,
                   output_file=None,

                   in_null=None,
                   out_null=None,
                   verbose=False,
                   force=None):
    if verbose:
        print('co-registering %s to  %s geometry..'
              % (input_file, reference_file))
    if output_file is None:

        time_stamp = str(int(time.time()))

        output_file = 'temp_coregister_geo_%s' % (time_stamp)
        flag_temporary = True
    else:
        flag_temporary = False
    command = ('plant_mosaic.py %s -g %s -o %s '
               % (input_file,
                  reference_file,
                  output_file))
    if in_null is not None:
        command += (' --in-null %s' % (str(in_null)))
    if out_null is not None:
        command += (' --out-null %s' % (str(out_null)))
    if force is not None or plant.plant_config.flag_all:
        if force or plant.plant_config.flag_all:
            command += ' -f'
    if plant.plant_config.flag_never:
        command += ' -N'
    image_obj = plant.execute(command)

    if flag_temporary:
        if plant.isfile(output_file):
            remove(output_file)
        if plant.isfile(output_file + '.vrt'):
            remove(output_file + '.vrt')
        if plant.isfile(output_file + '.xml'):
            remove(output_file + '.xml')
        if plant.isfile(output_file + '.hdr'):
            remove(output_file + '.hdr')

    return image_obj
