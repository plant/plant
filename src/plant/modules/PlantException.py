#!/usr/bin/env python3
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Gustavo H. X. Shiroma, Marco Lavalle
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import sys


class PlantExceptionKeyboardInterrupt(Exception):
    def __init__(self):
        print('WARNING Operation cancelled (keyboard interrupt)')
        self.args = [0]
        sys.excepthook = excepthook


class PlantExceptionError(Exception):
    def __init__(self, message):
        self.args = [message]
        sys.excepthook = excepthook


class PlantExceptionExit(Exception):
    def __init__(self, message):
        sys.exit(0)


class PlantReturn(Exception):
    def __init__(self, message):

        self.args = [message]
        sys.excepthook = excepthook


def excepthook(type, value, traceback):
    pass
