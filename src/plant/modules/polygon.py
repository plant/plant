import sys
import numpy


class Polygon:

    def __init__(self, *args):

        nbArgs = len(args)
        if nbArgs == 1:
            poly = args[0]
            self.path = poly
            self.x, self.y = poly2vect(poly)
        elif nbArgs == 2:
            polX = args[0]
            polY = args[1]
            self.x, self.y = polX, polY
            self.path = vect2poly(polX, polY)
        else:
            sys.exit("To instantiate the polygon, please give 1 argument as a list of (x, y) pairs OR 2 arguments as a list of X coordinates and a list of Y coordinates.")
        self.nbVertices = len(self.x)
        self.boundary = min(self.x), min(self.y), max(self.x), max(self.y)

    def isRectangle(self):

        if self.nbVertices != 4:
            return False
        Xmin, Ymin, Xmax, Ymax = self.boundary
        p = self.path
        for x in [Xmin, Xmax]:
            for y in [Ymin, Ymax]:
                if (x, y) not in p:
                    return False
        return True

    def getBoundary(self):

        Xmin, Ymin, Xmax, Ymax = self.boundary
        return Xmin, Ymin, (Xmax - Xmin + 1), (Ymax - Ymin + 1)

    def pnpoly(self, x, y):

        polX, polY = self.x, self.y
        npol = self.nbVertices
        inside = False
        i = 0
        j = npol - 1
        while i < npol:
            if (((polY[i] == y) and (polX[i] == x)) or
                    ((polY[j] == y) and (polX[j] == x))):
                return True
            if ((((polY[i] <= y) and (y < polY[j])) or
                 ((polY[j] <= y) and (y < polY[i]))) and
                    (x < (polX[j] - polX[i]) * (y - polY[i]) / (polY[j] - polY[i]) + polX[i])):
                inside = not inside
            j = i
            i = i + 1
        return inside

    def pnboundary(self):

        offsetX, offsetY, width, height = self.getBoundary()
        insides = []
        for j in range(height):
            for i in range(width):
                insides.append(self.pnpoly(offsetX + i, offsetY + j))
        return insides

    def inside(self):

        return self.pnboundary()

    def outside(self):

        offsetX, offsetY, width, height = self.getBoundary()
        outsides = []
        for j in range(int(height)):
            for i in range(int(width)):
                outsides.append(not self.pnpoly(offsetX + i, offsetY + j))
        return outsides

    def extractData(self, raster, dt, nCol):

        window = []
        dt = numpy.dtype(dt)
        mult = dt.itemsize
        offsetCol, offsetRow, width, height = self.getBoundary()
        f = open(raster)
        for h in range(0, int(height)):
            f.seek((nCol * (offsetRow + h) + offsetCol) * mult)
            w = numpy.fromfile(f, dtype=dt, count=int(width))
            window += w.tolist()
        f.close()
        window = numpy.ma.masked_invalid(window)
        if not self.isRectangle():
            outsides = self.outside()
            window = numpy.ma.masked_where(outsides, window)
        return window.compressed().tolist()

    def print2bin(self, filename, ncol, nrow):
        data = []
        for y in range(nrow):
            for x in range(ncol):
                if self.pnpoly(x, y):
                    data.append(numpy.uint8(255))
                else:
                    data.append(numpy.uint8(0))
        f = open(filename, 'wb')
        numpy.array(data).tofile(f)
        f.close()


def poly2vect(poly):

    vectX, vectY = [], []
    for i in range(len(poly)):
        x, y = poly[i]
        vectX.append(x)
        vectY.append(y)
    return vectX, vectY


def vect2poly(vectX, vectY):

    poly = []
    for i in range(len(vectX)):
        poly.append((vectX[i], vectY[i]))
    return poly
