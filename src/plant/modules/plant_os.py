#!/usr/bin/env python3
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Gustavo H. X. Shiroma, Marco Lavalle
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import os
import traceback
import math
from zipfile import ZipFile
import gzip
import tarfile
import glob as python_glob
from os import path, listdir, stat
import numpy as np
import plant


def get_error_message():
    error_message = traceback.format_exc()
    error_message = error_message.splitlines()[-1]
    error_message_splitted = error_message.split(':')

    if len(error_message_splitted) >= 2:
        error_message = (':'.join(error_message_splitted[1:])).lstrip()

    return error_message


def remove(filename):
    f_expanded = os.path.expanduser(filename)
    file_list = glob(f_expanded)
    for current_file in file_list:
        os.remove(current_file)


def dirname(filename):

    if path.isdir(filename):
        return filename
    directory = path.dirname(filename)
    if not directory:
        directory = '.'
    return directory


def get_subdirectories(input_dir):

    return [path.join(input_dir, name) for name in listdir(input_dir)
            if path.isdir(path.join(input_dir, name))]


def get_basename(filename_orig):
    filename = get_filename(filename_orig)
    return path.basename(filename)


def get_filename(filename_orig):

    if filename_orig is None:
        return

    f_expanded = os.path.expanduser(filename_orig)
    file_list = glob(f_expanded)

    if len(file_list) != 0:
        for current_file in file_list:
            try:
                if stat(current_file).st_size != 0:
                    return current_file

            except OSError:
                continue
    if plant.IMAGE_NAME_SEPARATOR in filename_orig:
        filename_splitted = filename_orig.split(
            plant.IMAGE_NAME_SEPARATOR)
        if len(filename_splitted) == 2:
            filename = filename_splitted[0]
        else:
            filename = filename_splitted[1]
    else:
        filename = filename_orig
    filename = os.path.expanduser(filename)
    file_list = glob(filename)
    if len(file_list) == 0:
        return
    for current_file in file_list:
        try:
            if stat(current_file).st_size != 0:
                return current_file
        except OSError:
            continue
    return


def glob(pattern):

    filelist_case_insensitive = python_glob.glob(pattern)
    filelist_case_sensitive_prefix = python_glob.glob(pattern + '*')
    output_list = [f for f in filelist_case_sensitive_prefix
                   if f in filelist_case_insensitive]

    return output_list


def isfile(filename_orig, flag_test_gdal=True):

    if (isinstance(filename_orig, plant.PlantImage) or
            isinstance(filename_orig, plant.PlantBand) or
            isinstance(filename_orig, np.ndarray)):
        return True
    if (isinstance(filename_orig, str) and
            filename_orig.upper().startswith('MEM:')):
        parameters_dict = {}
        plant.test_other_drivers(filename_orig,
                                 parameters_dict=parameters_dict)
        filename = parameters_dict['filename']
        for key, value in plant.plant_config.variables.items():
            if key == filename:

                return True

        return False
    filename = get_filename(filename_orig)

    if filename is not None:

        return True
    if flag_test_gdal:

        return plant.test_gdal_open(filename_orig)

    return False


def extract_to(compressed_file, dest, prefix='', root='', suffix='',
               verbose=True, extracted_file_list=[],
               flag_read_extracted_list=True,
               flag_save_extracted_list=False):

    kwargs = locals()

    if _check_extracted_list(**kwargs):
        return True
    if verbose:
        print('extracting: ' + compressed_file + ' to ' + dest)
    if not isfile(compressed_file):
        return False

    if compressed_file.endswith('.zip'):
        compress_function = ZipFile
    elif (compressed_file.endswith('.tar') or
          compressed_file.endswith('.tar.gz')):

        compress_function = tarfile.open
    else:
        if not isfile(compressed_file):
            return False
        input_file = gzip.open(compressed_file, 'rb')
        if path.isdir(dest):
            dest_file = path.join(dest, path.basename(compressed_file))
            dest_file = dest_file.replace('.gz', '')
        else:
            dest_file = dest
        output_file = open(dest_file, 'wb')
        output_file.write(input_file.read())
        input_file.close()
        output_file.close()
        return isfile(dest_file)
    try:
        with compress_function(compressed_file) as tar:
            if (type(prefix) is str and type(root) is str and
                    type(suffix) is str):
                if compressed_file.endswith('.zip'):
                    def selector(m): return (m.startswith(prefix)
                                             and root in m
                                             and m.endswith(suffix))
                else:
                    def selector(m): return (m.name.startswith(prefix)
                                             and root in m.name
                                             and m.name.endswith(suffix))
            if compressed_file.endswith('.zip'):
                members = [m for m in tar.namelist() if selector(m)]
                for i, m in enumerate(members):
                    extracted_file_list.append(str(m))
                    members[i] = path.basename(m)
            else:
                members = [m for m in tar.getmembers() if
                           selector(m)]
                for m in members:
                    extracted_file_list.append(m.name)
                    m.name = path.basename(m.name)

            tar.extractall(path=dest, members=members)
    except BaseException:
        error_message = get_error_message()

        if verbose:
            print('WARNING extracting files from %s. %s'
                  % (compressed_file, error_message))
        return False
    kwargs.pop('extracted_file_list')

    return verify_extraction(**kwargs)


def _check_extracted_list(compressed_file, dest, prefix='', root='', suffix='',
                          verbose=True, extracted_file_list=[],
                          flag_read_extracted_list=True,
                          flag_save_extracted_list=False):
    extracted_list_file = compressed_file + '.extracted'
    if not flag_read_extracted_list or not path.isfile(extracted_list_file):
        return
    with open(extracted_list_file, 'r') as f:
        lines = f.readlines()
        for i, current_line in enumerate(lines):
            splitted_line = current_line.split(':')
            if len(splitted_line) != 5:
                continue
            if (dest != splitted_line[0].strip() or
                prefix != splitted_line[1].strip() or
                root != splitted_line[2].strip() or
                    suffix != splitted_line[3].strip()):
                continue
            file_list = splitted_line[4].split(',')
            flag_all_exist = all([plant.isfile(path.join(dest, f.strip()))
                                  for f in file_list])
            if not flag_all_exist:
                extracted_file_list.extend(
                    [f for f in file_list if plant.isfile(f.strip())])
                continue
            return True


def verify_extraction(compressed_file, dest, prefix='', root='', suffix='',
                      verbose=True, extracted_file_list=[],
                      flag_read_extracted_list=True,
                      flag_save_extracted_list=False):

    kwargs = locals()

    if _check_extracted_list(**kwargs):
        return True

    if not isfile(compressed_file):
        return False

    if compressed_file.endswith('.zip'):
        compress_function = ZipFile
    elif (compressed_file.endswith('.tar') or
          compressed_file.endswith('.tar.gz')):

        compress_function = tarfile.open

    flag_all_extracted = False
    try:
        with compress_function(compressed_file) as tar:
            flag_all_extracted = True
            if (type(prefix) is str and type(root) is str and
                    type(suffix) is str):
                if compressed_file.endswith('.zip'):
                    def selector(m): return (m.startswith(prefix)
                                             and root in m
                                             and m.endswith(suffix))
                else:
                    def selector(m): return (m.name.startswith(prefix)
                                             and root in m.name
                                             and m.name.endswith(suffix))
            try:
                if compressed_file.endswith('.zip'):
                    members = [m for m in tar.namelist() if selector(m)]
                    for m in members:
                        m = path.basename(m)
                else:
                    members = [m for m in tar.getmembers() if
                               selector(m)]
                    for m in members:
                        m.name = path.basename(m.name)
                for m in members:
                    if compressed_file.endswith('.zip'):
                        m_name = m
                    else:
                        m_name = m.name
                    extracted_file_list.append(path.join(dest, m_name))
                    if not isfile(path.join(dest, m_name)):
                        if verbose:
                            print('WARNING extracted file not found: ' +
                                  path.join(dest, m_name))
                        flag_all_extracted = False
                        return False
            except BaseException:
                error_message = get_error_message()
                flag_all_extracted = False
                if verbose:
                    print('WARNING extraction verification for %s '
                          'could not be completed. %s'
                          % (compressed_file, error_message))
                return False
    except BaseException:
        if verbose:
            print('WARNING error opening file %s' % compressed_file)
        return False
    if flag_all_extracted and flag_save_extracted_list:
        extracted_list_file = compressed_file + '.extracted'
        extracted_file_list_str = ', '.join(
            [path.basename(f) for f in extracted_file_list])
        with open(extracted_list_file, 'a+') as f:
            f.write(f'{dest}:{prefix}:{root}:{suffix}:'
                    f'{extracted_file_list_str}\n')

        print('%s [OK]' % compressed_file)
    return flag_all_extracted


def overwrite_file_check(filename, force=None, element_str='file'):

    if (not isfile(filename)) or plant.plant_config.flag_all or force:
        return True
    if plant.plant_config.flag_never:
        return False
    while 1:
        res = plant.get_keys(f'The {element_str} {filename} already exists.'
                             ' Would you like to overwrite'
                             ' it? ([y]es/[n]o)/[A]ll/[N]one ')
        if res == 'n':
            return False
        elif res == 'N':
            plant.plant_config.flag_never = True
            return False
        elif res == 'y':
            return True
        elif res == 'A':
            plant.plant_config.flag_all = True
            return True


def overwrite_files_check(filelist,
                          force=None):

    any_existence = any([f != '' for f in glob(filelist)])
    if (not any_existence) or plant.plant_config.flag_all or force:
        return True
    if plant.plant_config.flag_never:
        return False
    while 1:
        res = plant.get_keys('Files ' + ', '.join(filelist) +
                             ' already exist. Would you like to overwrite'
                             ' them? ([y]es/[n]o)/[A]ll/[N]one ')
        if res.startswith('n'):
            return False
        elif res.startswith('N'):
            plant.plant_config.flag_never = True
            return
        elif res.startswith('y'):
            return True
        elif res.startswith('A'):
            plant.plant_config.flag_all = True
            return True


def get_common_directory(input_filelist):

    if not input_filelist:
        return

    current_directory = input_filelist[0]
    if not path.isdir(current_directory):
        current_directory = path.dirname(current_directory)

    while (not all(current_directory in d for d in input_filelist) and
           current_directory != '.' and current_directory != ''):
        current_directory = path.dirname(current_directory)
    if not current_directory:
        return '.'
    return current_directory


def get_file_list(input_filelist):

    if input_filelist is None:
        return
    if isinstance(input_filelist, str):
        if path.isdir(input_filelist):
            file_list = glob(path.join(input_filelist, '*'))
            if not file_list:
                return
            file_list = [f for f in file_list if isfile(f)]
            return file_list
        return [input_filelist]
    if len(input_filelist) == 1 and path.isdir(input_filelist[0]):
        file_list = glob(path.join(input_filelist[0], '*'))
        if not file_list:
            return
        else:
            return file_list
    file_list = [f for f in input_filelist if isfile(f)]
    return file_list


def search_cov_pol(input_filelist,
                   verbose=True):
    str_lex = ['hh', 'hv', 'vh', 'vv']
    ret_dict = {}
    identified_inputs = []
    for i in range(len(str_lex)):
        for j in range(len(str_lex)):
            str_search = str_lex[i] + str_lex[j]
            file_list = [f for f in input_filelist
                         if str_search in f.lower()]

            if len(file_list) > 1:
                file_list = plant.get_images_from_list(file_list)
            if len(file_list) != 0:
                ret_dict[str_search + '_file'] = file_list[0]
                identified_inputs.append(file_list[0])
                if verbose:
                    print(str_search + ' file: ',
                          file_list[0])
    ret_dict['identified_inputs'] = identified_inputs

    return ret_dict


def sort_pol(input_list, **kwargs):
    if (input_list is None or
            (isinstance(input_list, list) and
             len(input_list) <= 1)):
        return input_list
    ret = search_pol_list(input_list, **kwargs)
    if ret is None:
        return sorted(input_list)
    ret = ret['identified_inputs']
    return ret


def search_pol_list(input_list, **kwargs):
    kwargs['file_check'] = False
    ret = search_pol(input_list, **kwargs)
    return ret


def search_pol(input_filelist,
               verbose=True,
               mode=None,
               sym=False,
               vh_to_hv=True,
               exact_str_vv_search=True,
               file_check=True):

    kwargs = locals()
    if not file_check:
        filelist = list(input_filelist)
    else:
        filelist = get_file_list(input_filelist)
        if filelist is None:
            return
    if len(filelist) == 1:
        image_obj = plant.read_image(filelist[0],

                                     verbose=False,
                                     only_header=True)
        if image_obj is None:
            return
        name_to_file_hash = {}
        if image_obj.nbands == 3 or image_obj.nbands == 4:
            filelist_temp = []
            _ = kwargs.pop('input_filelist', None)
            for b in range(image_obj.nbands):
                band_name = image_obj.get_band(band=b).name
                if not band_name:
                    continue
                filelist_temp.append(band_name)
                name_to_file_hash[band_name] = filelist[0] + f':{b}'
            ret_dict = search_pol(filelist_temp, **kwargs)
            if ret_dict is not None:
                if 'hh_file' in ret_dict.keys() and ret_dict['hh_file']:
                    ret_dict['hh_file'] = name_to_file_hash[
                        ret_dict['hh_file']]
                if 'hv_file' in ret_dict.keys() and ret_dict['hv_file']:
                    ret_dict['hv_file'] = name_to_file_hash[
                        ret_dict['hv_file']]
                if 'vh_file' in ret_dict.keys() and ret_dict['vh_file']:
                    ret_dict['vh_file'] = name_to_file_hash[
                        ret_dict['vh_file']]
                if 'vv_file' in ret_dict.keys() and ret_dict['vv_file']:
                    ret_dict['vv_file'] = name_to_file_hash[
                        ret_dict['vv_file']]
                if 'identified_inputs' in ret_dict:
                    new_identified_inputs = []
                    for current_input in ret_dict['identified_inputs']:
                        new_identified_inputs.append(name_to_file_hash[
                            current_input])
                    ret_dict['identified_input'] = new_identified_inputs
                return ret_dict

    str_hh = None
    if mode is not None:
        for key, value in plant.RADAR_DECOMP_DICT:
            if key == mode.upper():
                str_hh, str_vv, str_hv, str_vh = value
                break
    if str_hh is None:
        str_hh, str_vv, str_hv, str_vh = plant.RADAR_DECOMP_DICT[0][1]

    hh_file = search_single_pol(filelist, pol=str_hh, file_check=file_check)

    if not hh_file and mode is None:

        attempts_vect = [key for key, value in plant.RADAR_DECOMP_DICT]
        max_results = 0
        for current_attempt in attempts_vect:
            ret = search_pol(filelist, mode=current_attempt,
                             verbose=False,
                             sym=sym, vh_to_hv=vh_to_hv,
                             file_check=file_check)
            if (ret is not None and
                    len(ret['identified_inputs']) > max_results):
                best_ret = ret
                max_results = len(ret['identified_inputs'])
        if max_results > 0:
            return best_ret
        return

    if verbose:
        if hh_file:
            print(str_hh + ' file: ' + hh_file)
        else:
            if str_hh == str_hh.lower():
                print('WARNING file not found (*' + str_hh + '*)')
            else:
                print('WARNING file not found (*' + str_hh + '*/*' +
                      str_hh.lower() + '*)')

    if hh_file:
        filelist.remove(hh_file)
    hv_file = search_single_pol(filelist, pol=str_hv, file_check=file_check)
    if hv_file:
        filelist.remove(hv_file)
    vh_file = search_single_pol(filelist, pol=str_vh, file_check=file_check)
    if vh_file:
        filelist.remove(vh_file)

    if verbose:
        if hv_file:
            print(str_hv + ' file: ' + hv_file)
        if (vh_file and str_hv != str_vh):
            print(str_vh + ' file: ' + vh_file)

    if not hv_file:
        if vh_file and sym:
            hv_file = vh_file
        elif vh_file and vh_to_hv:
            hv_file = vh_file
            vh_file = None
    elif not vh_file and sym:
        vh_file = hv_file
    vv_file = search_single_pol(filelist, pol=str_vv, file_check=file_check)
    if vv_file:
        vv_str = str_vv
    elif not exact_str_vv_search:
        if str_hh in hh_file:
            str_split = hh_file.split(str_hh)
        else:
            str_split = hh_file.split(str_hh.lower())
        file_list = [f for f in filelist
                     if (f.startswith(str_split[0]) and
                         f.endswith(str_split[-1]))]
        file_list.sort(key=len)
        for current_file in file_list:
            if (current_file != hh_file and current_file != hv_file):
                vv_file = current_file
                vv_str = vv_file.replace(str_split[0], '')
                vv_str = vv_str.replace(str_split[1], '')
                str_vv = vv_str
                break
    if verbose and vv_file:
        print(vv_str + ' file: ' + vv_file)

    identified_inputs = [hh_file, hv_file, vh_file, vv_file]
    identified_inputs = [f for f in identified_inputs if f]
    identified_inputs = list(set(identified_inputs))
    if mode == 'C4' or mode == 'T4' and len(identified_inputs) < 4:

        return

    if len(identified_inputs) == 0:
        return

    ret_dict = {'hh_file': hh_file,
                'hv_file': hv_file,
                'vh_file': vh_file,
                'vv_file': vv_file,
                'str_hh_file': str_hh,
                'str_hv_file': str_hv,
                'str_vh_file': str_vh,
                'str_vv_file': str_vv,
                'identified_inputs': identified_inputs}
    return ret_dict


def search_single_pol(input_filelist, pol='', file_check=True):

    if not file_check:
        filelist = input_filelist
    else:
        filelist = get_file_list(input_filelist)
    if any(pol in path.basename(s) for s in filelist):
        pol_str = pol
    elif any(pol.lower() in path.basename(s) for s in filelist):
        pol_str = pol.lower()
    else:
        return
    filelist.sort(key=len)
    filename = None
    for current_file in filelist:
        current_basename = path.basename(current_file)
        if (pol_str in current_basename
                and pol_str + 'p' not in current_basename
                and pol_str + 'm' not in current_basename
                and 'p' + pol_str not in current_basename
                and 'p' + pol_str not in current_basename
                and not current_basename.endswith('.xml')
                and not current_basename.endswith('.rdr')
                and not current_basename.endswith('.txt')):
            filename = current_file
            break
    if filename:
        return filename
    for current_file in filelist:
        current_basename = path.basename(current_file)
        if (pol_str in current_basename
                and pol_str + 'p' not in current_basename
                and pol_str + 'm' not in current_basename
                and 'p' + pol_str not in current_basename
                and 'p' + pol_str not in current_basename):
            filename = current_file
            break
    if not filename:
        return

    return filename


def replace_extension(output_file,
                      extension=None):
    if not extension:
        return output_file
    if (not extension.startswith('.') and
            not extension.startswith('_')):
        extension = '.' + extension
    output_file_splitted = output_file.split('.')
    if (len(output_file_splitted) == 1 or
            len(output_file_splitted[-1]) > 5):
        return output_file + extension
    if not extension.startswith('_'):
        return ('.'.join(output_file_splitted[:-1]) +
                extension)


def get_file_size_string(size):

    size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
    if size == 0:
        return '0B'
    i = int(math.floor(math.log(size, 1024)))
    p = math.pow(1024, i)
    s = round(size / p, 2)
    return '%s%s' % (s, size_name[i])
