#!/usr/bin/env python3
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Gustavo H. X. Shiroma, Marco Lavalle
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import numpy as np
import time
from os import path, makedirs
import sys
import importlib
import argparse as python_argparse
import plant


class PlantScript(object):

    def __init__(self,
                 parser=None,
                 argv=None,
                 parsed_args=None,
                 geo_default_step=None):

        self.flag_active = True

        if argv is None:
            argv = sys.argv[1:]

        self.start_time = time.time()
        self.verbose = True
        self.force = False

        attr = self.__dict__.keys()
        time_string = time.strftime("%Y_%m_%d_%H_%M_%S",
                                    time.localtime(time.time()))

        raised_exception = None
        flag_print_help = False
        self.parser = parser

        if parsed_args is None and parser is not None:
            argv = pre_parser(argv, parser=parser)

            if argv is not None:
                flag_print_help = parse_arg('help', parser, argv)

            if not flag_print_help:
                try:
                    parsed_args = parser.parse_args(argv)

                except BaseException:
                    raised_exception = sys.exc_info()[0]

        if parsed_args is not None:
            for i, current_arg in enumerate(parsed_args.__dict__):
                value = parsed_args.__dict__[current_arg]
                if isinstance(value, str):
                    if value.startswith(' '):
                        parsed_args.__dict__[current_arg] = value[1:]
                elif isinstance(value, list):
                    for j, list_value in enumerate(value):
                        if isinstance(list_value, str):
                            if list_value.startswith(' '):
                                parsed_args.__dict__[
                                    current_arg][j] = list_value[1:]

            if ('help' not in parsed_args.__dict__.keys() or
                    not parsed_args.help):
                for key, value in parsed_args.__dict__.items():

                    if key.endswith('_required') and not value:
                        key = key.replace('_required', '')
                        parser.print_usage()
                        print(f'ERROR the following arguments'
                              f' are required: {key}')
                        return
                    key = key.replace('_required', '')
                    self.__dict__[key] = value

        if 'debug_level' not in attr or self.debug_level is None:
            self.debug_level = plant.plant_config.debug_level

        if 'flag_debug' not in attr or self.flag_debug is None:
            self.flag_debug = plant.plant_config.flag_debug

        if 'flag_mute' in attr:
            if (plant.plant_config.logger_obj.flag_mute is None or
                    self.flag_mute is not None):
                plant.plant_config.logger_obj.flag_mute = self.flag_mute
        if 'flag_color_text' in attr:
            if (plant.plant_config.logger_obj.flag_color_text is None or
                    self.flag_color_text is not None):
                plant.plant_config.logger_obj.flag_color_text = \
                    self.flag_color_text
            else:
                self.flag_color_text = \
                    plant.plant_config.logger_obj.flag_color_text

        self.flag_main_script = plant.plant_config.main_script is None
        called_script_name_py = path.basename(sys.argv[0])
        called_script_name = called_script_name_py.replace('.py', '')
        called_script_class_name = called_script_name.replace('_', '')
        this_class = str(self.__class__.__name__).lower()

        plant.plant_config.script_list.append(self)
        plant.plant_config.current_script = self

        command_line = (f'{called_script_name_py} '
                        f'{get_command_line_from_argv(sys.argv[1:])}')
        if self.flag_main_script:
            plant.plant_config.main_script = self

            plant.plant_config.flag_debug = self.flag_debug
            plant.plant_config.debug_level = self.debug_level
            plant.plant_config.command_line = command_line
            if 'flag_all' in attr:
                plant.plant_config.flag_all = self.flag_all
            if 'flag_never' in attr:
                plant.plant_config.flag_never = self.flag_never
            if 'flag_keep_temporary' in attr:
                plant.plant_config.flag_keep_temporary = \
                    self.flag_keep_temporary
            else:
                plant.plant_config.flag_keep_temporary = False

            if (sys.platform == 'win32' or sys.platform == 'cygwin'):
                from colorama import init
                init()
                log_extension = '.bat'
            else:
                log_extension = '.log'
            if (called_script_class_name in this_class):
                log_file_prefix = called_script_name
            else:
                log_file_prefix = this_class

            self.log_dir = plant.plant_config.log_dir

            flag_log_error = self.log_dir is None
            if not flag_log_error and not path.isdir(self.log_dir):
                try:
                    makedirs(self.log_dir)
                except BaseException:
                    flag_log_error = True
            if not flag_log_error:
                self.logger_filename = path.join(self.log_dir,
                                                 log_file_prefix +
                                                 '_' + time_string +
                                                 log_extension)
                plant.plant_config.logger_obj.init_file(self.logger_filename)
            else:
                self.logger_filename = None
        if self.flag_main_script:
            self.print(f'PLAnT {plant.VERSION} - {command_line}')

        if (raised_exception is not None or
                parsed_args is None or
                flag_print_help):
            if parser is None:

                return
            elif raised_exception is not None:
                raise raised_exception

            if ('cli_mode' not in self.__dict__.keys() and
                    (parsed_args is None or
                     'cli_mode' not in parsed_args.__dict__.keys())):
                self.cli_mode = parse_arg('cli_mode', parser, argv)

            elif (parsed_args is not None and
                  'cli_mode' in parsed_args.__dict__.keys()):
                self.cli_mode = parsed_args.__dict__['cli_mode']

            argparse_print_help(parser,
                                cli_mode=self.cli_mode)

            if raised_exception is not None:
                raise raised_exception
            else:
                sys.exit(0)

        if 'help' in attr and self.help:

            parser.print_usage()
            sys.exit(0)

        if 'cmap_min' in attr and self.cmap_min is not None:

            cmap_min_obj = plant.read_image(self.cmap_min)
            if (cmap_min_obj.n_elements() == 1):
                self.cmap_min = float(cmap_min_obj.image[0, 0])
            else:
                self.cmap_min = cmap_min_obj.image[0, :].tolist()

        if 'cmap_max' in attr and self.cmap_max is not None:

            cmap_max_obj = plant.read_image(self.cmap_max)
            if (cmap_max_obj.n_elements() == 1):
                self.cmap_max = float(cmap_max_obj.image[0, 0])
            else:
                self.cmap_max = cmap_max_obj.image[0, :].tolist()

        if 'cmap_crop_min' in attr and self.cmap_crop_min is not None:

            cmap_crop_min_obj = plant.read_image(self.cmap_crop_min)
            if (cmap_crop_min_obj.n_elements() == 1):
                self.cmap_crop_min = float(cmap_crop_min_obj.image[0, 0])
            else:
                self.cmap_crop_min = cmap_crop_min_obj.image[0, :].tolist()

        if 'cmap_crop_max' in attr and self.cmap_crop_max is not None:

            cmap_crop_max_obj = plant.read_image(self.cmap_crop_max)
            if (cmap_crop_max_obj.n_elements() == 1):
                self.cmap_crop_max = float(cmap_crop_max_obj.image[0, 0])
            else:
                self.cmap_crop_max = cmap_crop_max_obj.image[0, :].tolist()

        self.plant_transform_obj = self.get_plant_transform_obj()

        if 'input_file' in attr:

            if (plant.is_sequence(self.input_file) and
                    len(self.input_file) > 1):
                self.print('ERROR the argument input file accepts at'
                           ' most one value')
                return
            if plant.is_sequence(self.input_file):
                self.input_file = self.input_file[0]
            if not isinstance(self.input_file, plant.PlantImage):
                self.input_file = path.expanduser(self.input_file)
            flag_input = not (self.input_file is None or
                              self.input_file == '')
            if (isinstance(self.input_file, str) and
                ('*' in self.input_file or
                 '?' in self.input_file)):
                input_file_temp = plant.glob(self.input_file)
                if len(input_file_temp) == 1:
                    self.input_file = input_file_temp[0]
            if not self.input_file and flag_input:
                self.print('ERROR input file not found')
                return
            print('input file:', self.input_file)

        if 'input_files' in attr:
            if (len(self.input_files) == 1 and
                    path.isdir(self.input_files[0])):
                self.input_files = [path.join(self.input_files[0], '*')]
            new_input_files = []
            for filename in self.input_files:
                f_expanded_list = plant.glob(path.expanduser(filename))
                if len(f_expanded_list) == 0 and plant.isfile(filename):
                    new_input_files.append(filename)
                    continue
                if len(f_expanded_list) == 0:
                    self.print(f'ERROR file not found: {filename}')
                    return
                for f_expanded in f_expanded_list:
                    new_input_files.append(f_expanded)
            self.input_files = new_input_files

            flag_input = len(self.input_files) != 0

            if len(self.input_files) == 0 and flag_input:
                self.print('ERROR input file(s) not found')
                return
            if self.input_sort:
                self.input_files = sorted(self.input_files)

        if 'band' in attr and self.band is not None:
            if isinstance(self.band, str):
                self.band = plant.read_matrix(self.band,
                                              verbose=False)
                self.band = self.band.ravel()
                self.band = self.band.astype(np.uint16)

        if 'output_dtype' in attr and self.output_dtype is not None:
            self.output_dtype = self.output_dtype.lower()

        if 'input_image' in attr:
            self.get_input_image()

        if 'input_images' in attr:
            self.get_input_images()
            if self.input_sort:
                self.input_images = sorted(self.input_images)

        if 'secondary_input_image' in attr:
            self.get_input_image(flag_secondary=True)

        if 'secondary_input_images' in attr:
            self.get_input_images(flag_secondary=True)

        if 'separator' in attr:
            if self.separator is not None:
                self.print('separator: ' + self.separator)
                if (self.separator == '\\t' or
                        self.separator.lower() == 'tab'):
                    self.separator = '\t'

        if ('topo_dir' in attr and

                self.topo_dir):
            ret_dict = plant.get_topo_files(self.topo_dir)
            if ret_dict is None:
                print('ERROR georeference not found in: %s'
                      % (self.topo_dir))
                return
            for key, value in ret_dict.items():
                if key not in attr or not self.__dict__[key]:

                    self.__dict__[key] = value

        elif ('topo_dir' in attr and
              not self.topo_dir and
              'lon_file' in attr and
              'lat_file' in attr):
            self.topo_dir = None
            number_of_geolocation_files = 0
            if ('lon_file' in attr and self.lon_file):
                lon_file_obj = self.read_image(self.lon_file)
                lon_file_obj.band.name = plant.X_BAND_NAME
                number_of_geolocation_files += 1
                self.topo_dir = lon_file_obj
            if ('lat_file' in attr and self.lat_file):
                lat_file_obj = self.read_image(self.lat_file)
                lat_file_obj.band.name = plant.Y_BAND_NAME
                number_of_geolocation_files += 1
                if self.topo_dir is not None:
                    band_obj = lat_file_obj.get_band()
                    self.topo_dir.set_band(band_obj, band=1)
                    self.topo_dir.name = 'Geolocation array'
            if number_of_geolocation_files == 1:
                self.print('ERROR only one geolocation array'
                           ' was provided (lat/northing or'
                           ' lon/easting.)')

        if ('topo_dir_ml' in attr and

                self.topo_dir_ml):
            ret_dict = plant.get_topo_files(self.topo_dir_ml)
            if ret_dict is None:
                print('ERROR georeference not found in: %s'
                      % (self.topo_dir_ml))
                return
            for key, value in ret_dict.items():
                ml_key = key + '_ml'
                if ml_key not in attr or not self.__dict__[ml_key]:
                    self.__dict__[ml_key] = value

        if ('topo_dir_ml' in attr and
                not self.topo_dir_ml and
                'lon_file_ml' in attr and
                'lat_file_ml' in attr):
            self.topo_dir_ml = None
            number_of_geolocation_files = 0
            if ('lon_file_ml' in attr and self.lon_file_ml):
                lon_file_ml_obj = self.read_image(self.lon_file_ml)
                lon_file_ml_obj.band.name = plant.X_BAND_NAME
                number_of_geolocation_files += 1
                self.topo_dir_ml = lon_file_ml_obj
            if ('lat_file_ml' in attr and self.lat_file_ml):
                lat_file_ml_obj = self.read_image(self.lat_file_ml)
                lat_file_ml_obj.band.name = plant.Y_BAND_NAME
                number_of_geolocation_files += 1
                if self.topo_dir_ml is not None:
                    band_obj = lat_file_ml_obj.get_band()
                    self.topo_dir_ml.set_band(band_obj, band=1)
            if number_of_geolocation_files == 1:
                self.print('ERROR only one geolocation array'
                           ' was provided (northing/lat or'
                           ' easting/lon.)')

        if 'ground' in attr and 'canopy' in attr:
            self.forest_height = (self.forest_height or
                                  not (self.ground or self.canopy))
            if self.ground:
                self.str_height = 'zg'
            elif self.canopy:
                self.str_height = 'zt'
            elif self.forest_height:
                self.str_height = 'hv'

        if ('save_as_raster' in attr and
                'save_as_vector' in attr and
                'save_as_text' in attr and
                'save_as_raster_gdal' in attr):
            if (not self.save_as_raster and
                    not self.save_as_vector and
                    not self.save_as_text and
                    not self.save_as_raster_gdal):
                self.save_as_raster = True

        if 'null' in attr and 'in_null' not in attr:
            self.in_null = self.null
        if 'null' in attr and 'out_null' not in attr:
            self.out_null = self.null
        attr = self.__dict__.keys()
        if ('null' in attr and

                self.null is not None and
                isinstance(self.null, str) and
                self.null.lower() == 'nan'):
            self.null = np.nan
        if 'in_null' in attr:
            if self.in_null is None and 'null' in attr:
                self.in_null = self.null
            elif (self.in_null is None and

                  isinstance(self.in_null, str) and
                  self.in_null.lower() == 'nan'):
                self.in_null = np.nan
        if 'out_null' in attr:
            if self.out_null is None and 'null' in attr:
                self.out_null = self.null
            elif (self.out_null is None and

                  isinstance(self.out_null, str) and
                  self.out_null.lower() == 'nan'):
                self.out_null = np.nan

        if ('output_files' not in attr and
                (('output_file' in attr and
                  self.output_file) or
                 ('output_dir' in attr and
                  self.output_dir) or
                 ('output_ext' in attr and
                  self.output_ext)) and
                (('input_file' in attr and
                  self.input_file) or
                 ('input_files' in attr and
                  self.input_files) or
                 ('input_image' in attr and
                  self.input_image) or
                 ('input_images' in attr and
                  self.input_images))):
            if 'input_file' in attr:
                input_files = [self.input_file]
            elif 'input_files' in attr:
                input_files = self.input_files
            elif 'input_image' in attr:
                input_files = [self.input_image]
            else:
                input_files = self.input_images
            self.output_files = []
            output_file = self.output_file if 'output_file' in attr else ''
            output_dir = self.output_dir if 'output_dir' in attr else ''
            output_ext = self.output_ext if 'output_ext' in attr else ''
            if 'output_keep_input_tree' in attr:
                output_keep_input_tree = self.output_keep_input_tree
            else:
                output_keep_input_tree = False
            if 'output_keep_input_flat_tree' in attr:
                output_keep_input_flat_tree = self.output_keep_input_flat_tree
            else:
                output_keep_input_flat_tree = False

            flag_separate = 'separate' in attr and self.separate
            for current_file in input_files:
                self.output_files.append(
                    get_output_file_name(
                        current_file,
                        input_files,
                        output_file,
                        output_dir,
                        output_keep_input_tree=output_keep_input_tree,
                        output_keep_input_flat_tree=output_keep_input_flat_tree,
                        extension=output_ext,
                        flag_separate=flag_separate))
            if ('output_file' in attr and
                    not self.output_file and
                    len(self.output_files) == 1):
                self.output_file = self.output_files[0]
        if 'output_dir' in attr:
            self.output_dir_orig = self.output_dir
        if ('output_file' in attr and
                'output_dir' in attr and
                not self.output_dir and self.output_file):
            self.output_dir = plant.dirname(self.output_file)

        if 'output_skip_if_existent' in attr and 'output_files' in attr:
            ret_list = []
            if self.output_skip_if_existent and self.output_files:
                flag_all_exist = True
                for current_file in self.output_files:
                    if not plant.isfile(current_file):
                        flag_all_exist = False
                        break
                    if current_file.endswith('kmz'):
                        ret_list.append(current_file)
                        continue
                    image_obj = plant.read_image(
                        current_file,
                        flag_exit_if_error=False)
                    if image_obj is None:
                        flag_all_exist = False
                        break
                    ret_list.append(image_obj)
                if flag_all_exist:
                    print('INFO output files already exist, '
                          'skipping execution..')
                    if len(ret_list) == 1:
                        plant.ret(ret_list[0])
                    plant.ret(ret_list)

        elif ('output_skip_if_existent' in attr and 'output_file' in attr and
              self.output_skip_if_existent and self.output_file and
              plant.isfile(self.output_file)):
            skip_message = ('INFO output files already exist, '
                            'skipping execution..')
            if self.output_file.endswith('kmz'):
                print(skip_message)
                plant.ret(self.output_file)
            image_obj = plant.read_image(self.output_file,
                                         flag_exit_if_error=False)
            if image_obj is not None:
                print(skip_message)
                plant.ret(image_obj)

        if 'projection' in attr:

            self.projection = plant.get_projection_wkt(self.projection)

        get_coordinates_kwargs = {}
        if 'output_projection' in attr:

            self.output_projection = plant.get_projection_wkt(
                self.output_projection)
            get_coordinates_kwargs['projection'] = self.output_projection

        if ('geo_center' in attr or
                'geo_search_str' in attr or
                'bbox' in attr or
                'bbox_topo' in attr or
                'bbox_file' in attr):
            if 'geo_center' not in attr:
                self.geo_center = None
            if 'geo_search_str' not in attr:
                self.geo_search_str = None
            if 'bbox' not in attr:
                self.bbox = None
            if 'step' not in attr:
                self.step = None
            if 'step_x' not in attr:
                self.step_x = None
            if 'step_y' not in attr:
                self.step_y = None
            if 'step_m' not in attr:
                self.step_m = None
            if 'step_m_x' not in attr:
                self.step_m_x = None
            if 'step_m_y' not in attr:
                self.step_m_y = None
            if 'bbox_topo' not in attr:
                self.bbox_topo = None
            if 'bbox_file' not in attr:
                self.bbox_file = None

            if this_class != 'plantmosaic':
                self.plant_geogrid_obj = plant.get_coordinates(
                    geo_center=self.geo_center,
                    geo_search_str=self.geo_search_str,
                    bbox=self.bbox,
                    step=self.step,
                    step_x=self.step_x,
                    step_y=self.step_y,
                    step_m=self.step_m,
                    step_m_x=self.step_m_x,
                    step_m_y=self.step_m_y,

                    bbox_topo=self.bbox_topo,
                    bbox_file=self.bbox_file,

                    verbose=self.verbose,
                    plant_transform_obj=self.plant_transform_obj,
                    default_step=geo_default_step,
                    **get_coordinates_kwargs)

        if 'pixel_size_x' in attr:
            self.pixel_size_rg = self.pixel_size_x
        if 'pixel_size_y' in attr:
            self.pixel_size_az = self.pixel_size_y

        if 'nlooks' in attr:
            nlooks_y, nlooks_x = plant.demux_input(self.nlooks, 2)
            if 'nlooks_x' not in attr or self.nlooks_x is None:
                self.nlooks_x = nlooks_x
            if 'nlooks_y' not in attr or self.nlooks_y is None:
                self.nlooks_y = nlooks_y
        if 'nlooks_x' in self.__dict__.keys():
            self.nlooks_rg = self.nlooks_x
        if 'nlooks_y' in self.__dict__.keys():
            self.nlooks_az = self.nlooks_y
        if ('nlooks_x' in self.__dict__.keys() and
                'nlooks_y' in self.__dict__.keys()):
            self.nlooks = [self.nlooks_y, self.nlooks_x]

        if ('height_diff' in attr and
                'height_max' in attr and
                'height_min' in attr and
                'height_step' in attr and
                'height_nbins' in attr):
            if (self.height_diff is None and
                    self.height_max is None):
                self.height_max = 51
            if (self.height_diff is None and
                    self.height_min is None):
                self.height_min = -50
            if (self.height_diff is None and
                    self.height_max is not None and
                    self.height_min is not None):
                self.height_diff = self.height_max - self.height_min
            if (self.height_step is None and
                    self.height_diff is not None and
                    self.height_nbins is not None):
                self.height_step = self.height_diff / self.height_nbins
            elif (self.height_nbins is None and
                    self.height_diff is not None and
                    self.height_step is not None):
                self.height_nbins = int(self.height_diff / self.height_step)
            else:
                self.height_step = 1.0
                self.height_nbins = int(self.height_diff / self.height_step)

    def get_input_image(self, flag_secondary=False):
        prefix = 'secondary_' if flag_secondary else ''
        input_image = self.__dict__[prefix + 'input_image']
        if input_image is None:
            self.__dict__[prefix + 'input_image'] = input_image
            return

        if (plant.is_sequence(input_image) and
            not (isinstance(input_image, str)) and
                len(input_image) > 1):
            print('ERROR only one input is allowed')
        elif (plant.is_sequence(input_image) and
                not (isinstance(input_image, str))):
            input_image = input_image[0]
        input_image = path.expanduser(input_image)

        flag_input = not (input_image is None or
                          input_image == '')
        not_found_list = []
        if isinstance(input_image, str):
            input_image = plant.search_image(
                input_image,
                invalid_list=not_found_list)
        ret = None
        valid_input_image = []

        for f in input_image:
            ret = self.read_image(
                f,
                only_header=True,

                verbose=False)
            if ret is not None:
                valid_input_image.append(f)
        flag_error = len(input_image) == 0 and flag_input
        message_type = 'ERROR'
        if len(not_found_list) > 0:
            self.print('%s input not found or invalid: %s'
                       % (message_type,
                          ', '.join(not_found_list)))
        if flag_error:
            return
        if len(valid_input_image) > 1:
            self.print('ERROR only one input image was expected')
            return
        if len(valid_input_image) == 0:
            self.print('ERROR invalid input: %s'
                       % (input_image))
            return
        input_image = valid_input_image[0]
        self.__dict__[prefix + 'input_image'] = input_image

    def get_input_images(self, flag_secondary=False):
        prefix = 'secondary_' if flag_secondary else ''
        input_images = self.__dict__[prefix + 'input_images']
        if input_images is None or len(input_images) == 0:
            return
        input_images = [f if isinstance(f, plant.PlantImage)
                        else path.expanduser(f)
                        for f in input_images]
        expanded_input_images = []
        not_found_list = []
        for current_image in input_images:

            expanded_input_images.extend(plant.search_image(
                current_image,
                invalid_list=not_found_list))

            if len(not_found_list) != 0:
                print('ERROR file not found or invalid:'
                      f' {current_image}')
                return

        input_images = expanded_input_images
        ret_dict = plant.search_cov_pol(input_images)
        self.identified_inputs = []
        self.__dict__.update(ret_dict)
        ret_dict = plant.search_pol(input_images,
                                    sym=False,
                                    vh_to_hv=False,
                                    verbose=False,
                                    mode='lex')
        if ret_dict is not None:
            keys = list(ret_dict.keys())
            if flag_secondary:
                new_dict = {}
                for key in keys:
                    new_dict[prefix + key] = ret_dict[key]
                ret_dict = new_dict
            self.__dict__.update(ret_dict)
            self.__dict__[prefix + 'reference_file'] = \
                self.__dict__[prefix + 'identified_inputs'][0]
        else:
            pol_list = ['hh_file', 'hv_file', 'vh_file', 'vv_file']
            for pol in pol_list:
                self.__dict__[prefix + pol] = ''
            reference_file = '' if len(input_images) == 0 else input_images[0]
            self.__dict__[prefix + 'reference_file'] = reference_file

        self.__dict__[prefix + 'input_images'] = input_images

    def print(self, message, level=0):

        attr = self.__dict__.keys()

        if message is None:
            message = 'None'
        elif not isinstance(message, str):
            message = str(message)
        if (('verbose' in attr and
                self.verbose) or
                level != 0 or
                message.startswith('ERROR') or
                message.startswith('WARNING') or
                message.startswith('EXIT')):
            print(message)

    def __getattr__(self, name):

        if name in self.__dir__():
            return
        wrapper_obj = plant.ModuleWrapper(name, ref=self)
        if not wrapper_obj._module_obj:
            raise AttributeError(
                f'{self.__class__.__name__}.{name} is invalid.')

        return wrapper_obj

    def execute(self, command, verbose=None, level=1):

        if verbose is None:
            verbose = self.verbose
        command_vector = plant.get_command_vector(command)

        module_name = command_vector[0]
        argv = command_vector[1:]

        module_name = module_name.replace('.py', '')

        flag_external_module = False
        try:
            module_obj = importlib.import_module('plant.app.' + module_name)
        except ModuleNotFoundError:
            flag_external_module = True
        if flag_external_module:
            return plant.execute(command, verbose=verbose, level=level)
        argparse_method = getattr(module_obj, 'get_parser')

        parser = argparse_method()
        remainder_dests = get_args_from_argparser(parser,
                                                  store_action=True,
                                                  store_true_action=False,
                                                  store_false_action=False,
                                                  get_dest=True)

        flag_geo = False

        for arg in argv:
            dest = get_args_from_argparser(parser,
                                           store_action=True,
                                           store_true_action=False,
                                           store_false_action=False,
                                           arg=arg,
                                           get_dest=True)
            if dest == []:
                continue
            flag_geo = (flag_geo or
                        dest[0] == 'geo_center' or
                        dest[0] == 'geo_search_str' or
                        dest[0] == 'bbox' or
                        dest[0] == 'bbox_topo' or
                        dest[0] == 'bbox_file')
            remainder_dests = set(set(remainder_dests) -
                                  set(dest))

        for dest in remainder_dests:
            if dest not in self.__dict__.keys():
                continue
            dest_arg = get_args_from_argparser(parser,
                                               dest=dest)

            dest_items = self.__dict__[dest]
            if (dest_items is None or
                (plant.is_sequence(dest_items) and
                    not (any(dest_items)))):
                continue
            if (flag_geo and (dest == 'geo_center' or
                              dest == 'geo_search_str' or
                              dest == 'bbox' or
                              dest == 'bbox_topo' or
                              dest == 'bbox_file')):
                continue
            elif (dest == 'geo_center' or
                  dest == 'geo_search_str' or
                  dest == 'bbox' or
                  dest == 'bbox_topo' or
                  dest == 'bbox_file'):
                flag_geo = True

            if len(dest_arg) > 0:
                argv += [dest_arg[0]]
            if isinstance(dest_items, list):
                for current_dest in dest_items:
                    argv += [str(current_dest)]
            elif isinstance(dest_items, np.ndarray):
                dest_items = dest_items.ravel()
                for current_dest_counter in range(dest_items.shape[0]):
                    argv += [str(dest_items[current_dest_counter])]
            else:
                argv += [str(dest_items)]

        remainder_dests = get_args_from_argparser(parser,
                                                  store_action=False,
                                                  store_true_action=True,
                                                  store_false_action=False,
                                                  get_dest=True)
        for arg in argv:
            if not arg.startswith('-'):
                continue
            current_dest = get_args_from_argparser(parser,
                                                   store_action=False,
                                                   store_true_action=True,
                                                   store_false_action=False,
                                                   arg=arg,
                                                   get_dest=True)
            remainder_dests = set(set(remainder_dests) -
                                  set(current_dest))

        for dest in remainder_dests:
            if dest in self.__dict__.keys():
                value = self.__dict__[dest]
                if isinstance(value, list):
                    value = value[0]

                if value:
                    argv += [get_args_from_argparser(parser,
                                                     store_action=False,
                                                     store_true_action=True,
                                                     store_false_action=False,
                                                     dest=dest)[0]]
        remainder_dests = get_args_from_argparser(parser,
                                                  store_action=False,
                                                  store_true_action=False,
                                                  store_false_action=True,
                                                  get_dest=True)

        for arg in argv:
            if not arg.startswith('-'):
                continue
            current_dest = get_args_from_argparser(parser,
                                                   store_action=False,
                                                   store_true_action=False,
                                                   store_false_action=True,
                                                   arg=arg,
                                                   get_dest=True)
            remainder_dests = set(set(remainder_dests) -
                                  set(current_dest))

        for dest in remainder_dests:
            if dest in self.__dict__.keys():
                value = self.__dict__[dest]
                if isinstance(value, list):
                    value = value[0]
                if not value:
                    argv += [get_args_from_argparser(parser,
                                                     store_action=False,
                                                     store_true_action=False,
                                                     store_false_action=True,
                                                     dest=dest)[0]]

        return plant.execute([module_name + '.py'] + argv, verbose=verbose,
                             level=level)

    def copy_image(self, *args, **kwargs):
        method_to_execute = plant.copy_image
        kwargs['method_to_execute'] = method_to_execute
        kwargs = populate_kwargs(self, *args, **kwargs)
        ret = method_to_execute(*args, **kwargs)
        return ret

    def rename_image(self, *args, **kwargs):
        method_to_execute = plant.rename_image
        kwargs['method_to_execute'] = method_to_execute
        kwargs = populate_kwargs(self, *args, **kwargs)
        ret = method_to_execute(*args, **kwargs)
        return ret

    def get_plant_transform_obj(self, *args, **kwargs):
        method_to_execute = plant.get_plant_transform_obj
        kwargs['method_to_execute'] = method_to_execute
        kwargs = populate_kwargs(self, *args, **kwargs)
        ret = method_to_execute(*args, **kwargs)
        return ret

    def read_image(self, *args, **kwargs):
        method_to_execute = plant.read_image
        kwargs['method_to_execute'] = method_to_execute
        kwargs = populate_kwargs(self, *args, **kwargs)
        ret = method_to_execute(*args, **kwargs)
        return ret

    def get_info(self, *args, **kwargs):
        method_to_execute = plant.get_info
        kwargs['method_to_execute'] = method_to_execute
        kwargs = populate_kwargs(self, *args, **kwargs)
        ret = method_to_execute(*args, **kwargs)
        return ret

    def overwrite_file_check(self, *args, **kwargs):
        method_to_execute = plant.overwrite_file_check
        kwargs['method_to_execute'] = method_to_execute
        kwargs = populate_kwargs(self, *args, **kwargs)
        ret = method_to_execute(*args, **kwargs)
        return ret

    def save_image(self, *args, **kwargs):
        method_to_execute = plant.save_image
        kwargs['method_to_execute'] = method_to_execute
        kwargs = populate_kwargs(self, *args, **kwargs)
        ret = method_to_execute(*args, **kwargs)
        return ret

    def save_vector(self, *args, **kwargs):
        method_to_execute = plant.save_vector
        kwargs['method_to_execute'] = method_to_execute
        kwargs = populate_kwargs(self, *args, **kwargs)
        ret = method_to_execute(*args, **kwargs)
        return ret

    def test_overlap(self, *args, **kwargs):
        method_to_execute = plant.test_overlap
        kwargs['method_to_execute'] = method_to_execute
        kwargs = populate_kwargs(self, *args, **kwargs)
        ret = method_to_execute(*args, **kwargs)
        return ret

    def render_vrt(self, *args, **kwargs):
        if 'context' not in kwargs.keys():
            kwargs['context'] = self
        return plant.render_vrt(*args, **kwargs)


def ret(args):
    raise plant.PlantReturn(args)


def get_command_line_from_argv(argv):
    command = ''
    for i in range(0, len(argv)):
        if i != 0:
            command += ' '
        for invalid_dash in plant.INVALID_DASH_LIST:
            argv[i] = argv[i].replace(invalid_dash, '-')
        command += argv[i] if argv[i].startswith('-') else '"' + argv[i] + '"'
    return command


def pre_parser(argv=None, parser=None, flag_remove_duplicates=True,
               verbose=True):

    if argv is None:
        argv = sys.argv[1:]

    if parser is not None and flag_remove_duplicates:
        arg_dict = {}
    else:
        arg_dict = None
    flag_skip_next = False
    output_argv = []

    for i, current_arg in enumerate(argv):

        if flag_skip_next:
            flag_skip_next = False
            continue

        for invalid_dash in plant.INVALID_DASH_LIST:
            current_arg = current_arg.replace(invalid_dash, '-')
        flag_parameter_key = (current_arg.startswith('-') and
                              not plant.is_numeric(current_arg))

        if (flag_parameter_key and
                ',' in current_arg):
            current_arg = ' ' + current_arg
        elif (flag_parameter_key and
              arg_dict is not None and
              current_arg not in arg_dict.keys()):
            arg_dict[current_arg] = True
        elif flag_parameter_key and arg_dict is not None:
            ret = plant.get_args_from_argparser(parser,
                                                store_true_action=True,
                                                store_false_action=True,
                                                store_action=False,
                                                help_action=True,
                                                get_dest=True,
                                                arg=current_arg)
            flag_single_parameter = len(ret) > 0

            if not flag_single_parameter:
                flag_skip_next = True
                if verbose and len(argv) > i + 1:
                    print(f'WARNING ignoring repeated argument:'
                          f' {current_arg} {argv[i+1]}')
            if verbose and flag_single_parameter or len(argv) <= i + 1:
                print(f'WARNING ignoring repeated argument:'
                      f' {current_arg}')
            continue

        if all([plant.isnumeric(x) for x in
                current_arg.split(plant.IMAGE_NAME_SEPARATOR)]):
            current_arg = ' ' + current_arg

        output_argv.append(current_arg)

    return output_argv


def get_output_file_name(current_file,
                         input_images,
                         output_file,
                         output_dir,
                         output_list=None,
                         output_keep_input_tree=None,
                         output_keep_input_flat_tree=None,
                         extension=None,
                         flag_separate=True):

    if output_keep_input_flat_tree and output_file:

        output_file = path.join(
            path.dirname(current_file),
            output_file).replace(
            path.sep,
            '_')

    elif output_keep_input_flat_tree:

        output_file = current_file.replace(path.sep, '_')

    if output_keep_input_tree and output_dir:

        output_dir = path.join(output_dir,
                               path.dirname(current_file))

    elif output_keep_input_tree:
        output_dir = path.dirname(current_file)

    if (not (plant.test_valid_number_sequence(current_file)) and

            plant.IMAGE_NAME_SEPARATOR in current_file):
        current_file_splitted = current_file.split(plant.IMAGE_NAME_SEPARATOR)

        flag_has_driver_name = current_file_splitted[0].upper(
        ) in plant.AVAILABLE_DRIVERS
        if flag_has_driver_name and len(current_file_splitted) > 2:
            current_file = current_file_splitted[1]

    if (extension and
            not extension.startswith('.') and
            not extension.startswith('_')):
        extension = '.' + extension

    if (len(input_images) == 1 and path.basename(output_file) and
            flag_separate):
        if path.dirname(output_file):
            output_file = plant.replace_extension(output_file,
                                                  extension)
            return output_file
        if output_dir:
            output_file = path.join(output_dir, output_file)
        output_file = plant.replace_extension(output_file,
                                              extension)
        return output_file

    elif path.basename(output_file):
        output_basename_root = path.basename(output_file)

    elif flag_separate:
        output_basename_root = path.basename(current_file)
    else:
        return

    output_basename_root_splitted = output_basename_root.split('.')

    if len(output_basename_root_splitted) == 1 and not extension:
        extension = ''
    elif len(output_basename_root_splitted) >= 1 and not extension:
        extension = '.' + output_basename_root_splitted[-1]
        output_basename_root = '.'.join(output_basename_root_splitted[:-1])
    elif len(output_basename_root_splitted) >= 1:

        output_basename_root = '.'.join(output_basename_root_splitted[:-1])

    output_file_test = ('%s%s'
                        % (output_basename_root,
                           extension))

    if output_dir:
        output_file = path.join(output_dir,
                                output_file_test)

    if plant.FLAG_OUTPUT_DIR_OVERWRITE:
        return output_file

    file_counter = 2
    while True:
        if output_list is not None:
            flag_exists = False
            for j in output_list:
                if output_file_test in output_list:
                    flag_exists = True
            if not flag_exists:
                output_list.append(output_file_test)
                break
        elif not plant.isfile(output_file):
            break
        output_file_test = ('%s_%d%s'
                            % (output_basename_root,
                               file_counter,
                               extension))
        if output_dir:
            output_file = path.join(output_dir,
                                    output_file_test)
        file_counter += 1

    return output_file


class ExtendAction(python_argparse.Action):

    def __call__(self, parser, namespace, values, option_string=None):

        items = getattr(namespace, self.dest)

        if not items:
            items = []
        elif not plant.is_sequence(items):
            items = [items]
        if not plant.is_sequence(values):
            items.append(values)
        else:
            items.extend(values)
        setattr(namespace, self.dest, items)


def argparse_print_help(parser, cli_mode=None):
    if cli_mode or cli_mode is None:
        parser.print_help()
        return

    module_name = parser.prog
    if module_name.startswith('plant_'):
        module_name = module_name.replace('plant_', '').replace('.py', '')
        module_name = 'plant.' + module_name
    module_description = parser.description

    module_epilog = parser.epilog
    args_list = []
    message = ''
    for group in parser.__dict__['_action_groups']:
        printed_args = []

        for i, x in enumerate(group._group_actions):
            arg = x.__dict__['dest']
            if arg in printed_args:
                continue
            if i == 0:
                message += f'OPTION {group.title}:\n'
            printed_args.append(arg)
            help_str = x.__dict__['help']
            type_str = x.__dict__['type']
            default = x.__dict__['default']
            args_list.append(f'{arg}={default}')
            nargs = x.__dict__['nargs']
            if (nargs is not None and plant.isnumeric(nargs) and
                    int(nargs) > 0):
                nargs_str = f', nargs={nargs}'
            if (nargs is not None and not plant.isnumeric(nargs)):
                nargs_str = f', nargs={nargs}'
            else:
                nargs_str = ''
            if type_str is None:
                type_str = 'bool'
            else:
                type_str = type_str.__name__
            required_str = '' if x.__dict__['required'] else ', optional'
            message += (f'PARAMETER  {arg}: {type_str}{nargs_str}'
                        f'{required_str}\n')
            message += f'    {help_str}\n'
    args_str = ', '.join(args_list)
    print('')
    lines_str = "=" * (len(module_name) + 2)
    print(f'## {lines_str}')
    print(f'##  {module_name}')
    print(f'## {lines_str}')
    print('')
    if module_description:
        for line in module_description.split('\n'):
            print(f'{line}')
        print('')
    print(f'OPTION usage:')
    print(f'  {module_name}({args_str})')
    print('')
    print(message)
    if module_epilog:
        for line in module_epilog.split('\n'):
            print(f'{line}')


def argparse(description='',
             epilog='',
             geo=0,
             bbox=0,
             bbox_topo=0,
             bbox_file=0,

             height_args=0,

             step=0,
             step_y=0,
             step_x=0,
             input_file=0,
             input_files=0,
             input_image=0,
             secondary_input_image=0,
             input_images=0,
             secondary_input_images=0,
             separate=0,
             output_ext=0,
             pickle_dir=0,

             username=0,
             password=0,

             topo_dir=0,
             topo_dir_ml=0,
             backward_geocoding_x=0,
             backward_geocoding_y=0,
             backward_geocoding=0,

             default_flags=0,
             input_format=0,
             input_key=0,

             input_length=0,
             input_width=0,
             input_depth=0,
             input_dtype=0,

             dem_file=0,
             dem_file_ml=0,

             phase_ref_file=0,
             phase_ref_file_ml=0,

             height_ref_file=0,
             height_ref_file_ml=0,

             unwrap_ref_file=0,
             unwrap_ref_file_ml=0,

             z_file=0,
             z_file_ml=0,

             inc_file=0,
             inc_file_ml=0,
             los_file=0,
             los_file_ml=0,
             psi_file=0,
             psi_file_ml=0,

             kz=0,
             kz_ml=0,
             slope=0,
             slope_ml=0,
             flag_wpha=0,
             flag_wcoh=0,
             min_coh=0,

             flag_debug=0,
             flag_mute=0,
             flag_quiet=0,
             flag_verbose=0,
             flag_force=0,
             flag_keep_temporary=0,
             flag_never=0,
             flag_all=0,

             flag_use_ctable=0,

             default_options=0,
             default_null_options=0,
             null=0,
             in_null=0,
             out_null=0,
             input_projection=0,
             default_geo_input_options=0,
             default_geo_output_options=0,
             output_projection=0,
             default_input_options=0,
             band=0,

             date_all=0,
             date_epoch=0,
             date_epoch_final=0,
             date_year=0,
             date_month=0,
             date_day=0,
             date_hour=0,
             date_minute=0,

             output_file=0,
             output_dir=0,
             default_output_options=0,
             output_format=0,
             output_dtype=0,
             output_scheme=0,
             output_skip_if_existent=0,
             save_header_only=0,

             cmap=0,
             cmap_min=0,
             cmap_max=0,
             background_color=0,
             percentile=0,
             kmz_cbar_offset=0,
             separator=0,

             save_as_text=0,
             save_as_vector=0,
             save_as_raster=0,
             save_as_raster_gdal=0,

             default_vector=0,
             default_lidar=0,
             forest_height=0,
             ground=0,
             canopy=0,
             multilook=0,
             geo_multilook=0,
             pixel_size=0,
             pixel_size_x=0,
             pixel_size_y=0,

             n_points=0,

             mask=0,
             transform=0):

    if description:
        description = '  ' + '\n  '.join(description.split('\n'))
        description = plant.DESCRIPTION_STRING + description

    if default_input_options or default_options:
        description += descr_inputs

    if default_output_options or default_options:
        description += descr_output

    if default_flags or default_options:
        description += descr_flags

    if topo_dir:
        description += descr_topo

    if geo:
        description += descr_geo

    if separate:
        description += descr_separate

    if epilog:
        epilog = '  ' + '\n  '.join(epilog.split('\n'))
        epilog = plant.USAGE_EXAMPLES_STRING + epilog

    parser = python_argparse.ArgumentParser(
        formatter_class=python_argparse.RawDescriptionHelpFormatter,
        epilog=epilog,
        add_help=False,

        description=description)
    parser.register('action', 'extend', ExtendAction)

    parser.add_argument('-h',
                        '--help',
                        action='store_true',
                        dest='help',
                        help='Show this help message and exit')

    parser_text_colors = parser.add_mutually_exclusive_group()
    parser_text_colors.add_argument('--text-color',
                                    action='store_true',
                                    default=None,
                                    dest='flag_color_text',
                                    help='Use coloured text in CLI/API mode')
    parser_text_colors.add_argument('--no-text-color',
                                    action='store_false',
                                    dest='flag_color_text',
                                    help='Prevent colouring text in'
                                    ' CLI/API mode')

    parser_bash = parser.add_mutually_exclusive_group()
    parser_bash.add_argument('--api',
                             '--interactive',
                             '--no-bash',
                             '--no-cli',
                             '--no-shell',
                             action='store_false',

                             dest='cli_mode',

                             help='Show help in interactive (API) mode')
    parser_bash.add_argument('--bash',
                             '--cli',
                             '--shell',
                             action='store_true',

                             dest='cli_mode',

                             help='Show help in CLI mode')

    if (input_file or
            input_files or
            input_image or
            secondary_input_image or
            input_images or
            secondary_input_images or
            default_options or
            default_input_options or
            band or
            input_format or
            input_key or
            input_width or
            input_length or
            input_depth or
            input_dtype or

            separator or
            pickle_dir):
        input_group = parser.add_argument_group(plant.PARSER_GROUP_SEPARATOR +
                                                'input arguments')

    if input_file:
        if input_file == 2:
            arg_dest = 'input_file_required'
        else:
            arg_dest = 'input_file'
        input_group.add_argument(arg_dest,
                                 type=str,
                                 metavar=(arg_dest.replace(
                                     '_required', '')),
                                 nargs='*',
                                 action='extend',
                                 help='Input file')
        input_group.add_argument('-i',
                                 dest=arg_dest,
                                 type=str,
                                 metavar=(arg_dest.replace(
                                     '_required', '')),
                                 nargs='*',
                                 action='extend',
                                 help='Input file')
    if input_files:
        if input_files == 2:
            arg_dest = 'input_files_required'
        else:
            arg_dest = 'input_files'
        input_group.add_argument(arg_dest,
                                 type=str,
                                 metavar=(arg_dest.replace(
                                     '_required', '')),
                                 nargs='*',
                                 action='extend',
                                 help='Input files')
        input_group.add_argument('-i',
                                 dest=arg_dest,
                                 type=str,
                                 metavar=(arg_dest.replace(
                                     '_required', '')),
                                 nargs='*',
                                 action='extend',
                                 help='Input files')

    if input_image:
        if input_image == 2:
            arg_dest = 'input_image_required'
        else:
            arg_dest = 'input_image'
        input_group.add_argument(arg_dest,
                                 type=str,
                                 metavar=(arg_dest.replace(
                                     '_required', '')),
                                 nargs='*',
                                 action='extend',
                                 help='Input image')
        input_group.add_argument('-i',
                                 dest=arg_dest,
                                 type=str,
                                 metavar=(arg_dest.replace(
                                     '_required', '')),
                                 nargs='*',
                                 action='extend',
                                 help='Input image')
    if input_images:
        if input_images == 2:
            arg_dest = 'input_images_required'
        else:
            arg_dest = 'input_images'
        input_group.add_argument(arg_dest,
                                 type=str,
                                 metavar=(arg_dest.replace(
                                     '_required', '')),
                                 nargs='*',
                                 action='extend',
                                 help='Input images')

        input_group.add_argument('-i',
                                 dest=arg_dest,
                                 type=str,
                                 metavar=(arg_dest.replace(
                                     '_required', '')),
                                 nargs='*',
                                 action='extend',
                                 help='Input images')
    if secondary_input_image:
        input_group.add_argument('--secondary',
                                 dest='secondary_input_image',
                                 type=str,
                                 required=secondary_input_image == 2,
                                 help='Secondary input image')
    if secondary_input_images:
        input_group.add_argument(
            '--secondary',
            dest='secondary_input_images',
            type=str,
            nargs='*' if secondary_input_images == 1 else '+',
            help='Secondary input images')

    if default_options or default_input_options or band:
        input_group.add_argument('--band',
                                 '--bands',
                                 dest='band',
                                 type=str,

                                 required=band == 2,
                                 help='Input band. If not provided'
                                 ', first band (0) is used.'
                                 ' single values applies to '
                                 'all input data or multiple values '
                                 'separated by commas')

    if default_options or default_input_options or input_format:
        input_group.add_argument('--if',
                                 '--input-format',

                                 dest='input_format',
                                 type=str,
                                 required=input_format == 2,
                                 help='Force input format:'
                                 'ISCE or any GDAL supported formats '
                                 '(e.g. GTiff, ENVI)')

    if default_options or default_input_options or input_key:
        input_group.add_argument('--ik',

                                 '--key',
                                 '--input-key',
                                 '--ovr',
                                 '--overview',
                                 type=str,
                                 dest='input_key',
                                 required=input_key == 2,
                                 help='Input key (e.g.: CSV)')

    if default_options or default_input_options or input_width:
        input_group.add_argument('--input-width',
                                 type=int,
                                 required=input_width == 2,
                                 help='Input width',
                                 dest='input_width')

    if default_options or default_input_options or input_length:
        input_group.add_argument('--input-length',
                                 type=int,
                                 required=input_length == 2,
                                 help='Input length',
                                 dest='input_length')

    if default_options or default_input_options or input_depth:
        input_group.add_argument('--input-depth',
                                 type=int,
                                 required=input_depth == 2,
                                 help='Input depth',
                                 dest='input_depth')

    if default_options or default_input_options or input_dtype:
        input_group.add_argument('--input-dtype',
                                 type=str,
                                 required=input_dtype == 2,
                                 help='Input dtype',
                                 dest='input_dtype')

    if input_files or input_images or secondary_input_images:
        input_group.add_argument('--sort',
                                 dest='input_sort',
                                 action='store_true',
                                 help='Sort inputs by name')

    if separator:
        input_group.add_argument('--separator',
                                 dest='separator',
                                 required=separator == 2,
                                 type=str,
                                 help='Character separator'
                                 ' (use \t for TAB)')

    if pickle_dir:
        input_group.add_argument('--pickle',
                                 dest='pickle_dir',
                                 type=str,
                                 help='pickle file directory',
                                 default='')

    if username:
        parser.add_argument('--user-name',
                            '--username',
                            dest='username',
                            type=str,
                            help='Username')

    if password:
        parser.add_argument('--password',
                            dest='password',
                            type=str,
                            help='Password')

    if (dem_file or
            dem_file_ml or

            phase_ref_file or
            phase_ref_file_ml or

            height_ref_file or
            height_ref_file_ml or

            unwrap_ref_file or
            unwrap_ref_file_ml or

            z_file or
            z_file_ml or

            inc_file or
            inc_file_ml or
            los_file or
            los_file_ml or
            psi_file or
            psi_file_ml or
            kz or
            kz_ml or
            slope or
            slope_ml or
            flag_wcoh or
            flag_wpha or
            min_coh):
        insar_group = parser.add_argument_group(plant.PARSER_GROUP_SEPARATOR +
                                                'InSAR arguments')

    if dem_file:
        insar_group.add_argument('--dem',
                                 '--dem-file',
                                 dest='dem_file',
                                 required=dem_file == 2,
                                 type=str,
                                 help='Reference DEM file')

    if dem_file_ml:
        insar_group.add_argument('--dem-ml',
                                 '--dem-ml-file',
                                 '--dem-file-ml',
                                 dest='dem_file_ml',
                                 required=dem_file_ml == 2,
                                 type=str,
                                 help='Reference DEM file (multilooked)')

    if phase_ref_file:
        insar_group.add_argument('--phase-ref',
                                 '--phase-ref-file',
                                 dest='phase_ref_file',
                                 required=phase_ref_file == 2,
                                 type=str,
                                 help='Reference phase file')

    if phase_ref_file_ml:
        insar_group.add_argument('--phase-ref-ml',
                                 '--phase-ref-ml-file',
                                 '--phase-ref-file-ml',
                                 dest='phase_ref_file_ml',
                                 required=phase_ref_file_ml == 2,
                                 type=str,
                                 help='Reference phase file (multilooked)')

    if height_ref_file:
        insar_group.add_argument('--height-ref',
                                 '--height-ref-file',
                                 dest='height_ref_file',
                                 required=height_ref_file == 2,
                                 type=str,
                                 help='Reference height file')

    if height_ref_file_ml:
        insar_group.add_argument('--height-ref-ml',
                                 '--height-ref-ml-file',
                                 '--height-ref-file-ml',
                                 dest='height_ref_file_ml',
                                 required=height_ref_file_ml == 2,
                                 type=str,
                                 help='Reference height file (multilooked)')

    if unwrap_ref_file:
        insar_group.add_argument('--unwrap-ref',
                                 '--unwrap-ref-file',
                                 dest='unwrap_ref_file',
                                 required=unwrap_ref_file == 2,
                                 type=str,
                                 help='Reference file for phase unwrap')

    if unwrap_ref_file_ml:
        insar_group.add_argument('--unwrap-ref-ml',
                                 '--unwrap-ref-ml-file',
                                 '--unwrap-ref-file-ml',
                                 dest='unwrap_ref_file_ml',
                                 required=unwrap_ref_file_ml == 2,
                                 type=str,
                                 help='Reference file for phase unwrap'
                                 ' (multilooked)')

    if z_file:
        insar_group.add_argument('-z',
                                 '--z-file',
                                 dest='z_file',
                                 required=z_file == 2,
                                 type=str,
                                 help='Reference elevation file')

    if z_file_ml:
        insar_group.add_argument('--z-ml',
                                 '--z-ml-file',
                                 '--z-file-ml',
                                 dest='z_file_ml',
                                 required=z_file_ml == 2,
                                 type=str,
                                 help='Reference elevation file (multilooked)')

    if inc_file:
        insar_group.add_argument('--inc',
                                 '--inc-file',
                                 dest='inc_file',
                                 help='Incidence angle file')

    if inc_file_ml:
        insar_group.add_argument('--inc-ml',
                                 '--inc-ml-file',
                                 '--inc-file-ml',
                                 dest='inc_file_ml',
                                 help='Incidence angle file')

    if los_file:
        insar_group.add_argument('--los',
                                 '--los-file',
                                 dest='los_file',
                                 help='Line-of-sight angle file')

    if los_file_ml:
        insar_group.add_argument('--los-ml',
                                 '--los-ml-file',
                                 '--los-file-ml',
                                 dest='los_file_ml',
                                 help='Line-of-sight angle file')

    if psi_file:
        insar_group.add_argument('--psi',
                                 '--psi-file',
                                 dest='psi_file',
                                 help='Projection angle file')

    if psi_file_ml:
        insar_group.add_argument('--psi-ml',
                                 '--psi-ml-file',
                                 '--psi-file-ml',
                                 dest='psi_file_ml',
                                 help='Projection angle file')

    if kz:
        insar_group.add_argument('--kz',
                                 '--kz-file',
                                 dest='kz',
                                 required=kz == 2,
                                 type=str,
                                 help='Interferometric vertical'
                                 ' wavenumber (kz) image')

    if kz_ml:
        insar_group.add_argument('--kz-ml',
                                 '--kz-ml-file',
                                 '--kz-file-ml',
                                 dest='kz_ml',
                                 required=kz_ml == 2,
                                 type=str,
                                 help='Interferometric vertical'
                                 ' wavenumber (kz) image'
                                 ' (multilooked)')

    if slope:
        insar_group.add_argument('--slope',
                                 '--slope-file',
                                 dest='slope',
                                 required=slope == 2,
                                 type=str,
                                 help='Interferometric vertical'
                                 ' wavenumber (slope) image')

    if slope_ml:
        insar_group.add_argument('--slope-ml',
                                 '--slope-ml-file',
                                 '--slope-file-ml',
                                 dest='slope_ml',
                                 required=slope_ml == 2,
                                 type=str,
                                 help='Interferometric vertical'
                                 ' wavenumber (slope) image'
                                 ' (multilooked)')

    if min_coh:
        insar_group.add_argument('--min-coh',
                                 '--min-coherence',
                                 type=float,
                                 required=min_coh == 2,
                                 help='min_coh')

    if flag_wcoh:
        insar_group.add_argument('--wcoh',
                                 dest='flag_wcoh',
                                 action='store_true',
                                 help='Save wrapped coherence')

    if flag_wpha:
        insar_group.add_argument('--wpha',
                                 dest='flag_wpha',
                                 action='store_true',
                                 help='Save wrapped phase')

    if (geo or
            bbox or
            bbox_topo or
            bbox_file or
            input_projection or
            step or
            step_y or
            step_x or
            default_geo_input_options or

            default_geo_output_options or
            output_projection):
        geo_group = parser.add_argument_group(plant.PARSER_GROUP_SEPARATOR +
                                              'geographic arguments')

    if (geo or bbox or bbox_topo or bbox_file or step or
            step_y or step_x):
        parser_geo = geo_group.add_mutually_exclusive_group(required=geo == 2)

    if geo or bbox:
        parser_geo.add_argument('-b',
                                '--bbox',
                                type=float,
                                nargs=4,
                                dest='bbox',
                                metavar=('Y_MIN', 'Y_MAX', 'X_MIN', 'X_MAX'),
                                help='Defines the spatial region in '
                                'the format south north west east.')
    if geo:
        parser_geo.add_argument('-c',
                                '--geo-center',
                                '--geocenter',
                                '--bbox-center',
                                '--b-center',
                                type=float,
                                nargs=4,
                                metavar=('Y_CENTER', 'X_CENTER', 'Y_SIZE',
                                         'X_SIZE'),
                                dest='geo_center',
                                help='Defines the bounding box of the center '
                                'position S-N, E-W, and the image dimensions '
                                'in S-N and E-W coordinates. '
                                'Ex: -p 3 4 1 1, the image will be centered '
                                'in lat=3 lon=4 with dimensions of 1 (length) '
                                'and 1 (width)')
    if geo:
        parser_geo.add_argument('--geo-search',
                                '--geosearch',
                                type=str,
                                nargs=3,
                                dest='geo_search_str',
                                help='Geolocate using search string and '
                                'image dimensions in S-N E-W coodinates.'
                                ' Ex: Brazil 5 5')
    if geo or bbox_topo:
        parser_geo.add_argument('--sr',
                                '--bbox-topo',
                                '--b-topo',
                                dest='bbox_topo',
                                type=str,
                                help='Import '
                                'coordinates from lat.rdr and lon.rdr '
                                'georreference files. Please inform the '
                                'directory location of these files')
    if geo or bbox_file:
        parser_geo.add_argument('-g',
                                '--geo-ref',
                                '--bbox-file',
                                '--b-file',
                                dest='bbox_file',
                                type=str,
                                help='Import coordinates '
                                'boundaries from a georeferenced file.')
    if geo or input_projection:
        geo_group.add_argument('--projection',
                               '--in-projection',
                               '--input-projection',
                               dest='projection',
                               type=str,
                               help='Input projection')
    if geo or step:
        geo_group.add_argument('-s',
                               '--step',
                               dest='step',
                               type=float,
                               help='Output latitude and '
                               'longitude pixel size in degrees ')
    if geo or step_y:
        geo_group.add_argument('--step-lat',
                               dest='step_y',
                               type=float,
                               help='Output latitude'
                               'pixel size in degrees (only for '
                               'landcover map downloading).')
    if geo or step_x:
        geo_group.add_argument('--step-lon',
                               dest='step_x',
                               type=float,
                               help='Output longitude'
                               'pixel size in degrees (only for '
                               'landcover map downloading)')

    if geo or step:
        geo_group.add_argument('--sm',
                               '--step-m',
                               dest='step_m',
                               type=float,
                               help='Output latitude and '
                               'longitude pixel size in meters ')
    if geo or step_y:
        geo_group.add_argument('--step-m-lat',
                               dest='step_m_y',
                               type=float,
                               help='Output latitude'
                               'pixel size in meters (only for '
                               'landcover map downloading).')
    if geo or step_x:
        geo_group.add_argument('--step-m-lon',
                               dest='step_m_x',
                               type=float,
                               help='Output longitude'
                               'pixel size in meters (only for '
                               'landcover map downloading)')

    if default_geo_output_options or output_projection:
        geo_group.add_argument('--out-proj',
                               '--out-projection',
                               '--output-projection',
                               dest='output_projection',
                               type=str,
                               help='Output projection')

    if topo_dir or topo_dir_ml:
        geolocation_group = parser.add_argument_group(
            plant.PARSER_GROUP_SEPARATOR + 'geolocation arguments')

    if topo_dir:
        geolocation_group.add_argument('-t',
                                       '--llh',
                                       '--topo-dir',
                                       '--topo',
                                       '--topo-file',
                                       dest='topo_dir',

                                       type=str,
                                       help='File or directory containing'
                                       ' geolocation/topographic files.'
                                       ' Options: ISCE topo.vrt,'
                                       ' topo directory (containing lat.rdr,'
                                       ' lon.rdr, inc.rdr, etc.), or UAVSAR'
                                       ' .llh file')

        geolocation_group.add_argument('--t-lat',
                                       '--lat-file',
                                       '--file-lat',
                                       dest='lat_file',

                                       type=str,
                                       help='File or directory containing'
                                       ' latitude geolocation array')

        geolocation_group.add_argument('--t-lon',
                                       '--lon-file',
                                       '--file-lon',
                                       dest='lon_file',

                                       type=str,
                                       help='File or directory containing'
                                       ' longitude geolocation array')

    if backward_geocoding or backward_geocoding_x:
        parser.add_argument('--bg-x',
                            '--backward-geocoding-x',
                            dest='backward_geocoding_x',
                            type=str,
                            help='Directory containing'
                            ' backward geocoding X image')

    if backward_geocoding or backward_geocoding_y:
        parser.add_argument('--bg-y',
                            '--backward-geocoding-y',
                            dest='backward_geocoding_y',
                            type=str,
                            help='Directory containing'
                            ' backward geocoding Y image')

    if topo_dir_ml:
        geolocation_group.add_argument('--t-ml',
                                       '--llh-ml',
                                       '--topo-dir-ml',
                                       '--topo-ml',
                                       '--topo-file-ml',
                                       dest='topo_dir_ml',

                                       type=str,
                                       help='File or directory containing'
                                       ' multilooked geolocation/topographic'
                                       ' files.'
                                       ' Options: ISCE topo.vrt,'
                                       ' topo directory (containing lat.rdr,'
                                       ' lon.rdr, inc.rdr, etc.), or UAVSAR'
                                       ' .llh file')

        geolocation_group.add_argument('--t-lat-ml',
                                       dest='lat_file_ml',

                                       type=str,
                                       help='File or directory containing'
                                       ' multilooked latitude geolocation'
                                       ' array')

        geolocation_group.add_argument('--t-lon-ml',
                                       dest='lon_file_ml',

                                       type=str,
                                       help='File or directory containing'
                                       ' multilooked longitude geolocation'
                                       ' array')

    if height_args:
        height_group = parser.add_argument_group(plant.PARSER_GROUP_SEPARATOR +
                                                 'height arguments')

        height_group.add_argument('--height-step',
                                  '--step-height',
                                  '--pixel-size-z',
                                  type=float,
                                  dest='height_step',
                                  required=height_args == 2,
                                  help='Height step in meters')

        height_group.add_argument('--height-diff',
                                  '--diff-height',
                                  type=float,
                                  dest='height_diff',
                                  required=height_args == 2,
                                  help='Height difference in meters')

        height_group.add_argument('--height-max',
                                  '--max-height',
                                  type=float,
                                  dest='height_max',
                                  required=height_args == 2,
                                  help='Maximum height in meters')

        height_group.add_argument('--height-min',
                                  '--min-height',
                                  type=float,
                                  dest='height_min',
                                  required=height_args == 2,
                                  help='Minimum height in meters')

        height_group.add_argument('--height-nbins',
                                  '--nbins-height',
                                  type=int,
                                  dest='height_nbins',
                                  required=height_args == 2,
                                  help='Height in number of bins')

    if (date_all,
            date_epoch,
            date_epoch_final,
            date_year,
            date_month,
            date_day,
            date_hour,
            date_minute):
        date_group = parser.add_argument_group(plant.PARSER_GROUP_SEPARATOR +
                                               'date arguments')
    if date_all or date_epoch:
        date_group.add_argument(
            '--epoch',
            dest='epoch',
            type=str,
            help='Acquisition epoch (e.g. "2015-05-31 21:00:00")',
            required=date_all == 2 or date_epoch == 2)
    if date_epoch_final:
        date_group.add_argument(
            '--final-epoch',
            '--f-epoch',
            '--epoch-final',
            '--epoch-f',
            dest='epoch_final',
            type=str,
            help='Final epoch (e.g. "2015-06-01 05:55:00")',
            required=date_epoch_final == 2)
    if date_all or date_year:
        date_group.add_argument('-y',
                                '--year',
                                dest='year',
                                type=int,
                                help='Acquisition year',
                                required=date_all == 2 or date_year == 2)
    if date_all or date_month:
        date_group.add_argument('-m',
                                '--month',
                                dest='month',
                                type=int,
                                help='Acquisition month',
                                required=date_all == 2 or date_month == 2)
    if date_all or date_day:
        date_group.add_argument('-d',
                                '--day',
                                dest='day',
                                type=int,
                                help='Acquisition day',
                                required=date_all == 2 or date_day == 2)
    if date_all or date_hour:
        date_group.add_argument('--hour',
                                dest='hour',
                                type=int,
                                help='Acquisition hour',
                                required=date_all == 2 or date_hour == 2)
    if date_all or date_minute:
        date_group.add_argument('--minute',
                                dest='minute',
                                type=int,
                                help='Acquisition minute',
                                required=date_all == 2 or date_minute == 2)

    if (separate or
            output_ext or
            output_file or
            output_dir or
            separate or
            default_options or
            default_output_options or
            output_format or
            output_dtype or
            output_scheme or
            output_skip_if_existent or
            save_header_only or
            cmap or
            cmap_min or
            cmap_max or
            background_color or
            percentile or
            kmz_cbar_offset):
        output_group = parser.add_argument_group(plant.PARSER_GROUP_SEPARATOR +
                                                 'output arguments')

    if separate:
        output_group.add_argument('--separate',
                                  '--sep',
                                  '--separated',
                                  dest='separate',
                                  action='store_true',
                                  help='Handle individually a '
                                  'list of files, resulting in multiple '
                                  'outputs instead of one.')

    if separate or output_ext or output_dir:
        output_group.add_argument('--ext',
                                  '--extension',
                                  '--output-ext',
                                  '--output-extenson',
                                  dest='output_ext',
                                  type=str,
                                  help='Output file extension')

    if output_file:
        output_group.add_argument('-o',
                                  '--output',
                                  '--output-file',
                                  '--output-image',
                                  dest='output_file',
                                  type=str,
                                  required=output_file == 2,
                                  help='Output file',
                                  default='')

    if output_dir or separate:
        output_group.add_argument('--od',
                                  '--output-dir',
                                  dest='output_dir',
                                  type=str,
                                  required=output_dir == 2,
                                  help='Output directory')

        output_group.add_argument('--keep-tree',
                                  '--keep-input-tree',
                                  '--keep-output-tree',
                                  '--output-tree',
                                  '--output-keep-tree',
                                  '--output-keep-input-tree',
                                  '--output-dir-tree',
                                  '--output-dir-keep-tree',
                                  '--output-dir-keep-input-tree',
                                  dest='output_keep_input_tree',
                                  action='store_true',
                                  help='Save results in output directory'
                                  ' keeping input tree structure')

        output_group.add_argument('--keep-flat-tree',
                                  '--keep-flat-input-tree',
                                  '--keep-flat-output-flat-tree',
                                  '--output-flat-tree',
                                  '--output-flat-keep-flat-tree',
                                  '--output-flat-keep-flat-input-tree',
                                  '--output-flat-dir-tree',
                                  '--output-flat-dir-keep-flat-tree',
                                  '--output-flat-dir-keep-flat-input-tree',
                                  dest='output_keep_input_flat_tree',
                                  action='store_true',
                                  help='Save results in output directory'
                                  ' keeping input tree structure substituting'
                                  ' directory separators by underscore')

    if default_options or default_output_options or output_format:
        output_group.add_argument('--of',
                                  '--output-format',
                                  '--out-format',
                                  dest='output_format',
                                  type=str,
                                  required=output_format == 2,
                                  help='Output format:'
                                  'ISCE or any GDAL supported formats '
                                  '(e.g. GTiff, ENVI)')

    if default_options or default_output_options or output_dtype:
        output_group.add_argument('--ot',
                                  '--odt',
                                  '--output-dtype',
                                  '--out-dtype',
                                  '--output-data-type',
                                  '--out-data-type',
                                  dest='output_dtype',
                                  type=str,
                                  required=output_dtype == 2,
                                  help='Output data type')

    if default_options or default_output_options or output_scheme:
        output_group.add_argument('--os',
                                  '--output-scheme',
                                  '--scheme',
                                  dest='output_scheme',
                                  type=str,
                                  required=output_scheme == 2,
                                  help='Output data scheme. Options: BIL,'
                                  ' BIP, BSQ.')

    if default_options or default_output_options or output_skip_if_existent:
        output_group.add_argument('--se',
                                  '--skip-if-existent',
                                  '--output-skip-if-existent',
                                  dest='output_skip_if_existent',
                                  action='store_true',
                                  help='Skips execution if '
                                  'output already exists')

    if default_options or default_output_options or save_header_only:
        parser.add_argument('--save-only-header',
                            '--save-header-only',
                            '--only-header',
                            dest='save_header_only',
                            help='Save header files only '
                            '(image file is not updated)',
                            action='store_true')

    if default_options or default_output_options or cmap:
        output_group.add_argument('--cmap',
                                  dest='cmap',
                                  type=str,
                                  help='Matplotlib color map')

    if default_options or default_output_options or cmap or cmap_min:
        output_group.add_argument('--cmap-min',
                                  dest='cmap_min',
                                  type=str,
                                  help='Matplotlib color map minimum value')

    if default_options or default_output_options or cmap or cmap_max:
        output_group.add_argument('--cmap-max',
                                  dest='cmap_max',
                                  type=str,
                                  help='Matplotlib color map maximum value')

    if default_options or default_output_options or cmap or cmap_min:
        output_group.add_argument('--cmap-crop-min',
                                  dest='cmap_crop_min',
                                  type=str,
                                  help='Crop matplotlib color map to data'
                                  ' values')

    if default_options or default_output_options or cmap or cmap_max:
        output_group.add_argument('--cmap-crop-max',
                                  dest='cmap_crop_max',
                                  type=str,
                                  help='Crop matplotlib color map to data'
                                  ' values')

    if default_options or default_output_options or cmap or background_color:
        output_group.add_argument('--bg', '--bg-color',
                                  '--background',
                                  '--background-color',
                                  dest='background_color',
                                  type=str,
                                  help='Matplotlib background color')

    if default_options or default_output_options or cmap or percentile:
        output_group.add_argument('-p',
                                  '--percentile',
                                  '--percent',
                                  dest='percentile', type=float,
                                  help='Percentile of the data to calculate '
                                  'data ranges')

    if default_options or default_output_options or kmz_cbar_offset:
        output_group.add_argument('--add-kmz-colorbar-offset',
                                  '--add-kml-colorbar-offset',
                                  '--add-kmz-cbar-offset',
                                  '--add-kml-cbar-offset',
                                  dest='flag_add_kmz_cbar_offset',
                                  action='store_true',
                                  default=None,
                                  help='Add KML/KMZ colorbar offset')
        output_group.add_argument('--kmz-colorbar-offset-color',
                                  '--kml-colorbar-offset-color',
                                  '--kmz-cbar-offset-color',
                                  '--kml-cbar-offset-color',
                                  dest='kmz_cbar_offset_color',
                                  type=str,
                                  default=None,
                                  help='KML/KMZ colorbar offset color')

        output_group.add_argument('--kmz-colorbar-offset-length',
                                  '--kml-colorbar-offset-length',
                                  '--kmz-cbar-offset-length',
                                  '--kml-cbar-offset-length',
                                  dest='kmz_cbar_offset_length',
                                  type=float,
                                  default=None,
                                  help='KML/KMZ colorbar offset length')

        output_group.add_argument('--kmz-colorbar-offset-width',
                                  '--kml-colorbar-offset-width',
                                  '--kmz-cbar-offset-width',
                                  '--kml-cbar-offset-width',
                                  dest='kmz_cbar_offset_width',
                                  type=float,
                                  default=None,
                                  help='KML/KMZ colorbar offset width')

        output_group.add_argument('--kmz-colorbar-offset-alpha',
                                  '--kml-colorbar-offset-alpha',
                                  '--kmz-cbar-offset-alpha',
                                  '--kml-cbar-offset-alpha',
                                  dest='kmz_cbar_offset_alpha',
                                  type=float,
                                  default=None,
                                  help='KML/KMZ colorbar offset alpha')

    if (default_options or
            default_flags or
            flag_mute or
            flag_quiet or
            flag_verbose or
            flag_force or
            flag_debug or
            flag_keep_temporary or
            flag_never or
            flag_all or
            null or
            in_null or
            out_null):
        default_group = parser.add_argument_group(
            plant.PARSER_GROUP_SEPARATOR +
            'default options arguments')

    if default_options or default_flags or flag_mute:
        parser_mute = default_group.add_mutually_exclusive_group()

    if default_options or default_flags or flag_mute:
        parser_mute.add_argument('--mute',
                                 default=None,
                                 dest='flag_mute',
                                 action='store_true',
                                 help='Activate mute mode')
        parser_mute.add_argument('--no-mute',
                                 dest='flag_mute',
                                 action='store_false',
                                 help='Prevent using mute mode')

    if default_options or default_flags or flag_quiet or flag_verbose:
        parser_verbose = default_group.add_mutually_exclusive_group()

    if default_options or default_flags or flag_quiet:
        parser_verbose.add_argument('-q',
                                    '--quiet',
                                    dest='verbose',
                                    action='store_false',
                                    help='Activate quiet (non-verbose) mode',
                                    default=True)
    if default_options or default_flags or flag_verbose:
        parser_verbose.add_argument('-v',
                                    '--verbose',
                                    dest='verbose',
                                    action='store_true',
                                    help='Activate verbose mode',
                                    default=True)

    if default_options or default_flags or flag_force:
        default_group.add_argument('-f',
                                   '--force',
                                   dest='force',
                                   action='store_true',
                                   help='Force execution. Never prompt')
    if default_options or default_flags or flag_debug:

        parser_debug = default_group.add_mutually_exclusive_group()

        parser_debug.add_argument('-u',
                                  '--debug',
                                  action='store_true',
                                  default=None,
                                  help='Debug mode',
                                  dest='flag_debug')
        parser_debug.add_argument('--no-debug',
                                  action='store_false',
                                  help='Prevent entering debug mode',
                                  dest='flag_debug')

        default_group.add_argument('--ul',
                                   '--debug-level',
                                   type=int,
                                   help='Debug level',
                                   dest='debug_level')

    if default_options or default_flags or flag_keep_temporary:
        default_group.add_argument('-k',
                                   '--keep',
                                   '--keep-temporary',
                                   '--keep-temporary-files',
                                   dest='flag_keep_temporary',
                                   action='store_true',
                                   help='Keep temporary files')

    if default_options or default_flags or flag_never:
        default_group.add_argument('-N',
                                   '--never-substitute',
                                   dest='flag_never',
                                   action='store_true',
                                   help='Never substitute existing files')
    if default_options or default_flags or flag_all:
        default_group.add_argument('-A',
                                   '--substitute-all',
                                   dest='flag_all',
                                   action='store_true',
                                   help='Substitute all existing files')

    if default_options or default_null_options or null:
        default_group.add_argument('--null',
                                   dest='null',
                                   type=float,
                                   help='Input/output null value.')
    if default_options or default_null_options or in_null:
        default_group.add_argument('--in-null',
                                   dest='in_null',
                                   type=float,
                                   help='Input null value.')
    if default_options or default_null_options or out_null:
        default_group.add_argument('--out-null',
                                   dest='out_null',
                                   type=float,
                                   help='Output null value.')

    if flag_use_ctable:
        parser_ctable = input_group.add_mutually_exclusive_group()
        parser_ctable.add_argument('--use-ctable',
                                   '--ctable',
                                   '--use-color-table',
                                   '--color-table',
                                   dest='flag_use_ctable',
                                   default=None,
                                   action='store_true',
                                   help='Use color table when available')
        parser_ctable.add_argument('--not-use-ctable',
                                   '--no-ctable',
                                   '--not-use-color-table',
                                   '--no-color-table',
                                   dest='flag_use_ctable',
                                   action='store_false',
                                   help='Prevent the app from using input'
                                   ' color tables (even if available)')

    if (default_lidar or
            forest_height or
            canopy or
            ground):
        lidar_group = parser.add_argument_group(plant.PARSER_GROUP_SEPARATOR +
                                                'default options arguments')

    if default_lidar:
        default_vector = True
        lidar_group.add_argument('--loreys-height',
                                 dest='loreys_height',
                                 action='store_true',
                                 help="Average heights using Lorey's height"
                                 ' (only available for option --raster)')

        lidar_group.add_argument('--sum-samples',
                                 dest='sum_samples',
                                 action='store_true',
                                 help="Sum samples inside bins, instead "
                                 'of averaging them '
                                 ' (only available for option --raster)')
    if default_lidar or forest_height:
        lidar_group.add_argument('--forest-height',
                                 dest='forest_height',
                                 action='store_true',
                                 help='Estimates forest height (canopy '
                                 'less ground height)')

    if default_lidar or ground:
        lidar_group.add_argument('--ground',
                                 dest='ground',
                                 action='store_true',
                                 help='Estimates ground height')
    if default_lidar or canopy:
        lidar_group.add_argument('--canopy',
                                 dest='canopy',
                                 action='store_true',
                                 help='Estimates upper canopy height')

    if (default_vector or
            save_as_text or
            save_as_vector or
            save_as_raster or
            save_as_raster_gdal):
        vector_group = parser.add_argument_group(plant.PARSER_GROUP_SEPARATOR +
                                                 'vector arguments')

    if default_vector or save_as_text:
        vector_group.add_argument('--text',
                                  dest='save_as_text',
                                  action='store_true',
                                  help='Output is saved as a text file')
    if default_vector or save_as_vector:
        vector_group.add_argument('--vector',
                                  dest='save_as_vector',
                                  action='store_true',
                                  help='Output is saved as vector file.')
    if default_vector or save_as_raster:
        vector_group.add_argument('--image',
                                  '--raster',
                                  dest='save_as_raster',
                                  action='store_true',
                                  help='Output is saved as a raster.')
    if default_vector or save_as_raster_gdal:
        vector_group.add_argument('--gdal-image',
                                  '--gdal-raster',
                                  dest='save_as_raster_gdal',
                                  action='store_true',
                                  help='Output is saved as a raster '
                                  'using GDAL.')

    if (multilook or
            geo_multilook or
            pixel_size or
            pixel_size_x or
            pixel_size_y):
        radar_group = parser.add_argument_group(plant.PARSER_GROUP_SEPARATOR +
                                                'radar arguments')

    if multilook:
        radar_group.add_argument('-r',
                                 '--range',
                                 '--nlooks-rg',
                                 '--nlooks-x',
                                 type=int,

                                 help='Number of looks in '
                                 'X-direction (range).',
                                 dest='nlooks_x')
        radar_group.add_argument('-a',
                                 '--azimuth',
                                 '--nlooks-az',
                                 '--nlooks-y',
                                 type=int,

                                 help='Number of looks in '
                                 'X-direction (azimuth).',
                                 dest='nlooks_y')
        radar_group.add_argument('--ml',
                                 '--nlooks',
                                 '--nlooks-mean',
                                 dest='nlooks',
                                 type=float, nargs='+',
                                 help='Number of looks'
                                 ' (default: %(default)s)')

    if geo_multilook:
        radar_group.add_argument('--nlooks-lon',
                                 type=int,
                                 help='Number of looks in longitude.',
                                 dest='nlooks_lon')
        radar_group.add_argument('--nlooks-lat',
                                 type=int,
                                 help='Number of looks in latitude.',
                                 dest='nlooks_lat')

    if pixel_size or pixel_size_x:
        radar_group.add_argument('--pixel-size-rg',
                                 '--pixel-size-x',
                                 '--pixel-spacing-rg',
                                 '--pixel-spacing-x',
                                 dest='pixel_size_x',
                                 type=float,
                                 help='Set pixel size [m] in '
                                 'X-direction (range)')
    if pixel_size or pixel_size_y:
        radar_group.add_argument('--pixel-size-az',
                                 '--pixel-size-y',
                                 '--pixel-spacing-az',
                                 '--pixel-spacing-y',
                                 dest='pixel_size_y',
                                 type=float,
                                 help='Set pixel size [m] in '
                                 'Y-direction (azimuth)')

    if n_points:
        other_group = parser.add_argument_group(plant.PARSER_GROUP_SEPARATOR +
                                                'other arguments')
    if n_points:
        other_group.add_argument('-n',
                                 '--npoints',
                                 '--n-points',
                                 type=int,
                                 required=n_points == 2,
                                 help='Number of points.',
                                 dest='n_points')

    if mask or default_input_options or default_options:
        mask_group = parser.add_argument_group(plant.PARSER_GROUP_SEPARATOR +
                                               'mask/data selection')
        mask_group.add_argument('--mask-equal',
                                '--different',
                                '--different-than',
                                '--ne',
                                dest='mask_equal',
                                type=float,
                                help='Mask values equal to')
        mask_group.add_argument('--mask-different',
                                '--equal',
                                '--equal-than',
                                '--eq',
                                dest='mask_different',
                                type=float,
                                help='Mask values different than')
        mask_group.add_argument('--mask-greater',
                                '--less-equal',
                                '--less-than-equal-to',
                                '--le',
                                dest='mask_greater',
                                type=float,
                                help='Mask values greater than')
        mask_group.add_argument('--mask-less',
                                '--greater-equal',
                                '--greater-than-equal-to',
                                '--ge',
                                dest='mask_less',
                                type=float,
                                help='Mask values less than')
        mask_group.add_argument('--mask-greater-equal',
                                '--less',
                                '--less-than',
                                '--lt',
                                dest='mask_greater_equal',
                                type=float,
                                help='Mask values greater than'
                                ' or equal to')
        mask_group.add_argument('--mask-less-equal',
                                '--greater',
                                '--greater-than',
                                '--gt',
                                dest='mask_less_equal',
                                type=float,
                                help='Mask values less than'
                                ' or equal to')
        mask_group.add_argument(
            '--mask-erode',
            '--mask-erosion',
            dest='mask_erode',
            type=int,
            help='Applies binary erosion to mask with a given size')
        mask_group.add_argument(
            '--mask-dilate',
            '--mask-dilation',
            dest='mask_dilate',
            type=int,
            help='Applies binary dilation to mask with a given size')
        mask_group.add_argument(
            '--mask-closing',
            dest='mask_closing',
            type=int,
            help='Applies binary closing to mask with a given size')
        mask_group.add_argument(
            '--mask-opening',
            dest='mask_opening',
            type=int,
            help='Applies binary opening to mask with a given size')
        mask_group.add_argument('--mask-border',
                                dest='mask_border',
                                type=int,
                                help='Erodes data (excluding small internal '
                                'NaN islands) with a given size')
        mask_group.add_argument('-w',
                                '-srcwin',
                                '--srcwin',
                                dest='srcwin',
                                type=int,
                                metavar=('X_OFF', 'Y_OFF', 'X_SIZE', 'Y_SIZE'),
                                help='Selects a subwindow from the source '
                                'image for copying based on pixel/line '
                                'location (from GDAL). Please, provide four '
                                'elements: xoff yoff xsize ysize',
                                nargs=4)
        mask_group.add_argument('--sel-x',
                                '--col',
                                '--cols',
                                '--col-list-string',

                                dest='select_col',
                                type=str,
                                help='Polygon vertices columns '
                                '(polygons separated by commas "," '
                                'and vertices separated by colons '
                                '":")')
        mask_group.add_argument('--sel-y',
                                '--row',
                                '--rows',
                                '--line',
                                '--lines',
                                '--row-list-string',

                                dest='select_row',
                                type=str,
                                help='Polygon vertices rows '
                                '(polygons separated by commas "," '
                                'and vertices separated by colons '
                                '":")')

        mask_group.add_argument('--polygon',
                                dest='polygon',
                                type=str,
                                help='Input polygon file')
        mask_group.add_argument('--sel-geo-polygon',
                                '--sel-polygon-geo',
                                '--polygon-geo',

                                dest='geo_polygon',
                                type=str,
                                help='Input polygon file')

        mask_group.add_argument('--sel-geo-x',
                                '--sel-lon',
                                dest='select_geo_x',
                                type=str,
                                help='Longitude selection')
        mask_group.add_argument('--sel-geo-y',
                                '--sel-lat',
                                dest='select_geo_y',
                                type=str,
                                help='Latitude selection')

        mask_group.add_argument('--ref-mask',
                                '--mask-ref',
                                dest='mask_ref',
                                type=str,
                                help='File to be used as '
                                'reference to generate the mask')

        mask_group.add_argument('--in-mask',
                                '--mask-in',
                                '--input-mask',
                                '--mask-input',
                                '--shapefile',
                                '--shape-file',
                                dest='in_mask',
                                type=str,
                                help='Input mask file')

        mask_group.add_argument('--out-mask',
                                '--mask-out',
                                dest='out_mask',
                                type=str,
                                help='Output mask file')

        mask_group.add_argument('--use-mask-as-input',
                                '--mask-as-input',
                                action='store_true',
                                dest='mask_as_input',

                                help='Use mask as input')

    if transform or default_input_options or default_options:
        transform_group = parser.add_argument_group(
            plant.PARSER_GROUP_SEPARATOR +
            'data transform')

        transform_group.add_argument('--module',

                                     '--absolute',

                                     '--amplitude',
                                     '--magnitude',
                                     '--mag',
                                     action='store_true',
                                     dest='transform_abs',
                                     help='Absolute value')

        transform_group.add_argument('--real',
                                     '--real-part',
                                     action='store_true',
                                     dest='transform_real',
                                     help='Real part')

        transform_group.add_argument('--imag',
                                     '--imaginary',
                                     '--imaginary-part',
                                     action='store_true',
                                     dest='transform_imag',
                                     help='Imaginary part')

        transform_group.add_argument('--fft-x',
                                     action='store_true',
                                     dest='transform_fft_x',
                                     help='Calculate FFT in X direction')

        transform_group.add_argument('--fft-y',
                                     action='store_true',
                                     dest='transform_fft_y',
                                     help='Calculate FFT in Y direction')

        transform_group.add_argument('--fft',
                                     action='store_true',
                                     dest='transform_fft',
                                     help='Calculate FFT')

        transform_group.add_argument('--ifft-x',
                                     '--inverse-fft-x',
                                     action='store_true',
                                     dest='transform_ifft_x',
                                     help='Calculate FFT inverse in X'
                                     ' direction')

        transform_group.add_argument('--ifft-y',
                                     '--inverse-fft-y',
                                     action='store_true',
                                     dest='transform_ifft_y',
                                     help='Calculate FFT inverse in Y'
                                     ' direction')

        transform_group.add_argument('--ifft',
                                     '--inverse-fft',
                                     action='store_true',
                                     dest='transform_ifft',
                                     help='Calculate FFT inverse')

        transform_group.add_argument('--square', '--sq',
                                     action='store_true',
                                     dest='transform_square',
                                     help='Square of input')

        transform_group.add_argument('--square-root', '--sqrt',
                                     action='store_true',
                                     dest='transform_square_root',
                                     help='Square root of input')

        transform_group.add_argument('--sin',
                                     '--sine',
                                     action='store_true',
                                     dest='transform_sin',
                                     help='Get sine of the input'
                                     ' (input in rad)')

        transform_group.add_argument('--asin',
                                     '--a-sin',
                                     '--arc-sin',
                                     '--arc-sine',
                                     '--arcsine',
                                     action='store_true',
                                     dest='transform_arcsin',
                                     help='Get arcsine of the input')

        transform_group.add_argument('--cos',
                                     '--cose',
                                     action='store_true',
                                     dest='transform_cos',
                                     help='Get cose of the input'
                                     ' (input in rad)')

        transform_group.add_argument('--acos',
                                     '--a-cos',
                                     '--arc-cos',
                                     '--arc-cosine',
                                     '--arccosine',
                                     action='store_true',
                                     dest='transform_arccos',
                                     help='Get arccosine of the input')

        transform_group.add_argument('--tan',
                                     '--tangent',
                                     action='store_true',
                                     dest='transform_tan',
                                     help='Get tangent of the input'
                                     ' (input in rad)')

        transform_group.add_argument('--atan',
                                     '--a-tan',
                                     '--arc-tan',
                                     '--arc-tangent',
                                     '--arctangent',
                                     action='store_true',
                                     dest='transform_arctan',
                                     help='Get arctangent of the input')

        transform_group.add_argument('--deg2rad',
                                     '--deg-to-rad',
                                     '--radians',
                                     action='store_true',
                                     dest='transform_deg2rad',
                                     help='Convert input from degrees to'
                                     ' radians')

        transform_group.add_argument('--rad2deg',
                                     '--rad-to-deg',
                                     '--degrees',
                                     action='store_true',
                                     dest='transform_rad2deg',
                                     help='Convert input from radians to'
                                     ' degrees')

        transform_group.add_argument('--negative',
                                     action='store_true',
                                     dest='transform_negative',
                                     help='Negative value')

        transform_group.add_argument('--inverse',
                                     '--inv',
                                     action='store_true',
                                     dest='transform_inverse',
                                     help='Inverse value')

        transform_group.add_argument('--reverse-x', '--x-reverse',
                                     action='store_true',
                                     dest='transform_reverse_x',
                                     help='Reverse image in X direction')

        transform_group.add_argument('--reverse-y', '--y-reverse',
                                     action='store_true',
                                     dest='transform_reverse_y',
                                     help='Reverse image in Y direction')

        transform_group.add_argument('--conj', '--conjugate',
                                     action='store_true',
                                     dest='transform_conj',
                                     help='Conjugate complex')

        transform_group.add_argument('--angle', '--ang', '--arg', '--phase',
                                     action='store_true',
                                     dest='transform_angle',
                                     help='Angle (argument) of a complex data')

        transform_group.add_argument('--angle-deg', '--ang-deg',
                                     '--arg-deg', '--phase-deg',
                                     action='store_true',
                                     dest='transform_angle_deg',
                                     help='Angle (argument) of a complex data')

        transform_group.add_argument('--phase-wrap', '--wrap-phase',
                                     '--wrap-phase',
                                     action='store_true',
                                     dest='transform_phase_wrap',
                                     help='Wrap phase')

        transform_group.add_argument('--db', '--dB', '--db10', '--db-10',
                                     dest='transform_db10',
                                     action='store_true',
                                     help='Data in dB: '
                                     '10.log10(data)')

        transform_group.add_argument('--db20', '--db-20',
                                     dest='transform_db20',
                                     action='store_true',
                                     help='Data in dB: '
                                     '20.log10(data)')

        transform_group.add_argument('--inv-db', '--inv-dB', '--inv-db10',
                                     '--inv-db_10',
                                     dest='transform_inv_db10',
                                     action='store_true',
                                     help='Data from dB to Np: '
                                     '10^(data/10)')

        transform_group.add_argument('--inv-db20', '--inv-db-20',
                                     dest='transform_inv_db20',
                                     action='store_true',
                                     help='Print from dB to Np: '
                                     '10^(data/20)')
    return parser


def parse_arg(arg, parser, argv):
    arg_value = None
    args_true = get_args_from_argparser(
        parser,
        store_true_action=True,
        store_false_action=False,
        store_action=False,
        dest=arg)
    for current_arg in args_true:
        if current_arg in argv:
            arg_value = True
            break
    if arg_value is None:
        args_false = get_args_from_argparser(
            parser,
            store_true_action=False,
            store_false_action=True,
            store_action=False,
            dest=arg)
        for current_arg in args_false:
            if current_arg in argv:
                arg_value = False
                break
    return arg_value


def get_args_from_argparser(parser,
                            store_true_action=True,
                            store_false_action=True,
                            store_action=True,
                            help_action=False,
                            get_dest=False,
                            arg=None,
                            dest=None):

    if arg is not None and dest is not None:
        print('ERROR please select only one option: '
              'arg or dest')
        return
    arg_list = []
    for x in parser.__dict__['_actions']:
        if not ((store_action and
                x.__class__.__name__ == '_StoreAction') or
                (help_action and
                x.__class__.__name__ == '_HelpAction') or
                (store_true_action and
                x.__class__.__name__ == '_StoreTrueAction') or
                (store_false_action and
                x.__class__.__name__ == '_StoreFalseAction')):
            continue
        if (arg is not None and
                arg in x.__dict__['option_strings'] and
                get_dest):
            return [x.__dict__['dest']]
        elif (arg is not None and
                arg in x.__dict__['option_strings']):
            return x.__dict__['option_strings']
        elif (dest is not None and
              dest == x.__dict__['dest'] and
              get_dest):
            return [x.__dict__['dest']]
        elif (dest is not None and
              dest == x.__dict__['dest']):
            arg_list += x.__dict__['option_strings']

        elif arg is None and dest is None and get_dest:
            arg_list += [x.__dict__['dest']]
        elif arg is None and dest is None:
            arg_list += x.__dict__['option_strings']

    return arg_list


def run_script(script, parser, argv):
    with plant.PlantLogger():
        self_obj = script(parser, argv)
        if self_obj.separate:
            input_images = self_obj.input_images
            ret_list = []
            for i, current_file in enumerate(input_images):
                self_obj.input_images = [current_file]
                self_obj.output_file = self_obj.output_files[i]
                if (self_obj.output_skip_if_existent and
                        plant.isfile(self_obj.output_file)):
                    print('INFO output file %s already exist, '
                          'skipping execution..' % self_obj.output_file)
                    continue
                ret = self_obj.run()
                ret_list.append(ret)
            if len(ret_list) == 1:
                return ret_list[0]
            return ret_list
        ret = self_obj.run()
        return ret


def populate_kwargs(caller_obj, *args, **kwargs):

    flag_private_attributes = kwargs.pop(
        'flag_private_attributes', False)
    method_kwargs_dict = kwargs.pop('method_kwargs_dict', None)
    method_to_execute = kwargs.pop('method_to_execute')

    method_kwargs_keys = list(method_to_execute.__code__.co_varnames)

    method_nargs = method_to_execute.__code__.co_argcount
    method_kwargs_keys = method_kwargs_keys[:method_nargs]

    args_already_present = []
    for i, arg in enumerate(args):
        args_already_present.append(method_kwargs_keys[i])

        if isinstance(arg, plant.PlantImage):
            if arg.geotransform is not None:
                args_already_present.append('geotransform')
            if arg.projection is not None:
                args_already_present.append('projection')

    if isinstance(caller_obj, dict):
        caller_dictionary = caller_obj
    else:
        caller_dictionary = caller_obj.__dict__

    for key in caller_dictionary.keys():

        if flag_private_attributes and not key.startswith('_'):
            continue
        elif flag_private_attributes:
            new_key = key[1:]
        else:
            new_key = key
        if (method_kwargs_dict is not None and
                new_key in method_kwargs_dict.keys()):

            new_key = method_kwargs_dict[new_key]
        if (new_key in method_kwargs_keys and
                new_key not in kwargs and
                new_key not in args_already_present and
                caller_dictionary[key] is not None):

            kwargs[new_key] = caller_dictionary[key]

    return kwargs


def get_kwargs_dict_read_image():
    kwargs = {}
    kwargs['file_format'] = 'input_format'
    kwargs['band_orig'] = 'band'
    kwargs['null'] = 'in_null'
    kwargs['length_orig'] = 'input_length'
    kwargs['width_orig'] = 'input_width'
    kwargs['depth_orig'] = 'input_depth'

    return kwargs


descr_inputs = (' Input data can be cropped, masked or transformed'
                ' using PLAnT framework options.')

descr_output = (' Output data can be saved to different formats'
                ' ("--of/--output-format")'
                ' including ENVI, GeoTiff and KMZ; different'
                ' data types (--ot/--output-dtype), schemes'
                ' (--os/--output-scheme).')

descr_flags = ('There are four different levels of verbosity:'
               ' regular verbose (default), quite (-q),'
               ' mute (--mute) or debug (-u).'
               ' The debug trace level can be set with the parameter'
               ' --ul/--debug-level followed by the level.'
               ' The force mode (force saving/force accepting)'
               ' can be turned on with the parameter -f.'
               ' Temporary files can be kept with the option -k.')

descr_topo = (' The geolocation arrays (defined by "-t")'
              ' can be LLH UAVSAR'
              ' files, ISCE topo directory or VRT file')

descr_geo = (' The bounding box/step options include:'
             ' bounding box (--bbox),'
             ' step (--step/--step-lat/--step-lon),'
             ' step in meters (--step-m/--step-m-lat/--step-m-lon)),'
             ' geographic center (--geocenter),'
             ' reference file (--geo-ref).')

descr_separate = (' Each file can be processed separately with'
                  ' the parameter "--separate". In this case,'
                  ' the output files are saved in the output directory'
                  ' (please use output directory "--output-dir"'
                  ' instead of a single output file "--output-file").')
