#!/usr/bin/env python3
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Gustavo H. X. Shiroma, Marco Lavalle
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import scipy

from collections.abc import Sequence

import itertools
import time
import numbers
from scipy import interpolate
from scipy.ndimage import shift as scipy_shift
from PIL import Image
import numpy as np
from datetime import datetime
import plant

from plant.modules.plant_constants import N_SIGNIFICANT_FIGURES


def datetime_support_decorator(method):
    def decorated(f):
        def wrapper(*args, **kwargs):
            ret = f(*args, **kwargs)
            if ret is not None:
                args = (ret, args[1])
            try:
                value = method(*args, **kwargs)
                return value
            except BaseException:
                pass

            if isinstance(args[0], list):
                datetime_flag_array = [plant.get_dtype_name(d) == 'datetime'
                                       for d in args[0]]
            else:
                datetime_flag_array = [plant.get_dtype_name(d.item(0)) ==
                                       'datetime'
                                       for d in np.nditer(args[0],
                                                          flags=['refs_ok'])]
            if all(datetime_flag_array):
                time_vector = datetime_to_array(args[0])
                if time_vector is None:
                    print(f'ERROR method {method.__name__}'
                          ' does not offser support for the data types in'
                          f' {args[0]}')
                    return
                value_timestamp = method(time_vector,
                                         *args[1:],
                                         **kwargs)
                value = datetime.fromtimestamp(value_timestamp)
                return value
            elif any(datetime_flag_array):
                ind = np.where(np.logical_not(datetime_flag_array))
                args = [np.asarray(args[0])[ind]] + list(args[1:])
            value = method(*args, **kwargs)
            return value
        return wrapper
    return decorated


@datetime_support_decorator(np.nanpercentile)
def nanpercentile(*args, **kwargs):
    if isinstance(args[0], np.ma.core.MaskedArray):
        image = args[0]
        valid_ind = np.where(~image.mask)
        return image[valid_ind]
    pass


@datetime_support_decorator(np.nanmin)
def nanmin(*args, **kwargs):
    pass


@datetime_support_decorator(np.nanmax)
def nanmax(*args, **kwargs):
    pass


def datetime_to_array(data, flag_matplotlib=False):
    if flag_matplotlib:

        import matplotlib.dates as mdates
        date2num = mdates.date2num
    else:
        date2num = time.mktime
    if isinstance(data, list):
        try:

            time_vector = [float(date2num(d.timetuple()))
                           for d in data]
        except AttributeError:
            return
    else:

        try:
            time_vector = [float(date2num(d.item(0).timetuple()))
                           for d in np.nditer(data,
                                              flags=['refs_ok'])]
        except AttributeError:
            return
        time_vector = np.asarray(time_vector)
    return time_vector


def get_bool_string(data, invalid_value=None):
    if isinstance(data, bool):
        return data
    if not data:
        return invalid_value
    ret = data.lower() in ['true', '1', 't', 'y', 'yes', 'on']
    return ret


def array_to_datetime(data):
    if isinstance(data, list):
        try:
            time_vector = [datetime.fromtimestamp(d)
                           for d in data]
        except AttributeError:
            return
    else:
        try:
            time_vector = [datetime.fromtimestamp(d.item(0).timetuple())
                           for d in np.nditer(data,
                                              flags=['refs_ok'])]
        except AttributeError:
            return
        time_vector = np.asarray(time_vector)
    return time_vector


def curve_fit(*args, **kwargs):
    from scipy.optimize import curve_fit as scipy_curve_fit
    if plant.get_dtype_name(args[1]) == 'datetime':
        time_vector = datetime_to_array(args[1])
        args = [args[0], time_vector] + list(args[2:])
    if plant.get_dtype_name(args[2]) == 'datetime':
        time_vector = datetime_to_array(args[2])
        args = list(args[0:2]) + [time_vector] + list(args[3:])
    return scipy_curve_fit(*args, **kwargs)


def isvalid(*args, **kwargs):
    return np.logical_not(isnan(*args, **kwargs))


def isnan(data, null=None):

    if isinstance(data, list):

        return [isnan(d, null=null) for d in data]
    else:
        data_type = plant.get_dtype_name(data)

    if ('str' in data_type
            or 'datetime' in data_type):
        return data == str(null)

    if data_type == 'NoneType':
        return True

    if isinstance(data, np.ma.MaskedArray):
        return np.logigal_or(np.ma.getmask(data),
                             isnan(np.ma.getdata(data)))

    if ('int' in data_type.lower() and
            null is None):
        return np.logical_or(data == plant.get_int_nan(data),
                             data == plant.DOUBLE_NAN)

    elif ('int' in data_type.lower() and
          not np.isfinite(null)):
        return np.logical_or(data == plant.get_int_nan(data),
                             data == plant.DOUBLE_NAN)

    elif 'int' in data_type.lower():
        return np.logical_or(data == null, data == plant.DOUBLE_NAN)

    elif null is None:
        return np.logical_or(np.logical_not(np.isfinite(data)),
                             data == plant.DOUBLE_NAN)

    elif not np.isfinite(null):
        return np.logical_or(np.logical_not(np.isfinite(data)),
                             data == plant.DOUBLE_NAN)

    else:
        return np.logical_or(np.logical_or(np.logical_not(np.isfinite(data)),
                                           data == null),
                             data == plant.DOUBLE_NAN)


def has_nan(data):
    for i in range(data.shape[0]):
        if isnan(data[i]).any():
            return True
    return False


def isnumeric(data):

    x = str(data)
    x = x.replace('-', '')
    x = x.replace('.', '')
    x = x.strip()
    if x == 'nan' or x == 'NaN':
        return True
    if 'e' in x:
        try:
            float(x)
        except ValueError:
            return False
        return True
    return x.isnumeric()


def is_numeric(*args, **kwargs):
    return isnumeric(*args, **kwargs)


def isinteger(x):
    return np.equal(np.mod(x, 1), 0)


def is_integer(*args, **kwargs):
    return isinteger(*args, **kwargs)


def get_sequence_depth(seq):
    if is_numeric(seq):
        return 0
    if isinstance(seq, str):
        return 0

    depth = isinstance(seq, Sequence) and max(map(get_sequence_depth, seq)) + 1

    return depth


def get_sequence_shape(seq, sequence_depth=None):
    if (seq is None or
        (isinstance(seq, Sequence) and
         len(seq) == 0)):
        return [0]
    if sequence_depth is None:
        sequence_depth = get_sequence_depth(seq)
    if sequence_depth == 0:
        return [1]
    if sequence_depth == 1:
        return [len(seq)]
    if sequence_depth == 2:
        max_len = None
        for sub_seq in seq:
            this_len = get_sequence_shape(sub_seq)[0]
            if max_len is None:
                max_len = this_len
            else:
                max_len = max([max_len, this_len])
        return [len(seq), max_len]
    max_shape = np.zeros(sequence_depth, dtype=np.int32) - 1
    max_shape[0] = len(seq)
    for sub_seq in seq:
        this_len = get_sequence_shape(sub_seq)
        for i in range(len(this_len)):
            if this_len[i] > max_shape[i + 1]:
                max_shape[i + 1] = this_len[i]
    max_shape_list = [max([0, m]) for m in max_shape]
    return max_shape_list


def _sequence_to_array_nan_padding_1d(seq, shape=None):
    if shape is None:
        return np.asarray(seq)
    image = np.zeros((shape))
    image[:] = np.nan

    if isnumeric(seq):

        image[0] = seq

    else:
        for i in range(len(seq)):

            image[i] = seq[i]

    return image


def sequence_to_array_nan_padding(seq, shape=None):
    if isnumeric(seq):
        return _sequence_to_array_nan_padding_1d(seq, shape=shape)
    sequence_depth = get_sequence_depth(seq)
    if sequence_depth == 1:
        return _sequence_to_array_nan_padding_1d(seq, shape=shape)
    shape = get_sequence_shape(seq)
    image = np.zeros((shape))
    image[:] = np.nan
    for i, sub_seq in enumerate(seq):
        sub_image = sequence_to_array_nan_padding(sub_seq, shape=shape[1:])
        image[i, ...] = sub_image
    return image


def shape_image(data):
    if data is None:
        return data

    if (isinstance(data, Sequence) and
            isinstance(data[0], str)):

        data = np.asarray(data)
    elif isinstance(data, Sequence):
        data = sequence_to_array_nan_padding(data)
    elif not isinstance(data, np.ndarray):
        data = np.asarray(data)
    if len(data.shape) == 0:
        data = data.reshape(1, 1)
    elif len(data.shape) == 1:
        data = data.reshape(1, data.size)
    elif len(data.shape) == 3 and data.shape[0] == 1:
        data = data.reshape(data.shape[1:])
    return data


def nan_compare(input_1, input_2):
    if isnan(input_1) and isnan(input_2):
        return True
    elif ((isnan(input_1) and isvalid(input_2)) or
          (isvalid(input_1) and isnan(input_1))):
        return False
    return input_1 == input_2


def get_intersection(*args):
    args = list(args)
    for i in range(1, len(args)):
        if len(args[0].shape) != len(args[i].shape):
            print('ERROR input dimensions do not match: %s %s.'
                  % (args[0].shape, args[i].shape))
            return
    flag_get_intersection = False
    for i in range(1, len(args)):
        if args[0].shape != args[i].shape:
            print('WARNING input dimensions do not match: %s and %s.'
                  ' Considering data intersection only.'
                  % (args[0].shape, args[i].shape))
            flag_get_intersection = True
    if not flag_get_intersection:
        return args
    min_shape = np.zeros((len(args[0].shape)))
    for j in range(len(args[0].shape)):
        min_shape[j] = np.min([args[i].shape[j]
                               for i in range(len(args))])
    for i in range(len(args)):
        if len(min_shape) == 1:
            args[i] = args[i][0:int(min_shape[0])]
        elif len(min_shape) == 2:
            args[i] = args[i][0:int(min_shape[0]),
                              0:int(min_shape[1])]
        elif len(min_shape) == 3:
            args[i] = args[i][0:int(min_shape[0]),
                              0:int(min_shape[1]),
                              0:int(min_shape[2])]
        else:
            print('ERROR input with 4 dimensions or more is'
                  ' not supported')
            return
    return args


def copy_shape(*args, null=np.nan,
               flag_exit_if_error=True,
               flag_no_messages=False):
    args = list(args)
    args[0] = args[0].shape
    ret = copy_shape_from_tuple(*args, null=np.nan,
                                flag_exit_if_error=True,
                                flag_no_messages=False)
    return ret


def copy_shape_from_tuple(*args, null=np.nan,
                          flag_exit_if_error=True,
                          flag_no_messages=False):

    args = list(args)
    for i in range(1, len(args)):
        if len(args[0]) != len(args[i].shape):
            if flag_exit_if_error:
                print('ERROR input dimensions do not match: %s %s.'
                      % (args[0], args[i].shape))
            return
    flag_get_intersection = False
    for i in range(1, len(args)):
        if args[0] != args[i].shape:
            if not flag_no_messages:
                print('WARNING input dimensions do not match: %s and %s.'
                      ' Considering data intersection only.'
                      % (args[0], args[i].shape))
            flag_get_intersection = True
    if not flag_get_intersection and len(args) == 2:
        return args[1]
    elif not flag_get_intersection:
        return args[1:]
    ref_shape = args[0]

    for i in range(1, len(args)):
        args[i] = cut_image_with_shape(args[i], ref_shape, null=null)
    if len(args) == 2:
        return args[1]
    return args[1:]


def concatenate_y_filelist(filelist):
    image_obj_list = []
    max_nbands = None
    ref_image_obj = None
    for filename in filelist:
        image_obj = plant.read_image(filename, verbose=False)
        image_obj_list.append(image_obj)
        if max_nbands is None:
            max_nbands = image_obj.nbands
            ref_image_obj = image_obj.copy()
        else:
            max_nbands = max([max_nbands,
                              image_obj.nbands])

    for b in range(max_nbands):
        image_list = []
        max_x = None
        for image_obj in image_obj_list:
            image = image_obj.get_image(band=b)

            if image is None:
                continue
            if max_x is None:
                max_x = image.shape[1]
            else:
                max_x = np.max([max_x, image.shape[1]])
            image_list.append(image)
        new_image_list = []
        for image in image_list:
            if image.shape[1] != max_x:
                new_image = np.zeros((image.shape[0],
                                      max_x), dtype=image.dtype)
                new_image[:, 0:image.shape[1]] = image
                new_image_list.append(new_image)
            else:
                new_image_list.append(image)
        image = np.concatenate(new_image_list, axis=0)
        ref_image_obj.set_image(image, band=b)
    return ref_image_obj


def cut_image_with_shape(image_orig, ref_shape, null=np.nan):
    if (not isinstance(ref_shape, list) and
            not isinstance(ref_shape, tuple)):
        ref_shape = [ref_shape]
    if list(image_orig.shape) == list(ref_shape):
        return image_orig
    if len(ref_shape) != len(image_orig.shape):
        image = image_orig.copy()
        while len(ref_shape) > len(image.shape):
            image = np.expand_dims(image, axis=0)
        while len(ref_shape) < len(image.shape):
            image = np.squeeze(image, axis=0)
        if list(image.shape) == list(ref_shape):
            return image
    else:
        image = image_orig
    if image.shape == ref_shape:
        return image
    print('*** ref_shape:', ref_shape)
    out_image = np.zeros((ref_shape), dtype=image.dtype)
    out_image[:] = null
    if (len(image.shape) == 1 and
            ref_shape[0] > image.shape[0]):
        out_image[0:image.shape[0]] = image
    elif len(image.shape) == 1:
        out_image = image[0:int(ref_shape[0])]
    else:
        for i in range(ref_shape[0]):
            if i > image.shape[0] - 1:
                break
            out_image[i, ...] = cut_image_with_shape(
                image[i, ...], ref_shape[1:])
    return out_image


def get_min_max_percentile(image,
                           percentile=None,
                           cmap_min=None,
                           cmap_max=None,
                           band=0):
    if image is None:
        return
    if percentile is not None:
        percentile_effective = percentile
    else:
        percentile_effective = plant.DEFAULT_MIN_MAX_PERCENTILE
    if cmap_min is None:
        cmap_min_band = np.nanpercentile(image, 100 - percentile_effective)
    else:
        cmap_min_band = get_element_from_arg(cmap_min, index=band)
    if cmap_max is None:
        cmap_max_band = np.nanpercentile(image, percentile_effective)
    else:
        cmap_max_band = get_element_from_arg(cmap_max, index=band)

    if cmap_max_band <= cmap_min_band and percentile is None:
        if cmap_min is None:
            cmap_min_band = np.nanmin(image)
        if cmap_max is None:
            cmap_max_band = np.nanmax(image)
    return cmap_min_band, cmap_max_band


def get_element_from_arg(arg, index=0):
    if not hasattr(arg, '__getitem__') or isnumeric(arg):
        return arg
    arg_index = arg[np.nanmin([index, len(arg) - 1])]
    return arg_index


def scale_data(data,
               vmin=None,
               vmax=None,
               new_max=1.0,
               vmean=None,
               clip=True):
    if vmin is None:
        vmin = np.nanmin(data)
    if vmax is None:
        vmax = np.nanmax(data)
    divisor = vmax - vmin
    out_data = np.zeros_like(data)
    if divisor == 0 and 'int' not in plant.get_dtype_name(data):
        out_data *= np.inf
        return data
    if divisor == 0:
        return data

    if vmean is None:
        out_data = new_max * (data - vmin) / divisor
        out_data = np.clip(out_data, 0, new_max)

    else:
        out_data = new_max * (data - vmean) / divisor
        out_data = np.clip(out_data, -new_max, new_max)
    return out_data


def insert_nan(data_orig, indexes=None, out_null=None):
    data_type = plant.get_dtype_name(data_orig)

    if data_type == 'NoneType':
        return data_orig

    if 'int' in data_type and out_null is None:
        return data_orig

    if isinstance(data_orig, np.ndarray):
        data = data_orig.copy()
    else:
        data = data_orig
    if (isinstance(data, np.ma.core.MaskedArray)):
        print(out_null)
        print(data.get_fill_value())
        if out_null == 999999 or data.get_fill_value() == 999999:
            raise
    if (isinstance(data, np.ma.core.MaskedArray) and
            (np.isnan(out_null) or
             out_null == data.get_fill_value())):
        data[indexes] = np.ma.masked
        return data
    if ('int' not in data_type or
            ('int' in data_type and
             not np.isnan(out_null))):

        if indexes is not None:
            try:
                data[indexes] = out_null
            except TypeError:
                if get_indexes_len(data) == 1:
                    data = out_null
        else:
            try:
                data[:] = out_null
            except TypeError:
                if get_indexes_len(data) == 1:
                    data = out_null

    return data


def get_indexes_len(indexes):
    if (indexes is None or isinstance(indexes, numbers.Number) or
            len(indexes) == 0):
        return 0
    if (isinstance(indexes[0], list) or
            isinstance(indexes[0], tuple)):
        return len(indexes[0])
    if (isinstance(indexes[0], np.ndarray)):
        return indexes[0].size
    return len(indexes)


def truncate_mantissa(input_array: np.ndarray, nonzero_mantissa_bits=10):

    if not isinstance(input_array, np.ndarray):
        err_str = 'ERROR input array is not of type np.ndarray'

        raise ValueError(err_str)

    if not issubclass(input_array.dtype.type, np.floating):
        err_str = 'ERROR input array is not complex float or float type'

        raise ValueError(err_str)

    z = input_array.copy()

    if np.iscomplexobj(z):
        truncate_mantissa(z.real, nonzero_mantissa_bits)
        truncate_mantissa(z.imag, nonzero_mantissa_bits)
        return

    mant_bits = np.finfo(z.dtype).nmant
    float_bytes = z.dtype.itemsize

    if nonzero_mantissa_bits == mant_bits:
        return

    if not 0 < nonzero_mantissa_bits <= mant_bits:
        err_str = f'Require 0 < {nonzero_mantissa_bits} <= {mant_bits}'

        raise ValueError(err_str)

    allbits = (1 << (float_bytes * 8)) - 1

    nzero_bits = mant_bits - nonzero_mantissa_bits
    bitmask = (allbits << nzero_bits) & allbits

    utype = np.dtype(f"u{float_bytes}")

    u = z.view(utype)

    u &= bitmask

    return z


def calculate_r2_rmse(x_vect,
                      y_vect,
                      n_vect=None,
                      verbose=True,
                      flag_calculate_r2=True,
                      flag_calculate_rmse=True,
                      n_figs=N_SIGNIFICANT_FIGURES):

    if (x_vect is None or y_vect is None or x_vect.size <= 3 or
            y_vect.size <= 3):
        return
    if n_vect is None:
        from scipy.stats.stats import linregress
        slope, intercept, r_value, p_value, std_err = linregress(x_vect,
                                                                 y_vect)
        if flag_calculate_r2:
            r2 = r_value ** 2
        if flag_calculate_rmse:
            res_sum = np.nansum((x_vect - y_vect)**2)

            rmse = np.sqrt(res_sum / x_vect.shape[0])
    else:

        res_sum = np.nansum(n_vect * ((x_vect - y_vect)**2))
        ind = np.where(isvalid(x_vect))
        tot_sum = np.sum(n_vect[ind] * (x_vect[ind] - np.average(
            x_vect[ind], weights=n_vect[ind]))**2)
        if flag_calculate_r2:
            r2 = 1.0 - res_sum / tot_sum
        if flag_calculate_rmse:
            rmse = np.sqrt(res_sum / np.sum(n_vect[ind]))
    if verbose and flag_calculate_rmse:
        rmse_str = round_to_sig_figs(rmse, n_figs)
        print('    RMSE: ' + str(rmse_str))
    if verbose and flag_calculate_r2:
        r2_str = round_to_sig_figs(r2, n_figs)
        print('    r2: ' + str(r2_str))
    if flag_calculate_r2 and flag_calculate_rmse:
        return r2, rmse
    elif flag_calculate_r2:
        return r2
    elif flag_calculate_rmse:
        return rmse


def golden_ratio():
    ret = (1.0 + np.sqrt(5.0)) / 2.0
    return ret


def expj(phi):
    ret = np.exp(1j * phi)
    return ret


def wrap_phase(phi, deg=False):
    ret = np.angle(expj(phi), deg=deg)
    return ret


def get_phase_jumps(phi):
    phase_jumps = (phi + np.pi) // (2 * np.pi)
    return phase_jumps


def unwrap_phase(image, method=1):
    if method == 1:
        from skimage.restoration import unwrap_phase
        image = unwrap_phase(image)
    elif method == 2:
        from unwrap import unwrap as unwrap_phase
        image = unwrap_phase(image)
    else:

        boxcar = 21
        if 'complex' in plant.get_dtype_name(image).lower():
            residual_phase_image = image.copy()
            phase_image = np.angle(image, deg=False)
        else:
            residual_phase_image = np.exp(1j * image)
            phase_image = image.copy()
        filtered_image = filter_data(residual_phase_image,
                                     boxcar=boxcar)
        surface = np.angle(filtered_image, deg=False)
        surface = filter_data(surface,
                              boxcar=boxcar)
        phase_jumps = get_phase_jumps(surface)

        plant.imshow(phase_image, title='before unwrap')
        unwrapped_phase_image = phase_image + phase_jumps
        plant.imshow(unwrapped_phase_image, title='after unwrap 1')

        unwrapped_phase_image = filter_data(unwrapped_phase_image,
                                            boxcar=boxcar)

        phase_jumps = get_phase_jumps(unwrapped_phase_image)

        image = phase_image + phase_jumps
        plant.imshow(image, title='after unwrap 2')

    return image


def multilook_decorator(filter_method=np.nanmean):
    def decorated(f):
        def wrapper(data,
                    nlooks_y,
                    nlooks_x,
                    image_obj=None,
                    verbose=True):
            length = data.shape[0]
            width = data.shape[1]
            new_width = int(np.ceil(float(width) /
                                    nlooks_x))
            new_length = int(np.ceil(float(length) /
                                     nlooks_y))
            new_data = np.zeros((new_length, new_width))

            for i in range(new_length):

                for j in range(new_width):
                    plant.print_progress(i, new_length,
                                         j, new_width,
                                         verbose=verbose)
                    end_x = np.min([(j + 1) * nlooks_x, width])
                    end_y = np.min([(i + 1) * nlooks_y, length])
                    if filter_method == np.nanstd:
                        ndata = np.sum(isvalid(data[i * nlooks_y:end_y,
                                                    j * nlooks_x:end_x]))
                        if ndata < 3:
                            new_data[i, j] = np.nan
                            continue
                    new_data[i, j] = filter_method(data[i * nlooks_y:
                                                        end_y,
                                                        j * nlooks_x:
                                                        end_x])
            plant.print_progress(new_length, new_length,
                                 new_width, new_width,
                                 verbose=verbose)
            if image_obj is not None:
                image_obj.setImage(new_data)
            if (image_obj is not None and
                    image_obj.geotransform is not None):

                geotransform = image_obj.geotransform
                geotransform[1] = geotransform[1] * nlooks_x
                geotransform[5] = geotransform[5] * nlooks_y

                image_obj.geotransform = geotransform
            return new_data, image_obj
        return wrapper
    return decorated


@multilook_decorator(np.nanmedian)
def median_multilook(*args, **kwargs):
    pass


@multilook_decorator(np.nanmin)
def min_multilook(*args, **kwargs):
    pass


@multilook_decorator(np.nanmax)
def max_multilook(*args, **kwargs):
    pass


@multilook_decorator(np.nanstd)
def stddev_multilook(*args, **kwargs):
    pass


@multilook_decorator()
def multilook(*args, **kwargs):
    pass


def ndata_multilook(*args, **kwargs):
    mask = np.asarray(isvalid(args[0]), dtype=np.float32)
    nlooks_y_ndata = args[1]
    nlooks_x_ndata = args[2]
    data, image_obj = multilook(mask,
                                nlooks_y_ndata,
                                nlooks_x_ndata,
                                **kwargs)
    data *= nlooks_x_ndata * nlooks_y_ndata
    return data, image_obj


def filter_decorator(filter_method=np.nanmean):
    def decorated(f):
        def wrapper(data,
                    filter_size_y,
                    filter_size_x,
                    image_obj=None,
                    verbose=True):
            length = data.shape[0]
            width = data.shape[1]
            filter_range_x = int(filter_size_x / 2)
            filter_range_y = int(filter_size_y / 2)
            new_data = np.zeros((length, width))
            for i in range(length):
                for j in range(width):
                    plant.print_progress(i, length,
                                         j, width,
                                         verbose=verbose)
                    beg_x = np.max([j - filter_range_x, 0])
                    beg_y = np.max([i - filter_range_y, 0])
                    end_x = np.min([j + filter_range_x + 1, width])
                    end_y = np.min([i + filter_range_y + 1, length])
                    if filter_method == np.nanstd:
                        ndata = np.sum(isvalid(data[beg_y:end_y,
                                                    beg_x:end_x]))
                        if ndata < 3:
                            new_data[i, j] = np.nan
                            continue
                    new_data[i, j] = filter_method(data[beg_y:end_y,
                                                        beg_x:end_x])
            plant.print_progress(length, length,
                                 width, width,
                                 verbose=verbose)
            return new_data
        return wrapper
    return decorated


@filter_decorator(np.nanmedian)
def median_filter(*args, **kwargs):
    pass


@filter_decorator(np.nanmin)
def min_filter(*args, **kwargs):
    pass


@filter_decorator(np.nanmax)
def max_filter(*args, **kwargs):
    pass


@filter_decorator(np.nanstd)
def stddev_window(*args, **kwargs):
    pass


def filter_data(input_name,
                overwrite_obj=False,
                min_points=None,

                trapezoid=1,
                mean=1,
                boxcar=1,
                gaussian=1,
                median=1,
                min_filter_size=1,
                max_filter_size=1,
                stddev_window_size=1,
                ndata_window_size=1,

                nlooks_zoom=1,

                nlooks=1,
                nlooks_median=1,
                nlooks_min=1,
                nlooks_max=1,
                nlooks_stddev=1,
                nlooks_ndata=1,

                trapezoid_slope=None,
                output_width=None,
                output_length=None,
                oversampling=None,
                resize_interpolation=None,

                replace_null=False,
                normalize_kernel=True,
                keep_null=False,
                null=None,
                verbose=False,
                nearest_neighbor=False):
    kwargs = locals()
    kwargs.pop('input_name', None)
    kernel = None
    if input_name is None:
        print('ERROR not valid input.')
        return
    if isinstance(input_name, str):
        image_obj = plant.read_image(plant.search_image(input_name)[0])
    elif isinstance(input_name, plant.PlantImage):
        image_obj = input_name
    else:
        image_obj = None
        out_image_obj = None

        if len(input_name.shape) > 1:
            length = input_name.shape[0]
            width = input_name.shape[1]
            input_data = input_name
        else:
            length = 1
            width = input_name.shape[0]
            input_data = input_name.reshape(length, width)
        dtype = input_name.dtype

    if (image_obj is not None and
            image_obj.nbands > 1):
        out_image_obj = image_obj.soft_copy()

        out_image_obj.set_nbands(1)

        for b in range(image_obj.nbands):
            if b == 0:
                out_image_obj = filter_data(out_image_obj, **kwargs)
                continue
            current_band = image_obj.get_image(band=b)
            current_band = filter_data(current_band, **kwargs)
            out_image_obj.set_image(current_band, band=b)
        return out_image_obj

    if image_obj is not None and not overwrite_obj:
        out_image_obj = image_obj.deep_copy()
    elif image_obj is not None:
        out_image_obj = image_obj

    if image_obj is not None:
        input_data = image_obj.image
        width = image_obj.width
        length = image_obj.length

        dtype = image_obj.dtype

    if 'complex' in str(dtype).lower():
        if verbose:
            print('processing real part...')
        if 'complex128' in str(dtype):
            float_dtype = np.float64
            complex_dtype = np.complex128
        else:
            float_dtype = np.float32
            complex_dtype = np.complex64
        args = [np.asarray(np.real(input_data), dtype=float_dtype)]
        output_data = filter_data(*args, **kwargs)
        if verbose:
            print('processing imaginary part...')
        args = [np.asarray(np.imag(input_data), dtype=float_dtype)]
        output_data = output_data + 1j * filter_data(*args, **kwargs)
        output_data = np.asarray(output_data, dtype=complex_dtype)
        if image_obj is not None:
            out_image_obj.set_image(output_data)
            return out_image_obj

        return output_data
    flag_filter_applied = False
    data = input_data.copy()
    if keep_null:
        null_ind = np.where(plant.isnan(data, null=null))
        if get_indexes_len(null_ind) == 0:
            null_ind = None
    else:
        null_ind = None
    if nearest_neighbor:
        x, y = np.where(~(np.isnan(input_data)))
        xym = np.vstack((x, y)).T
        func = interpolate.NearestNDInterpolator(xym, input_data[x, y])
        xnew, ynew = np.meshgrid(np.arange(input_data.shape[1]),
                                 np.arange(input_data.shape[0]))
        data = func(ynew, xnew).reshape(input_data.shape)

    ndata = None

    default_window_size_y = None
    default_window_size_x = None

    trapezoid_y, trapezoid_x = demux_input(trapezoid, 2, dtype=np.float32)
    if trapezoid_x > 1 or trapezoid_y > 1:
        default_window_size_y = trapezoid_y
        default_window_size_x = trapezoid_x
        from astropy.convolution import TrapezoidDisk2DKernel, convolve
        if trapezoid_slope is None:
            trapezoid_slope = 0.2
        trapezoid_mean_filter_size = (trapezoid_y + trapezoid_x) / 2.0
        if trapezoid_y != trapezoid_x:
            print('WARNING the trapezoid filter only accepts a single'
                  ' value for the radius. Substituting'
                  f' filter size  ({trapezoid_y},{trapezoid_x})'
                  f' by {trapezoid_mean_filter_size}')
        kernel = TrapezoidDisk2DKernel(trapezoid_mean_filter_size / 2,
                                       slope=trapezoid_slope)
        if not normalize_kernel:
            kernel = kernel / np.nanmax(kernel)

        if verbose:
            print('trapezoid filter size (diameter): %f (astropy)'
                  % (trapezoid_mean_filter_size))
        data = convolve(data, kernel, boundary='extend',
                        normalize_kernel=normalize_kernel)

        flag_filter_applied = True

    mean_y, mean_x = demux_input(mean, 2, dtype=np.int32)
    if mean_x > 1 or mean_y > 1:
        default_window_size_y = mean_y
        default_window_size_x = mean_x

        kernel = get_ellipse_kernel(mean_y, mean_x,
                                    normalize_kernel=normalize_kernel)

        if null_ind is None and not has_nan(data):
            from scipy.ndimage.filters import convolve
            if verbose:
                print('mean filter size (x, y): %d, %d (scipy)'
                      % (mean_x, mean_y))
            data = convolve(data, kernel)
        else:
            from astropy.convolution import convolve
            if verbose:
                print('mean filter size (x, y): %d, %d (astropy)'
                      % (mean_x, mean_y))
            data = convolve(data, kernel, boundary='extend',
                            normalize_kernel=normalize_kernel)
            invalid_indexes = isnan(data)

        flag_filter_applied = True

    boxcar_y, boxcar_x = demux_input(boxcar, 2, dtype=np.int32)
    if boxcar_x > 1 or boxcar_y > 1:
        default_window_size_y = boxcar_y
        default_window_size_x = boxcar_x
        if null_ind is None and not has_nan(data):
            from scipy.ndimage.filters import uniform_filter as filt
            if verbose:
                print('boxcar filter size (x, y): %d, %d (scipy)'
                      % (boxcar_x, boxcar_y))
            data = filt(data, size=(default_window_size_y,
                                    default_window_size_x))
            if not normalize_kernel:
                data *= boxcar_x * boxcar_y
        else:
            kernel = np.ones((boxcar_y, boxcar_x), dtype=np.float32)

            from astropy.convolution import convolve
            if verbose:
                print('boxcar filter size (x, y): %d, %d (astropy)'
                      % (boxcar_x, boxcar_y))
            data = convolve(data, kernel, boundary='extend',
                            normalize_kernel=normalize_kernel)
            invalid_indexes = isnan(data)
        flag_filter_applied = True

    gaussian_y, gaussian_x = demux_input(gaussian, 2, dtype=np.int32)
    if gaussian_x > 1 or gaussian_y > 1:
        default_window_size_y = gaussian_y
        default_window_size_x = gaussian_x

        try:
            from scipy.ndimage.filters import convolve
        except ImportError:
            print('ERROR importing scipy.ndimage.filters.convolve')
            return
        if verbose:
            print('gaussian filter size (x, y): %d, %d (scipy)'
                  % (gaussian_x, gaussian_y))
        dummy_mask = np.asarray(isnan(data), dtype=np.float32)
        dummy_mask = scipy.ndimage.filters.gaussian_filter(
            dummy_mask, [gaussian_y, gaussian_x])
        dummy_mask = np.where(dummy_mask != 0)
        data[np.where(isnan(data))] = 0
        data = scipy.ndimage.filters.gaussian_filter(
            data, [gaussian_y, gaussian_x])
        data = insert_nan(data, dummy_mask)

        flag_filter_applied = True

    median_y, median_x = demux_input(median, 2, dtype=np.int32)
    if median_x > 1 or median_y > 1:
        default_window_size_y = median_y
        default_window_size_x = median_x
        if null_ind is not None or has_nan(data):
            if verbose:
                print('median filter size (x, y): %d, %d (numpy)'
                      % (median_x, median_y))
            data = median_filter(data,
                                 median_y,
                                 median_x,
                                 verbose=verbose)
        else:
            from scipy.ndimage.filters import median_filter as filt
            if verbose:
                print('median filter size (x, y): %d, %d (scipy)'
                      % (median_x, median_y))
            data = filt(data, size=(median_y, median_x))
        flag_filter_applied = True

    min_filter_size_y, min_filter_size_x = \
        demux_input(min_filter_size, 2, dtype=np.int32)
    if min_filter_size_x > 1 or min_filter_size_y > 1:
        default_window_size_y = min_filter_size_y
        default_window_size_x = min_filter_size_x
        if null_ind is not None or has_nan(data):
            if verbose:
                print('min filter size (x, y): %d, %d (numpy)'
                      % (min_filter_size_x, min_filter_size_y))
            data = min_filter(data,
                              min_filter_size_y,
                              min_filter_size_x,
                              verbose=verbose)
        else:
            if verbose:
                print('min filter size (x, y): %d, %d (scipy)'
                      % (min_filter_size_x, min_filter_size_y))
            from scipy.ndimage.filters import minimum_filter as filt
            data = filt(data, size=(min_filter_size_y,
                                    min_filter_size_x))
        flag_filter_applied = True

    max_filter_size_y, max_filter_size_x = \
        demux_input(max_filter_size, 2, dtype=np.int32)
    if max_filter_size_x > 1 or max_filter_size_y > 1:
        default_window_size_y = max_filter_size_y
        default_window_size_x = max_filter_size_x
        if null_ind is not None or has_nan(data):
            if verbose:
                print('max filter size (x, y): %d, %d (numpy)'
                      % (max_filter_size_x, max_filter_size_y))
            data = max_filter(data,
                              max_filter_size_y,
                              max_filter_size_x,
                              verbose=verbose)
        else:
            if verbose:
                print('max filter size (x, y): %d, %d (scipy)'
                      % (max_filter_size_x, max_filter_size_y))
            from scipy.ndimage.filters import maximum_filter as filt
            data = filt(data, size=(max_filter_size_y,
                                    max_filter_size_x))
        flag_filter_applied = True

    stddev_window_size_y, stddev_window_size_x = \
        demux_input(stddev_window_size, 2, dtype=np.int32)
    if stddev_window_size_x > 1 or stddev_window_size_y > 1:
        if verbose:
            print('stddev window size (x, y): %d, %d (numpy)'
                  % (stddev_window_size_x, stddev_window_size_y))
        default_window_size_y = stddev_window_size_y
        default_window_size_x = stddev_window_size_x
        data = stddev_window(data,
                             stddev_window_size_y,
                             stddev_window_size_x,
                             verbose=verbose)
        flag_filter_applied = True

    ndata_window_size_y, ndata_window_size_x = \
        demux_input(ndata_window_size, 2, dtype=np.int32)
    if (ndata_window_size_x > 1 or
            ndata_window_size_y > 1 or
            (min_points is not None and flag_filter_applied)):
        if (ndata_window_size_x > 1 or
                ndata_window_size_y > 1) and verbose:
            print('ndata (x, y): %d, %d'
                  % (ndata_window_size_x, ndata_window_size_y))
        if (ndata_window_size_x > 1 or
                ndata_window_size_y > 1):
            ndata = get_ndata(data,
                              ndata_window_size_x,
                              ndata_window_size_y)
            data = ndata
        else:
            ndata = get_ndata(data,
                              default_window_size_x,
                              default_window_size_y,
                              kernel=kernel)
        if min_points is not None:
            if (isinstance(min_points, str) and
                    '%' in min_points or float(min_points) < 1):
                n_elements = (default_window_size_x *
                              default_window_size_y)
                min_points_int = int(n_elements *
                                     np.float32(min_points.replace('%', '')))
                if (isinstance(min_points, str) and
                        '%' in min_points):
                    min_points_int /= 100
                min_points_int = np.max([min_points_int, 1])
                if verbose:
                    print('min. number of points: %d/%d'
                          % (min_points_int, n_elements))
            else:
                min_points_int = int(min_points)
            data = insert_nan(data,
                              np.where(ndata < min_points_int))

        flag_filter_applied = True

    default_window_nlooks_size_y = None
    default_window_nlooks_size_x = None

    flag_looks_applied = False
    nlooks_zoom_y, nlooks_zoom_x = \
        demux_input(nlooks_zoom, 2, dtype=np.int32)
    if nlooks_zoom_x > 1 or nlooks_zoom_y > 1:
        if verbose:
            print('nlooks_zoom (x, y): %d, %d'
                  % (nlooks_zoom_x, nlooks_zoom_y))
        default_window_nlooks_size_y = nlooks_zoom_y
        default_window_nlooks_size_x = nlooks_zoom_x
        data, out_image_obj = multilook_zoom(data,
                                             nlooks_zoom_y,
                                             nlooks_zoom_x,
                                             image_obj=out_image_obj,
                                             verbose=verbose)
        flag_looks_applied = True

    nlooks_y, nlooks_x = \
        demux_input(nlooks, 2, dtype=np.int32)
    if nlooks_x > 1 or nlooks_y > 1:
        if verbose:
            print('nlooks (x, y): %d, %d'
                  % (nlooks_x, nlooks_y))
        default_window_nlooks_size_y = nlooks_y
        default_window_nlooks_size_x = nlooks_x
        try:
            data, out_image_obj = multilook_python(data,
                                                   nlooks_y,
                                                   nlooks_x,
                                                   image_obj=out_image_obj,
                                                   verbose=verbose)
        except BaseException:
            data, out_image_obj = multilook(data,
                                            nlooks_y,
                                            nlooks_x,
                                            image_obj=out_image_obj,
                                            verbose=verbose)
        flag_looks_applied = True

    nlooks_y_median, nlooks_x_median = \
        demux_input(nlooks_median, 2, dtype=np.int32)
    if nlooks_x_median > 1 or nlooks_y_median > 1:
        if verbose:
            print('nlooks median (x, y): %d, %d'
                  % (nlooks_x_median, nlooks_y_median))
        default_window_nlooks_size_y = nlooks_y_median
        default_window_nlooks_size_x = nlooks_x_median
        data, out_image_obj = median_multilook(data,
                                               nlooks_y_median,
                                               nlooks_x_median,
                                               image_obj=out_image_obj,
                                               verbose=verbose)
        flag_looks_applied = True

    nlooks_y_min, nlooks_x_min = \
        demux_input(nlooks_min, 2, dtype=np.int32)
    if nlooks_x_min > 1 or nlooks_y_min > 1:
        if verbose:
            print('nlooks min (x, y): %d, %d'
                  % (nlooks_x_min, nlooks_y_min))
        default_window_nlooks_size_y = nlooks_y_min
        default_window_nlooks_size_x = nlooks_x_min
        data, out_image_obj = min_multilook(data,
                                            nlooks_y_min,
                                            nlooks_x_min,
                                            image_obj=out_image_obj,
                                            verbose=verbose)
        flag_looks_applied = True

    nlooks_y_max, nlooks_x_max = \
        demux_input(nlooks_max, 2, dtype=np.int32)
    if nlooks_x_max > 1 or nlooks_y_max > 1:
        if verbose:
            print('nlooks max (x, y): %d, %d'
                  % (nlooks_x_max, nlooks_y_max))
        default_window_nlooks_size_y = nlooks_y_max
        default_window_nlooks_size_x = nlooks_x_max
        data, out_image_obj = max_multilook(data,
                                            nlooks_y_max,
                                            nlooks_x_max,
                                            image_obj=out_image_obj,
                                            verbose=verbose)
        flag_looks_applied = True

    nlooks_y_stddev, nlooks_x_stddev = \
        demux_input(nlooks_stddev, 2, dtype=np.int32)
    if nlooks_x_stddev > 1 or nlooks_y_stddev > 1:
        if verbose:
            print('nlooks stddev (x, y): %d, %d'
                  % (nlooks_x_stddev, nlooks_y_stddev))
        default_window_nlooks_size_y = nlooks_y_stddev
        default_window_nlooks_size_x = nlooks_x_stddev
        data, out_image_obj = stddev_multilook(data,
                                               nlooks_y_stddev,
                                               nlooks_x_stddev,
                                               image_obj=out_image_obj,
                                               verbose=verbose)
        flag_looks_applied = True

    nlooks_y_ndata, nlooks_x_ndata = \
        demux_input(nlooks_ndata, 2, dtype=np.int32)
    if ((nlooks_x_ndata > 1 or
         nlooks_y_ndata > 1) or
            (min_points is not None and flag_looks_applied)):

        if ((nlooks_x_ndata > 1 or
                nlooks_y_ndata > 1) and
                verbose):
            print('nlooks ndata (x, y): %d, %d'
                  % (nlooks_x_ndata, nlooks_y_ndata))

        if nlooks_x_ndata > 1 or nlooks_y_ndata > 1:
            ndata, out_image_obj = ndata_multilook(data,
                                                   nlooks_y_ndata,
                                                   nlooks_x_ndata,
                                                   image_obj=out_image_obj,
                                                   verbose=verbose)
            data = ndata
        else:
            ndata, out_image_obj = ndata_multilook(
                input_data,
                default_window_nlooks_size_y,
                default_window_nlooks_size_x,
                image_obj=out_image_obj,
                verbose=verbose)
        if min_points is not None:
            if (isinstance(min_points, str) and
                    '%' in min_points or float(min_points) < 1):
                n_elements = (default_window_nlooks_size_x *
                              default_window_nlooks_size_y)
                min_points_int = int(n_elements *
                                     np.float32(min_points.replace('%', '')))
                if (isinstance(min_points, str) and
                        '%' in min_points):
                    min_points_int /= 100
                min_points_int = np.max([min_points_int, 1])
                if verbose:
                    print('min. number of points: %d/%d'
                          % (min_points_int, n_elements))
            else:
                min_points_int = int(min_points)
            data = insert_nan(data,
                              np.where(ndata < min_points_int))
        flag_looks_applied = True

    if (output_width is not None or
            output_length is not None or
            oversampling is not None):

        if output_width is None or output_length is None:
            oversampling_y, oversampling_x = \
                demux_input(oversampling, 2, dtype=np.float32)

            if output_length is None:
                output_length = int(np.round(
                    input_data.shape[0] * oversampling_y))
            if output_width is None:
                output_width = int(np.round(
                    input_data.shape[1] * oversampling_x))

        if resize_interpolation is None:
            resize_interpolation = Image.ANTIALIAS

        im = Image.fromarray(data)
        im = im.resize((output_width, output_length), resize_interpolation)
        data = np.array(im)

        if image_obj is not None:
            out_image_obj.set_image(data)
            out_image_obj.geotransform = None

    if null_ind is not None:
        data = insert_nan(data, null_ind, out_null=null)
    if replace_null:
        from astropy.convolution import interpolate_replace_nans
        data = interpolate_replace_nans(data)

    if image_obj is not None:
        out_image_obj.set_image(data)
        return out_image_obj
    return data


def function_decorator(function=None):
    def decorated(f):
        def wrapper(image_obj):
            ret_obj = image_obj.copy()
            for b in range(ret_obj.nbands):
                image = ret_obj.get_image(band=b)
                image = function(image)
                ret_obj.set_image(image, band=b)
            return ret_obj
        return wrapper
    return decorated


@function_decorator(np.real)
def real(*args, **kwargs):
    pass


@function_decorator(np.imag)
def imag(*args, **kwargs):
    pass


def get_ndata(data, filter_size_y, filter_size_x, kernel=None):
    if kernel is None:
        kernel = ((np.ones((filter_size_y,
                            filter_size_x),
                           dtype=np.float32)) /
                  (filter_size_x * filter_size_y))
    flag_error = False
    try:
        from astropy.convolution import astropy_convolve
    except ImportError:
        flag_error = True
    if flag_error:
        try:
            from scipy.ndimage.filters import convolve
        except ImportError:
            print('ERROR importing astropy.convolution and '
                  'scipy.ndimage.filters.convolve')
            return
    else:
        convolve = astropy_convolve
    ndata = convolve(np.asarray(np.isfinite(data), dtype=np.float32) *
                     (filter_size_x * filter_size_y),
                     kernel)
    return ndata


def multilook_python(data,
                     nlooks_y,
                     nlooks_x,
                     image_obj=None,
                     verbose=True):

    length = data.shape[0]
    width = data.shape[1]
    nlooks_x = int(nlooks_x)
    nlooks_y = int(nlooks_y)
    for i in range(nlooks_y):
        for j in range(nlooks_x):
            plant.print_progress(i, nlooks_y,
                                 j, nlooks_x,
                                 verbose=verbose)
            data_slice = data[i::nlooks_y,
                              j::nlooks_x]
            if i == 0 and j == 0:
                current_data = data_slice.copy()
                out_data = np.zeros((current_data.shape))
                ndata = np.zeros((current_data.shape))
            else:
                current_data[0:data_slice.shape[0],
                             0:data_slice.shape[1]] = \
                    data_slice
            ind = isvalid(current_data)
            out_data[ind] += current_data[ind]
            ndata[ind] += 1
    plant.print_progress(nlooks_y, nlooks_y,
                         nlooks_x, nlooks_x,
                         verbose=verbose)
    ind = np.where(ndata > 0)
    out_data[ind] /= ndata[ind]
    out_data = insert_nan(out_data,
                          np.where(ndata == 0),
                          out_null=np.nan)

    if image_obj is not None:
        image_obj.setImage(out_data)
    if (image_obj is not None and
            image_obj.geotransform is not None):

        geotransform = image_obj.geotransform
        geotransform[1] = geotransform[1] * nlooks_x
        geotransform[5] = geotransform[5] * nlooks_y

        image_obj.geotransform = geotransform
    return out_data, image_obj


def multilook_zoom(data,
                   nlooks_y,
                   nlooks_x,
                   image_obj=None,
                   verbose=True):
    from scipy.ndimage import zoom

    nlooks_x = int(nlooks_x)
    nlooks_y = int(nlooks_y)
    ind = np.where(isnan(data))
    indexes_len = get_indexes_len(ind)
    if indexes_len == 0:
        out_data = zoom(data, [1 / nlooks_y, 1 / nlooks_x],
                        order=1)
    else:
        mask = np.asarray(isvalid(data), np.float32)
        data[ind] = 0
        out_data = zoom(data, [1 / nlooks_y, 1 / nlooks_x],
                        order=1)
        out_mask = zoom(mask, [1 / nlooks_y, 1 / nlooks_x],
                        order=1)
        ind_small = np.where(out_mask == 0)
        out_data[ind_small] = np.nan
        ind_small = np.where(out_mask >= 0.5)

    if image_obj is not None:
        image_obj.setImage(out_data)
    if (image_obj is not None and
            image_obj.geotransform is not None):

        geotransform = image_obj.geotransform
        geotransform[1] = geotransform[1] * nlooks_x
        geotransform[5] = geotransform[5] * nlooks_y

        image_obj.geotransform = geotransform
    return out_data, image_obj


def is_deg(image):
    length = image.shape[0] * image.shape[1]
    step = int(max([length / (2**16), 1]))
    for i in range(0, image.shape[0], step):
        if (np.nanmax(image[i, ::step]) > 2 * np.pi or
                np.nanmin(image[i, ::step]) < -2 * np.pi):
            return True
    return False


def get_int_list(data, dtype=int):
    if data is None:
        return
    if not isinstance(data, list) and isinstance(data, np.ndarray):
        data = data.ravel().tolist()
    elif not isinstance(data, list):
        data = [data]
    if dtype is None:
        return data
    data = [type(dtype(0))(d) for d in data]
    return data


def parse_arg_str(data, dtype=None):
    if data is None:
        return
    data_splitted = data.split(plant.ELEMENT_SEPARATOR)
    if dtype is not None:
        data_splitted = [type(dtype(0))(d) for d in data_splitted]
    if len(data_splitted) == 1:
        return data_splitted[0]
    return data_splitted


def list_insert(input_list, index, value):
    if input_list is None:
        input_list = []
    if index > len(input_list):
        for i in range(index + 1 - len(input_list)):
            input_list.append(None)
    if index == len(input_list):
        input_list.append(value)
    else:
        input_list[index] = value
    return input_list


def is_sequence(input_data):
    flag_is_sequence = (isinstance(input_data, Sequence) or
                        isinstance(input_data, np.ndarray))
    return flag_is_sequence


def get_element_from_scalar_or_sequence(scalar_or_sequence, element_index):
    if not is_sequence(scalar_or_sequence):

        return scalar_or_sequence

    if len(scalar_or_sequence) <= element_index:
        return np.nan

    return scalar_or_sequence[element_index]


def demux_input(input_data, n_outputs, dtype=None):
    if isinstance(input_data, np.ndarray):

        flag_convert_to_ndarray = True
        input_data = input_data.tolist()
    else:

        flag_convert_to_ndarray = False

    if (isinstance(input_data, Sequence) and
            len(input_data) == 1):

        output_value = input_data * n_outputs
    elif (isinstance(input_data, Sequence) and
            len(input_data) >= n_outputs):

        output_value = input_data[0: n_outputs]
    elif (isinstance(input_data, Sequence)):

        output_value = (input_data +
                        [None for _ in range(n_outputs - len(input_data))])
    else:

        output_value = [input_data for _ in range(n_outputs)]
    if flag_convert_to_ndarray:

        output_value = np.asarray(output_value)

    if dtype is None:
        return output_value

    if 'int' in plant.get_dtype_name(dtype):
        output_value = [element if isvalid(element) else 0
                        for element in output_value]
    return np.asarray(output_value, dtype=dtype)


def get_stats_str(data):
    data_min = np.nanmin(data)
    data_mean = np.nanmean(data)
    data_max = np.nanmax(data)
    output_str = f'min: {data_min} mean: {data_mean} max: {data_max}'
    return output_str


def print_mean_vector(data):
    n_dims = data.shape[-1]
    if n_dims == 0:
        return
    norm = np.linalg.norm(data, axis=n_dims - 1)
    vector_str = '('
    for i in range(n_dims):
        v = np.nanmean(data[..., i] / norm)
        vector_str += f'{v}, '
    vector_str = vector_str[:-2] + ')'
    print(vector_str)
    return vector_str


def get_normal_vector(image, pixel_size_y=None, pixel_size_x=None):
    gradient_h = np.gradient(image[..., 2])

    if pixel_size_x is None:
        derivative_e = np.gradient(image[..., 0], axis=1)
        alpha = np.full_like(derivative_e, np.nan)
        derivative_e = np.clip(derivative_e, 0.00001, 1000)
        alpha = gradient_h[1] / derivative_e
    else:
        alpha = gradient_h[1] / pixel_size_x

    if pixel_size_y is None:
        derivative_n = np.gradient(-image[..., 1], axis=0)
        beta = np.full_like(derivative_n, np.nan)
        derivative_n = np.clip(derivative_n, 0.00001, 1000)
        beta = gradient_h[0] / derivative_n
    else:
        beta = gradient_h[0] / pixel_size_y

    normal = np.dstack((-alpha, -beta, np.ones_like(alpha)))
    return normal


def get_normal_versor(image, pixel_size_y=None, pixel_size_x=None):
    normal_vector = get_normal_vector(image,
                                      pixel_size_y=pixel_size_x,
                                      pixel_size_x=pixel_size_x)
    normal_versor = normalize_vector(normal_vector)
    return normal_versor


def normalize_vector(image):
    norm = np.linalg.norm(image, axis=2)
    new_image = np.zeros_like(image)
    new_image[:] = np.nan
    ind = np.where(norm != 0)
    for i in range(image.shape[-1]):
        new_image[..., i][ind] = image[..., i][ind] / norm[ind]

    return new_image


def inner(image_1, image_2):
    product = 0
    for i in range(image_1.shape[-1]):
        product += image_1[..., i] * image_2[..., i]
    return product


def congrid(input_name, new_length, new_width):
    from scipy.ndimage import zoom
    if input_name is None:
        print('ERROR not valid input.')
        return
    if isinstance(input_name, str):
        image_obj = plant.read_image(plant.search_image(input_name)[0])
    elif isinstance(input_name, plant.PlantImage):
        image_obj = input_name
    else:
        image_obj = None
        if len(input_name.shape) > 1:
            length = input_name.shape[0]
            width = input_name.shape[1]
            input_data = input_name
        else:
            length = 1
            width = input_name.shape[0]
            input_data = input_name.reshape(length, width)
    if image_obj is not None:
        input_data = image_obj.image
        width = image_obj.width
        length = image_obj.length
    out_data = zoom(input_data,
                    (length / new_length,
                     width / new_width))
    if image_obj is not None:
        image_obj.set_image(out_data)
        return image_obj
    return out_data


def tree_diameter_from_height(tree_height, E=None):

    E = 2.0 if E is None else E
    return np.exp((-0.76 + np.sqrt(0.760**2 -
                                   4 * -0.034 *
                                   (0.893 - E - np.log(tree_height))) /
                   (2 * -0.034)))


def tree_height_from_diameter(tree_diameter, E=None):

    E = 2.0 if E is None else E
    return np.exp(0.893 - E + 0.760 * np.log(tree_diameter) -
                  0.0340 * (np.log(tree_diameter))**2)


def debug_decorator(debug_level=None):
    def decorated(f):
        def wrapper(*args, **kwargs):
            debug_level = None
            sink = plant.DummySink
            print('*** loger: ', plant.plant_config.logger_obj)
            print('*** debug level', debug_level)
            if plant.debug_level == 0 and debug_level is not None:
                print('*** debug level ok')
                logger_obj = plant.plant_config.logger_obj
                if logger_obj is None:
                    sink = plant.PlantLogger
                debug_level = plant.debug_level
                plant.debug_level = 5
            print('sink', sink.__class__)
            print('plant.debug_level: ', plant.debug_level)
            print('plant.plant_config.flag_debug: ',
                  plant.plant_config.flag_debug)

            with sink():
                print('************************************')
                print(f'Calling function: {f.__name__}')
                print('************************************')
                print(f'args: {len(args)}')
                with plant.PlantIndent():
                    for i, arg in enumerate(args):
                        print(f'arg {i}: {args}')
                print(f'kwargs: {len(kwargs)}')
                with plant.PlantIndent():
                    for i, kw in enumerate(kwargs.items()):
                        key, value = kw
                        print(f'kwarg {i}: {key} = {value}')
                print('....................................')
                print('executing function:')
                with plant.PlantIndent():
                    flag_debug = plant.plant_config.flag_debug
                    plant.plant_config.flag_debug = True
                    ret = f(*args, **kwargs)
                    plant.plant_config.flag_debug = flag_debug
                print('....................................')
                if not ret:
                    print('no return')
                else:
                    print(f'return:')
                    with plant.PlantIndent():
                        print(ret)
                print('************************************')
                if debug_level is not None:
                    plant.debug_level = debug_level
            return ret
        return wrapper
    return decorated


def _expand_slice(input_slice, default_start=0,
                  default_stop=None, default_step=1):
    start = input_slice.start
    if start is None:
        start = default_start
    stop = input_slice.stop
    if stop is None:
        stop = default_stop
    step = input_slice.step
    if step is None:
        step = default_step
    if stop is not None:
        slice_cut = update_slice_cut(input_slice, start,
                                     stop_cut=stop)
        args = [start, stop]
        if step is not None:
            args.extend([step])
        ret = range(*args).__getitem__(slice_cut)
        return ret

    return


def expand_slice(*args, **kwargs):
    ret = _expand_slice(*args, **kwargs)
    if isinstance(ret, numbers.Number):
        return [ret]
    return ret


def update_slice_cut(input_key, offset_cut, size_cut=None,
                     stop_cut=None):
    if size_cut is None and stop_cut is None:
        print('ERROR (update_slice_cut) please provide'
              ' size_cut or stop_cut')

    if stop_cut is None:
        stop_cut = offset_cut + size_cut

    if isinstance(input_key, numbers.Number):
        return input_key - offset_cut

    start = input_key.start
    stop = input_key.stop
    step = input_key.step

    if start is not None and start >= 0:
        start = start - offset_cut
    elif start is not None:
        start = stop_cut + start
    elif start is None:
        start = offset_cut

    if stop is not None and stop >= 0:
        stop = stop - offset_cut
    elif stop is not None:
        stop = stop_cut + stop + 1
    elif stop is None:
        stop = stop_cut

    return slice(start, stop, step)


def str_to_slice(input_str):
    if isinstance(input_str, slice):
        return input_str
    if isinstance(input_str, numbers.Number):
        return slice(input_str)
    input_str_splitted = input_str.split(':')
    if len(input_str_splitted) > 3:
        raise AttributeError
    ret = slice(*map(lambda s: np.int32(s.strip().split('.')[0])
                     if s.strip() else None,
                     input_str_splitted))
    return ret


def str_to_number(input_str):
    try:
        return int(input_str)
    except ValueError:
        return float(input_str)


def get_db(image_in, db10=False, db20=False):

    if image_in is None:
        return
    if 'complex' in plant.get_dtype_name(image_in).lower():
        image = np.absolute(image_in)
    else:
        image = image_in
    image = insert_nan(image,
                       np.where(image == 0),
                       out_null=np.nan)
    if db20:
        image = 20 * np.log10(image)
    else:
        image = 10 * np.log10(image)
    if 'complex' in plant.get_dtype_name(image_in).lower():
        image = image * np.exp(1j * np.angle(image_in,
                                             deg=False))
    return image


def shift(input_data, input_shift, **kwargs):
    if 'complex' in plant.get_dtype_name(input_data).lower():
        data_real = shift(np.real(input_data), input_shift, **kwargs)
        data_imag = shift(np.imag(input_data), input_shift, **kwargs)
        ret = data_real + 1j * data_imag
        return ret
    ret = scipy_shift(input_data, input_shift, **kwargs)
    return ret


def get_inv_db(image_in, inv_db10=False, inv_db20=False):

    if image_in is None:
        return
    if 'complex' in plant.get_dtype_name(image_in).lower():
        image = np.absolute(image_in)
    else:
        image = image_in
    try:
        image = insert_nan(image,
                           np.where(image == 0),
                           out_null=np.nan)
    except ValueError:
        pass
    if inv_db20:
        image = 10 ** (image / 20)
    else:
        image = 10 ** (image / 10)
    if 'complex' in plant.get_dtype_name(image_in).lower():
        image = image * np.exp(1j * np.angle(image_in,
                                             deg=False))
    return image


def fill_nan_1d(input_data, **kwargs):
    x_vect = np.arange(input_data.size)
    ind = np.where(plant.isvalid(input_data))
    f1 = interpolate.interp1d(x_vect[ind], input_data[ind],
                              bounds_error=False, **kwargs)
    input_data_interp = f1(x_vect)
    return input_data_interp


def auto_correlation_1d(input_data, **kwargs):
    auto_correlation = cross_correlation_1d(input_data,
                                            input_data,
                                            **kwargs)
    return auto_correlation


def cross_correlation_1d(input_data, ref_data, **kwargs):
    cross_correlation = np.convolve(input_data, np.flip(ref_data),
                                    **kwargs)
    return cross_correlation


def normalized_cross_correlation_1d(input_data, ref_data, **kwargs):
    cross_correlation = cross_correlation_1d(input_data, ref_data,
                                             **kwargs)

    return cross_correlation


def coregister_1d(input_data_orig, ref_data_orig,
                  verbose=False, flag_ncc=True):

    if flag_ncc:
        input_data = input_data_orig.copy()
        ref_data = ref_data_orig.copy()
    else:
        input_mean = np.nanmean(input_data_orig)
        ref_mean = np.nanmean(ref_data_orig)
        input_data = input_data_orig.copy() - input_mean
        ref_data = ref_data_orig.copy() - ref_mean

    ind = np.where(plant.isnan(input_data))
    input_data[ind] = 0
    ind = np.where(plant.isnan(ref_data))
    ref_data[ind] = 0
    kwargs = {}
    kwargs['mode'] = 'same'
    correlation = normalized_cross_correlation_1d(input_data,
                                                  ref_data, **kwargs)
    index = np.argmax(correlation) - input_data.size // 2
    if verbose:
        print(f'coregistering vector: {index}/{input_data.size}')
    output_data = np.roll(input_data_orig.ravel(), -index)

    return output_data


def get_image_integration_x(image, pixel_size_x=None, offset=None):
    if pixel_size_x is None:
        pixel_size_x = 1.0
    integration_x = np.nancumsum(image, axis=1) * pixel_size_x
    if offset is not None and not hasattr(offset, '__getitem__'):
        integration_x += offset
    elif offset is not None:
        for i, off in enumerate(offset):
            integration_x[i, :] += off
    return integration_x


def get_image_integration_y(image, pixel_size_y=None, offset=None):
    if pixel_size_y is None:
        pixel_size_y = 1.0
    integration_y = np.nancumsum(image, axis=0) * pixel_size_y
    if offset is not None and not hasattr(offset, '__getitem__'):
        integration_y += offset
    elif offset is not None:
        for i, off in enumerate(offset):
            integration_y[:, i] += off
    return integration_y


def get_image_derivative_x(image, pixel_size_x=None):
    if pixel_size_x is None:
        pixel_size_x = 1.0

    derivative_x = np.gradient(image,
                               pixel_size_x,
                               axis=1)
    return derivative_x


def get_image_derivative_y(image, pixel_size_y=None):
    if pixel_size_y is None:
        pixel_size_y = 1.0

    derivative_y = np.gradient(image,
                               pixel_size_y,
                               axis=0)
    return derivative_y


def get_image_derivative_phase_x(image, pixel_size_x=None):
    if pixel_size_x is None:
        pixel_size_x = 1.0
    derivative_x = np.zeros((image.shape))
    image = expj(image)

    for i in range(0, image.shape[0]):
        derivative_x[i, 1:-1] = (np.angle(image[i, 2:] *
                                          np.conj(image[i, :-2])) /
                                 (2.0 * float(pixel_size_x)))

    return derivative_x


def get_image_derivative_phase_y(image, pixel_size_y=None):
    if pixel_size_y is None:
        pixel_size_y = 1.0
    derivative_y = np.zeros((image.shape))
    image = expj(image)
    for i in range(1, image.shape[0] - 1):
        derivative_y[i, :] = (np.angle(image[i + 1, :] *
                                       np.conj(image[i - 1, :])) /
                              (2.0 * float(pixel_size_y)))

    return derivative_y


def get_dirac(y, x):
    img = np.zeros((y, x), np.float32)
    img[y // 2, x // 2] = 1
    return img


def get_ellipse_kernel(dy, dx, normalize_kernel=True, dtype=np.float32):
    from skimage.draw import ellipse
    ry, rx = dy // 2 + 1, dx // 2 + 1
    rr, cc = ellipse(ry - 1, rx - 1, ry, rx)
    img = np.zeros((max(rr) + 1, max(cc) + 1), dtype=dtype)
    img[rr, cc] = 1
    if (dx >= 3 and dx <= 5) or (dy >= 3 and dy <= 5):
        img[0, 0] = 0
        img[0, img.shape[1] - 1] = 0
        img[img.shape[0] - 1, 0] = 0
        img[img.shape[0] - 1, img.shape[1] - 1] = 0
    if normalize_kernel:
        img /= np.sum(img)
    return img


__logBase10of2 = 3.01029995663981195213738894724493026768189881462108e-1


def format_number(number,
                  decimal_places=None,
                  sigfigs=None,
                  flag_auto=False):
    if isinstance(number, str):
        return number
    kwargs = locals()
    kwargs.pop('number')
    if (isinstance(number, Sequence) or
            isinstance(number, np.ndarray)):
        number_list = [format_number(n, **kwargs)
                       for n in number]
        return number_list
    if (decimal_places is None and sigfigs is None and flag_auto and
            np.issubdtype(number, np.integer)):
        decimal_places = 0
    elif (decimal_places is None and sigfigs is None and flag_auto and
            abs(number) > 10000):
        decimal_places = 1
    elif (decimal_places is None and sigfigs is None and flag_auto and
            abs(number) > 1000):
        decimal_places = 2
    elif (decimal_places is None and sigfigs is None and flag_auto and
            abs(number) > 100):
        decimal_places = 3
    elif (decimal_places is None and sigfigs is None and flag_auto and
            abs(number) > 10):
        decimal_places = 4
    elif (decimal_places is None and sigfigs is None and flag_auto and
            abs(number) > 1):
        decimal_places = 5
    elif decimal_places is None and sigfigs is None and flag_auto:
        sigfigs = 5
    if decimal_places is None and sigfigs is None:
        return str(number)
    if decimal_places is not None and decimal_places >= 0:
        decimal_format = '%.' + str(decimal_places) + 'f'
        number = decimal_format % number
        return number
    elif decimal_places is not None:
        number = '%.0f' % (number // 10**-decimal_places * 10**-decimal_places)
        return number
    number = round_to_sig_figs(number, sigfigs)
    return number


def round_to_sig_figs(x, sigfigs=N_SIGNIFICANT_FIGURES):

    if sigfigs is None:
        return x
    if isinstance(x, list):
        ret = [get_sig_fig(single_x, sigfigs) for single_x in x]
        return ret
    x_format = '%.' + str(int(sigfigs)) + 'g'
    return float(x_format % x)


def get_sig_fig(x, sigfigs):
    rounded_number = round_to_sig_figs(x, sigfigs)
    return 10.0**(get_number_characteristic(rounded_number) - sigfigs)


def get_number_characteristic(x):
    return np.int32(np.floor(np.log10(np.absolute(x))))


def get_n_elements_from_shape(args):
    if args is None:
        return
    n_elements = 1
    for arg in args:
        n_elements *= arg
    return n_elements


def idlgen_decorator(dtype=np.int32):
    def decorated(f):
        def wrapper(*args):
            n_elements = 1
            for arg in args:
                n_elements *= arg
                ret = np.arange(n_elements,
                                dtype=dtype)
            ret = ret.reshape(args[::-1])
            return ret
        return wrapper
    return decorated


@idlgen_decorator(np.int32)
def indgen(*args):
    pass


@idlgen_decorator(np.float32)
def findgen(*args):
    pass


@idlgen_decorator(np.double)
def dindgen(*args):
    pass


def idlarr_decorator(dtype=np.int32):
    def decorated(f):
        def wrapper(*args):
            dim_tuple = tuple(args[::-1])
            ret = np.zeros(dim_tuple)
            return ret
        return wrapper
    return decorated


@idlarr_decorator(np.int32)
def intarr(*args):
    pass


@idlarr_decorator(np.float32)
def fltarr(*args):
    pass


@idlarr_decorator(np.double)
def dblarr(*args):
    pass


def n_elements(value):
    ret = value.shape[::-1]
    return ret


def reform(value, width, length):
    value.reshape(length, width)
    return (value)
