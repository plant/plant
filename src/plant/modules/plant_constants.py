#!/usr/bin/env python3
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Author: Gustavo H. X. Shiroma, Marco Lavalle
# Copyright 2010-2011, by the California Institute of Technology. ALL RIGHTS
# RESERVED. United States Government Sponsorship acknowledged.
# Any commercial use must be negotiated with the Office of Technology Transfer
# at the California Institute of Technology.
#
# This software may be subject to U.S. export control laws. By accepting this
# software, the user agrees to comply with all applicable U.S.
# export laws and regulations. User has the responsibility to obtain export
# licenses, or other export authority as may be required before exporting such
# information to foreign countries or providing access to foreign persons.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import os
import numpy as np
from plant.dynamic_version import version_tuple


def get_version():
    return '.'.join([str(v) for v in version_tuple[0:4]])


__version__ = version = VERSION = get_version()

MATPLOTLIB_BACKEND = None

PLANT_CONFIG_FILE = 'plant_config.txt'
DEFAULT_OUTPUT_FORMAT = 'ISCE'
FLAG_OUTPUT_DIR_OVERWRITE = True

LINE_SEPARATOR = '#'
IMAGE_NAME_SEPARATOR = ':'
ELEMENT_SEPARATOR = ','
LINE_MULTIPLIER = '**'
ELEMENT_MULTIPLIER = '*'
SEPARATOR_LIST = ['_', '-', ',', ':']
PARSER_GROUP_SEPARATOR = "\n\nOPTION "
USAGE_EXAMPLES_STRING = 'usage examples: \n'
DESCRIPTION_STRING = 'description: \n'

Y_BAND_NAME = 'Geolocation: Y'
X_BAND_NAME = 'Geolocation: X'

DEFAULT_MIN_MAX_PERCENTILE = 95

INT_NAN = -32768

DOUBLE_NAN = 1.7976931348623157e+308
MAX_VALUE = 1.0e30

EARTH_RADIUS = 6378137

DOWNLOAD_DIR = os.path.join('dat', 'plant')
DOWNLOAD_DIR_ICESAT_GLAS = 'ICESat_GLAS'
ICESAT_GLAS_DB_FILE = os.path.join('db', 'glas.db')

MAX_WINDOW_WIDTH = 1024
MAX_WINDOW_HEIGHT = 768
DEFAULT_PLOT_SIZE_X = 640
DEFAULT_PLOT_SIZE_Y = 545

IMSHOW_BACKGROUND_COLOR = 'BLUE'
PLOT_BACKGROUND_COLOR = 'WHITE'
IMSHOW_BACKGROUND_COLOR_DARK = 'BLACK'
PLOT_BACKGROUND_COLOR_DARK = 'BLACK'

N_SIGNIFICANT_FIGURES = 5

KML_TILE_SIZE = 8192

KMZ_TILE_SIZE = 2048

DEFAULT_CMAP = 'gray'

HEADER_EXTENSIONS = ['.xml', '.hdr', '.ann']
EXT_READ_FROM_ANN_FILE = ['mlc', 'slc', 'grd', 'dat', 'hgt', 'slope',
                          'inc', 'sch', 'kz', 'llh', 'dop', 'lkv',
                          'baseline', 'ann', 'amp1', 'amp2', 'cor', 'int',
                          'unw', 'rtc', 'rtcgrd']
EXT_READ_FROM_ANN_FILE_GEO = ['grd', 'hgt', 'inc', 'rtc', 'rtcgrd']

FIG_DRIVERS = ['EPS', 'JPEG', 'JPG', 'PDF',
               'PGF', 'PNG', 'PS', 'RAW', 'RGBA',
               'SVG', 'SVGZ']

MDX_NOT_SUPPORTED_FORMATS = ['TIF', 'TIFF', 'GTIFF', 'VRT'] + FIG_DRIVERS

DATA_DRIVERS = ['MEM', 'ISCE', 'SRF', 'BIN', 'NUMPY', 'SENTINEL', 'HDF5',
                'NETCDF', 'NISAR', 'ISCE', 'ANN']
TEXT_DRIVERS = ['TXT', 'TEXT', 'CSV']
AVAILABLE_DRIVERS = DATA_DRIVERS + TEXT_DRIVERS

SRF_MAGIC_NUMBER = 1504078485

INVALID_DASH_LIST = ['—', '–']

SRF_DTYPE = [(1, np.byte), (20, np.float32), (30, np.complex64),
             (21, np.complex64)]

DEFAULT_COLOR_LIST = ['rosybrown',
                      'teal',
                      'steelblue',
                      'darkred',
                      'darkgreen',
                      'darkblue',
                      'red',
                      'green',
                      'blue',
                      'maroon',
                      'grey',
                      'black',
                      'darkgreen',
                      'darkcyan',
                      'sienna']

DEFAULT_COLOR_LIST_1 = ['steelblue',
                        'rosybrown',
                        'teal',
                        'blue',
                        'red',
                        'green',
                        'darkblue',
                        'darkred',
                        'darkgreen',
                        'grey',
                        'black',
                        'maroon',
                        'sienna',
                        'darkcyan',
                        'darkgreen']

LINE_AND_POINTS_COLOR_LIST = [['blue', '#396AB1'],
                              ['orange', '#DA7C30'],
                              ['green', '#3E9651'],
                              ['red', '#CC2529'],
                              ['gray', '#535154'],
                              ['purple', '#6B4C9A'],
                              ['darkred', '#922428'],
                              ['beige', '#948B3D']]

BAR_COLOR_LIST = [['blue', (114, 147, 203)],
                  ['orange', (225, 151, 76)],
                  ['green', (132, 186, 91)],
                  ['red', (211, 94, 96)],
                  ['gray', (128, 133, 133)],
                  ['purple', (144, 103, 167)],
                  ['darkred', (171, 104, 87)],
                  ['beige', (204, 194, 16)]]

POL_COLOR_LIST = [3, 2, 0, 1]

NISAR_BAND_LIST = ['LSAR', 'SSAR']

NISAR_PRODUCT_LIST = [os.path.join('SLC', 'swaths'),
                      os.path.join('RSLC', 'swaths'),
                      os.path.join('GSLC', 'grids'),
                      os.path.join('GCOV', 'grids')]

LINESTYLE_LIST = ['solid',

                  'dotted',

                  'dashed',

                  'dashdot']

DEFAULT_GDAL_FORMAT = 'GTiff'
DEFAULT_GDAL_EXTENSION = '.tif'

OUTPUT_FORMAT_MAP = {}
OUTPUT_FORMAT_MAP['BIN'] = 'ENVI'
PROJECTION_REF_WKT = ('GEOGCS["GCS_WGS_1984",DATUM["WGS_1984",'
                      'SPHEROID["WGS_84",6378137,298.257223563]],'
                      'PRIMEM["Greenwich",0],'
                      'UNIT["Degree",0.017453292519943295]]')

PROJECTION_REF = ('+proj=longlat +ellps=WGS84 '
                  '+datum=WGS84 +no_defs')

PROJECTION_ANTIMERIDIAN_REF_WKT = (
    'GEOGCS["WGS 84",DATUM["WGS_1984",'
    'SPHEROID["WGS 84",6378137,298.257223563,'
    'AUTHORITY["EPSG","7030"]],'
    'AUTHORITY["EPSG","6326"]],'
    'PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],'
    'NIT["degree",0.0174532925199433,AUTHORITY["EPSG","9122"]],'
    'AXIS["Latitude",NORTH],'
    'AXIS["Longitude",EAST],'
    'AUTHORITY["EPSG","4326"]]')

PROJECTION_ANTIMERIDIAN_REF = \
    '+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs +lon_wrap=180'

MARKER_LIST = ['o', '^', 's', 'd', 'v', '<', '>', '.', ',', 'x', '+']

HATCH_LIST = ['', '////', '//', '/////', '///', '/']

CMAP_LIGHT_LIST = ['Reds', 'Greens', 'Blues', 'Greys',
                   'Oranges', 'Purples']

CMAP_DARK_LIST = ['bone', 'gist_heat', 'gray', 'copper',
                  'pink', 'hot']

RADAR_DECOMP_DICT = []
RADAR_DECOMP_DICT.append(['LEX_SQUARE', ['HHHH', 'VVVV', 'HVHV', 'VHVH']])
RADAR_DECOMP_DICT.append(['LEX', ['HH', 'VV', 'HV', 'VH']])
RADAR_DECOMP_DICT.append(['PAULI', ['HHmVV', 'HHpVV', 'Hv', 'Vh']])
RADAR_DECOMP_DICT.append(['PAULI_2', ['MM', 'PP', 'HV', 'VH']])
RADAR_DECOMP_DICT.append(['S2', ['s11', 's22', 's12', 's21']])
RADAR_DECOMP_DICT.append(['C4', ['C11', 'C44', 'C22', 'C33']])

RADAR_DECOMP_DICT.append(['T4', ['T22', 'T11', 'T33', 'T44']])
RADAR_DECOMP_DICT.append(['C3', ['C11', 'C33', 'C22', 'C22']])

RADAR_DECOMP_DICT.append(['T3', ['T22', 'T11', 'T33', 'T33']])
RADAR_DECOMP_DICT.append(['ELEM', ['Dbl', 'Odd', 'Vol', 'Vol']])
RADAR_DECOMP_DICT.append(['RVOG', ['_md', '_ms', '_mv', '_mv']])
RADAR_DECOMP_DICT.append(['KROGAGER', ['_Ks', '_Kd', '_Kh', '_Kh']])
RADAR_DECOMP_DICT.append(['OPT', ['Opt1', 'Opt2', 'Opt3', 'Opt3']])
RADAR_DECOMP_DICT.append(['COLOR', ['Red', 'Blue', 'Green', 'Green']])

WIKI_API = "https://en.wikipedia.org/w/api.php"


class bcolors:

    BOLD = '\033[1m'

    ColorOff = "\033[0m"

    Black = "\033[0;30m"
    Red = "\033[0;31m"
    Green = "\033[0;32m"
    Yellow = "\033[0;33m"
    Blue = "\033[0;34m"
    Purple = "\033[0;35m"
    Cyan = "\033[0;36m"
    White = "\033[0;37m"

    BBlack = "\033[1;30m"
    BRed = "\033[1;31m"
    BGreen = "\033[1;32m"
    BYellow = "\033[1;33m"
    BBlue = "\033[1;34m"
    BPurple = "\033[1;35m"
    BCyan = "\033[1;36m"
    BWhite = "\033[1;37m"

    UBlack = "\033[4;30m"
    URed = "\033[4;31m"
    UGreen = "\033[4;32m"
    UYellow = "\033[4;33m"
    UBlue = "\033[4;34m"
    UPurple = "\033[4;35m"
    UCyan = "\033[4;36m"
    UWhite = "\033[4;37m"

    On_Black = "\033[40m"
    On_Red = "\033[41m"
    On_Green = "\033[42m"
    On_Yellow = "\033[43m"
    On_Blue = "\033[44m"
    On_Purple = "\033[45m"
    On_Cyan = "\033[46m"
    On_White = "\033[47m"

    IBlack = "\033[0;90m"
    IRed = "\033[0;91m"
    IGreen = "\033[0;92m"
    IYellow = "\033[0;93m"
    IBlue = "\033[0;94m"
    IPurple = "\033[0;95m"
    ICyan = "\033[0;96m"
    IWhite = "\033[0;97m"

    BIBlack = "\033[1;90m"
    BIRed = "\033[1;91m"
    BIGreen = "\033[1;92m"
    BIYellow = "\033[1;93m"
    BIBlue = "\033[1;94m"
    BIPurple = "\033[1;95m"
    BICyan = "\033[1;96m"
    BIWhite = "\033[1;97m"

    On_IBlack = "\033[0;100m"
    On_IRed = "\033[0;101m"
    On_IGreen = "\033[0;102m"
    On_IYellow = "\033[0;103m"
    On_IBlue = "\033[0;104m"
    On_IPurple = "\033[10;95m"
    On_ICyan = "\033[0;106m"
    On_IWhite = "\033[0;107m"

    BLACK = "\033[00;30m"
    DARY_GRAY = "\033[01;30m"
    RED = "\033[00;31m"
    BRIGHT_RED = "\033[01;31m"
    GREEN = "\033[00;32m"
    BRIGHT_GREEN = "\033[01;32m"
    BROWN = "\033[00;33m"
    YELLOW = "\033[01;33m"
    BLUE = "\033[00;34m"
    BRIGHT_BLUE = "\033[01;34m"
    PURPLE = "\033[00;35m"
