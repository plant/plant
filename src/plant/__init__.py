import sys

class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args,
                                                                 **kwargs)
        return cls._instances[cls]

alias_dict = {'d': 'display',
              'u': 'plant_util.py',
              'i': 'plant_info.py',
              'f': 'plant_filter.py',
              'l': 'plant_ls.py',
              'm': 'plant_mosaic.py'}

class NameWrapper(object):
    def __init__(self, wrapped):
        self.wrapped = wrapped

    def __getattr__(self, name):
        try:
            return getattr(self.wrapped, name)
        except AttributeError:
            pass
        if name == '__version__':
            return plant.VERSION
        if name in alias_dict.keys():
            name = alias_dict[name]
        return plant.ModuleWrapper(name)

    def __dir__(self):
        return plant.__dir__()

from .modules import *

sys.modules[__name__] = NameWrapper(sys.modules[__name__])

plant_config.reset()

if plant_config.logger_obj is None:
    plant_config.logger_obj = PlantLogger()

